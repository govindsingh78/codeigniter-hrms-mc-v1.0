<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mastercontrol extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Schoolusers_model', 'schuser_model');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    //Add Project Master..
    public function add_project() {
        checkRolePermission('add_project');
        $this->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[3]|max_length[55]|is_unique[project_master.project_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Master Project Management";
            $this->load->view('project/add_project', $data);
        } else {
            $insertedArr = array('project_name' => $this->input->post('project_name'), 'create_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('project_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Project Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Ajax Display Project..
    public function project_data_ajax() {
        checkRolePermission('project_data_ajax');
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("project_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->project_name;
            $row[] = "<a title='Edit' href='" . base_url("editproject/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit Project ...
    public function editproject($edt_id) {
        checkRolePermission('editproject');
        $this->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[3]|max_length[55]|is_unique[project_master.project_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit Project Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("project_master", $edt_id);
            $data['SingleRowprojsum'] = $this->db->get_where("project_summary", array('project_id' => $edt_id))->row();
            $data['state_name'] = $this->mastermodel->GetAllTableRecord('tbl_states');
            //.. tbl_cities
            if (@$data['SingleRowprojsum']->state_id) {
                $this->db->select('c.fld_id,c.city_name');
                $this->db->from('tbl_cities as c');
                $this->db->where(["c.state_id" => $data['SingleRowprojsum']->state_id, "c.status" => '1']);
                $citiesArr = $this->db->get()->result();
                $data['CityListArr'] = $citiesArr;
            }
            $data['component_name'] = $this->mastermodel->GetAllTableRecord('component_master');
            $data['contractor_name'] = $this->mastermodel->GetAllTableRecord('contractor_master');
            $this->load->view('project/edit_project_view', $data);
        } else {
            $UpdArr = array('project_name' => $this->input->post('project_name'));
            $this->db->where(["fld_id" => $edt_id]);
            $respon = $this->db->update('project_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Project Updated successfully');
                redirect(base_url("add_project"));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url("add_project"));
            endif;
        }
    }

    //Add Project Summary(By Durgesh 11-12-2019)...
    public function add_project_summary($id) {
        if ($id) {
            $numRecs = $this->db->where(["project_id" => $id])->from("project_summary")->count_all_results();
            if ($numRecs > 0):
                $this->session->set_flashdata('error_msg', 'project Summary Record Already Exists');
                redirect(base_url("editproject/" . $id));
            endif;

            $InserArr = array('project_id' => $id,
                'state_id' => $this->input->post('statename_id'),
                'district_id' => implode(',', $this->input->post('district_id')),
                'component_id' => $this->input->post('component_id'),
                'contractor_id' => $this->input->post('contractor_id'),
                'no_of_package' => $this->input->post('no_of_package'),
                'no_of_works_roads' => $this->input->post('no_of_works_roads'),
                'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('project_summary', $InserArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Project Summary Updated successfully');
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
            endif;
        }
        redirect(base_url("editproject/" . $id));
    }

    //District Master..
    public function district_management() {
        checkRolePermission('district_management');
        $this->form_validation->set_rules('district_name', 'District Name', 'required|min_length[3]|max_length[55]|is_unique[project_master.project_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Master District Management";
            $this->load->view('district/add_new', $data);
        } else {
            $insertedArr = array('district_name' => $this->input->post('district_name'), 'create_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('district_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'District Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Ajax Display District..
    public function district_data_ajax() {
        checkRolePermission('district_data_ajax');
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("district_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->district_name;
            $row[] = "<a title='Edit' href='" . base_url("editdistrict/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit District..
    public function editdistrict($edt_id) {
        $record_num = $this->uri->segment(10);
        checkRolePermission('editdistrict');
        $this->form_validation->set_rules('district_name', 'District Name', 'required|min_length[3]|max_length[55]|is_unique[project_master.project_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit District Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("district_master", $edt_id);
            $this->load->view('district/edit_rec', $data);
        } else {
            $UpdArr = array('district_name' => $this->input->post('district_name'));
            $this->db->where(["fld_id" => $edt_id]);
            $respon = $this->db->update('district_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'District Updated successfully');
                redirect(base_url("district_management"));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url("district_management"));
            endif;
        }
    }

    //Component Management..
    public function component_management() {
        checkRolePermission('component_management');
        $this->form_validation->set_rules('component_name', 'Component Name', 'required|min_length[3]|max_length[55]|is_unique[component_master.component_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Master Component Management";
            $this->load->view('component/add_new', $data);
        } else {
            $insertedArr = array('component_name' => $this->input->post('component_name'), 'create_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('component_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Component Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Component Data Ajax..
    public function component_data_ajax() {
        checkRolePermission('component_data_ajax');
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("component_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->component_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_component/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    public function edit_component($edt_id) {
        checkRolePermission('edit_component');
        $this->form_validation->set_rules('component_name', 'Component Name', 'required|min_length[3]|max_length[55]|is_unique[component_master.component_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit Component Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("component_master", $edt_id);
            $this->load->view('component/edit_rec', $data);
        } else {
            $UpdArr = array('component_name' => $this->input->post('component_name'));
            $this->db->where(["fld_id" => $edt_id]);
            $respon = $this->db->update('component_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Component Updated successfully');
                redirect(base_url("component_management"));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url("component_management"));
            endif;
        }
    }

    //##############################################################################################
    #################################### OLD #######################################################
    #################################################################################################
    //Call Back Function for Section..
    public function check_section() {
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->input->post('class_id');
        $section_name = $this->input->post('section_name');
        $this->db->select('fld_id');
        $this->db->from('section_master');
        $this->db->where(["school_id" => $school_id, "class_id" => $class_id, "section_name" => $section_name, "status" => "1"]);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //User Student Add / Member Added..
    public function add_newuser() {
        $this->form_validation->set_rules('username', 'Student ID', 'required|trim|min_length[5]|max_length[12]|alpha_numeric');
        $this->form_validation->set_rules('session_id', 'Admission Session', 'required|trim');
        $this->form_validation->set_rules('class_id', 'Class of admission', 'required|trim');
        $this->form_validation->set_rules('date_ofjoin', 'Date Admitted', 'required|trim');
        $this->form_validation->set_rules('user_fullname', 'Student Full Name', 'required|trim|min_length[4]|max_length[100]|alpha_numeric_spaces');
        $this->form_validation->set_rules('gender_id', 'Gender', 'required|trim');
        $this->form_validation->set_rules('date_ofbirth', 'Date of Birth', 'required|trim');
        $this->form_validation->set_rules('contact_no', 'Contact no', 'required|trim|numeric|min_length[6]|max_length[10]');
        $this->form_validation->set_rules('email_id', 'Email ID', 'required|trim|valid_email');
        $this->form_validation->set_rules('registration_no', 'Scholar Reg. No', 'required|trim|min_length[6]|max_length[40]');
        $this->form_validation->set_rules('full_address', 'Full Address', 'required|trim|min_length[20]|max_length[250]');

        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Add/Registration Student";
            $data['OptClassListArr'] = $this->mastermodel->GetClassOptionArr();
            $data['OptSessionListArr'] = $this->mastermodel->GetSessionOptionArr();
            $data['OptGenderListArr'] = $this->mastermodel->GetGenderOptionArr();
            $data['UPC_USERID'] = $this->mastermodel->GetStdLatestUSERID();
            $this->load->view('add_member/add_member_view', $data);
        } else {
            $insertedArr = array(
                'school_id' => $this->session->userdata('school_id'),
                'assign_role' => '5',
                'username' => $this->input->post('username'),
                'session_id' => $this->input->post('session_id'),
                'class_id' => $this->input->post('class_id'),
                'date_ofjoin' => date("Y-m-d", strtotime($this->input->post('date_ofjoin'))),
                'user_fullname' => $this->input->post('user_fullname'),
                'gender_id' => $this->input->post('gender_id'),
                'date_ofbirth' => date("Y-m-d", strtotime($this->input->post('date_ofjoin'))),
                'contact_no' => $this->input->post('contact_no'),
                'email_id' => $this->input->post('email_id'),
                'registration_no' => $this->input->post('registration_no'),
                'full_address' => $this->input->post('full_address'),
                'password' => md5("123456"),
                'createdby' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('users', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Student Registration Complete Successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //User Edit Views Pannel..
    public function editupd_view_user() {
        $data['error'] = '';
        $data['title'] = "View Edit/Update Users Table";
        $this->load->view('add_member/viewusers_table_view', $data);
    }

    //Ajax User View Details Table..
    public function allusers_data_ajax() {
        $list = $this->schuser_model->get_datatables();
        $gendrArr = ["1" => "Male", "2" => "Female", "3" => "Other"];
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($recRow->username) ? $recRow->username : "";
            $row[] = (@$recRow->registration_no) ? @$recRow->registration_no : "";
            $row[] = ($recRow->user_fullname) ? $recRow->user_fullname : "";
            $row[] = ($recRow->date_ofjoin) ? date("d-m-Y", strtotime($recRow->date_ofjoin)) : "";
            $row[] = ($recRow->gender_id) ? $gendrArr[$recRow->gender_id] : "";
            $row[] = ($recRow->date_ofbirth) ? date("d-m-Y", strtotime($recRow->date_ofbirth)) : "";
            $row[] = ($recRow->contact_no) ? $recRow->contact_no : "";
            $row[] = ($recRow->email_id) ? $recRow->email_id : "";
            $row[] = "<a title='Edit/View' href='" . base_url("single_userdetails_edit/" . $recRow->id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->schuser_model->count_all(),
            "recordsFiltered" => $this->schuser_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //Edit / View Single User Dashboard..
    public function single_userdetails_edit($user_id) {
        $data['SingleUserRec'] = $this->mastermodel->GetUserAllInfoByUserID($user_id);
        $data['title'] = "Student Basic View/Edit";
        if ($data['SingleUserRec']->assign_role == 5) {
            $data['OptClassListArr'] = $this->mastermodel->GetClassOptionArr();
            $data['OptSessionListArr'] = $this->mastermodel->GetSessionOptionArr();
            $data['OptGenderListArr'] = $this->mastermodel->GetGenderOptionArr();
            // $data['SingleUserRec'] = $userInfoArr;
            $this->load->view('add_member/userdashboard_view', $data);
        }
    }

    //Access Denied..
    public function access_denied() {
        $data['title'] = "Access Denied";
        $this->load->view('access_denied', $data);
    }

    //code by durgesh for get city
    public function get_city($state_id) {
        if ($state_id) {
            $this->db->select('city_name,fld_id');
            $this->db->from('tbl_cities');
            $this->db->where(array('state_id' => $state_id, 'status' => '1'));
            $resultArr = $this->db->get()->result();
            echo json_encode($resultArr);
        }
        exit();
    }

    //add the district though ajax
    public function add_district() {
        $stateid = $_REQUEST['stateid'];
        $cityid = $_REQUEST['cityid'];
        $numRow = $this->db->get_where('tbl_cities', array('state_id' => $stateid, 'city_name' => $cityid))->num_rows();
        if ($numRow < 1) {
            $insertArr = array(
                'state_id' => $stateid,
                'city_name' => $cityid,
                'status' => '1',
                'created_by' => $this->session->userdata('loginid'),
                'modified_by' => $this->session->userdata('loginid'),
                'modified_date' => date('y-m-d h:i:s')
            );
            $respon = $this->db->insert('tbl_cities', $insertArr);
            $msg = "City Added Successfully";
        } else {
            $msg = "City Already exist";
        }
        echo $msg;
    }

    //add the state though ajax
    public function add_state() {
        $stateid = $_REQUEST['stateid'];
        $numRow = $this->db->get_where('tbl_states', array('state_name' => $stateid))->num_rows();
        if ($numRow < 1) {
            $insertArr = array(
                'state_name' => $stateid,
                'status' => '1',
                'created_by' => $this->session->userdata('loginid'),
                'modified_by' => $this->session->userdata('loginid'),
                'modified_date' => date('y-m-d h:i:s')
            );
            $respon = $this->db->insert('tbl_states', $insertArr);
            $msg = "State Added Successfully";
        } else {
            $msg = "State Already exist";
        }
        echo $msg;
    }

    //add the component though ajax
    public function add_component() {
        $componentname = $_REQUEST['componentid'];
        $numRow = $this->db->get_where('component_master', array('component_name' => $componentname))->num_rows();
        if ($numRow < 1) {
            $insertArr = array(
                'component_name' => $componentname,
                'create_by' => $this->session->userdata('loginid'),
            );
            $respon = $this->db->insert('component_master', $insertArr);
            $msg = "Component Added Successfully";
        } else {
            $msg = "Component Already exist";
        }
        echo $msg;
    }

    //add the contractor though ajax
    public function add_contractor() {
        $contractorname = $_REQUEST['contractorid'];
        $numRow = $this->db->get_where('contractor_master', array('contractor_name' => $contractorname))->num_rows();
        if ($numRow < 1) {
            $insertArr = array(
                'contractor_name' => $contractorname,
                'create_by' => $this->session->userdata('loginid'),
            );
            $respon = $this->db->insert('contractor_master', $insertArr);
            $msg = "Contractor Added Successfully";
        } else {
            $msg = "Contractor Already exist";
        }
        echo $msg;
    }

    //code by durgesh for package management.
    public function package_view($fld_id) {
        $data['title'] = 'Package Management';
        $data['package_data'] = $this->db->get_where('project_summary', array('fld_id' => $fld_id))->row_array();
        $data['basicDetailsdata'] = $this->mastermodel->GetProjectDetailsByProjSummaryID($fld_id);
        $this->load->view('package/add_package_view', $data);
    }

    //add the component though ajax
    public function savepackage_add() {
        $proj_sumr_id = $this->input->post('proj_summid');
        if ($proj_sumr_id) {
            $insertArr = array(
                'proj_summ_id' => $this->input->post('proj_summid'),
                'pkg_number' => $this->input->post('no_of_package'),
                'name_of_district' => $this->input->post('district_id'),
                'package_no' => $this->input->post('package_no'),
                'no_of_road' => $this->input->post('no_of_road'),
                'awarded_contract_value' => $this->input->post('awarded_contract_value'),
                'agreement_date' => date('Y-m-d', strtotime($this->input->post('agreement_date'))),
                'stipulated_date_of_start' => date('Y-m-d', strtotime($this->input->post('stipulated_date_of_start'))),
                'stipulated_date_of_completion' => date('Y-m-d', strtotime($this->input->post('stipulated_date_of_completion'))),
                'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('package_summary', $insertArr);
            $msg = "Package Added Successfully";
            $this->session->set_flashdata('success_msg', 'Package Added Successfully');
            redirect(base_url("package_view/" . $proj_sumr_id));
        } else {
            $msg = "project name not found";
            $this->session->set_flashdata('error_msg', 'Package Not Added');
            redirect(base_url("package_view/" . $proj_sumr_id));
        }
    }

    //Manage Work in Package..
    public function managework_in_package($pkg_sum_id) {
        $data['title'] = 'Manage work details in package';
        $data['pkg_sum_no_of_road'] = $this->db->get_where('package_summary', array('fld_id' => $pkg_sum_id))->row_array();
        $data['basicDetailsdata'] = $this->mastermodel->GetProjDetailsByPckgSummrID($pkg_sum_id);
        $this->load->view('work/managework_in_package_view', $data);
    }

    public function save_package_work_detail($pkg_sum_id) {
        $_POST['pkg_summary_id'] = $pkg_sum_id;
        $_REQUEST['created_by'] = $this->session->userdata('loginid');
        if ($this->input->post('work_id')):
            $response = $this->db->insert('package_work_detail', $this->input->post());
        endif;
        if ($response):
            $this->session->set_flashdata('success_msg', 'package work detail added successfully');
            redirect(base_url('managework_in_package/' . $pkg_sum_id));
        else:
            $this->session->set_flashdata('error_msg', 'package work detail not added successfully');
            redirect(base_url('managework_in_package/' . $pkg_sum_id));
        endif;
    }

    //District Master..
    public function package_management() {
        checkRolePermission('district_management');
        $data['title'] = "Package Management";
        $this->load->view('package/package_mgmt_view', $data);
    }

    //Clearance Work..
    public function clearance_work() {
        $data['title'] = 'Package Clearance Detail';
        $data['ProjectList'] = $this->mastermodel->GetAllTableRecord("project_master");
        if (@$_REQUEST['filter_project']) {
            @$packageID = @$_REQUEST['filter_package'];
            $data['workDetailArr'] = $this->mastermodel->GetWorkDetailsByPkgID($packageID);
        }
        $this->load->view('clearance/clearance_work_by_package', $data);
    }

    //Set Pckg..
    public function ajax_setpackage_byprojid() {
        $projectID = $_REQUEST['project'];
        if ($projectID) {
            $this->db->select('b.fld_id,b.package_no');
            $this->db->from('project_summary as a');
            $this->db->join('package_summary as b', 'a.fld_id=b.proj_summ_id', 'LEFT');
            $this->db->where(array('a.project_id' => $projectID, 'a.status' => '1'));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

    //Set Habitation..
    public function ajax_sethabitation_bypckgid() {
        $packageID = $_REQUEST['package'];
        if ($packageID) {
            $this->db->select('a.fld_id,a.work_id,a.name_of_work');
            $this->db->from('package_work_detail as a');
            // $this->db->join('package_summary as b', 'a.fld_id=b.proj_summ_id', 'LEFT');
            $this->db->where(array('a.pkg_summary_id' => $packageID, 'a.status' => '1'));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

    //No of Work ID..
    public function ajax_setno_of_habitation_byworkid() {
        $workID = $_REQUEST['workedid'];
        if ($workID) {
            $this->db->select('a.fld_id');
            $this->db->from('habitation_master as a');
            $this->db->where(array('a.work_summ_id' => $workID, 'a.status' => '1'));
            $resultArr = $this->db->get()->num_rows();
        }
        echo $resultArr;
    }

    //Clearnc..
    public function clearance_save_submit() {
        $insertArr = $this->input->post();
        $work_id = $insertArr['work_id'];
        if ($insertArr and $work_id):
            $response = $this->db->insert('clearance', $insertArr);
            $projrecArr = $this->mastermodel->GetProjBasicDetailByWorkID($work_id);
        endif;
        if ($response):
            $this->session->set_flashdata('success_msg', 'Clearance Detail added successfully');
            redirect(base_url("clearance_work?f_submit=1&filter_project=" . $projrecArr->project_id . "&filter_package=" . $projrecArr->pkg_summary_id));
        else:
            $this->session->set_flashdata('error_msg', 'Error Clearance Detail not added ');
            redirect(base_url('clearance_work'));
        endif;
    }

//Habitation Details //
    public function habitation_on_workid() {
        $data['error'] = '';
        $data['ProjectList'] = $this->mastermodel->GetAllTableRecord("project_master");
        $data['title'] = "Habitation Management";
        $this->load->view('habitation/habitation_home_view', $data);
    }

    //Save No of Habitation..
    public function set_noofhabitation_onworkid() {
        $recordArr = $this->input->post();
        if ($recordArr['filter_project'] and $recordArr['filter_package'] and $recordArr['filter_workid'] and $recordArr['noof_habitation']) {
            $numRecs = $this->db->where(["project_id" => $recordArr['filter_project'], "package_summ_id" => $recordArr['filter_package'], "work_summ_id" => $recordArr['filter_workid']])->from("habitation_master")->count_all_results();
            if ($numRecs < 1) {
                for ($i = 1; $i <= $recordArr['noof_habitation']; $i++) {
                    $GenID = "HAB" . $recordArr['filter_workid'] . "/00" . $i;
                    $inserArr = array("project_id" => $recordArr['filter_project'],
                        "package_summ_id" => $recordArr['filter_package'],
                        "work_summ_id" => $recordArr['filter_workid'],
                        "habitation_GenID" => $GenID,
                        "entry_by" => $this->session->userdata('loginid'));
                    $respon = $this->db->insert('habitation_master', $inserArr);
                    $this->session->set_flashdata('success_msg', 'Habitation Detail added successfully');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Habitation Detail Already Exists');
            }
        }
        redirect(base_url('habitation_on_workid'));
    }

    //Entry of ..Habitation..
    public function entryinhabitation($workID) {
        $data['error'] = '';
        $data['title'] = "Habitation Entry Management";
        $this->db->select('a.*');
        $this->db->from('habitation_master as a');
        $this->db->where(array('a.work_summ_id' => $workID, 'a.status' => '1'));
        $data['resultArr'] = $this->db->get()->result();
        $data['basicDetailsProj'] = $this->mastermodel->GetProjBasicDetailByWorkID($workID);
        $this->load->view('habitation/entry_habitation_view', $data);
    }

    //Save Or Update Habitation Details...
    public function saveupd_habitation_details() {
        if ($_REQUEST['work_id_chk']) {
            $chkArr = $_REQUEST['work_id_chk'];
            foreach ($chkArr as $rows) {
                $numRecs = $this->db->where(["status" => '1', "fld_id" => $rows])->from("habitation_master")->count_all_results();
                if ($numRecs > 0) {
                    //Update Rec..
                    $UpdtArr = array("habitation_name" => $this->input->post('habitation_name_' . $rows),
                        "population" => $this->input->post('population_' . $rows),
                        "connected_or_not" => $this->input->post('connected_or_not_' . $rows));
                    $UpdArr = array('state_name' => $this->input->post('state_name'));
                    $this->db->where(["status" => '1', "fld_id" => $rows]);
                    $respon = $this->db->update('habitation_master', $UpdtArr);
                    $this->session->set_flashdata('success_msg', 'Habitation Detail Updated Successfully');
                }
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Checkbox Checked Error');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    //#######################################################################################
    //@@@@@@@@@@@@@@ (Master Data Entry) Code By Asheesh 19-12-2019  @@@@@@@@@@@@@@@@@@@@@@@@
    //???????????????????????????????????????????????????????????????????????????????????????
    //State Management..
    public function state_management() {
        // checkRolePermission('component_management');
        $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[3]|max_length[55]|is_unique[tbl_states.state_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "State Management";
            $this->load->view('master/add_state', $data);
        } else {
            $insertedArr = array('state_name' => $this->input->post('state_name'), 'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('tbl_states', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'State Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //State Data Ajax..
    public function state_data_ajax() {
        checkRolePermission('component_data_ajax');
        error_reporting(0);
        $list = $this->mastermodel->GetAllStateRecord();
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->state_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_state/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit State
    public function edit_state($stateID) {
        $this->form_validation->set_rules('state_name', 'State Name', 'required|min_length[3]|max_length[55]|is_unique[tbl_states.state_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit State Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("tbl_states", $stateID);
            $this->load->view('master/edit_state', $data);
        } else {
            $UpdArr = array('state_name' => $this->input->post('state_name'));
            $this->db->where(["fld_id" => $stateID]);
            $respon = $this->db->update('tbl_states', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'State Updated successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function division_management() {
        $this->form_validation->set_rules('division_name', 'Division Name', 'required|min_length[3]|max_length[55]|is_unique[division_master.division_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Division Management";
            $this->load->view('master/add_division', $data);
        } else {
            $insertedArr = array('division_name' => $this->input->post('division_name'), 'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('division_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Division Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Ajax Division...
    public function division_data_ajax() {
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("division_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->division_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_division/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    public function edit_division($dvsnID) {
        $this->form_validation->set_rules('division_name', 'Division Name', 'required|min_length[3]|max_length[55]|is_unique[division_master.division_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit Division Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("division_master", $dvsnID);
            $this->load->view('master/edit_division', $data);
        } else {
            $UpdArr = array('division_name' => $this->input->post('division_name'));
            $this->db->where(["fld_id" => $dvsnID]);
            $respon = $this->db->update('division_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Division Updated successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function mpconstituency_management() {
        $this->form_validation->set_rules('mpconstituency_name', 'MP Constituency Name', 'required|min_length[3]|max_length[55]|is_unique[mpconstituency_master.mpconstituency_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "MP Constituency Management";
            $this->load->view('master/add_mpconstituency', $data);
        } else {
            $insertedArr = array('mpconstituency_name' => $this->input->post('mpconstituency_name'), 'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('mpconstituency_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'MP Constituency Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function mpconstituency_data_ajax() {
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("mpconstituency_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->mpconstituency_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_mpconstituency/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit MP Constituency..
    public function edit_mpconstituency($editID) {
        $this->form_validation->set_rules('mpconstituency_name', 'MP Constituency Name', 'required|min_length[3]|max_length[55]|is_unique[mpconstituency_master.mpconstituency_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit MP Constituency Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("mpconstituency_master", $editID);
            $this->load->view('master/edit_mpconstituency', $data);
        } else {
            $UpdArr = array('mpconstituency_name' => $this->input->post('mpconstituency_name'));
            $this->db->where(["fld_id" => $editID]);
            $respon = $this->db->update('mpconstituency_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'MP Constituency Updated successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function mlaconstituency_management() {
        $this->form_validation->set_rules('mlaconstituency_name', 'MLA Constituency Name', 'required|min_length[3]|max_length[55]|is_unique[mlaconstituency_master.mlaconstituency_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "MLA Constituency_management Management";
            $this->load->view('master/add_mlaconstituency', $data);
        } else {
            $insertedArr = array('mlaconstituency_name' => $this->input->post('mlaconstituency_name'), 'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('mlaconstituency_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'MLA Constituency Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function mlaconstituency_data_ajax() {
        error_reporting(0);
        $list = $this->mastermodel->GetAllTableRecord("mlaconstituency_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->mlaconstituency_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_mlaconstituency/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit MLA..
    public function edit_mlaconstituency($editID) {
        $this->form_validation->set_rules('mlaconstituency_name', 'MLA Constituency Name', 'required|min_length[3]|max_length[55]|is_unique[mlaconstituency_master.mlaconstituency_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit MLA Constituency Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("mlaconstituency_master", $editID);
            $this->load->view('master/edit_mlaconstituency', $data);
        } else {
            $UpdArr = array('mlaconstituency_name' => $this->input->post('mlaconstituency_name'));
            $this->db->where(["fld_id" => $editID]);
            $respon = $this->db->update('mlaconstituency_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'MLA Constituency Updated successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Mandal Master
    public function mandal_management() {
        $this->form_validation->set_rules('mandal_name', 'Mandal Name', 'required|min_length[3]|max_length[55]|is_unique[mandal_master.mandal_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Mandal Management Management";
            $this->load->view('master/add_mandal', $data);
        } else {
            $insertedArr = array('mandal_name' => $this->input->post('mandal_name'), 'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('mandal_master', $insertedArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Mandal Added successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    public function mandal_data_ajax() {
        $list = $this->mastermodel->GetAllTableRecord("mandal_master");
        $CurrStatus_Color = $data = array();
        $no = $_POST['start'];
        foreach ($list as $recRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $recRow->mandal_name;
            $row[] = "<a title='Edit' href='" . base_url("edit_mandal/" . $recRow->fld_id) . "'><span class='glyphicon glyphicon-edit'></span></a>";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data);
        echo json_encode($output);
    }

    //Edit Mandal..
    public function edit_mandal($editID) {
        $this->form_validation->set_rules('mandal_name', 'Mandal Name', 'required|min_length[3]|max_length[55]|is_unique[mandal_master.mandal_name]');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = "Edit Mandal Management";
            $data['SingleRow'] = $this->mastermodel->GetSingleRecByID("mandal_master", $editID);
            $this->load->view('master/edit_mandal', $data);
        } else {
            $UpdArr = array('mandal_name' => $this->input->post('mandal_name'));
            $this->db->where(["fld_id" => $editID]);
            $respon = $this->db->update('mandal_master', $UpdArr);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Mandal Updated successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //#################### Physical Progress Section Detail Start From Here ##########################
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    public function phys_progress_details() {
        $data['title'] = 'Physical Progress Detail';
        $data['ProjectList'] = $this->mastermodel->GetAllTableRecord("project_master");
        if (@$_REQUEST['filter_project']) {
            @$packageID = @$_REQUEST['filter_package'];
            $data['workDetailArr'] = $this->mastermodel->GetWorkDetailsByPkgID($packageID);
        }
        $this->load->view('physicalprogress/physicalprogress_entry_workid', $data);
    }

    //Physical Prog..
    public function phys_progress_save_submit() {
        $recordArr = $this->input->post();
        $insertArr = array("grounded_yn" => $recordArr['grounded_yn'],
            "ew_km" => $recordArr['ew_km'],
            "gsb_km" => $recordArr['gsb_km'],
            "wmm_km" => $recordArr['wmm_km'],
            "bt_km" => $recordArr['bt_km'],
            "cc_km" => $recordArr['cc_km'],
            "slab_culvert" => $recordArr['slab_culvert'],
            "boxpipe_culvert" => $recordArr['boxpipe_culvert'],
            "foundation_in_prog_compt" => $recordArr['foundation_in_prog_compt'],
            "substructure_prog_compt" => $recordArr['substructure_prog_compt'],
            "superstructure_prog_compt" => $recordArr['superstructure_prog_compt'],
            "wingwalls_prog_compt" => $recordArr['wingwalls_prog_compt'],
            "approach_no_bridge" => $recordArr['approach_no_bridge'],
            "compl_all_respect" => $recordArr['compl_all_respect'],
            "qc1_complated" => $recordArr['qc1_complated'],
            "qc2_complated" => $recordArr['qc2_complated'],
            "qc3_complated" => $recordArr['qc3_complated'],
            "work_complated" => $recordArr['work_complated'],
            "actual_date_of_complatation" => $recordArr['actual_date_of_complatation'],
            "reason_for_delay" => $recordArr['reason_for_delay'],
            "work_id" => $recordArr['work_id'],
            "entry_by" => $this->session->userdata('loginid'));

        if ($insertArr and $recordArr['work_id']):
            $response = $this->db->insert('physical_progress_details', $insertArr);
            $work_id = $recordArr['work_id'];
            $projrecArr = $this->mastermodel->GetProjBasicDetailByWorkID($work_id);

        endif;
        if ($response):
            $this->session->set_flashdata('success_msg', 'Physical Progress Detail added successfully');
            redirect(base_url("phys_progress_details?f_submit=1&filter_project=" . $projrecArr->project_id . "&filter_package=" . $projrecArr->pkg_summary_id));
        else:
            $this->session->set_flashdata('error_msg', 'Error Physical Progress Details not added ');
            redirect(base_url('phys_progress_details'));
        endif;
    }

    //MileStone Section..
    public function milestone_fin_progr() {
        $data['error'] = '';
        $data['ProjectList'] = $this->mastermodel->GetAllTableRecord("project_master");
        $data['title'] = "MileStone Management";
        $this->load->view('milestone/milestone_home_view', $data);
    }

    public function ajax_setno_of_milestone_bypckgid() {
        $pckgID = $_REQUEST['pckgid'];
        if ($pckgID) {
            $this->db->select('a.fld_id');
            $this->db->from('milestone_master as a');
            $this->db->where(array('a.package_summ_id' => $pckgID, 'a.status' => '1'));
            $resultArr = $this->db->get()->num_rows();
        }
        echo $resultArr;
    }

    public function set_noofmilestone_onpackageid() {
        $recordArr = $this->input->post();
        if ($recordArr['filter_project'] and $recordArr['filter_package'] and $recordArr['noof_milestone']) {
            $numRecs = $this->db->where(["project_id" => $recordArr['filter_project'], "package_summ_id" => $recordArr['filter_package']])->from("milestone_master")->count_all_results();
            if ($numRecs < 1) {
                for ($i = 1; $i <= $recordArr['noof_milestone']; $i++) {
                    $GenID = "MLS" . $recordArr['filter_package'] . "/00" . $i;
                    $inserArr = array("project_id" => $recordArr['filter_project'],
                        "package_summ_id" => $recordArr['filter_package'],
                        "milestone_GenID" => $GenID,
                        "entry_by" => $this->session->userdata('loginid'));
                    $respon = $this->db->insert('milestone_master', $inserArr);
                    $this->session->set_flashdata('success_msg', 'MileStone Detail added successfully');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'MileStone Detail Already Exists');
            }
        }
        redirect(base_url('milestone_fin_progr'));
    }

    //Entry MileStone Details..
    public function entryinmilestone($pckgID) {
        $data['error'] = '';
        $data['title'] = "MileStone Entry Management";
        $this->db->select('a.*');
        $this->db->from('milestone_master as a');
        $this->db->where(array('a.package_summ_id' => $pckgID, 'a.status' => '1'));
        $data['resultArr'] = $this->db->get()->result();
        $data['basicDetailsProj'] = $this->mastermodel->GetProjDetailsByPckgSummrID($pckgID);
        $this->load->view('milestone/entry_milestone_view', $data);
    }

    //Save or Update MileStone Data..
    public function save_upd_milestonedata() {
        if ($_REQUEST['chk_milestone_id']) {
            $chkArr = $_REQUEST['chk_milestone_id'];
            foreach ($chkArr as $rows) {
                $numRecs = $this->db->where(["status" => '1', "fld_id" => $rows])->from("milestone_master")->count_all_results();
                if ($numRecs > 0) {
                    //Update Rec..
                    $UpdtArr = array("scheduled_date" => $this->input->post('scheduled_date_' . $rows), "actual_date_achievm" => $this->input->post('actual_date_achievm_' . $rows));
                    $UpdArr = array('state_name' => $this->input->post('state_name'));
                    $this->db->where(["status" => '1', "fld_id" => $rows]);
                    $respon = $this->db->update('milestone_master', $UpdtArr);
                    $this->session->set_flashdata('success_msg', 'Milestone Detail Updated Successfully');
                }
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Checkbox Checked Error');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    //Set Contractor..
    public function ajax_setallcontractor() {
        @$projectID = $_REQUEST['project'];
        if ($projectID) {
            $this->db->select('a.*');
            $this->db->from('contractor_master as a');
            $this->db->where(array('a.status' => '1'));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

    //All Componnent..
    public function ajax_setallcomponent() {
        @$projectID = $_REQUEST['project'];
        if ($projectID) {
            $this->db->select('a.*');
            $this->db->from('component_master as a');
            $this->db->where(array('a.status' => '1'));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

    //All State Ajax Rec..
    public function ajax_setallstaterecall() {
        @$projectID = $_REQUEST['project'];
        if ($projectID) {
            $this->db->select('a.*');
            $this->db->from('tbl_states as a');
            $this->db->where(array('a.status' => '1'));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

    //All City Details..
    public function ajax_setallcitynameall() {
        @$stateID = $_REQUEST['gotstateid'];
        if ($stateID) {
            $this->db->select('a.*');
            $this->db->from('tbl_cities as a');
            $this->db->where(array('a.status' => '1', 'a.state_id' => $stateID));
            $resultArr = $this->db->get()->result();
        }
        echo json_encode($resultArr);
    }

}
