<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shortleave_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    //Get IO Details By TL..
    public function myshort_leave_list() {
        $data['error'] = '';
        $data['title'] = "Monthly short Leave Summary";
        $UserId = $this->session->userdata('loginid');
        $tOtLateMinute = 0;

        if ((@$_REQUEST['filtermonth']) and ( @$_REQUEST['filteryear']) and ( @$_REQUEST['submit'])) {
            $monthName = MonthNameArr($_REQUEST['filtermonth']);
            $yearName = $_REQUEST['filteryear'];
            $first_dateM = date('Y-m-d', strtotime("first day of $monthName $yearName"));
            $last_dateM = date('Y-m-d', strtotime("last day of $monthName $yearName"));
            $BasicDetailRec = $this->mastermodel->GetBasicRecLoginUser();
            $aLLHolidaysArr = $this->mastermodel->firs_third_Sat_Holidays_ListCurrentMonth();
            $RecLeaveArr = $this->mastermodel->GetAllLeaveListArr($first_dateM, $last_dateM, $UserId);
            $RecTourArr = $this->mastermodel->GetAllTourListArr($first_dateM, $last_dateM, $UserId);
            $array_1 = array_merge($aLLHolidaysArr, $RecLeaveArr, $RecTourArr);
            $AllHolidLeaveTourDateArr = array_unique($array_1);
            $data['allPunchDataArr'] = $this->mastermodel->GetAllPunchDataByDateRange($BasicDetailRec->machine_id, $AllHolidLeaveTourDateArr, $first_dateM, $last_dateM);
            $data['CountShortLeaveInMonth'] = $this->mastermodel->CountTotShortDayLeave($_REQUEST['filteryear'], $_REQUEST['filtermonth'], $UserId);
        }

        $data['filterrecdata'] = $_REQUEST;
        $this->load->view("shortleave/myshort_leave_list_view", $data);
    }

    //Ajax All Details..
    public function ajax_alldetails_bydevicelogid() {
        $deviceLogID = $_REQUEST['devicelog_id'];
        if ($deviceLogID) {
            $this->db->select("a.LogDate,b.reviewing_officer_ro,c.user_id,c.reporting_manager,c.reporting_manager_name");
            $this->db->from("devicelogs_processed as a");
            $this->db->join("emp_otherofficial_data as b", "a.UserId=b.machine_id", "LEFT");
            $this->db->join("main_employees_summary as c", "b.user_id=c.user_id", "LEFT");
            $this->db->where(["a.DeviceLogId" => $deviceLogID, "b.status" => "1"]);
            $recordDetails = $this->db->get()->row();
            if ($recordDetails):
                $recordDetails->intime = date("h:i:s A", strtotime($recordDetails->LogDate));
                $recordDetails->leaveDate = date("Y-m-d", strtotime($recordDetails->LogDate));
            endif;
            echo json_encode($recordDetails);
        }
    }

    
    
    //Short Leave Form Submit Data..
    public function shortleave_formsubmit() { 
        $loginID = $this->session->userdata('loginid');
        if ($_REQUEST['leave_type'] == "Half Leave") {
            $leave_count = "half_day";
        }
        if ($_REQUEST['leave_type'] == "Short Leave") {
            $leave_count = "short_leave";
        }
        $insertArr = array("devicelog_id" => $_REQUEST['devicelog_id'], "user_id" => $loginID, "io_userid" => $_REQUEST['reporting_mngr_id'], "month" => $_REQUEST['short_lv_month'], "year" => $_REQUEST['short_lv_year'], "leave_date" => $_REQUEST['leave_date'], "leave_type" => 'Late', "leave_count" => $leave_count, "full_in_time" => $_REQUEST['full_intime']);
        $num_results = $this->db->where(["status" => '1', "user_id" => $loginID, "leave_date" => $_REQUEST['leave_date']])->count_all_results("short_leave");
        if ($num_results < 1) {
            $resps = $this->db->insert("short_leave", $insertArr);
            if ($resps) {
                $this->session->set_flashdata('successmsg', 'Short Leave Apply successfully.');
            }
        } else {
            $this->session->set_flashdata('errormsg', 'Short Leave already exists on this date');
        }
        redirect(base_url("myshort_leave_list?filtermonth=" . $_REQUEST['short_lv_month'] . "&filteryear=" . $_REQUEST['short_lv_year'] . "&submit=1"));
    }

    
    
    
}
