<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Travelticket_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Travelticket_model', 'travelticketmodel');
        $this->load->library('tank_auth');
        $this->load->library('form_validation');
		if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function add() {
        $data['error'] = '';
        $UserempID = $this->tank_auth->get_user_id();
        $this->db->select('a.*,b.userfullname,b.department_name,b.emailaddress,b.businessunit_name,b.contactnumber');
        $this->db->from('travelticket_tbl as a');
        $this->db->join('main_employees_summary as b', 'a.travel_empid=b.user_id', "LEFT");
        $this->db->where(array('a.status' => '1'));
        $this->db->order_by('a.fld_id', 'DESC');
        $this->db->limit("15");
        $Recresult = $this->db->get()->result();
        $data['latestrec'] = $Recresult;
        $data['title'] = "Ticket Add Management";
        $user_id = $this->session->userdata('loginid');
        if(!empty($user_id)){
        $this->load->view("travelticket/addmanage_view", $data);
        }
        else{
            redirect('admin', true);
        }
    }

    public function getemppdetailbyid_ajax($empid) {
        $this->db->select('a.department_name,a.emailaddress,a.businessunit_name,a.contactnumber,b.profileimg,a.user_id,b.profileimg');
        $this->db->from('main_employees_summary as a');
        $this->db->join('main_users as b', 'a.user_id=b.id', "LEFT");
        $this->db->where(array('a.user_id' => $empid, 'a.isactive' => '1'));
        $result = $this->db->get()->result();
        echo json_encode($result);
        exit();
    }

    public function employeedetailsById($loginID) {
        $this->db->select('a.user_id,a.businessunit_name');
        $this->db->from('main_employees_summary as a');
        $this->db->where(array('user_id' => $loginID, 'isactive' => '1'));
        $result = $this->db->get()->row();
        return $result;
    }

    public function travelticket_save() {
        $tProjIdsArr = $this->input->post('tickproject');
        $strprojIds = implode(",", $tProjIdsArr);
        $hrms_or_otherempl = '1';
        $otherempl_details = '';
        $otherempl_detailsJson = '';
        if ($this->input->post('empid') == 'other') {
            $hrms_or_otherempl = '2';
            $otherempl_details = array('empname' => $this->input->post('otherempl'),
                'aboutdetails' => $this->input->post('otherempldetails'),
                'otherempbunit' => $this->input->post('otherempbunit'));
            $otherempl_detailsJson = json_encode($otherempl_details);
        }

        if ($this->input->post('triptype') == '1') {
            $insertArr = array(
                'travel_empid' => $this->input->post('empid'),
                'trav_from' => $this->input->post('jfrom'),
                'trav_to' => $this->input->post('jto'),
                'dateoftravel' => $this->input->post('dateoftravel'),
                'dateofbooking' => $this->input->post('dateofbooking'),
                'tickproject' => $strprojIds,
                'trip_type' => $this->input->post('triptype'),
                'tickcost' => $this->input->post('tickcost'),
                'other' => '1',
                'hrms_or_otherempl' => $hrms_or_otherempl,
                'otherempl_details' => $otherempl_detailsJson,
                'other_proj_bunit' => $this->input->post('otheremplbunit'),
                'approvedby' => $this->input->post('approvedby'),
                'airlines' => $this->input->post('airlines'),
                'tickcomment' => $this->input->post('tickcomment'),
                'entry_by' => $this->tank_auth->get_user_id());
            $insLastID = $this->mastermodel->InsertMasterData($insertArr, 'travelticket_tbl');
            if ($insLastID):
                $trip_invoiceno = "INVC-" . date('dmY') . "-" . $insLastID;
                $this->db->where('fld_id', $insLastID);
                $return = $this->db->update('travelticket_tbl', array('trip_invoiceno' => $trip_invoiceno));
            endif;
        }
        if ($this->input->post('triptype') == '2') {
            $insertArr1 = array(
                'travel_empid' => @$this->input->post('empid'),
                'trav_from' => $this->input->post('jfrom'),
                'trav_to' => $this->input->post('jto'),
                'dateoftravel' => $this->input->post('dateoftravel'),
                'dateofbooking' => $this->input->post('dateofbooking'),
                'tickproject' => $strprojIds,
                'other' => '1',
                'hrms_or_otherempl' => $hrms_or_otherempl,
                'otherempl_details' => $otherempl_detailsJson,
                'other_proj_bunit' => $this->input->post('otheremplbunit'),
                'airlines' => $this->input->post('airlines'),
                'trip_type' => $this->input->post('triptype'),
                'tickcost' => $this->input->post('tickcost'),
                'approvedby' => $this->input->post('approvedby'),
                'tickcomment' => $this->input->post('tickcomment'),
                'entry_by' => $this->tank_auth->get_user_id());
            $insertArr2 = array(
                'travel_empid' => @$this->input->post('empid'),
                'trav_from' => $this->input->post('returnjfrom'),
                'trav_to' => $this->input->post('returnjto'),
                'dateoftravel' => $this->input->post('returndateoftravel'),
                'dateofbooking' => $this->input->post('dateofbooking'),
                'tickproject' => $strprojIds,
                'airlines' => $this->input->post('returnairlines'),
                'trip_type' => $this->input->post('triptype'),
                'tickcost' => $this->input->post('tickcost'),
                'other' => '2',
                'hrms_or_otherempl' => $hrms_or_otherempl,
                'otherempl_details' => $otherempl_detailsJson,
                'other_proj_bunit' => $this->input->post('otheremplbunit'),
                'approvedby' => $this->input->post('approvedby'),
                'tickcomment' => $this->input->post('tickcomment'),
                'entry_by' => $this->tank_auth->get_user_id());
            $insLastID = $this->mastermodel->InsertMasterData($insertArr1, 'travelticket_tbl');
            $insLastID2 = $this->mastermodel->InsertMasterData($insertArr2, 'travelticket_tbl');
            if ($insLastID):
                $trip_invoiceno = "INVC-" . date('dmY') . "-" . $insLastID;
                $this->db->where("(fld_id='" . $insLastID . "' OR fld_id='" . $insLastID2 . "')", NULL, FALSE);
                $return = $this->db->update('travelticket_tbl', array('trip_invoiceno' => $trip_invoiceno));
            endif;
        }
        if ($insLastID) {
            $this->session->set_flashdata('successmsg', 'Travel Ticket Details Added Success');
        } else {
            $this->session->set_flashdata('errormsg', 'Something Went Wrong');
        }
        redirect(base_url('travelticket'));
    }

    public function travelticket_list() {
        $data['error'] = '';
        $data['title'] = 'Ticket List';
        $user_id = $this->session->userdata('loginid');
        if(!empty($user_id)){
        $this->load->view("travelticket/ticketlist_view", $data);
        }
        else{
            redirect('admin', true);
        }
    }

    public function editticket($decodedID) {
        // $DecId = base64_decode($decodedID);
        $DecId = $this->uri->segment(2);
        // echo $DecId; die;
        $UserempID = $this->tank_auth->get_user_id();
        $this->db->select('a.*,c.businessunit_id,b.fld_id as returnfld_id,b.trav_from as returntrav_from,b.trav_to as returntrav_to,b.dateoftravel as returndateoftravel,b.dateofbooking as returndateofbooking,b.tickproject as returntickproject,b.tickcost as returntickcost,b.approvedby as returnapprovedby,b.airlines as returnairlines,b.trip_type as returntrip_type,b.trip_invoiceno as returntrip_invoiceno');
        $this->db->from('travelticket_tbl as a');
        $this->db->join('travelticket_tbl as b', 'a.trip_invoiceno=b.trip_invoiceno', "LEFT");
        $this->db->join('main_employees_summary as c', 'a.travel_empid=c.user_id', "LEFT");
        $this->db->where(array('a.status' => '1', 'a.other' => '1', 'a.fld_id' => $DecId));
        $resultR = $this->db->get()->row();
        $data['tickdetailRow'] = ($resultR) ? $resultR : '';
        $data['title'] = "Edit Travel Ticket ";
        $this->load->view("travelticket/editticket_view", $data);
    }

    //Ajax List of Tickets..
    public function ajax_list_tickets() {
        $listArr = $this->travelticketmodel->get_datatables();

        $UserempID = $this->tank_auth->get_user_id();

        $data = array();
        $tripTypeArr = array('1' => 'One-Way', '2' => 'Round-Way');
        $no = 0;
        foreach ($listArr as $key => $value) {
            $row = array();
            $no++;
            $tickProj = base64_encode($value->tickproject);
            $proJectDetails = $this->projectlistdetails($tickProj);
            $row[] = $no;
            if ($value->hrms_or_otherempl == '1') {
                $row[] = $value->userfullname;
            } else {
                $decoderV = json_decode($value->otherempl_details, true);
                $row[] = @$decoderV['empname'] . " (<span style='color:red'> Other </span>)";
            }
            $row[] = $value->department_name;
            $row[] = $value->trav_from;
            $row[] = $value->trav_to;
            $row[] = date("d-m-Y", strtotime($value->dateoftravel));
            $row[] = date("d-m-Y", strtotime($value->dateofbooking));
            $row[] = $proJectDetails;
            if ($value->other < 2) {
                $row[] = number_format($value->tickcost, 2);
            } else {
                $row[] = '';
            }
//            $row[] = @$value->trip_invoiceno;
            $row[] = $tripTypeArr[$value->trip_type];
            $row[] = @$value->airlines;
            $row[] = $value->aprvuserfullname;
            $row[] = date("d-m-Y H:i:s A", strtotime($value->entry_date));
            $row[] = @$value->entrybyuserfullname;
            if (($value->other == '1') and ( $value->hrms_or_otherempl == '1')) {
                if ($UserempID != '287') {
                    $row[] = '<a href="' . base_url('editticket/' .$value->fld_id) . '"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;&nbsp;&nbsp';
                } else {
                    $row[] = 'Single-Trip';
                }
            } else {
                $row[] = 'Round-Trip';
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => '',
            "recordsTotal" => $this->travelticketmodel->count_all(),
            "recordsFiltered" => $this->travelticketmodel->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
    }

    public function projectlistdetails($tickProj) {
        $tickProjIdesArr = base64_decode($tickProj);
        $stngsArr = explode(",", $tickProjIdesArr);
        $recS = '';
        if ($stngsArr) {
            foreach ($stngsArr as $prow) {
                $recS .= get_project_name($prow) . ",<br>";
            }
        }
        return $recS;
    }

//Update / Save ..
    public function travelticket_update() {
        if ($_REQUEST) {
            $tripType = $this->input->post('fld_triptype');
            $recRowId = $this->input->post('fld_id_one');
            //Single Trip One Way..
            if ($tripType == '1') {
                $tProjIdsArr = $this->input->post('tickproject');
                $strprojIds = implode(",", $tProjIdsArr);
                $recRowId = $this->input->post('fld_id_one');
                //Single Row Update Code...
                $updArray1 = array('trav_from' => $this->input->post('jfrom'),
                    'trav_to' => $this->input->post('jto'),
                    'dateoftravel' => $this->input->post('dateoftravel'),
                    'dateofbooking' => $this->input->post('dateofbooking'),
                    'tickproject' => $strprojIds,
                    'tickcost' => $this->input->post('tickcost'),
                    'approvedby' => $this->input->post('approvedby'),
                    'tickcomment' => $this->input->post('tickcomment'),
                    'airlines' => $this->input->post('airlines'));
                $this->db->where(array('fld_id' => $recRowId, 'other' => '1'));
                $return = $this->db->update('travelticket_tbl', $updArray1);
            }
            //Round Trip...
            if ($tripType == '2') {
                $tProjIdsArr = $this->input->post('tickproject');
                $strprojIds = implode(",", $tProjIdsArr);
                $recRowId = $this->input->post('fld_id_one');
                $recRowIdReturn = $this->input->post('fld_id_two');
                //Single Row Update Code...
                $updArray1 = array('trav_from' => $this->input->post('jfrom'),
                    'trav_to' => $this->input->post('jto'),
                    'dateoftravel' => $this->input->post('dateoftravel'),
                    'dateofbooking' => $this->input->post('dateofbooking'),
                    'tickproject' => $strprojIds,
                    'tickcost' => $this->input->post('tickcost'),
                    'approvedby' => $this->input->post('approvedby'),
                    'tickcomment' => $this->input->post('tickcomment'),
                    'airlines' => $this->input->post('airlines'));
                $this->db->where(array('fld_id' => $recRowId, 'other' => '1'));
                $return = $this->db->update('travelticket_tbl', $updArray1);
                //Next Row.
                $updArray2 = array('trav_from' => $this->input->post('returnjfrom'),
                    'trav_to' => $this->input->post('returnjto'),
                    'dateoftravel' => $this->input->post('returndateoftravel'),
                    'dateofbooking' => $this->input->post('returndateofbooking'),
                    'tickproject' => $strprojIds,
                    'tickcost' => $this->input->post('tickcost'),
                    'approvedby' => $this->input->post('approvedby'),
                    'tickcomment' => $this->input->post('tickcomment'),
                    'airlines' => $this->input->post('returnairlines'));
                $this->db->where(array('fld_id' => $recRowIdReturn, 'other' => '2'));
                $return = $this->db->update('travelticket_tbl', $updArray2);
            }
            $this->session->set_flashdata('successmsg', 'Travel Ticket Details Updated Success');
        } else {
            $this->session->set_flashdata('errormsg', 'Something Went Wrong');
        }
        redirect(base_url('editticket/' . $recRowId));
    }

}
