<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisalon_project_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session', 'form_validation');
        $this->load->model('Mastermodel', 'mastermodel');
        $this->load->model('appraisalonproject/Appraisalonproject_model', 'assignappronproj');

        if (!($this->session->userdata('loginid'))) {
            redirect(base_url('admin'));
        }
    }

    //Employee List Controller... Report 1
    public function assignbyhr() {
        $data['error'] = array();
        $data['htitle'] = "Employee Basic Details Report";
        $data['jobgroupArr'] = $this->mastermodel->GetTableData("main_jobtitles", array("isactive" => "1"));
        $this->load->view('appraisal_project/apprproj_assignbyhr', $data);
    }

    public function appronproj_employeelistajax_list() {
        $list = $this->assignappronproj->get_datatables();
        $CurrStatusArr = array("1" => "<small style='color:#667eea'>Assigned</small>",
            "2" => "<small>Assigned</small>",
            "3" => "<small>Filled & Locked</small>",
            "4" => "<small> Approved </small>",
            "5" => "<small> Rejected </small>");

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $empRow) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $empRow->reporting_manager_name;
            $row[] = $empRow->employeeId;
            $row[] = $empRow->emailaddress;
            $row[] = $empRow->jobtitle_name;
            $row[] = $empRow->department_name;
            $row[] = $empRow->position_name;
            $row[] = ($empRow->current_status) ? $CurrStatusArr[$empRow->current_status] : "";
            $row[] = ($empRow->fld_id == "") ? '<span onclick="assignbyhrajax(' . $empRow->reporting_manager . ')" class="btn btn-info" id="asssign_' . $empRow->reporting_manager . '"> Assign </span>' : "<span class='btn btn-info' style='background-color:green'> Done </span> ";
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->assignappronproj->count_all(),
            "recordsFiltered" => $this->assignappronproj->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    //All Active Group...
    public function appronproj_assigninsert() {
        $userID = $_REQUEST["uid"];
        if ($userID) {
            $this->db->where(['rep_manager_id' => $userID, 'status' => '1', 'appr_session' => '2020']);
            $numRows = $this->db->count_all_results('appr_proj_assignbyhr');
            if ($numRows < 1):
                $insertArr = array("rep_manager_id" => $userID,
                    "appr_session" => "2020",
                    "assign_by_hrid" => $this->session->userdata('loginid'));
                $this->db->insert("appr_proj_assignbyhr", $insertArr);
                $insert_id = $this->db->insert_id();

                if ($insert_id) {
                    $emplRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $userID, "isactive" => "1"));
                    $data['error'] = '';
                    $data['prefix_name'] = $emplRec[0]->prefix_name;
                    $data['userfullname'] = $emplRec[0]->userfullname;
                    $emailaddress = $emplRec[0]->emailaddress;
                    $to = $emailaddress;
                    // $to = "asheesh9308@gmail.com";
                    $subject = "Team Appraisal Pending with Appraiser (IO)";
                    $msgDetails = $this->load->view('email/appr_assign_email_cegproj', $data, true);
                    $mailresp = $this->sendMail($to, $subject, $msgDetails);
                }
                echo $insert_id;
            endif;
        }
    }

    //Appraisal On Site ofc ... Project
    public function apprai_projsiteofc() {
        $data['error'] = array();
        $data['title'] = "Project Appraisal - List of Employee";
        $loginUserID = $this->session->userdata('loginid');

        $this->db->select('a.date_of_joining,a.user_id,a.userfullname,a.prefix_name,a.employeeId,a.emailaddress,a.department_name,a.position_name,a.jobtitle_name');
        $this->db->from("main_employees_summary as a");
        $this->db->join("main_empsalarydetails as sal", "a.user_id=sal.user_id", "LEFT");
        $this->db->where(array("a.isactive" => "1", "a.businessunit_id" => "3", "a.reporting_manager" => $loginUserID));
        $this->db->where("(a.jobtitle_id='9' OR a.jobtitle_id='10' OR a.jobtitle_id='11' OR a.jobtitle_id='12')", NULL, FALSE);
        // $this->db->where("((a.date_of_joining >='2018-08-01' AND a.date_of_joining <='2020-01-31') OR (a.date_of_joining <'2018-08-31'))", NULL, FALSE);
        $this->db->where(array("sal.isactive" => "1", "sal.appraisalduedate" => "April 2020"));
        $this->db->group_by("a.user_id");
        $data['EmplListArr'] = $this->db->get()->result();

        $this->db->select('a1.rep_manager_id');
        $this->db->from('appr_proj_assignbyhr as a1');
        $this->db->where(array("a1.status" => "1", "a1.rep_manager_id" => $loginUserID));
        $RecasRow = $this->db->get()->row();

        //As RO.. EMployee List Rec.. BC
        $this->db->select('a2.user_id,em.date_of_joining,em.userfullname,em.prefix_name,em.employeeId,em.emailaddress,em.department_name,em.position_name,em.jobtitle_name');
        $this->db->from('appr_proj_formdata_bc as a2');
        $this->db->join("main_employees_summary as em", "a2.user_id=em.user_id", "LEFT");
        $this->db->where(array("a2.status" => "1", "a2.session_appr_year" => "2020", "a2.rep_ro_id" => $loginUserID));
        $this->db->where("a2.current_status >", "1");
        $RecasRow2 = $this->db->get()->result();

        //As RO.. EMployee List Rec.. DE
        $this->db->select('a3.user_id,em1.date_of_joining,em1.userfullname,em1.prefix_name,em1.employeeId,em1.emailaddress,em1.department_name,em1.position_name,em1.jobtitle_name');
        $this->db->from('appr_proj_formdata_de as a3');
        $this->db->join("main_employees_summary as em1", "a3.user_id=em1.user_id", "LEFT");
        $this->db->where(array("a3.status" => "1", "a3.session_appr_year" => "2020", "a3.rep_ro_id" => $loginUserID));
        $this->db->where("a3.current_status >", "1");
        $RecasRow3 = $this->db->get()->result();
        if ((@$RecasRow->rep_manager_id == "") and ( $RecasRow2[0]->user_id == "") and ( $RecasRow3[0]->user_id == "")) {
            redirect(base_url("admin-dashboard"));
        }
        $data['rep_manager_assignbyhr'] = (@$RecasRow->rep_manager_id) ? true : false;
        $data['echeck_as_roDataArr'] = (@$RecasRow2) ? $RecasRow2 : null;
        $data['form_deRODataArr'] = (@$RecasRow3) ? $RecasRow3 : null;


//        echo "<pre>";
//        print_r($data);
//        die;

        $this->load->view('appraisal_project/apprproj_emplist', $data);
    }

    //Single Employee Fill Appraisal By IO/RO...
    public function single_emp_projappr($emplID) {
        $loginUserID = $this->session->userdata('loginid');
        $this->db->select('a.reporting_manager,a.reporting_manager_name,b.reviewing_officer_ro,c.prefix_name as reviewing_officer_roprefixname,c.userfullname as reviewing_officer_roname,a.jobtitle_id,a.date_of_joining,a.user_id,a.userfullname,a.prefix_name,a.employeeId,a.emailaddress,a.department_name,a.position_name,a.jobtitle_name,b.on_project');
        $this->db->from("main_employees_summary as a");
        $this->db->join("emp_otherofficial_data as b", "a.user_id=b.user_id", "LEFT");
        $this->db->join("main_employees_summary as c", "c.user_id=b.reviewing_officer_ro", "LEFT");
        $this->db->where(array("a.isactive" => "1", "a.user_id" => $emplID));
        $SingleRecArr = $this->db->get()->row();

        if (($SingleRecArr == null) or ( ( $SingleRecArr->reporting_manager != $loginUserID ) and ( $SingleRecArr->reviewing_officer_ro != $loginUserID ))) {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
            redirect(base_url('apprai_projsiteofc'));
        }

        if ($SingleRecArr) {
            $pRojidArr = explode(",", $SingleRecArr->on_project);
            $SingleRecArr->allprojName = $this->GetAllProjList($pRojidArr);
            $SingleRecArr->identityro_io = ($SingleRecArr->reporting_manager == $loginUserID ) ? "IO" : "RO";

            $SingleappraisalRec = $this->GetSingleRecByUserID($emplID);
            $SingleappraisalRecBC = $this->GetSingleRecByUserBCID($emplID);

            if (($SingleRecArr->reporting_manager == $loginUserID) and ( $SingleRecArr->reviewing_officer_ro == $loginUserID)) {
                $SingleRecArr->identityro_io = "BOTH";
            }
            $totOtherExpr = $this->GetOtherExpr($emplID);
            $totCegExpr = $this->GetCegExpr($emplID, $SingleRecArr->date_of_joining);
            $recEducationalDetail = GetEducationByUserid($emplID);
        }

        $this->load->view('appraisal_project/apprproj_emp_singlerec', compact("recEducationalDetail", "totCegExpr", "totOtherExpr", "SingleRecArr", "SingleappraisalRec", "SingleappraisalRecBC"));
    }

    //Total On Proj
    public function GetAllProjList($pRojidArr) {
        $this->db->select('a.project_name');
        $this->db->from("tm_projects as a");
        $this->db->where("a.is_active", "1");
        $this->db->where_in("a.id", $pRojidArr);
        $RecprojNms = $this->db->get()->result();
        $ProjectS = '';
        if (count($RecprojNms) == 1) {
            $ProjectS = $RecprojNms[0]->project_name;
        }
        if (($RecprojNms) and ( count($RecprojNms) > 1)) {
            foreach ($RecprojNms as $rEcD) {
                $ProjectS .= $rEcD->project_name . " , ";
            }
        }
        return ($ProjectS) ? $ProjectS : "";
    }

    //Save or Update Appraisal Form Data...
    public function apprai_projsiteofc_saveorupd() {
        $recArr = $this->input->post();
        if ($recArr and $recArr['formio_rotype'] == "IO") {
            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $numRows = $this->db->count_all_results('appr_proj_formdata_de');
            if ($numRows < 1):
                $insertArr = $this->formrequestinsertbyio($recArr);
                $this->db->insert("appr_proj_formdata_de", $insertArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form Submitted Successfully');
            endif;
            if ($numRows > 0):
                $UpdateArr = $this->formrequestupdatebyio($recArr);
                $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
                $this->db->update("appr_proj_formdata_de", $UpdateArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
            endif;
        }

        //Login As RO
        if ($recArr and $recArr['formio_rotype'] == "RO") {
            $UpdateArrRO = $this->formrequestupdatebyro_de($recArr);
            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $UpdateArrRO['current_status'] = "3";
            $this->db->update("appr_proj_formdata_de", $UpdateArrRO);
            $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
        }
        //Login As Both IO RO Same...
        if ($recArr and $recArr['formio_rotype'] == "BOTH") {
            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $numRows = $this->db->count_all_results('appr_proj_formdata_de');
            if ($numRows < 1):
                $insertArr = $this->formrequestinsertbyboth_de($recArr);
                $this->db->insert("appr_proj_formdata_de", $insertArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form Submitted Successfully');
            endif;
            if ($numRows > 0):
                $UpdateArr = $this->formrequestupdatebyboth_de($recArr);
                $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
                $this->db->update("appr_proj_formdata_de", $UpdateArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
            endif;
        }
        redirect(base_url("single_emp_projappr/" . $recArr['user_id']));
    }

    //Form BC Save Or Update...
    public function apprai_projsiteofcbcform_saveorupd() {
        $recArr = $this->input->post();
        if ($recArr and $recArr['formio_rotype'] == "IO") {
            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $numRows = $this->db->count_all_results('appr_proj_formdata_bc');
            if ($numRows < 1):
                $insertArr = $this->formbcrequestinsertbyio($recArr);
                $this->db->insert("appr_proj_formdata_bc", $insertArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form Submitted Successfully');
            endif;
            if ($numRows > 0):
                $UpdateArr = $this->formbcrequestupdatebyio($recArr);
                $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
                $this->db->update("appr_proj_formdata_bc", $UpdateArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
            endif;
        }
        //Login As RO
        if ($recArr and $recArr['formio_rotype'] == "RO") {
            $UpdateArrRO = $this->formbcrequestupdatebyRO($recArr);

            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $UpdateArrRO['current_status'] = "3";
            $this->db->update("appr_proj_formdata_bc", $UpdateArrRO);
            $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
        }
        //Login As Both IO RO Same...
        if ($recArr and $recArr['formio_rotype'] == "BOTH") {
            $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
            $numRows = $this->db->count_all_results('appr_proj_formdata_bc');
            if ($numRows < 1):
                $insertArr = $this->formbcrequestinsertby_BOTH($recArr);
                $this->db->insert("appr_proj_formdata_bc", $insertArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form Submitted Successfully');
            endif;
            if ($numRows > 0):
                $UpdateArr = $this->formbcrequestupdateby_BOTH($recArr);
                $this->db->where(['user_id' => $recArr['user_id'], 'status' => '1', 'session_appr_year' => '2020']);
                $this->db->update("appr_proj_formdata_bc", $UpdateArr);
                $this->session->set_flashdata('successmsg', 'Appraisal form record updated Successfully');
            endif;
        }
        redirect(base_url("single_emp_projappr/" . $recArr['user_id']));
    }

    private function formbcrequestinsertbyio($recArr) {
        $insertArr = array('user_id' => $recArr['user_id'],
            'rep_io_id' => $recArr['io_userid'],
            'rep_ro_id' => $recArr['ro_userid'],
            'bcform_jobkno_techfunc_ratingIO' => ($recArr['bcform_jobkno_techfunc_ratingIO']) ? $recArr['bcform_jobkno_techfunc_ratingIO'] : NULL,
            'bcform_person_communskill_ratingIO' => ($recArr['bcform_person_communskill_ratingIO']) ? $recArr['bcform_person_communskill_ratingIO'] : Null,
            'bcform_person_teammanageinterpskill_ratingIO' => ($recArr['bcform_person_teammanageinterpskill_ratingIO']) ? $recArr['bcform_person_teammanageinterpskill_ratingIO'] : Null,
            'bcform_managerialskill_delegationskill_ratingIO' => ($recArr['bcform_managerialskill_delegationskill_ratingIO']) ? $recArr['bcform_managerialskill_delegationskill_ratingIO'] : Null,
            'bcform_managerialskill_probsolv_ratingIO' => ($recArr['bcform_managerialskill_probsolv_ratingIO']) ? $recArr['bcform_managerialskill_probsolv_ratingIO'] : Null,
            'bcform_managerialskill_planningmonit_ratingIO' => ($recArr['bcform_managerialskill_planningmonit_ratingIO']) ? $recArr['bcform_managerialskill_planningmonit_ratingIO'] : Null,
            'bcform_managerialskill_negotiation_ratingIO' => ($recArr['bcform_managerialskill_negotiation_ratingIO']) ? $recArr['bcform_managerialskill_negotiation_ratingIO'] : Null,
            'bcform_leadership_conflictmanag_ratingIO' => ($recArr['bcform_leadership_conflictmanag_ratingIO']) ? $recArr['bcform_leadership_conflictmanag_ratingIO'] : Null,
            'bcform_leadership_strategic_ratingIO' => ($recArr['bcform_leadership_strategic_ratingIO']) ? $recArr['bcform_leadership_strategic_ratingIO'] : Null,
            'bcform_leadership_customerorient_ratingIO' => ($recArr['bcform_leadership_customerorient_ratingIO']) ? $recArr['bcform_leadership_customerorient_ratingIO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingIO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingIO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingIO'] : Null,
            'bcform_attitude_initiativproactiv_ratingIO' => ($recArr['bcform_attitude_initiativproactiv_ratingIO']) ? $recArr['bcform_attitude_initiativproactiv_ratingIO'] : Null,
            'bcform_attitude_resultorient_ratingIO' => ($recArr['bcform_attitude_resultorient_ratingIO']) ? $recArr['bcform_attitude_resultorient_ratingIO'] : Null,
            'bcform_attitude_openideas_ratingIO' => ($recArr['bcform_attitude_openideas_ratingIO']) ? $recArr['bcform_attitude_openideas_ratingIO'] : Null,
            'bcform_describe_highlight_significant_achievementIO' => ($recArr['bcform_describe_highlight_significant_achievementIO']) ? $recArr['bcform_describe_highlight_significant_achievementIO'] : Null,
            'bcform_describe_constraints_limitations_facedIO' => ($recArr['bcform_describe_constraints_limitations_facedIO']) ? $recArr['bcform_describe_constraints_limitations_facedIO'] : Null,
            'bcform_technical_trainingIO_01' => ($recArr['bcform_technical_trainingIO_01']) ? $recArr['bcform_technical_trainingIO_01'] : Null,
            'bcform_technical_trainingIO_02' => ($recArr['bcform_technical_trainingIO_02']) ? $recArr['bcform_technical_trainingIO_02'] : Null,
            'bcform_technical_trainingIO_03' => ($recArr['bcform_technical_trainingIO_03']) ? $recArr['bcform_technical_trainingIO_03'] : Null,
            'bcform_technical_trainingIO_04' => ($recArr['bcform_technical_trainingIO_04']) ? $recArr['bcform_technical_trainingIO_04'] : Null,
            'bcform_technical_trainingIO_05' => ($recArr['bcform_technical_trainingIO_05']) ? $recArr['bcform_technical_trainingIO_05'] : Null,
            'bcform_behaviour_trainingIO_01' => ($recArr['bcform_behaviour_trainingIO_01']) ? $recArr['bcform_behaviour_trainingIO_01'] : Null,
            'bcform_behaviour_trainingIO_02' => ($recArr['bcform_behaviour_trainingIO_02']) ? $recArr['bcform_behaviour_trainingIO_02'] : Null,
            'bcform_behaviour_trainingIO_03' => ($recArr['bcform_behaviour_trainingIO_03']) ? $recArr['bcform_behaviour_trainingIO_03'] : Null,
            'bcform_behaviour_trainingIO_04' => ($recArr['bcform_behaviour_trainingIO_04']) ? $recArr['bcform_behaviour_trainingIO_04'] : Null,
            'bcform_behaviour_trainingIO_05' => ($recArr['bcform_behaviour_trainingIO_05']) ? $recArr['bcform_behaviour_trainingIO_05'] : Null,
            'bcform_ideasuggestionIO_01' => ($recArr['bcform_ideasuggestionIO_01']) ? $recArr['bcform_ideasuggestionIO_01'] : Null,
            'bcform_ideasuggestionIO_02' => ($recArr['bcform_ideasuggestionIO_02']) ? $recArr['bcform_ideasuggestionIO_02'] : Null,
            'bcform_ideasuggestionIO_03' => ($recArr['bcform_ideasuggestionIO_03']) ? $recArr['bcform_ideasuggestionIO_03'] : Null,
            'bcform_ideasuggestionIO_04' => ($recArr['bcform_ideasuggestionIO_04']) ? $recArr['bcform_ideasuggestionIO_04'] : Null,
            'current_status' => "1",
            'session_appr_year' => "2020",
            'entry_by' => $this->session->userdata('loginid')
        );
        return $insertArr;
    }

    private function formbcrequestupdatebyio($recArr) {
        $insertArr = array(
            'bcform_jobkno_techfunc_ratingIO' => ($recArr['bcform_jobkno_techfunc_ratingIO']) ? $recArr['bcform_jobkno_techfunc_ratingIO'] : Null,
            'bcform_person_communskill_ratingIO' => ($recArr['bcform_person_communskill_ratingIO']) ? $recArr['bcform_person_communskill_ratingIO'] : Null,
            'bcform_person_teammanageinterpskill_ratingIO' => ($recArr['bcform_person_teammanageinterpskill_ratingIO']) ? $recArr['bcform_person_teammanageinterpskill_ratingIO'] : Null,
            'bcform_managerialskill_delegationskill_ratingIO' => ($recArr['bcform_managerialskill_delegationskill_ratingIO']) ? $recArr['bcform_managerialskill_delegationskill_ratingIO'] : Null,
            'bcform_managerialskill_probsolv_ratingIO' => ($recArr['bcform_managerialskill_probsolv_ratingIO']) ? $recArr['bcform_managerialskill_probsolv_ratingIO'] : Null,
            'bcform_managerialskill_planningmonit_ratingIO' => ($recArr['bcform_managerialskill_planningmonit_ratingIO']) ? $recArr['bcform_managerialskill_planningmonit_ratingIO'] : Null,
            'bcform_managerialskill_negotiation_ratingIO' => ($recArr['bcform_managerialskill_negotiation_ratingIO']) ? $recArr['bcform_managerialskill_negotiation_ratingIO'] : Null,
            'bcform_leadership_conflictmanag_ratingIO' => ($recArr['bcform_leadership_conflictmanag_ratingIO']) ? $recArr['bcform_leadership_conflictmanag_ratingIO'] : Null,
            'bcform_leadership_strategic_ratingIO' => ($recArr['bcform_leadership_strategic_ratingIO']) ? $recArr['bcform_leadership_strategic_ratingIO'] : Null,
            'bcform_leadership_customerorient_ratingIO' => ($recArr['bcform_leadership_customerorient_ratingIO']) ? $recArr['bcform_leadership_customerorient_ratingIO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingIO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingIO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingIO'] : Null,
            'bcform_attitude_initiativproactiv_ratingIO' => ($recArr['bcform_attitude_initiativproactiv_ratingIO']) ? $recArr['bcform_attitude_initiativproactiv_ratingIO'] : Null,
            'bcform_attitude_resultorient_ratingIO' => ($recArr['bcform_attitude_resultorient_ratingIO']) ? $recArr['bcform_attitude_resultorient_ratingIO'] : Null,
            'bcform_attitude_openideas_ratingIO' => ($recArr['bcform_attitude_openideas_ratingIO']) ? $recArr['bcform_attitude_openideas_ratingIO'] : Null,
            'bcform_describe_highlight_significant_achievementIO' => ($recArr['bcform_describe_highlight_significant_achievementIO']) ? $recArr['bcform_describe_highlight_significant_achievementIO'] : Null,
            'bcform_describe_constraints_limitations_facedIO' => ($recArr['bcform_describe_constraints_limitations_facedIO']) ? $recArr['bcform_describe_constraints_limitations_facedIO'] : Null,
            'bcform_technical_trainingIO_01' => ($recArr['bcform_technical_trainingIO_01']) ? $recArr['bcform_technical_trainingIO_01'] : Null,
            'bcform_technical_trainingIO_02' => ($recArr['bcform_technical_trainingIO_02']) ? $recArr['bcform_technical_trainingIO_02'] : Null,
            'bcform_technical_trainingIO_03' => ($recArr['bcform_technical_trainingIO_03']) ? $recArr['bcform_technical_trainingIO_03'] : Null,
            'bcform_technical_trainingIO_04' => ($recArr['bcform_technical_trainingIO_04']) ? $recArr['bcform_technical_trainingIO_04'] : Null,
            'bcform_technical_trainingIO_05' => ($recArr['bcform_technical_trainingIO_05']) ? $recArr['bcform_technical_trainingIO_05'] : Null,
            'bcform_behaviour_trainingIO_01' => ($recArr['bcform_behaviour_trainingIO_01']) ? $recArr['bcform_behaviour_trainingIO_01'] : Null,
            'bcform_behaviour_trainingIO_02' => ($recArr['bcform_behaviour_trainingIO_02']) ? $recArr['bcform_behaviour_trainingIO_02'] : Null,
            'bcform_behaviour_trainingIO_03' => ($recArr['bcform_behaviour_trainingIO_03']) ? $recArr['bcform_behaviour_trainingIO_03'] : Null,
            'bcform_behaviour_trainingIO_04' => ($recArr['bcform_behaviour_trainingIO_04']) ? $recArr['bcform_behaviour_trainingIO_04'] : Null,
            'bcform_behaviour_trainingIO_05' => ($recArr['bcform_behaviour_trainingIO_05']) ? $recArr['bcform_behaviour_trainingIO_05'] : Null,
            'bcform_ideasuggestionIO_01' => ($recArr['bcform_ideasuggestionIO_01']) ? $recArr['bcform_ideasuggestionIO_01'] : Null,
            'bcform_ideasuggestionIO_02' => ($recArr['bcform_ideasuggestionIO_02']) ? $recArr['bcform_ideasuggestionIO_02'] : Null,
            'bcform_ideasuggestionIO_03' => ($recArr['bcform_ideasuggestionIO_03']) ? $recArr['bcform_ideasuggestionIO_03'] : Null,
            'bcform_ideasuggestionIO_04' => ($recArr['bcform_ideasuggestionIO_04']) ? $recArr['bcform_ideasuggestionIO_04'] : Null);
        return $insertArr;
    }

    private function formbcrequestupdatebyRO($recArr) {
        $insertArr = array('bcform_jobkno_techfunc_ratingRO' => ($recArr['bcform_jobkno_techfunc_ratingRO']) ? $recArr['bcform_jobkno_techfunc_ratingRO'] : Null,
            'bcform_person_communskill_ratingRO' => ($recArr['bcform_person_communskill_ratingRO']) ? $recArr['bcform_person_communskill_ratingRO'] : Null,
            'bcform_person_teammanageinterpskill_ratingRO' => ($recArr['bcform_person_teammanageinterpskill_ratingRO']) ? $recArr['bcform_person_teammanageinterpskill_ratingRO'] : Null,
            'bcform_managerialskill_delegationskill_ratingRO' => ($recArr['bcform_managerialskill_delegationskill_ratingRO']) ? $recArr['bcform_managerialskill_delegationskill_ratingRO'] : Null,
            'bcform_managerialskill_probsolv_ratingRO' => ($recArr['bcform_managerialskill_probsolv_ratingRO']) ? $recArr['bcform_managerialskill_probsolv_ratingRO'] : Null,
            'bcform_managerialskill_planningmonit_ratingRO' => ($recArr['bcform_managerialskill_planningmonit_ratingRO']) ? $recArr['bcform_managerialskill_planningmonit_ratingRO'] : Null,
            'bcform_managerialskill_negotiation_ratingRO' => ($recArr['bcform_managerialskill_negotiation_ratingRO']) ? $recArr['bcform_managerialskill_negotiation_ratingRO'] : Null,
            'bcform_leadership_conflictmanag_ratingRO' => ($recArr['bcform_leadership_conflictmanag_ratingRO']) ? $recArr['bcform_leadership_conflictmanag_ratingRO'] : Null,
            'bcform_leadership_strategic_ratingRO' => ($recArr['bcform_leadership_strategic_ratingRO']) ? $recArr['bcform_leadership_strategic_ratingRO'] : Null,
            'bcform_leadership_customerorient_ratingRO' => ($recArr['bcform_leadership_customerorient_ratingRO']) ? $recArr['bcform_leadership_customerorient_ratingRO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingRO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingRO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingRO'] : Null,
            'bcform_attitude_initiativproactiv_ratingRO' => ($recArr['bcform_attitude_initiativproactiv_ratingRO']) ? $recArr['bcform_attitude_initiativproactiv_ratingRO'] : Null,
            'bcform_attitude_resultorient_ratingRO' => ($recArr['bcform_attitude_resultorient_ratingRO']) ? $recArr['bcform_attitude_resultorient_ratingRO'] : Null,
            'bcform_attitude_openideas_ratingRO' => ($recArr['bcform_attitude_openideas_ratingRO']) ? $recArr['bcform_attitude_openideas_ratingRO'] : Null,
            'bcform_describe_highlight_significant_achievementRO' => ($recArr['bcform_describe_highlight_significant_achievementRO']) ? $recArr['bcform_describe_highlight_significant_achievementRO'] : Null,
            'bcform_describe_constraints_limitations_facedRO' => ($recArr['bcform_describe_constraints_limitations_facedRO']) ? $recArr['bcform_describe_constraints_limitations_facedRO'] : Null
        );
        return $insertArr;
    }

    private function formbcrequestinsertby_BOTH($recArr) {
        $insertArr = array('user_id' => $recArr['user_id'],
            'rep_io_id' => $recArr['io_userid'],
            'rep_ro_id' => $recArr['ro_userid'],
            'bcform_jobkno_techfunc_ratingIO' => ($recArr['bcform_jobkno_techfunc_ratingIO']) ? $recArr['bcform_jobkno_techfunc_ratingIO'] : Null,
            'bcform_jobkno_techfunc_ratingRO' => ($recArr['bcform_jobkno_techfunc_ratingRO']) ? $recArr['bcform_jobkno_techfunc_ratingRO'] : Null,
            'bcform_person_communskill_ratingIO' => ($recArr['bcform_person_communskill_ratingIO']) ? $recArr['bcform_person_communskill_ratingIO'] : Null,
            'bcform_person_communskill_ratingRO' => ($recArr['bcform_person_communskill_ratingRO']) ? $recArr['bcform_person_communskill_ratingRO'] : Null,
            'bcform_person_teammanageinterpskill_ratingIO' => ($recArr['bcform_person_teammanageinterpskill_ratingIO']) ? $recArr['bcform_person_teammanageinterpskill_ratingIO'] : Null,
            'bcform_person_teammanageinterpskill_ratingRO' => ($recArr['bcform_person_teammanageinterpskill_ratingRO']) ? $recArr['bcform_person_teammanageinterpskill_ratingRO'] : Null,
            'bcform_managerialskill_delegationskill_ratingIO' => ($recArr['bcform_managerialskill_delegationskill_ratingIO']) ? $recArr['bcform_managerialskill_delegationskill_ratingIO'] : Null,
            'bcform_managerialskill_delegationskill_ratingRO' => ($recArr['bcform_managerialskill_delegationskill_ratingRO']) ? $recArr['bcform_managerialskill_delegationskill_ratingRO'] : Null,
            'bcform_managerialskill_probsolv_ratingIO' => ($recArr['bcform_managerialskill_probsolv_ratingIO']) ? $recArr['bcform_managerialskill_probsolv_ratingIO'] : Null,
            'bcform_managerialskill_probsolv_ratingRO' => ($recArr['bcform_managerialskill_probsolv_ratingRO']) ? $recArr['bcform_managerialskill_probsolv_ratingRO'] : Null,
            'bcform_managerialskill_planningmonit_ratingIO' => ($recArr['bcform_managerialskill_planningmonit_ratingIO']) ? $recArr['bcform_managerialskill_planningmonit_ratingIO'] : Null,
            'bcform_managerialskill_planningmonit_ratingRO' => ($recArr['bcform_managerialskill_planningmonit_ratingRO']) ? $recArr['bcform_managerialskill_planningmonit_ratingRO'] : Null,
            'bcform_managerialskill_negotiation_ratingIO' => ($recArr['bcform_managerialskill_negotiation_ratingIO']) ? $recArr['bcform_managerialskill_negotiation_ratingIO'] : Null,
            'bcform_managerialskill_negotiation_ratingRO' => ($recArr['bcform_managerialskill_negotiation_ratingRO']) ? $recArr['bcform_managerialskill_negotiation_ratingRO'] : Null,
            'bcform_leadership_conflictmanag_ratingIO' => ($recArr['bcform_leadership_conflictmanag_ratingIO']) ? $recArr['bcform_leadership_conflictmanag_ratingIO'] : Null,
            'bcform_leadership_conflictmanag_ratingRO' => ($recArr['bcform_leadership_conflictmanag_ratingRO']) ? $recArr['bcform_leadership_conflictmanag_ratingRO'] : Null,
            'bcform_leadership_strategic_ratingIO' => ($recArr['bcform_leadership_strategic_ratingIO']) ? $recArr['bcform_leadership_strategic_ratingIO'] : Null,
            'bcform_leadership_strategic_ratingRO' => ($recArr['bcform_leadership_strategic_ratingRO']) ? $recArr['bcform_leadership_strategic_ratingRO'] : Null,
            'bcform_leadership_customerorient_ratingIO' => ($recArr['bcform_leadership_customerorient_ratingIO']) ? $recArr['bcform_leadership_customerorient_ratingIO'] : Null,
            'bcform_leadership_customerorient_ratingRO' => ($recArr['bcform_leadership_customerorient_ratingRO']) ? $recArr['bcform_leadership_customerorient_ratingRO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingIO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingIO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingIO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingRO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingRO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingRO'] : Null,
            'bcform_attitude_initiativproactiv_ratingIO' => ($recArr['bcform_attitude_initiativproactiv_ratingIO']) ? $recArr['bcform_attitude_initiativproactiv_ratingIO'] : Null,
            'bcform_attitude_initiativproactiv_ratingRO' => ($recArr['bcform_attitude_initiativproactiv_ratingRO']) ? $recArr['bcform_attitude_initiativproactiv_ratingRO'] : Null,
            'bcform_attitude_resultorient_ratingIO' => ($recArr['bcform_attitude_resultorient_ratingIO']) ? $recArr['bcform_attitude_resultorient_ratingIO'] : Null,
            'bcform_attitude_resultorient_ratingRO' => ($recArr['bcform_attitude_resultorient_ratingRO']) ? $recArr['bcform_attitude_resultorient_ratingRO'] : Null,
            'bcform_attitude_openideas_ratingIO' => ($recArr['bcform_attitude_openideas_ratingIO']) ? $recArr['bcform_attitude_openideas_ratingIO'] : Null,
            'bcform_attitude_openideas_ratingRO' => ($recArr['bcform_attitude_openideas_ratingRO']) ? $recArr['bcform_attitude_openideas_ratingRO'] : Null,
            'bcform_describe_highlight_significant_achievementIO' => ($recArr['bcform_describe_highlight_significant_achievementIO']) ? $recArr['bcform_describe_highlight_significant_achievementIO'] : Null,
            'bcform_describe_constraints_limitations_facedIO' => ($recArr['bcform_describe_constraints_limitations_facedIO']) ? $recArr['bcform_describe_constraints_limitations_facedIO'] : Null,
            'bcform_technical_trainingIO_01' => ($recArr['bcform_technical_trainingIO_01']) ? $recArr['bcform_technical_trainingIO_01'] : Null,
            'bcform_technical_trainingIO_02' => ($recArr['bcform_technical_trainingIO_02']) ? $recArr['bcform_technical_trainingIO_02'] : Null,
            'bcform_technical_trainingIO_03' => ($recArr['bcform_technical_trainingIO_03']) ? $recArr['bcform_technical_trainingIO_03'] : Null,
            'bcform_technical_trainingIO_04' => ($recArr['bcform_technical_trainingIO_04']) ? $recArr['bcform_technical_trainingIO_04'] : Null,
            'bcform_technical_trainingIO_05' => ($recArr['bcform_technical_trainingIO_05']) ? $recArr['bcform_technical_trainingIO_05'] : Null,
            'bcform_behaviour_trainingIO_01' => ($recArr['bcform_behaviour_trainingIO_01']) ? $recArr['bcform_behaviour_trainingIO_01'] : Null,
            'bcform_behaviour_trainingIO_02' => ($recArr['bcform_behaviour_trainingIO_02']) ? $recArr['bcform_behaviour_trainingIO_02'] : Null,
            'bcform_behaviour_trainingIO_03' => ($recArr['bcform_behaviour_trainingIO_03']) ? $recArr['bcform_behaviour_trainingIO_03'] : Null,
            'bcform_behaviour_trainingIO_04' => ($recArr['bcform_behaviour_trainingIO_04']) ? $recArr['bcform_behaviour_trainingIO_04'] : Null,
            'bcform_behaviour_trainingIO_05' => ($recArr['bcform_behaviour_trainingIO_05']) ? $recArr['bcform_behaviour_trainingIO_05'] : Null,
            'bcform_ideasuggestionIO_01' => ($recArr['bcform_ideasuggestionIO_01']) ? $recArr['bcform_ideasuggestionIO_01'] : Null,
            'bcform_ideasuggestionIO_02' => ($recArr['bcform_ideasuggestionIO_02']) ? $recArr['bcform_ideasuggestionIO_02'] : Null,
            'bcform_ideasuggestionIO_03' => ($recArr['bcform_ideasuggestionIO_03']) ? $recArr['bcform_ideasuggestionIO_03'] : Null,
            'bcform_ideasuggestionIO_04' => ($recArr['bcform_ideasuggestionIO_04']) ? $recArr['bcform_ideasuggestionIO_04'] : Null,
            'current_status' => "1",
            'session_appr_year' => "2020",
            'entry_by' => $this->session->userdata('loginid')
        );
        return $insertArr;
    }

    private function formbcrequestupdateby_BOTH($recArr) {
        $insertArr = array('bcform_jobkno_techfunc_ratingIO' => ($recArr['bcform_jobkno_techfunc_ratingIO']) ? $recArr['bcform_jobkno_techfunc_ratingIO'] : Null,
            'bcform_jobkno_techfunc_ratingRO' => ($recArr['bcform_jobkno_techfunc_ratingRO']) ? $recArr['bcform_jobkno_techfunc_ratingRO'] : Null,
            'bcform_person_communskill_ratingIO' => ($recArr['bcform_person_communskill_ratingIO']) ? $recArr['bcform_person_communskill_ratingIO'] : Null,
            'bcform_person_communskill_ratingRO' => ($recArr['bcform_person_communskill_ratingRO']) ? $recArr['bcform_person_communskill_ratingRO'] : Null,
            'bcform_person_teammanageinterpskill_ratingIO' => ($recArr['bcform_person_teammanageinterpskill_ratingIO']) ? $recArr['bcform_person_teammanageinterpskill_ratingIO'] : Null,
            'bcform_person_teammanageinterpskill_ratingRO' => ($recArr['bcform_person_teammanageinterpskill_ratingRO']) ? $recArr['bcform_person_teammanageinterpskill_ratingRO'] : Null,
            'bcform_managerialskill_delegationskill_ratingIO' => ($recArr['bcform_managerialskill_delegationskill_ratingIO']) ? $recArr['bcform_managerialskill_delegationskill_ratingIO'] : Null,
            'bcform_managerialskill_delegationskill_ratingRO' => ($recArr['bcform_managerialskill_delegationskill_ratingRO']) ? $recArr['bcform_managerialskill_delegationskill_ratingRO'] : Null,
            'bcform_managerialskill_probsolv_ratingIO' => ($recArr['bcform_managerialskill_probsolv_ratingIO']) ? $recArr['bcform_managerialskill_probsolv_ratingIO'] : Null,
            'bcform_managerialskill_probsolv_ratingRO' => ($recArr['bcform_managerialskill_probsolv_ratingRO']) ? $recArr['bcform_managerialskill_probsolv_ratingRO'] : Null,
            'bcform_managerialskill_planningmonit_ratingIO' => ($recArr['bcform_managerialskill_planningmonit_ratingIO']) ? $recArr['bcform_managerialskill_planningmonit_ratingIO'] : Null,
            'bcform_managerialskill_planningmonit_ratingRO' => ($recArr['bcform_managerialskill_planningmonit_ratingRO']) ? $recArr['bcform_managerialskill_planningmonit_ratingRO'] : Null,
            'bcform_managerialskill_negotiation_ratingIO' => ($recArr['bcform_managerialskill_negotiation_ratingIO']) ? $recArr['bcform_managerialskill_negotiation_ratingIO'] : Null,
            'bcform_managerialskill_negotiation_ratingRO' => ($recArr['bcform_managerialskill_negotiation_ratingRO']) ? $recArr['bcform_managerialskill_negotiation_ratingRO'] : Null,
            'bcform_leadership_conflictmanag_ratingIO' => ($recArr['bcform_leadership_conflictmanag_ratingIO']) ? $recArr['bcform_leadership_conflictmanag_ratingIO'] : Null,
            'bcform_leadership_conflictmanag_ratingRO' => ($recArr['bcform_leadership_conflictmanag_ratingRO']) ? $recArr['bcform_leadership_conflictmanag_ratingRO'] : Null,
            'bcform_leadership_strategic_ratingIO' => ($recArr['bcform_leadership_strategic_ratingIO']) ? $recArr['bcform_leadership_strategic_ratingIO'] : Null,
            'bcform_leadership_strategic_ratingRO' => ($recArr['bcform_leadership_strategic_ratingRO']) ? $recArr['bcform_leadership_strategic_ratingRO'] : Null,
            'bcform_leadership_customerorient_ratingIO' => ($recArr['bcform_leadership_customerorient_ratingIO']) ? $recArr['bcform_leadership_customerorient_ratingIO'] : Null,
            'bcform_leadership_customerorient_ratingRO' => ($recArr['bcform_leadership_customerorient_ratingRO']) ? $recArr['bcform_leadership_customerorient_ratingRO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingIO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingIO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingIO'] : Null,
            'bcform_leadership_trainingdevteambuild_ratingRO' => ($recArr['bcform_leadership_trainingdevteambuild_ratingRO']) ? $recArr['bcform_leadership_trainingdevteambuild_ratingRO'] : Null,
            'bcform_attitude_initiativproactiv_ratingIO' => ($recArr['bcform_attitude_initiativproactiv_ratingIO']) ? $recArr['bcform_attitude_initiativproactiv_ratingIO'] : Null,
            'bcform_attitude_initiativproactiv_ratingRO' => ($recArr['bcform_attitude_initiativproactiv_ratingRO']) ? $recArr['bcform_attitude_initiativproactiv_ratingRO'] : Null,
            'bcform_attitude_resultorient_ratingIO' => ($recArr['bcform_attitude_resultorient_ratingIO']) ? $recArr['bcform_attitude_resultorient_ratingIO'] : Null,
            'bcform_attitude_resultorient_ratingRO' => ($recArr['bcform_attitude_resultorient_ratingRO']) ? $recArr['bcform_attitude_resultorient_ratingRO'] : Null,
            'bcform_attitude_openideas_ratingIO' => ($recArr['bcform_attitude_openideas_ratingIO']) ? $recArr['bcform_attitude_openideas_ratingIO'] : Null,
            'bcform_attitude_openideas_ratingRO' => ($recArr['bcform_attitude_openideas_ratingRO']) ? $recArr['bcform_attitude_openideas_ratingRO'] : Null,
            'bcform_describe_highlight_significant_achievementIO' => ($recArr['bcform_describe_highlight_significant_achievementIO']) ? $recArr['bcform_describe_highlight_significant_achievementIO'] : Null,
            'bcform_describe_constraints_limitations_facedIO' => ($recArr['bcform_describe_constraints_limitations_facedIO']) ? $recArr['bcform_describe_constraints_limitations_facedIO'] : Null,
            'bcform_technical_trainingIO_01' => ($recArr['bcform_technical_trainingIO_01']) ? $recArr['bcform_technical_trainingIO_01'] : Null,
            'bcform_technical_trainingIO_02' => ($recArr['bcform_technical_trainingIO_02']) ? $recArr['bcform_technical_trainingIO_02'] : Null,
            'bcform_technical_trainingIO_03' => ($recArr['bcform_technical_trainingIO_03']) ? $recArr['bcform_technical_trainingIO_03'] : Null,
            'bcform_technical_trainingIO_04' => ($recArr['bcform_technical_trainingIO_04']) ? $recArr['bcform_technical_trainingIO_04'] : Null,
            'bcform_technical_trainingIO_05' => ($recArr['bcform_technical_trainingIO_05']) ? $recArr['bcform_technical_trainingIO_05'] : Null,
            'bcform_behaviour_trainingIO_01' => ($recArr['bcform_behaviour_trainingIO_01']) ? $recArr['bcform_behaviour_trainingIO_01'] : Null,
            'bcform_behaviour_trainingIO_02' => ($recArr['bcform_behaviour_trainingIO_02']) ? $recArr['bcform_behaviour_trainingIO_02'] : Null,
            'bcform_behaviour_trainingIO_03' => ($recArr['bcform_behaviour_trainingIO_03']) ? $recArr['bcform_behaviour_trainingIO_03'] : Null,
            'bcform_behaviour_trainingIO_04' => ($recArr['bcform_behaviour_trainingIO_04']) ? $recArr['bcform_behaviour_trainingIO_04'] : Null,
            'bcform_behaviour_trainingIO_05' => ($recArr['bcform_behaviour_trainingIO_05']) ? $recArr['bcform_behaviour_trainingIO_05'] : Null,
            'bcform_ideasuggestionIO_01' => ($recArr['bcform_ideasuggestionIO_01']) ? $recArr['bcform_ideasuggestionIO_01'] : Null,
            'bcform_ideasuggestionIO_02' => ($recArr['bcform_ideasuggestionIO_02']) ? $recArr['bcform_ideasuggestionIO_02'] : Null,
            'bcform_ideasuggestionIO_03' => ($recArr['bcform_ideasuggestionIO_03']) ? $recArr['bcform_ideasuggestionIO_03'] : Null,
            'bcform_ideasuggestionIO_04' => ($recArr['bcform_ideasuggestionIO_04']) ? $recArr['bcform_ideasuggestionIO_04'] : Null);
        return $insertArr;
    }

    private function formrequestinsertbyio($recArr) {
        $insertArr = array('user_id' => $recArr['user_id'],
            'rep_io_id' => $recArr['io_userid'],
            'rep_ro_id' => $recArr['ro_userid'],
            'deform_jobkno_techfunc_ratingIO' => ($recArr['deform_jobkno_techfunc_ratingIO']) ? $recArr['deform_jobkno_techfunc_ratingIO'] : Null,
            'deform_persnskill_communskill_ratingIO' => ($recArr['deform_persnskill_communskill_ratingIO']) ? $recArr['deform_persnskill_communskill_ratingIO'] : Null,
            'deform_persnskill_qualityconc_ratingIO' => ($recArr['deform_persnskill_qualityconc_ratingIO']) ? $recArr['deform_persnskill_qualityconc_ratingIO'] : Null,
            'deform_persnskill_selfmanagement_ratingIO' => ($recArr['deform_persnskill_selfmanagement_ratingIO']) ? $recArr['deform_persnskill_selfmanagement_ratingIO'] : Null,
            'deform_persnskill_commercialknowledge_ratingIO' => ($recArr['deform_persnskill_commercialknowledge_ratingIO']) ? $recArr['deform_persnskill_commercialknowledge_ratingIO'] : Null,
            'deform_persnskill_problemsolv_ratingIO' => ($recArr['deform_persnskill_problemsolv_ratingIO']) ? $recArr['deform_persnskill_problemsolv_ratingIO'] : Null,
            'deform_persnskill_interpersskills_ratingIO' => ($recArr['deform_persnskill_interpersskills_ratingIO']) ? $recArr['deform_persnskill_interpersskills_ratingIO'] : Null,
            'deform_persnskill_resourcemobilization_ratingIO' => ($recArr['deform_persnskill_resourcemobilization_ratingIO']) ? $recArr['deform_persnskill_resourcemobilization_ratingIO'] : Null,
            'deform_persnskill_processorient_ratingIO' => ($recArr['deform_persnskill_processorient_ratingIO']) ? $recArr['deform_persnskill_processorient_ratingIO'] : Null,
            'deform_attitude_initiativeproact_ratingIO' => ($recArr['deform_attitude_initiativeproact_ratingIO']) ? $recArr['deform_attitude_initiativeproact_ratingIO'] : Null,
            'deform_attitude_costconcious_ratingIO' => ($recArr['deform_attitude_costconcious_ratingIO']) ? $recArr['deform_attitude_costconcious_ratingIO'] : Null,
            'deform_attitude_integrity_ratingIO' => ($recArr['deform_attitude_integrity_ratingIO']) ? $recArr['deform_attitude_integrity_ratingIO'] : Null,
            'deform_describe_highlight_significant_achievementIO' => ($recArr['deform_describe_highlight_significant_achievementIO']) ? $recArr['deform_describe_highlight_significant_achievementIO'] : Null,
            'deform_describe_constraints_limitations_facedIO' => ($recArr['deform_describe_constraints_limitations_facedIO']) ? $recArr['deform_describe_constraints_limitations_facedIO'] : Null,
            'deform_technical_trainingIO_01' => ($recArr['deform_technical_trainingIO_01']) ? $recArr['deform_technical_trainingIO_01'] : Null,
            'deform_technical_trainingIO_02' => ($recArr['deform_technical_trainingIO_02']) ? $recArr['deform_technical_trainingIO_02'] : Null,
            'deform_technical_trainingIO_03' => ($recArr['deform_technical_trainingIO_03']) ? $recArr['deform_technical_trainingIO_03'] : Null,
            'deform_technical_trainingIO_04' => ($recArr['deform_technical_trainingIO_04']) ? $recArr['deform_technical_trainingIO_04'] : Null,
            'deform_technical_trainingIO_05' => ($recArr['deform_technical_trainingIO_05']) ? $recArr['deform_technical_trainingIO_05'] : Null,
            'deform_behaviour_trainingIO_01' => ($recArr['deform_behaviour_trainingIO_01']) ? $recArr['deform_behaviour_trainingIO_01'] : Null,
            'deform_behaviour_trainingIO_02' => ($recArr['deform_behaviour_trainingIO_02']) ? $recArr['deform_behaviour_trainingIO_02'] : Null,
            'deform_behaviour_trainingIO_03' => ($recArr['deform_behaviour_trainingIO_03']) ? $recArr['deform_behaviour_trainingIO_03'] : Null,
            'deform_behaviour_trainingIO_04' => ($recArr['deform_behaviour_trainingIO_04']) ? $recArr['deform_behaviour_trainingIO_04'] : Null,
            'deform_behaviour_trainingIO_05' => ($recArr['deform_behaviour_trainingIO_05']) ? $recArr['deform_behaviour_trainingIO_05'] : Null,
            'deform_ideasuggestionIO_01' => ($recArr['deform_ideasuggestionIO_01']) ? $recArr['deform_ideasuggestionIO_01'] : Null,
            'deform_ideasuggestionIO_02' => ($recArr['deform_ideasuggestionIO_02']) ? $recArr['deform_ideasuggestionIO_02'] : Null,
            'deform_ideasuggestionIO_03' => ($recArr['deform_ideasuggestionIO_03']) ? $recArr['deform_ideasuggestionIO_03'] : Null,
            'deform_ideasuggestionIO_04' => ($recArr['deform_ideasuggestionIO_04']) ? $recArr['deform_ideasuggestionIO_04'] : Null,
            'current_status' => "1",
            'session_appr_year' => "2020",
            'entry_by' => $this->session->userdata('loginid')
        );
        return $insertArr;
    }

    private function formrequestupdatebyio($recArr) {
        $updateArr = array('user_id' => $recArr['user_id'],
            'deform_jobkno_techfunc_ratingIO' => ($recArr['deform_jobkno_techfunc_ratingIO']) ? $recArr['deform_jobkno_techfunc_ratingIO'] : Null,
            'deform_persnskill_communskill_ratingIO' => ($recArr['deform_persnskill_communskill_ratingIO']) ? $recArr['deform_persnskill_communskill_ratingIO'] : Null,
            'deform_persnskill_qualityconc_ratingIO' => ($recArr['deform_persnskill_qualityconc_ratingIO']) ? $recArr['deform_persnskill_qualityconc_ratingIO'] : Null,
            'deform_persnskill_selfmanagement_ratingIO' => ($recArr['deform_persnskill_selfmanagement_ratingIO']) ? $recArr['deform_persnskill_selfmanagement_ratingIO'] : Null,
            'deform_persnskill_commercialknowledge_ratingIO' => ($recArr['deform_persnskill_commercialknowledge_ratingIO']) ? $recArr['deform_persnskill_commercialknowledge_ratingIO'] : Null,
            'deform_persnskill_problemsolv_ratingIO' => ($recArr['deform_persnskill_problemsolv_ratingIO']) ? $recArr['deform_persnskill_problemsolv_ratingIO'] : Null,
            'deform_persnskill_interpersskills_ratingIO' => ($recArr['deform_persnskill_interpersskills_ratingIO']) ? $recArr['deform_persnskill_interpersskills_ratingIO'] : Null,
            'deform_persnskill_resourcemobilization_ratingIO' => ($recArr['deform_persnskill_resourcemobilization_ratingIO']) ? $recArr['deform_persnskill_resourcemobilization_ratingIO'] : Null,
            'deform_persnskill_processorient_ratingIO' => ($recArr['deform_persnskill_processorient_ratingIO']) ? $recArr['deform_persnskill_processorient_ratingIO'] : Null,
            'deform_attitude_initiativeproact_ratingIO' => ($recArr['deform_attitude_initiativeproact_ratingIO']) ? $recArr['deform_attitude_initiativeproact_ratingIO'] : Null,
            'deform_attitude_costconcious_ratingIO' => ($recArr['deform_attitude_costconcious_ratingIO']) ? $recArr['deform_attitude_costconcious_ratingIO'] : Null,
            'deform_attitude_integrity_ratingIO' => ($recArr['deform_attitude_integrity_ratingIO']) ? $recArr['deform_attitude_integrity_ratingIO'] : Null,
            'deform_describe_highlight_significant_achievementIO' => ($recArr['deform_describe_highlight_significant_achievementIO']) ? $recArr['deform_describe_highlight_significant_achievementIO'] : Null,
            'deform_describe_constraints_limitations_facedIO' => ($recArr['deform_describe_constraints_limitations_facedIO']) ? $recArr['deform_describe_constraints_limitations_facedIO'] : Null,
            'deform_technical_trainingIO_01' => ($recArr['deform_technical_trainingIO_01']) ? $recArr['deform_technical_trainingIO_01'] : Null,
            'deform_technical_trainingIO_02' => ($recArr['deform_technical_trainingIO_02']) ? $recArr['deform_technical_trainingIO_02'] : Null,
            'deform_technical_trainingIO_03' => ($recArr['deform_technical_trainingIO_03']) ? $recArr['deform_technical_trainingIO_03'] : Null,
            'deform_technical_trainingIO_04' => ($recArr['deform_technical_trainingIO_04']) ? $recArr['deform_technical_trainingIO_04'] : Null,
            'deform_technical_trainingIO_05' => ($recArr['deform_technical_trainingIO_05']) ? $recArr['deform_technical_trainingIO_05'] : Null,
            'deform_behaviour_trainingIO_01' => ($recArr['deform_behaviour_trainingIO_01']) ? $recArr['deform_behaviour_trainingIO_01'] : Null,
            'deform_behaviour_trainingIO_02' => ($recArr['deform_behaviour_trainingIO_02']) ? $recArr['deform_behaviour_trainingIO_02'] : Null,
            'deform_behaviour_trainingIO_03' => ($recArr['deform_behaviour_trainingIO_03']) ? $recArr['deform_behaviour_trainingIO_03'] : Null,
            'deform_behaviour_trainingIO_04' => ($recArr['deform_behaviour_trainingIO_04']) ? $recArr['deform_behaviour_trainingIO_04'] : Null,
            'deform_behaviour_trainingIO_05' => ($recArr['deform_behaviour_trainingIO_05']) ? $recArr['deform_behaviour_trainingIO_05'] : Null,
            'deform_ideasuggestionIO_01' => ($recArr['deform_ideasuggestionIO_01']) ? $recArr['deform_ideasuggestionIO_01'] : Null,
            'deform_ideasuggestionIO_02' => ($recArr['deform_ideasuggestionIO_02']) ? $recArr['deform_ideasuggestionIO_02'] : Null,
            'deform_ideasuggestionIO_03' => ($recArr['deform_ideasuggestionIO_03']) ? $recArr['deform_ideasuggestionIO_03'] : Null,
            'deform_ideasuggestionIO_04' => ($recArr['deform_ideasuggestionIO_04']) ? $recArr['deform_ideasuggestionIO_04'] : Null,
        );
        return $updateArr;
    }

    private function formrequestupdatebyro_de($recArr) {
        $updateArr = array('user_id' => $recArr['user_id'],
            'deform_jobkno_techfunc_ratingRO' => ($recArr['deform_jobkno_techfunc_ratingRO']) ?: Null,
            'deform_persnskill_communskill_ratingRO' => ($recArr['deform_persnskill_communskill_ratingRO']) ?: Null,
            // 'deform_persnskill_qualityconc_ratingIO' => ($recArr['deform_persnskill_qualityconc_ratingIO']) ?: Null,
            'deform_persnskill_qualityconc_ratingRO' => ($recArr['deform_persnskill_qualityconc_ratingRO']) ?: Null,
            // 'deform_persnskill_selfmanagement_ratingIO' => ($recArr['deform_persnskill_selfmanagement_ratingIO']) ?: Null,
            'deform_persnskill_selfmanagement_ratingRO' => ($recArr['deform_persnskill_selfmanagement_ratingRO']) ?: Null,
            // 'deform_persnskill_commercialknowledge_ratingIO' => ($recArr['deform_persnskill_commercialknowledge_ratingIO']) ?: Null,
            'deform_persnskill_commercialknowledge_ratingRO' => ($recArr['deform_persnskill_commercialknowledge_ratingRO']) ?: Null,
            //'deform_persnskill_problemsolv_ratingIO' => ($recArr['deform_persnskill_problemsolv_ratingIO']) ?: Null,
            'deform_persnskill_problemsolv_ratingRO' => ($recArr['deform_persnskill_problemsolv_ratingRO']) ?: Null,
            //'deform_persnskill_interpersskills_ratingIO' => ($recArr['deform_persnskill_interpersskills_ratingIO']) ?: Null,
            'deform_persnskill_interpersskills_ratingRO' => ($recArr['deform_persnskill_interpersskills_ratingRO']) ?: Null,
            // 'deform_persnskill_resourcemobilization_ratingIO' => ($recArr['deform_persnskill_resourcemobilization_ratingIO']) ?: Null,
            'deform_persnskill_resourcemobilization_ratingRO' => ($recArr['deform_persnskill_resourcemobilization_ratingRO']) ?: Null,
            // 'deform_persnskill_processorient_ratingIO' => ($recArr['deform_persnskill_processorient_ratingIO']) ?: Null,
            'deform_persnskill_processorient_ratingRO' => ($recArr['deform_persnskill_processorient_ratingRO']) ?: Null,
            // 'deform_attitude_initiativeproact_ratingIO' => ($recArr['deform_attitude_initiativeproact_ratingIO']) ?: Null,
            'deform_attitude_initiativeproact_ratingRO' => ($recArr['deform_attitude_initiativeproact_ratingRO']) ?: Null,
            //'deform_attitude_costconcious_ratingIO' => ($recArr['deform_attitude_costconcious_ratingIO']) ?: Null,
            'deform_attitude_costconcious_ratingRO' => ($recArr['deform_attitude_costconcious_ratingRO']) ?: Null,
            // 'deform_attitude_integrity_ratingIO' => ($recArr['deform_attitude_integrity_ratingIO']) ?: Null,
            'deform_attitude_integrity_ratingRO' => ($recArr['deform_attitude_integrity_ratingRO']) ?: Null,
            // 'deform_describe_highlight_significant_achievementIO' => ($recArr['deform_describe_highlight_significant_achievementIO']) ?: Null,
            'deform_describe_highlight_significant_achievementRO' => ($recArr['deform_describe_highlight_significant_achievementRO']) ?: Null,
            // 'deform_describe_constraints_limitations_facedIO' => ($recArr['deform_describe_constraints_limitations_facedIO']) ?: Null,
            'deform_describe_constraints_limitations_facedRO' => ($recArr['deform_describe_constraints_limitations_facedRO']) ?: Null,
        );
        return $updateArr;
    }

    private function formrequestinsertbyboth_de($recArr) {
        $insertArr = array('user_id' => $recArr['user_id'],
            'rep_io_id' => $recArr['io_userid'],
            'rep_ro_id' => $recArr['ro_userid'],
            'deform_jobkno_techfunc_ratingIO' => ($recArr['deform_jobkno_techfunc_ratingIO']) ? $recArr['deform_jobkno_techfunc_ratingIO'] : Null,
            'deform_jobkno_techfunc_ratingRO' => ($recArr['deform_jobkno_techfunc_ratingRO']) ? $recArr['deform_jobkno_techfunc_ratingRO'] : Null,
            'deform_persnskill_communskill_ratingIO' => ($recArr['deform_persnskill_communskill_ratingIO']) ? $recArr['deform_persnskill_communskill_ratingIO'] : Null,
            'deform_persnskill_communskill_ratingRO' => ($recArr['deform_persnskill_communskill_ratingRO']) ? $recArr['deform_persnskill_communskill_ratingRO'] : Null,
            'deform_persnskill_qualityconc_ratingIO' => ($recArr['deform_persnskill_qualityconc_ratingIO']) ? $recArr['deform_persnskill_qualityconc_ratingIO'] : Null,
            'deform_persnskill_qualityconc_ratingRO' => ($recArr['deform_persnskill_qualityconc_ratingRO']) ? $recArr['deform_persnskill_qualityconc_ratingRO'] : Null,
            'deform_persnskill_selfmanagement_ratingIO' => ($recArr['deform_persnskill_selfmanagement_ratingIO']) ? $recArr['deform_persnskill_selfmanagement_ratingIO'] : Null,
            'deform_persnskill_selfmanagement_ratingRO' => ($recArr['deform_persnskill_selfmanagement_ratingRO']) ? $recArr['deform_persnskill_selfmanagement_ratingRO'] : Null,
            'deform_persnskill_commercialknowledge_ratingIO' => ($recArr['deform_persnskill_commercialknowledge_ratingIO']) ? $recArr['deform_persnskill_commercialknowledge_ratingIO'] : Null,
            'deform_persnskill_commercialknowledge_ratingRO' => ($recArr['deform_persnskill_commercialknowledge_ratingRO']) ? $recArr['deform_persnskill_commercialknowledge_ratingRO'] : Null,
            'deform_persnskill_problemsolv_ratingIO' => ($recArr['deform_persnskill_problemsolv_ratingIO']) ? $recArr['deform_persnskill_problemsolv_ratingIO'] : Null,
            'deform_persnskill_problemsolv_ratingRO' => ($recArr['deform_persnskill_problemsolv_ratingRO']) ? $recArr['deform_persnskill_problemsolv_ratingRO'] : Null,
            'deform_persnskill_interpersskills_ratingIO' => ($recArr['deform_persnskill_interpersskills_ratingIO']) ? $recArr['deform_persnskill_interpersskills_ratingIO'] : Null,
            'deform_persnskill_interpersskills_ratingRO' => ($recArr['deform_persnskill_interpersskills_ratingRO']) ? $recArr['deform_persnskill_interpersskills_ratingRO'] : Null,
            'deform_persnskill_resourcemobilization_ratingIO' => ($recArr['deform_persnskill_resourcemobilization_ratingIO']) ? $recArr['deform_persnskill_resourcemobilization_ratingIO'] : Null,
            'deform_persnskill_resourcemobilization_ratingRO' => ($recArr['deform_persnskill_resourcemobilization_ratingRO']) ? $recArr['deform_persnskill_resourcemobilization_ratingRO'] : Null,
            'deform_persnskill_processorient_ratingIO' => ($recArr['deform_persnskill_processorient_ratingIO']) ? $recArr['deform_persnskill_processorient_ratingIO'] : Null,
            'deform_persnskill_processorient_ratingRO' => ($recArr['deform_persnskill_processorient_ratingRO']) ? $recArr['deform_persnskill_processorient_ratingRO'] : Null,
            'deform_attitude_initiativeproact_ratingIO' => ($recArr['deform_attitude_initiativeproact_ratingIO']) ? $recArr['deform_attitude_initiativeproact_ratingIO'] : Null,
            'deform_attitude_initiativeproact_ratingRO' => ($recArr['deform_attitude_initiativeproact_ratingRO']) ? $recArr['deform_attitude_initiativeproact_ratingRO'] : Null,
            'deform_attitude_costconcious_ratingIO' => ($recArr['deform_attitude_costconcious_ratingIO']) ? $recArr['deform_attitude_costconcious_ratingIO'] : Null,
            'deform_attitude_costconcious_ratingRO' => ($recArr['deform_attitude_costconcious_ratingRO']) ? $recArr['deform_attitude_costconcious_ratingRO'] : Null,
            'deform_attitude_integrity_ratingIO' => ($recArr['deform_attitude_integrity_ratingIO']) ? $recArr['deform_attitude_integrity_ratingIO'] : Null,
            'deform_attitude_integrity_ratingRO' => ($recArr['deform_attitude_integrity_ratingRO']) ? $recArr['deform_attitude_integrity_ratingRO'] : Null,
            'deform_describe_highlight_significant_achievementIO' => ($recArr['deform_describe_highlight_significant_achievementIO']) ? $recArr['deform_describe_highlight_significant_achievementIO'] : Null,
            'deform_describe_highlight_significant_achievementRO' => ($recArr['deform_describe_highlight_significant_achievementRO']) ? $recArr['deform_describe_highlight_significant_achievementRO'] : Null,
            'deform_describe_constraints_limitations_facedIO' => ($recArr['deform_describe_constraints_limitations_facedIO']) ? $recArr['deform_describe_constraints_limitations_facedIO'] : Null,
            'deform_describe_constraints_limitations_facedRO' => ($recArr['deform_describe_constraints_limitations_facedRO']) ? $recArr['deform_describe_constraints_limitations_facedRO'] : Null,
            'deform_technical_trainingIO_01' => ($recArr['deform_technical_trainingIO_01']) ? $recArr['deform_technical_trainingIO_01'] : Null,
            'deform_technical_trainingIO_02' => ($recArr['deform_technical_trainingIO_02']) ? $recArr['deform_technical_trainingIO_02'] : Null,
            'deform_technical_trainingIO_03' => ($recArr['deform_technical_trainingIO_03']) ? $recArr['deform_technical_trainingIO_03'] : Null,
            'deform_technical_trainingIO_04' => ($recArr['deform_technical_trainingIO_04']) ? $recArr['deform_technical_trainingIO_04'] : Null,
            'deform_technical_trainingIO_05' => ($recArr['deform_technical_trainingIO_05']) ? $recArr['deform_technical_trainingIO_05'] : Null,
            'deform_behaviour_trainingIO_01' => ($recArr['deform_behaviour_trainingIO_01']) ? $recArr['deform_behaviour_trainingIO_01'] : Null,
            'deform_behaviour_trainingIO_02' => ($recArr['deform_behaviour_trainingIO_02']) ? $recArr['deform_behaviour_trainingIO_02'] : Null,
            'deform_behaviour_trainingIO_03' => ($recArr['deform_behaviour_trainingIO_03']) ? $recArr['deform_behaviour_trainingIO_03'] : Null,
            'deform_behaviour_trainingIO_04' => ($recArr['deform_behaviour_trainingIO_04']) ? $recArr['deform_behaviour_trainingIO_04'] : Null,
            'deform_behaviour_trainingIO_05' => ($recArr['deform_behaviour_trainingIO_05']) ? $recArr['deform_behaviour_trainingIO_05'] : Null,
            'deform_ideasuggestionIO_01' => ($recArr['deform_ideasuggestionIO_01']) ? $recArr['deform_ideasuggestionIO_01'] : Null,
            'deform_ideasuggestionIO_02' => ($recArr['deform_ideasuggestionIO_02']) ? $recArr['deform_ideasuggestionIO_02'] : Null,
            'deform_ideasuggestionIO_03' => ($recArr['deform_ideasuggestionIO_03']) ? $recArr['deform_ideasuggestionIO_03'] : Null,
            'deform_ideasuggestionIO_04' => ($recArr['deform_ideasuggestionIO_04']) ? $recArr['deform_ideasuggestionIO_04'] : Null,
            'current_status' => "1",
            'session_appr_year' => "2020",
            'entry_by' => $this->session->userdata('loginid')
        );
        return $insertArr;
    }

    private function formrequestupdatebyboth_de($recArr) {
        $updateArr = array('user_id' => $recArr['user_id'],
            'deform_jobkno_techfunc_ratingIO' => ($recArr['deform_jobkno_techfunc_ratingIO']) ?: Null,
            'deform_jobkno_techfunc_ratingRO' => ($recArr['deform_jobkno_techfunc_ratingRO']) ?: Null,
            'deform_persnskill_communskill_ratingIO' => ($recArr['deform_persnskill_communskill_ratingIO']) ?: Null,
            'deform_persnskill_communskill_ratingRO' => ($recArr['deform_persnskill_communskill_ratingRO']) ?: Null,
            'deform_persnskill_qualityconc_ratingIO' => ($recArr['deform_persnskill_qualityconc_ratingIO']) ?: Null,
            'deform_persnskill_qualityconc_ratingRO' => ($recArr['deform_persnskill_qualityconc_ratingRO']) ?: Null,
            'deform_persnskill_selfmanagement_ratingIO' => ($recArr['deform_persnskill_selfmanagement_ratingIO']) ?: Null,
            'deform_persnskill_selfmanagement_ratingRO' => ($recArr['deform_persnskill_selfmanagement_ratingRO']) ?: Null,
            'deform_persnskill_commercialknowledge_ratingIO' => ($recArr['deform_persnskill_commercialknowledge_ratingIO']) ?: Null,
            'deform_persnskill_commercialknowledge_ratingRO' => ($recArr['deform_persnskill_commercialknowledge_ratingRO']) ?: Null,
            'deform_persnskill_problemsolv_ratingIO' => ($recArr['deform_persnskill_problemsolv_ratingIO']) ?: Null,
            'deform_persnskill_problemsolv_ratingRO' => ($recArr['deform_persnskill_problemsolv_ratingRO']) ?: Null,
            'deform_persnskill_interpersskills_ratingIO' => ($recArr['deform_persnskill_interpersskills_ratingIO']) ?: Null,
            'deform_persnskill_interpersskills_ratingRO' => ($recArr['deform_persnskill_interpersskills_ratingRO']) ?: Null,
            'deform_persnskill_resourcemobilization_ratingIO' => ($recArr['deform_persnskill_resourcemobilization_ratingIO']) ?: Null,
            'deform_persnskill_resourcemobilization_ratingRO' => ($recArr['deform_persnskill_resourcemobilization_ratingRO']) ?: Null,
            'deform_persnskill_processorient_ratingIO' => ($recArr['deform_persnskill_processorient_ratingIO']) ?: Null,
            'deform_persnskill_processorient_ratingRO' => ($recArr['deform_persnskill_processorient_ratingRO']) ?: Null,
            'deform_attitude_initiativeproact_ratingIO' => ($recArr['deform_attitude_initiativeproact_ratingIO']) ?: Null,
            'deform_attitude_initiativeproact_ratingRO' => ($recArr['deform_attitude_initiativeproact_ratingRO']) ?: Null,
            'deform_attitude_costconcious_ratingIO' => ($recArr['deform_attitude_costconcious_ratingIO']) ?: Null,
            'deform_attitude_costconcious_ratingRO' => ($recArr['deform_attitude_costconcious_ratingRO']) ?: Null,
            'deform_attitude_integrity_ratingIO' => ($recArr['deform_attitude_integrity_ratingIO']) ?: Null,
            'deform_attitude_integrity_ratingRO' => ($recArr['deform_attitude_integrity_ratingRO']) ?: Null,
            'deform_describe_highlight_significant_achievementIO' => ($recArr['deform_describe_highlight_significant_achievementIO']) ?: Null,
            'deform_describe_highlight_significant_achievementRO' => ($recArr['deform_describe_highlight_significant_achievementRO']) ?: Null,
            'deform_describe_constraints_limitations_facedIO' => ($recArr['deform_describe_constraints_limitations_facedIO']) ?: Null,
            'deform_describe_constraints_limitations_facedRO' => ($recArr['deform_describe_constraints_limitations_facedRO']) ?: Null,
            'deform_technical_trainingIO_01' => ($recArr['deform_technical_trainingIO_01']) ? $recArr['deform_technical_trainingIO_01'] : Null,
            'deform_technical_trainingIO_02' => ($recArr['deform_technical_trainingIO_02']) ? $recArr['deform_technical_trainingIO_02'] : Null,
            'deform_technical_trainingIO_03' => ($recArr['deform_technical_trainingIO_03']) ? $recArr['deform_technical_trainingIO_03'] : Null,
            'deform_technical_trainingIO_04' => ($recArr['deform_technical_trainingIO_04']) ? $recArr['deform_technical_trainingIO_04'] : Null,
            'deform_technical_trainingIO_05' => ($recArr['deform_technical_trainingIO_05']) ? $recArr['deform_technical_trainingIO_05'] : Null,
            'deform_behaviour_trainingIO_01' => ($recArr['deform_behaviour_trainingIO_01']) ? $recArr['deform_behaviour_trainingIO_01'] : Null,
            'deform_behaviour_trainingIO_02' => ($recArr['deform_behaviour_trainingIO_02']) ? $recArr['deform_behaviour_trainingIO_02'] : Null,
            'deform_behaviour_trainingIO_03' => ($recArr['deform_behaviour_trainingIO_03']) ? $recArr['deform_behaviour_trainingIO_03'] : Null,
            'deform_behaviour_trainingIO_04' => ($recArr['deform_behaviour_trainingIO_04']) ? $recArr['deform_behaviour_trainingIO_04'] : Null,
            'deform_behaviour_trainingIO_05' => ($recArr['deform_behaviour_trainingIO_05']) ? $recArr['deform_behaviour_trainingIO_05'] : Null,
            'deform_ideasuggestionIO_01' => ($recArr['deform_ideasuggestionIO_01']) ? $recArr['deform_ideasuggestionIO_01'] : Null,
            'deform_ideasuggestionIO_02' => ($recArr['deform_ideasuggestionIO_02']) ? $recArr['deform_ideasuggestionIO_02'] : Null,
            'deform_ideasuggestionIO_03' => ($recArr['deform_ideasuggestionIO_03']) ? $recArr['deform_ideasuggestionIO_03'] : Null,
            'deform_ideasuggestionIO_04' => ($recArr['deform_ideasuggestionIO_04']) ? $recArr['deform_ideasuggestionIO_04'] : Null,
        );
        return $updateArr;
    }

    public function GetSingleRecByUserBCID($emplID) {
        $this->db->select('apr.*');
        $this->db->from("appr_proj_formdata_bc as apr");
        $this->db->where(array("apr.status" => "1", "apr.session_appr_year" => "2020", "apr.user_id" => $emplID));
        $ApprRecArr = $this->db->get()->row();
        return ($ApprRecArr) ? $ApprRecArr : null;
    }

    public function GetSingleRecByUserID($emplID) {
        $this->db->select('apr.*');
        $this->db->from("appr_proj_formdata_de as apr");
        $this->db->where(array("apr.status" => "1", "apr.session_appr_year" => "2020", "apr.user_id" => $emplID));
        $ApprRecArr = $this->db->get()->row();
        return ($ApprRecArr) ? $ApprRecArr : null;
    }

    //Get Other Exp..
    public function GetOtherExpr($empid) {
        $getmonth = 0;
        $otherExpArr = $this->mastermodel->GetTableData('main_empexperiancedetails', array('user_id' => $empid, 'isactive' => '1'));
        if ($otherExpArr) {
            foreach ($otherExpArr as $recRw) {
                $getmonth += getDateDiffMonth($recRw->from_date, $recRw->to_date);
            }
            $expRecArr = ($getmonth > 0) ? number_format(($getmonth / 12), 2) : 0;
        } else {
            $expRecArr = '0';
        }
        return $expRecArr;
    }

    //Get Total Exp in CEG...
    public function GetCegExpr($empid, $dojEmp) {
        $todayDate = date("Y-m-d");
        $getmonth = 0;
        if (($empid) and ( $dojEmp) and ( $todayDate)) {
            $getmonth = getDateDiffMonth($dojEmp, $todayDate);
        }
        $expRecArr = ($getmonth > 0) ? number_format(($getmonth / 12), 2) : 0;
        return ($expRecArr) ? $expRecArr : "0";
    }

    //Lock After Fill Appraisal... BC Form
    public function lockprojappra_bc() {
        $Userid = $_REQUEST['id'];
        $Loginiden = $_REQUEST['Loginiden'];
        //Get Rec By ID..
        $SaverecData = $this->GetSingleRecByUserBCID($Userid);
        if ($Userid and $Loginiden and $Loginiden == "BOTH") {
            //Check Validation on Lock...
            if (($SaverecData->bcform_jobkno_techfunc_ratingIO < 1) or ( $SaverecData->bcform_jobkno_techfunc_ratingRO < 1) or ( $SaverecData->bcform_person_communskill_ratingIO < 1) or ( $SaverecData->bcform_person_communskill_ratingRO < 1) or ( $SaverecData->bcform_person_teammanageinterpskill_ratingIO < 1) or ( $SaverecData->bcform_person_teammanageinterpskill_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_delegationskill_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_delegationskill_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_probsolv_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_probsolv_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_planningmonit_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_planningmonit_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_negotiation_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_negotiation_ratingRO < 1) or ( $SaverecData->bcform_leadership_conflictmanag_ratingIO < 1) or ( $SaverecData->bcform_leadership_conflictmanag_ratingRO < 1) or ( $SaverecData->bcform_leadership_strategic_ratingIO < 1) or ( $SaverecData->bcform_leadership_strategic_ratingRO < 1) or ( $SaverecData->bcform_leadership_customerorient_ratingIO < 1) or ( $SaverecData->bcform_leadership_customerorient_ratingRO < 1) or ( $SaverecData->bcform_leadership_trainingdevteambuild_ratingIO < 1) or ( $SaverecData->bcform_leadership_trainingdevteambuild_ratingRO < 1) or ( $SaverecData->bcform_attitude_initiativproactiv_ratingIO < 1) or ( $SaverecData->bcform_attitude_initiativproactiv_ratingRO < 1) or ( $SaverecData->bcform_attitude_resultorient_ratingIO < 1) or ( $SaverecData->bcform_attitude_resultorient_ratingRO < 1) or ( $SaverecData->bcform_attitude_openideas_ratingIO < 1) or ( $SaverecData->bcform_attitude_openideas_ratingRO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($SaverecData->bcform_technical_trainingIO_01 == "") or ( $SaverecData->bcform_behaviour_trainingIO_01 == "") or ( $SaverecData->bcform_describe_highlight_significant_achievementIO == "") or ( $SaverecData->bcform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            $UpdateArr = array("current_status" => "4");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_bc", $UpdateArr);
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }

        if ($Userid and $Loginiden and $Loginiden == "RO") {
            if (($SaverecData->bcform_jobkno_techfunc_ratingRO < 1) or ( $SaverecData->bcform_person_communskill_ratingRO < 1) or ( $SaverecData->bcform_person_teammanageinterpskill_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_delegationskill_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_probsolv_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_planningmonit_ratingRO < 1) or ( $SaverecData->bcform_managerialskill_negotiation_ratingRO < 1) or ( $SaverecData->bcform_leadership_conflictmanag_ratingRO < 1) or ( $SaverecData->bcform_leadership_strategic_ratingRO < 1) or ( $SaverecData->bcform_leadership_customerorient_ratingRO < 1) or ( $SaverecData->bcform_leadership_trainingdevteambuild_ratingRO < 1) or ( $SaverecData->bcform_attitude_initiativproactiv_ratingRO < 1) or ( $SaverecData->bcform_attitude_resultorient_ratingRO < 1) or ( $SaverecData->bcform_attitude_openideas_ratingRO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($SaverecData->bcform_technical_trainingIO_01 == "") or ( $SaverecData->bcform_behaviour_trainingIO_01 == "")or ( $SaverecData->bcform_describe_highlight_significant_achievementIO == "") or ( $SaverecData->bcform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            $UpdateArr = array("current_status" => "4");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_bc", $UpdateArr);
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }

        if ($Userid and $Loginiden and $Loginiden == "IO") {
            if (($SaverecData->bcform_jobkno_techfunc_ratingIO < 1) or ( $SaverecData->bcform_person_communskill_ratingIO < 1) or ( $SaverecData->bcform_person_teammanageinterpskill_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_delegationskill_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_probsolv_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_planningmonit_ratingIO < 1) or ( $SaverecData->bcform_managerialskill_negotiation_ratingIO < 1) or ( $SaverecData->bcform_leadership_conflictmanag_ratingIO < 1) or ( $SaverecData->bcform_leadership_strategic_ratingIO < 1) or ( $SaverecData->bcform_leadership_customerorient_ratingIO < 1) or ( $SaverecData->bcform_leadership_trainingdevteambuild_ratingIO < 1) or ( $SaverecData->bcform_attitude_initiativproactiv_ratingIO < 1) or ( $SaverecData->bcform_attitude_resultorient_ratingIO < 1) or ( $SaverecData->bcform_attitude_openideas_ratingIO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($SaverecData->bcform_technical_trainingIO_01 == "") or ( $SaverecData->bcform_behaviour_trainingIO_01 == "")or ( $SaverecData->bcform_describe_highlight_significant_achievementIO == "") or ( $SaverecData->bcform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }

            $UpdateArr = array("current_status" => "2");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_bc", $UpdateArr);

            //Send Mail To RO Code By.. Ash..
            $roSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $SaverecData->rep_ro_id, "isactive" => "1"));
            $ioSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $SaverecData->rep_io_id, "isactive" => "1"));
            $userSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $SaverecData->user_id, "isactive" => "1"));
            $data['prefix_name_ro'] = $roSingleRec[0]->prefix_name;
            $data['userfullname_ro'] = $roSingleRec[0]->userfullname;
            $data['prefix_name_io'] = $ioSingleRec[0]->prefix_name;
            $data['userfullname_io'] = $ioSingleRec[0]->userfullname;
            $data['prefix_name'] = $userSingleRec[0]->prefix_name;
            $data['userfullname'] = $userSingleRec[0]->userfullname;
            $data['error'] = '';
            $to = $roSingleRec[0]->emailaddress;
            $subject = "Team Leader submitted the Performance Appraisal";
            $msgDetails = $this->load->view('email/appr_formfill_mailto_ro', $data, true);
            $mailresp = $this->sendMail($to, $subject, $msgDetails);
            //Close Send To RO Code..
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }
        redirect(base_url("single_emp_projappr/" . $Userid));
    }

    //Lock After Fill Appraisal... DE Form
    public function lockprojappra_de() {
        $Userid = $_REQUEST['id'];
        $Loginiden = $_REQUEST['Loginiden'];

        $appraiRecArr = $this->GetSingleRecByUserID($Userid);

        if ($Userid and $Loginiden and $Loginiden == "BOTH") {
            if (($appraiRecArr->deform_jobkno_techfunc_ratingIO < 1) or ( $appraiRecArr->deform_jobkno_techfunc_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_communskill_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_communskill_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_qualityconc_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_qualityconc_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_selfmanagement_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_selfmanagement_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_commercialknowledge_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_commercialknowledge_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_problemsolv_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_problemsolv_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_interpersskills_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_interpersskills_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_resourcemobilization_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_resourcemobilization_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_processorient_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_processorient_ratingRO < 1) or ( $appraiRecArr->deform_attitude_initiativeproact_ratingIO < 1) or ( $appraiRecArr->deform_attitude_initiativeproact_ratingRO < 1) or ( $appraiRecArr->deform_attitude_costconcious_ratingIO < 1) or ( $appraiRecArr->deform_attitude_costconcious_ratingRO < 1) or ( $appraiRecArr->deform_attitude_integrity_ratingIO < 1) or ( $appraiRecArr->deform_attitude_integrity_ratingRO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($appraiRecArr->deform_technical_trainingIO_01 == "") or ( $appraiRecArr->deform_behaviour_trainingIO_01 == "") or ( $appraiRecArr->deform_describe_highlight_significant_achievementIO == "") or ( $appraiRecArr->deform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            $UpdateArr = array("current_status" => "4");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_de", $UpdateArr);
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }

        if ($Userid and $Loginiden and $Loginiden == "RO") {
            if (($appraiRecArr->deform_jobkno_techfunc_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_communskill_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_qualityconc_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_selfmanagement_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_commercialknowledge_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_problemsolv_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_interpersskills_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_resourcemobilization_ratingRO < 1) or ( $appraiRecArr->deform_persnskill_processorient_ratingRO < 1) or ( $appraiRecArr->deform_attitude_initiativeproact_ratingRO < 1) or ( $appraiRecArr->deform_attitude_costconcious_ratingRO < 1) or ( $appraiRecArr->deform_attitude_integrity_ratingRO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($appraiRecArr->deform_technical_trainingIO_01 == "") or ( $appraiRecArr->deform_behaviour_trainingIO_01 == "") or ( $appraiRecArr->deform_describe_highlight_significant_achievementIO == "") or ( $appraiRecArr->deform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            $UpdateArr = array("current_status" => "4");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_de", $UpdateArr);
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }

        if ($Userid and $Loginiden and $Loginiden == "IO") {
            if (($appraiRecArr->deform_jobkno_techfunc_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_communskill_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_qualityconc_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_selfmanagement_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_commercialknowledge_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_problemsolv_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_interpersskills_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_resourcemobilization_ratingIO < 1) or ( $appraiRecArr->deform_persnskill_processorient_ratingIO < 1) or ( $appraiRecArr->deform_attitude_initiativeproact_ratingIO < 1) or ( $appraiRecArr->deform_attitude_costconcious_ratingIO < 1) or ( $appraiRecArr->deform_attitude_integrity_ratingIO < 1)) {
                $this->session->set_flashdata('errormsg', 'All Rating Fields are required, Please fill all mandatory fields. ');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            if (($appraiRecArr->deform_technical_trainingIO_01 == "") or ( $appraiRecArr->deform_behaviour_trainingIO_01 == "") or ( $appraiRecArr->deform_describe_highlight_significant_achievementIO == "") or ( $appraiRecArr->deform_describe_constraints_limitations_facedIO == "")) {
                $this->session->set_flashdata('errormsg', 'Technical/Behavioral Training is required.');
                redirect(base_url("single_emp_projappr/" . $Userid));
            }
            $UpdateArr = array("current_status" => "2");
            $this->db->where(['user_id' => $Userid, 'status' => '1', 'session_appr_year' => '2020']);
            $this->db->update("appr_proj_formdata_de", $UpdateArr);

            //Send Mail To RO Code By.. Ash..
            $roSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $appraiRecArr->rep_ro_id, "isactive" => "1"));
            $ioSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $appraiRecArr->rep_io_id, "isactive" => "1"));
            $userSingleRec = $this->mastermodel->GetTableData("main_employees_summary", array("user_id" => $appraiRecArr->user_id, "isactive" => "1"));

            $data['prefix_name_ro'] = $roSingleRec[0]->prefix_name;
            $data['userfullname_ro'] = $roSingleRec[0]->userfullname;
            $data['prefix_name_io'] = $ioSingleRec[0]->prefix_name;
            $data['userfullname_io'] = $ioSingleRec[0]->userfullname;
            $data['prefix_name'] = $userSingleRec[0]->prefix_name;
            $data['userfullname'] = $userSingleRec[0]->userfullname;
            $data['error'] = '';
            $to = $roSingleRec[0]->emailaddress;
            $subject = "Team Leader submitted the Performance Appraisal";
            $msgDetails = $this->load->view('email/appr_formfill_mailto_ro', $data, true);
            $mailresp = $this->sendMail($to, $subject, $msgDetails);
            //Close Send To RO Code..
            $this->session->set_flashdata('successmsg', 'Appraisal form Locked Successfully');
        }
        redirect(base_url("single_emp_projappr/" . $Userid));
    }

    //Test Mail After IO Lock..
    function sendMail($to, $subject, $msgDetails) {
        // $to = "ts.admin@cegindia.com";
        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $CI->email->from('hrms.admin@cegindia.com', 'CEG-Appraisal');
        $CI->email->to($to);
        $CI->email->bcc('ts.admin@cegindia.com');
        $CI->email->subject($subject);
        $CI->email->message($msgDetails);
        $resp = $CI->email->send();
        return ($resp) ? $resp : true;
    }

}
