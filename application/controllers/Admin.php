<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->helper('cookie');
    }

    public function index() {
        $data['error'] = '';
        if (!empty($this->session->userdata('loginid'))) {
            redirect(base_url("userdashboard"));
        }
        $this->load->view('admin/auth/login', $data);
    }

    //Login Control..
    public function loginchecker() {
        if (@$_REQUEST['flag'] == "") {
            $empid = $this->input->post('username');
            $empPass = $this->input->post('password');
        } else {
            $empid = $_REQUEST['username'];
            $empPass = $_REQUEST['password'];
        }
        if ($empid == '' or $empPass == ''):
            $this->session->set_flashdata('error_msg', 'Required Field Must be Validated');
        else:
            $LoginCredentialArr = array('username' => $empid, 'password' => $empPass);
            $user_details = $this->mastermodel->login_user_action($LoginCredentialArr);
            if ($user_details):
                $this->session->set_userdata(array('assign_role' => $user_details[0]->emprole, 'loginid' => $user_details[0]->id, 'username' => $user_details[0]->userfullname));
                redirect(base_url("userdashboard"));
            else:
                $this->session->set_flashdata('error_msg', 'Error! Your Login Credentials are Invalid. (While entering incorrect login details)');
                redirect(base_url(""));
            endif;
        endif;
    }

    //Smart Login Code By Asheesh..
    public function smartlogin() {
        if ($_REQUEST['assign_role'] and $_REQUEST['loginid'] and $_REQUEST['username'] and $_REQUEST['turn']) {
            $this->session->set_userdata(array('assign_role' => $_REQUEST['assign_role'], 'loginid' => $_REQUEST['loginid'], 'username' => $_REQUEST['username']));
            // Redirect HRMS..
            if (@$_REQUEST['turn'] == '1'):
                redirect(base_url("userdashboard"));
            endif;
            //  Redirect TS..
            if (@$_REQUEST['turn'] == '2'):
                redirect(base_url("timesheet_entry"));
            endif;
            redirect(base_url("userdashboard"));
        } else {
            redirect(base_url(""));
        }
    }
    
    

}
