<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userdashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if ($this->session->userdata('loginid') == "") {
            redirect(base_url(""));
        }
    }

    
    public function home() {
		
		$this->session->set_userdata(array('self_identifier' => '1', 'team_identifier' => '0'));
        $data['error'] = '';
        $data['title'] = 'HRMS Personal';
        $first_dateM = date('Y-m-d', strtotime('first day of this month'));
        $last_dateM = date('Y-m-d', strtotime('last day of this month'));
        $UserId = $this->session->userdata('loginid');
        $BasicDetailRec = $this->mastermodel->GetBasicRecLoginUser();
        $data['YesterdayOutTimeDetails'] = $this->mastermodel->GetYesterdayOutTime($BasicDetailRec->machine_id);
        $data['TodayInTimeDetails'] = $this->mastermodel->GetTodayInTime($BasicDetailRec->machine_id);
        $aLLHolidaysArr = $this->mastermodel->firs_third_Sat_Holidays_ListCurrentMonth();
        $RecLeaveArr = $this->mastermodel->GetAllLeaveListArr($first_dateM, $last_dateM, $UserId);
        $RecTourArr = $this->mastermodel->GetAllTourListArr($first_dateM, $last_dateM, $UserId);
        $array_1 = array_merge($aLLHolidaysArr, $RecLeaveArr, $RecTourArr);
        $AllHolidLeaveTourDateArr = array_unique($array_1);
        $data['allPunchDataArr'] = $this->mastermodel->GetAllPunchData($BasicDetailRec->machine_id, $AllHolidLeaveTourDateArr);

        $totPunchMinute = 0;
        $AllPunchCurrentMonth = $this->AllPunchInMonth();
        if ($AllPunchCurrentMonth) {
            foreach ($AllPunchCurrentMonth as $rOws) {
                $recCdd = $this->mastermodel->GetTotalWorkingHourWithPunch($rOws->punchtime, $BasicDetailRec->machine_id);
                $totPunchMinute += $recCdd;
            }
        }
        $data['totPunchMinute'] = $totPunchMinute;
        $this->load->view('userdashboard_view', $data);
    }

    public function AllPunchInMonth() {
        $BasicDetailRec = $this->mastermodel->GetBasicRecLoginUser();
        $today_dateM = date("d-m-Y");
        $mmdd = date('m-Y');
        $this->db->select("DATE_FORMAT(`LogDate`,'%d-%m-%Y') as punchtime");
        $this->db->from("devicelogs_processed as a");
        $this->db->where(["a.UserId" => $BasicDetailRec->machine_id]);
        $this->db->where("DATE_FORMAT(`LogDate`,'%m-%Y')='" . $mmdd . "'");
        $this->db->where("DATE_FORMAT(`LogDate`,'%d-%m-%Y')!='" . $today_dateM . "'");
        $this->db->group_by("DATE_FORMAT(`LogDate`,'%d-%m-%Y')");
        $punchRecDetail = $this->db->get()->result();
        return ($punchRecDetail) ? $punchRecDetail : "";
    }


    public function logout() {
        $this->session->sess_destroy();
        $this->session->set_userdata(array('loginid' => null, 'username' => null));
        redirect(base_url(""));
    }

}
