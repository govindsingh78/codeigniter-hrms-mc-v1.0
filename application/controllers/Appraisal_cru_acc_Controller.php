<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisal_cru_acc_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        // $this->load->model('Appraisal_new_model', 'appraisalmodel');
        $this->load->model('Appraisal_cru_part_model', 'Appraisalcrupartmodel');
        $this->load->model('Appraisal_acc_part_model', 'Appraisalaccpartmodel');
		 
        // $this->load->model('Edititrprofiledetail_model', 'eipm');
        // $this->load->model('Appraisal_model', 'appraisal');
        // $this->load->model('Appraisalio_model', 'appraisalio');
        // $this->load->model('Appraisalreporthr_model', 'appraisalreport');
        // $this->load->model('Appraisalreporthrcegth_model', 'appraisalreportcegth');
        // $this->load->model('Appraisalreportajax_model', 'appraisalreportajax');
        // $this->load->model('Appraisalreportajaxcegth_model', 'appraisalreportajaxcegth');

        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
		
    }
	
	//==========Coded By Gaurav==========//
	public function annual_appraisal_cru_part() {
        $id = $this->session->userdata('loginid');
		$data['title'] = "Annual Appraisal CRU";
		$this->load->view('appraisal/appraisal_cru_part_view',$data);
	}
	
	
	//Appraisal annual cru part
    public function ajax_list_appraisal_cru() {
        $list = $this->Appraisalcrupartmodel->get_datatables();
		// echo "<pre>";print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $currdate = date('d-m-Y');
        foreach ($list as $value) {
			$edulevelName = get_edulevel_name($value->highestedulevelfield);
			$pRojidArr = explode(",", $value->on_project);
			$prName = $this->GetAllProjList($pRojidArr);
			// $ProjName = get_proj_name($value->on_project);
            $years = 0;
            $months = 0;
            $diff = 0;
            $CountCegExp = 0;
            $countOtherExp = 0;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($value->userfullname) ? $value->userfullname : '';
            $row[] = ($value->position_name) ? $value->position_name : '';
            $row[] = ($value->date_of_joining) ? $value->date_of_joining : '';
            $row[] = ($prName) ? $prName : '';
            $row[] = ($edulevelName) ? $edulevelName : '';
            $row[] = ($value->institution_name) ? $value->institution_name : '';
            $row[] = ($value->from_date) ? $value->from_date : '';
            $row[] = ($value->years_exp) ? $value->years_exp : '';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Total Marks" type="text" name="total_marks_' . $value->user_id . '" id="total_marks_' . $value->user_id . '" class="form-control" value="'.$value->total_marks .'">';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Total Marks" type="text" name="marks_obt' . $value->user_id . '" id="marks_obt' . $value->user_id . '" class="form-control" value="'.$value->marks_obtn .'">';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Total Marks" type="text" name="criticality' . $value->user_id . '" id="criticality' . $value->user_id . '" class="form-control" value="'.$value->criticality .'">';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Total Marks" type="text" name="market_rate' . $value->user_id . '" id="market_rate' . $value->user_id . '" class="form-control" value="'.$value->marks_rate .'">'; 
            $row[] = '<span id="msg_' . $value->user_id . '" class="alert alert-success alert-dismissible" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success ! </strong></span><input style="width:150px;" autocomplete="off" placeholder="Enter Remarks" type="text" name="remarks_' . $value->user_id . '" id="remarks_' . $value->user_id . '" class="form-control" value="'.$value->remarks .'">';
            $row[] = '<button type="submit" onclick="update_appr_cru(' . $value->user_id . ')" class="btn btn-info">Update</button>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Appraisalcrupartmodel->count_all(),
            "recordsFiltered" => $this->Appraisalcrupartmodel->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
	
	//Total On Proj
    public function GetAllProjList($pRojidArr) {
        $this->db->select('a.project_name');
        $this->db->from("tm_projects as a");
        $this->db->where("a.is_active", "1");
        $this->db->where_in("a.id", $pRojidArr);
        $RecprojNms = $this->db->get()->result();
        $ProjectS = '';
        if (count($RecprojNms) == 1) {
            $ProjectS = $RecprojNms[0]->project_name;
        }
        if (($RecprojNms) and ( count($RecprojNms) > 1)) {
            foreach ($RecprojNms as $rEcD) {
                $ProjectS .= $rEcD->project_name . " , ";
            }
        }
        return ($ProjectS) ? $ProjectS : "";
    }
	
	public function annual_appraisal_acc_part() {
        $id = $this->session->userdata('loginid');
		// echo "tst"; die;
		$data['title'] = "Annual Appraisal ACC";
		$this->load->view('appraisal/appraisal_acc_part_view',$data);
	}
	
	//Appraisal annual cru part
    public function ajax_list_appraisal_acc() {
        $list = $this->Appraisalaccpartmodel->get_datatables();
		// echo "<pre>";print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        $currdate = date('d-m-Y');
        foreach ($list as $value) {
			
			$pRojidArr = explode(",", $value->on_project);
			$prName = $this->GetAllProjList($pRojidArr);
			
            $years = 0;
            $months = 0;
            $diff = 0;
            $CountCegExp = 0;
            $countOtherExp = 0;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ($value->userfullname) ? $value->userfullname : '';
            $row[] = ($value->position_name) ? $value->position_name : '';
            $row[] = ($prName) ? $prName : '';
            $row[] = ($value->date_of_joining) ? $value->date_of_joining : '';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Bill Rate" type="text" name="bill_rate_' . $value->user_id . '" id="bill_rate_' . $value->user_id . '" class="form-control" value="'. $value->billRate .'">';
            $row[] = '<input style="width:150px;" autocomplete="off" placeholder="Enter Orig. Bill Rate" type="text" name="orig_bill_rate_' . $value->user_id . '" id="orig_bill_rate_' . $value->user_id . '" class="form-control" value="'. $value->origBillrate .'">';
            $row[] = '<span id="msg_' . $value->user_id . '" class="alert alert-success alert-dismissible" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success ! </strong></span><input style="width:150px;" autocomplete="off" placeholder="Enter Remark" type="text" name="remark_' . $value->user_id . '" id="remark_' . $value->user_id . '" class="form-control" value="'.$value->remark .'">';
            $row[] = '<button type="submit" onclick="update_appr_acc(' . $value->user_id . ')" class="btn btn-info">Update</button>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Appraisalcrupartmodel->count_all(),
            "recordsFiltered" => $this->Appraisalcrupartmodel->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
	
	public function updateextAppacc() {
        $empid = $_REQUEST['empid'];
		$id=$this->session->userdata('loginid');
        $billRate = $_REQUEST['billrate'];
        $origBillrate = $_REQUEST['origbillrate'];
        $remark = $_REQUEST['remark'];
		// echo $billRate.", ".$origBillrate.", ".$remark; die;
        if ($billRate or $origBillrate or $remark) {
			// echo "test"; die;
			$recArr = array(
                'user_id' => $empid,
                'billRate' => $billRate,
                'origBillrate' => $origBillrate,
                'remark' => $remark,
                'modifiedby' => $this->session->userdata('loginid'),
                'modifieddate' => date('Y-m-d h:i:s')
            );
			$this->db->where('user_id',$empid);
			$rec = $this->db->get('annual_appraisal_account');
			if ( $rec->num_rows() <= 0 ):
				$respp = $this->mastermodel->InsertMasterData($recArr, 'annual_appraisal_account');
			else:
				 $respp = $this->mastermodel->UpdataRecord('annual_appraisal_account', $recArr, array('user_id' => $empid));
			endif;
            echo json_encode($respp);
        }
    }
	
	public function updateextAppCru() {
        $empid = $_REQUEST['empid'];
		// echo $empid; die;
		$id=$this->session->userdata('loginid');
        $total_marks = $_REQUEST['total_marks'];
        $marks_obtn = $_REQUEST['marks_obtn'];
        $criticality = $_REQUEST['criticality'];
        $marks_rate = $_REQUEST['marks_rate'];
        $remarks = $_REQUEST['remarks'];
		// echo $total_marks.", ".$marks_obtn.", ".$criticality.", ".$marks_rate.", ".$remarks; die;
        if ($total_marks || $marks_obtn) {
			// echo "test"; die;
			$recArr = array(
                'user_id' => $empid,
                'total_marks' => $total_marks,
                'marks_obtn' => $marks_obtn,
                'criticality' => $criticality,
                'marks_rate' => $marks_rate,
                'remarks' => $remarks,
                'modifiedby' => $this->session->userdata('loginid'),
                'modifieddate' => date('Y-m-d h:i:s')
            );
			$this->db->where('user_id',$empid);
			$rec = $this->db->get('annual_appraisal_cru');
			// $a = $rec->num_rows();
			// echo $a; die;
			if ( $rec->num_rows() <= 0 ):
				$respp = $this->mastermodel->InsertMasterData($recArr, 'annual_appraisal_cru');
			else:
				 $respp = $this->mastermodel->UpdataRecord('annual_appraisal_cru', $recArr, array('user_id' => $empid));
			endif;
      
            echo json_encode($respp);
        }
		
    }

}
