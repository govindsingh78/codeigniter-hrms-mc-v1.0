<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function mypayslip() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['title'] = "My Pay Slips";
        if (@$_REQUEST['selectedyear']) {
            $data['selyear'] = $_REQUEST['selectedyear'];
            $data['empPayslipDetailArr'] = $this->mastermodel->GetEmpPayslipsRecByuserID($data['loginUserRecd']->payroll_with_name, $data['selyear']);
        } else {
            $dateYear = date("Y");
            $data['selyear'] = date("Y");
            $data['empPayslipDetailArr'] = $this->mastermodel->GetEmpPayslipsRecByuserID($data['loginUserRecd']->payroll_with_name, $dateYear);
        }

//        echo "<pre>";
//        print_r($data);
//        die;

        $this->load->view("account-section/payslips_view", $data);
    }

    //My Salary Details..
    public function mysalarydetails() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['title'] = "My Salary Details";
        $data['empSalDetailArr'] = $this->mastermodel->GetEmpSalaryDetailRecByID($id);
        $this->load->view("account-section/mysalarydetails_view", $data);
    }

    public function updateBankDetails() {
        $id = $this->session->userdata('loginid');
       
        $bankname = $_REQUEST['bankname'];
        $accountholder_name = $_REQUEST['accountholder_name'];
        $accountnumber = $_REQUEST['accountnumber'];
        $ifsc_code = $_REQUEST['ifsc_code'];
        $micr_code = $_REQUEST['micr_code'];
        $branchname = $_REQUEST['branchname'];
        
         
        $data = array(
             
            'bankname' => $bankname,
            'accountholder_name' => $accountholder_name,
            'accountnumber' => $accountnumber,
            'ifsc_code' => $ifsc_code,
            'micr_code' => $micr_code,
            'branchname' => $branchname,
            
        );


         
                                    
                                    
                                      
                                    
                                    


        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empsalarydetails', $data);
 


        if($updateUser){
            $message = 'Bank Details Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);
        
    }


    //Form 16 / ITR..
    public function myformitr() {
        $data['error'] = '';
        $id = $this->session->userdata('loginid');
        $data['ItrFormDetailsRecArr'] = $this->mastermodel->GetItrFormDetailsRecByID($id);
        $data['title'] = "My ITR Form Details";
        $this->load->view("account-section/itrform_view", $data);
    }

    //Data Table Demo ..
    public function datatable() {
        $data['error'] = '';
        $this->load->view("demodatatable_view", $data);
    }

    
    
    //Attendance Section ...
    public function myattendance() {
        $data['error'] = '';
        $data['title'] = "My Attendance Details";
        $first_dateM = date('Y-m-d', strtotime('first day of this month'));
        $last_dateM = date('Y-m-d', strtotime('last day of this month'));
        $UserId = $this->session->userdata('loginid');

        $BasicDetailRec = $this->mastermodel->GetBasicRecLoginUser();
        $data['machine_userid'] = ($BasicDetailRec->machine_id);

        if (@$_REQUEST["attendance_month"] and @ $_REQUEST["attendance_month"]) {
            $data['atten_month'] = $_REQUEST["attendance_month"];
            $data['atten_year'] = $_REQUEST["attendance_year"];
            //From Date To Date Comb..
            $from_date = "01-" . $_REQUEST["attendance_month"] . "-" . $_REQUEST["attendance_year"];
            $to_date = date("t-m-Y", strtotime($from_date));
        } else {
            $data['atten_month'] = date("m");
            $data['atten_year'] = date("Y");
            $from_date = "01-" . date("m-Y");
            $to_date = date("t-m-Y", strtotime($from_date));
        }
        $data['dateRangeArr'] = $this->mastermodel->getDatesFromRange($from_date, $to_date);
        $data['holidaysDateArr'] = $this->mastermodel->holidayCount(date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime($to_date)));
        $data['leaveDateArr'] = $this->mastermodel->GetAllLeaveListArr(date('Y-m-d', strtotime($from_date)), date('Y-m-d', strtotime($to_date)), $UserId);
        $data['tourDateArr'] = $this->mastermodel->GetAllTourListArr($from_date, $to_date, $UserId);
        $this->load->view("account-section/attendancepunch_list_view", $data);
    }

    
    
    
    public function atest() {
        //Code For Send Attachment File on Mail..
        //Mail Lebraray
        $to = '';
        $subject = "Asheesh Test Mail";
        $msgDetails = "Hello Test 123";
        $recDD = $this->sendMail($to, $subject, $msgDetails);
        echo "<pre>";
        print_r($recDD);
        die;
    }

    //Pay Slip Attachment Send On Mail..
    public function payslip_sendonmail($payrollID) {
        $this->db->select("d.password,a.filename,a.fullpath,a.payroll_with_name,a.payrollid,b.user_id,c.prefix_name,c.userfullname,c.emailaddress,c.contactnumber");
        $this->db->from("payslip_filepath as a");
        $this->db->join("emp_otherofficial_data as b", "a.payrollid=b.payrollcode", "LEFT");
        $this->db->join("main_employees_summary as c", "b.user_id=c.user_id", "LEFT");
        $this->db->join("payslip_pass as d", "b.payroll_with_name=d.payroll_with_name", "LEFT");
        $this->db->where(["a.is_active" => "1", "a.id" => $payrollID]);
        $recData = $this->db->get()->row();
        if ($recData) {
            //Send Attachment Payslip on Mail..
            $subject = "Pay Slip Attachemnt";
            $msgDetails = "Password :- " . $recData->password;
            $attachment = str_replace(" ", "%20", $recData->filename);
            $respOns = $this->AttachmentsendMail($recData->emailaddress, $subject, $msgDetails, $attachment);
            $this->session->set_flashdata('successmsg', 'Payslip Attachment sent successfully.');
            $url = $_SERVER['HTTP_REFERER'];
            redirect($url);
        }
    }
    

    function AttachmentsendMail($to, $subject, $msgDetails, $attachment) {
        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $attachment1 = 'http://hrms.cegindia.com/public/uploads/payslipsnew/' . $attachment;
        $CI->email->from('hrms.admin@cegindia.com', 'CEG-Payslip');
        $CI->email->to($to);
        $CI->email->bcc('marketing@cegindia.com');
        $CI->email->subject($subject);
        $CI->email->message($msgDetails);
        $CI->email->attach($attachment1);
        $resp = $CI->email->send();
        return ($resp) ? $resp : $CI->email->print_debugger();
    }

}
