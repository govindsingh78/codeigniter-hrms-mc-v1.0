<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Team_fun_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Team_function_model');
        $this->load->model('Team_leave_model');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function team_under_officers() {
		$id = $this->session->userdata('loginid');
		$this->session->set_userdata(array('self_identifier' => '0', 'team_identifier' => '1'));
		// $ident = $this->session->userdata('team_identifier');
		// echo $ident; die;
        $data['myTeamDetailsRec'] = count($this->Team_function_model->GetMyTeamDetailsRecByID($id));
        // echo "<pre>"; print_r(count($data['myTeamTourDetailsRec'])); die;
		
		$myTeamDetailsRec = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($id);
		$teamArr = array();
		foreach($myTeamDetailsRec as $val):
			$teamArr[] = $val->user_id;
			$teamd  = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($val->user_id);
			foreach($teamd as $valu):
				$teamArr[] = $valu->user_id;
			endforeach;
		endforeach;
		// echo "<pre>"; print_r($teamArr); die;
		$myTeamPresentDetailsRecArr = count($this->Team_function_model->count_present_atten_report_ceg_list($teamArr));
		$myTeamLeaveDetailsRecArr = count($this->Team_function_model->leave_atten_report_ceg_list($teamArr));
		$myTeamTourDetailsRec = count($this->Team_function_model->tour_atten_report_ceg1($teamArr));
		$data['myTeamLeaveDetailsRecArr1'] = count($this->Team_function_model->leave_atten_report_ceg_list($teamArr));
		$data['myTeamTourDetailsRec'] = count($this->Team_function_model->tour_atten_report_ceg1($teamArr));
		$data['myTeamPresentDetailsArr'] = $this->Team_function_model->count_present_atten_report_list_logTime($teamArr);
		// echo "<pre>"; print_r($myTeamLeaveDetailsRecArr); die;
		$data['myTeamAbsentDetailsRecArr'] =  count($teamArr) - ($myTeamTourDetailsRec + $myTeamLeaveDetailsRecArr + $myTeamPresentDetailsRecArr);

		// echo "<pre>"; print_r($data['myTeamAbsentDetailsRecArr']); die;
		//code for absent list count
		$absnt = array();
		$myTeamTourcountRec = $this->Team_function_model->tour_atten_report_ceg1_for_absent($teamArr);
		$myTeamLeavecountRecArr = $this->Team_function_model->leave_atten_report_ceg_list_for_absent($teamArr);
		$myTeamPresentcountRecArr = $this->Team_function_model->count_present_atten_report_ceg_list_absent($teamArr);
		$tourNleave = array_merge($myTeamTourcountRec,$myTeamLeavecountRecArr);
		$tourLeaveNPresent = array_merge($tourNleave,$myTeamPresentcountRecArr);
		foreach($tourLeaveNPresent as $value):
			$absnt[] =  $value->user_id;
		endforeach;
		// echo "<pre>"; print_r($absnt); die;
		$absentEmpArr = array_diff($teamArr,$absnt);
		// echo "<pre>"; print_r($absentEmpArr); die;
		if(count($absentEmpArr) == '0'):
			$data['totalAbsent'] = '0';
		else:
			$data['totalAbsent'] = count($this->Team_function_model->GetMyTeamDetailsRecByID($absentEmpArr));
		endif;
		
		// echo "<pre>"; print_r($data['totalAbsent']); die;
        $today = date("Y-m-d");
        $data['title'] = "Teams";
        $data['teamArr'] = count($teamArr);
		// $Dispdate = date("d-m-Y");
		// echo $Dispdate; die;
		// $machine_userid = $this->session->userdata('loginid');
		// $punchRec = getSingleDateAttByUserID($Dispdate, '280');
		// echo "<pre>"; print_r($punchRec); die;
        $this->load->view("Team_functions/Team_functions_view", $data);
    }
	public function team_under_officers_list() {
		$id = $this->session->userdata('loginid');
        $myTeamDetailsRecArr= $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($id);
        // echo "<pre>"; print_r($presentEmpLeave); die;
        $teamArr = array();
        $teamArr1 = array();
        $teamDetail = array();
		foreach($myTeamDetailsRecArr as $val):
			$teamArr[] = $val->user_id;
			$teamd  = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($val->user_id);
			foreach($teamd as $valu):
				$teamArr[] = $valu->user_id;
			endforeach;
		endforeach;
		$data['myTeamDetailsArr'] = $this->Team_function_model->GetMyTeamDetailsRecByID($teamArr);
		
		$today = date("Y-m-d");
        $data['title'] = "Teams";

        $this->load->view("Team_functions/myteam_list_view", $data);
    }
	
	public function tour_under_officers_list() {
		$id = $this->session->userdata('loginid');
		// echo $id; die;
        $myTeamTourDetailsRecArr = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($id);
        // echo "<pre>"; print_r($myTeamTourDetailsRecArr); die;
		$teamArr = array();
		foreach($myTeamTourDetailsRecArr as $val):
			$teamArr[] = $val->user_id;
			$teamd  = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($val->user_id);
			foreach($teamd as $valu):
				$teamArr[] = $valu->user_id;
			endforeach;
		endforeach;
		$data['myTeamTourDetailsRecArr'] = $this->Team_function_model->tour_atten_report_ceg1($teamArr);
		// $myTeamTourDetailsRecArr = $this->mastermodel->tour_atten_report_ceg1($id);
        // echo "<pre>"; print_r($data['myTeamTourDetailsRecArr']); die;
        // echo "<pre>"; print_r($teamArr); die;
        $today = date("Y-m-d");
        $data['title'] = "Tours";

        $this->load->view("Team_functions/myteam_Tour_list_view", $data);
    }
	public function leave_under_officers_list() {
		$id = $this->session->userdata('loginid');
        $myTeamLeaveDetailsArr = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($id);
        // echo "<pre>"; print_r($data['myTeamLeaveDetailsRecArr']); die;
        
		$teamArr = array();
		foreach($myTeamLeaveDetailsArr as $val):
			$teamArr[] = $val->user_id;
			$teamd  = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($val->user_id);
			foreach($teamd as $valu):
				$teamArr[] = $valu->user_id;
			endforeach;
		endforeach;
		$data['myTeamLeaveDetailsRecArr'] = $this->Team_function_model->leave_atten_report_ceg_list($teamArr);
		// echo "<pre>"; print_r($data['myTeamLeaveDetailsRecArr']); die;
		$today = date("Y-m-d");
        $data['title'] = "Team Emp on Leave";

        $this->load->view("Team_functions/myteam_Leave_list_view", $data);
    }
	
	public function absent_under_officers_list() {
		
		$id = $this->session->userdata('loginid');
        $myTeamDetailsRec = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($id);
        // $abc= $this->Team_function_model->check_self_IO_RO($id);
		
		// $f = in_array($id,$abc);
		// foreach($abc as $val):
			// if($val->reporting_manager != $id):
				// echo $id; die;
			// else:
				// echo "users";
			// endif;
		// endforeach; die;
		// echo "<pre>"; print_r($abc); die;
        // $myTeamTourDetailsRec = $this->Team_function_model->tour_atten_report_ceg1($id);
		// $myTeamLeaveDetailsRecArr = $this->Team_function_model->leave_atten_report_ceg_list($id);
		$myTeamPresentDetailsRecArr = $this->Team_function_model->count_present_atten_report_ceg_list($id);
		$test = array();
		
		$teamTotalArr = array();
		foreach($myTeamDetailsRec as $val):
			$teamTotalArr[] = $val->user_id;
			$teamd  = $this->Team_function_model->GetMyTeamDetailsRecByID_io_ro($val->user_id);
			foreach($teamd as $valu):
				$teamTotalArr[] = $valu->user_id;
			endforeach;
		endforeach;
		$absnt = array();
		$myTeamTourDetailsRec = $this->Team_function_model->tour_atten_report_ceg1_for_absent($teamTotalArr);
		$myTeamLeaveDetailsRecArr = $this->Team_function_model->leave_atten_report_ceg_list_for_absent($teamTotalArr);
		$myTeamPresentDetailsRecArr = $this->Team_function_model->count_present_atten_report_ceg_list_absent($teamTotalArr);
		$tourNleave = array_merge($myTeamTourDetailsRec,$myTeamLeaveDetailsRecArr);
		$tourLeaveNPresent = array_merge($tourNleave,$myTeamPresentDetailsRecArr);
		// $absentEmpArr = array_diff($tourLeaveNPresent,$teamTotalArr);
		foreach($tourLeaveNPresent as $value):
			$absnt[] =  $value->user_id;
		endforeach;
		$absentEmpArr = array_diff($teamTotalArr,$absnt);
		if(count($absentEmpArr) == '0'):
			$totalAbsent = '0';
		else:
			$totalAbsent = $this->Team_function_model->GetMyTeamDetailsRecByID($absentEmpArr);
		endif;
		// $totalAbsent = $this->Team_function_model->GetMyTeamDetailsRecByID($absentEmpArr);
		// echo "<pre>"; print_r($absentEmpArr); die;
		
        $today = date("Y-m-d");
        $data['title'] = "Team Emp on Leave";
        $data['totalAbsent'] = $totalAbsent;
		
        $this->load->view("Team_functions/myteam_absent_list_view", $data);
    }
	
	
	 //Employee Appraisal List..
    public function team_leavestatus_list() {
        error_reporting(0);
        $data = array();
        $data['title'] = "All Leave Status";
        $data['Emplylist'] = $this->Team_leave_model->get_datatables();
        $this->load->view('Team_functions/team_leave__status_view', $data);
    }
	
	public function ajax_list_leave_team() {
        $list = $this->Team_leave_model->get_datatables();
        // print_r($list); die;
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->userfullname;
            $row[] = date('d-m-Y', strtotime($customers->from_date));
            $row[] = date('d-m-Y', strtotime($customers->to_date));
            $row[] = $customers->appliedleavescount;
            $row[] = $customers->leavetype_name;
            $row[] = $customers->leavestatus;
			if($customers->leavestatus == 'Pending for approval'):
            $row[] = '<button type="submit" onclick="update_appr_acc(' . $customers->user_id . ')" class="btn btn-info">Approve</button>';
            else:
			$row[] = '-';
            endif;
			$data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Team_leave_model->count_all(),
            "recordsFiltered" => $this->Team_leave_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}
