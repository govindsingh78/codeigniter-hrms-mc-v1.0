<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Courier_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
		$this->load->model('Courier_model');
        $this->load->model('Inward_model');
        $this->load->library('tank_auth');
        $this->load->library('form_validation');
		$this->load->library('breadcrumbcomponent'); 
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }
	
    public function courier_dashboard() {
		$this->session->set_userdata(array('courier_identifier' => '1'));
		$this->breadcrumbcomponent->add('Home', base_url());
		$this->breadcrumbcomponent->add('test', base_url().'test');  
		$this->breadcrumbcomponent->add('test 2', base_url().'test/test-2');
		// $data['breadcrumbs'] = $this->breadcrumbcomponent->render();
		// print_r($data['breadcrumbs']); die;
        $today = date("Y-m-d");
        $data['title'] = "Courier Dashboard";
        $this->load->view("courier/courier_dashboard_view", $data);
    }
	
	public function index() {
        $this->load->helper('url');
        $this->load->helper('form');
        $businessunit = $this->Courier_model->get_list_businessunit();
        $companyname = $this->Courier_model->get_list_companyname();
        $courier = $this->Courier_model->getCourierservice();
        $data['form_businessunit'] = $businessunit;
        $data['companyname'] = $companyname;
        $data['courier'] = $courier;
        $data['title'] = "Outward Courier Details";
        $user_id = $this->session->userdata('loginid');
        if(!empty($user_id)){
        $this->load->view('courier/Courier_dispatch_view', $data);
        }
        else{
            redirect('admin', true);
        }
    }
	
	public function ajax_list() {
        $list = $this->Courier_model->get_datatables();
        date_default_timezone_set('Asia/Kolkata');
        /* echo '<pre>';
          print_r($list); die; */
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = $no . "&nbsp;" . '<a data-toggle="modal" data-target="#myModal" onclick="updproccess(' . "'" . @$value->id . "','" . @$value->courier_serviceid . "'" . ')" Title="Edit" href="#"><i class="glyphicon glyphicon-edit"></i></a>';
            $row[] = isset($value->unitname) ? $value->unitname : '';
            $row[] = isset($value->dispatch) ? $value->dispatch : '';
            $row[] = isset($value->userfullname) ? $value->userfullname : '';
            $row[] = isset($value->contactnumber) ? $value->contactnumber : '';
            $row[] = isset($value->receivername) ? $value->receivername : '';
            $row[] = isset($value->company_name) ? $value->company_name : '';
            $row[] = isset($value->city_name) ? $value->city_name : '';
            $row[] = isset($value->pincode) ? $value->pincode : '';
			$row[] = isset($value->courier_weight) ? $value->courier_weight : '';
            $row[] = isset($value->courier_service) ? $value->courier_service : '';
            $row[] = isset($value->created) ? date("d-m-Y", strtotime($value->created)) : '';
            $row[] = '<a href="Courier_controller/editcourier/' . @$value->id . '"><li class="fa fa-edit"></li></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Courier_model->count_all(),
            "recordsFiltered" => $this->Courier_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function add_company() {
        $this->form_validation->set_rules('CName', 'Company Name', 'trim|required|min_length[2]|max_length[255]|is_unique[tbl_companylist.company_name]');
        $companyname = $this->Courier_model->get_list_companyname();
        $country = $this->Courier_model->getCountryname();
        $data['companyname'] = $companyname;
        $data['country'] = $country;
        if ($this->form_validation->run() == false) {
            $data['title'] = "Add Company For Courier Details";
              $user_id = $this->session->userdata('loginid');
              if(!empty($user_id)){
               $this->load->view('courier/addcompany_view', $data);
              }
              else{
                  redirect('admin', true);
              }
        } else {
            $data1 = array(
                'company_name' => $this->input->post('CName')
            );
            $this->Courier_model->insert2($data1);
            redirect(base_url('add_company'));
        }
    }
	
	public function company_ajax_list() {
        $list = $this->Courier_model->get_company();
        date_default_timezone_set('Asia/Kolkata');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = isset($value->company_name) ? $value->company_name : '';
            $row[] = isset($value->pincode) ? $value->pincode : '';
            $row[] = isset($value->country_name) ? $value->country_name : '';
            $row[] = isset($value->state_name) ? $value->state_name : '';
            $row[] = isset($value->city_name) ? $value->city_name : '';
            $row[] = isset($value->address1) ? $value->address1 : '';
            $row[] = isset($value->address2) ? $value->address2 : '';
            $row[] = isset($value->address3) ? $value->address3 : '';
            $row[] = isset($value->created) ? date("d-m-Y", strtotime($value->created)) : '';
            $row[] = '<a href="Courier_controller/editcompany/' . @$value->id . '"><li class="fa fa-edit"></li></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Courier_model->count_all(),
            "recordsFiltered" => $this->Courier_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
	
	public function inwardcourier() {
        $businessunit = $this->Courier_model->get_list_businessunit();
        $companyname = $this->Courier_model->get_list_companyname();
        $employename = $this->Courier_model->get_list_empname();
        $data['country'] = $this->Courier_model->getCountryname();
        $data['form_businessunit'] = $businessunit;
        $data['companyname'] = $companyname;
        $data['employename'] = $employename;
        $data['title'] = "Inward Courier Details";
        $user_id = $this->session->userdata('loginid');
        if(!empty($user_id)){
        $this->load->view('courier/inward_view', $data);
        }
        else{
            redirect('admin', true);
        }
    }
	
	public function inwardajax_list() {
        $list = $this->Inward_model->get_datatables();
        $data = array();
        $no = $_POST['start'];

          //For Admin...
          if ($this->tank_auth->get_user_id() == 1) {

          $no++;
          $row = array();
          $row[] = $no;
          $row[] = isset($value->unitname) ? $value->unitname : '';
          $row[] = isset($value->letter_date) ? $value->letter_date : '';
          $row[] = isset($value->from_received) ? $value->from_received : '';

          $row[] = isset($value->company_name) ? $value->company_name : '';
          $row[] = isset($value->pincode) ? $value->pincode : '';
          $row[] = isset($value->subject) ? $value->subject : '';
          $row[] = isset($value->from_received) ? $value->from_received : '';
          $row[] = isset($value->agency_reference) ? $value->agency_reference : '';
          $row[] = isset($value->officer_filepass) ? $value->officer_filepass : '';
          $row[] = isset($value->emp1) ? $value->emp1 : '';
          $row[] = isset($value->emp2) ? $value->emp2 : '';
          $row[] = '<a href="' . base_url('Courier_controller/inwardcourieredit/' . $value->id) . '">Edit</a>';
          $data[] = $row;
          }

        foreach ($list as $value) {
            if (!empty($value->pincode)) {
                $pincod = $value->pincode;
            } else {
                $pincod = $value->receiverpincode;
            }

            if (!empty($value->company_id)) {
                $company_name = $value->company_name;
            } else {
                $company_name = 'Other';
            }
            if (($value->businessunitid == $this->getbunitbyloginid()) and ( $this->tank_auth->get_user_id() != 1)) {
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = isset($value->unitname) ? $value->unitname : '';
                $row[] = isset($value->letter_date) ? $value->letter_date : '';
                $row[] = isset($value->from_received) ? $value->from_received : '';
                $row[] = isset($company_name) ? $company_name : '';
                $row[] = isset($pincod) ? $pincod : '';
                $row[] = isset($value->subject) ? $value->subject : '';
                $row[] = isset($value->agency_reference) ? $value->agency_reference : '';
                $row[] = isset($value->officer_filepass) ? $value->officer_filepass : '';
                $row[] = isset($value->emp1) ? $value->emp1 : '';
                $row[] = isset($value->emp2) ? $value->emp2 : '';
                $row[] = '<a href="' . base_url('Courier_controller/inwardcourieredit/' . $value->id) . '">Edit</a>';
                $data[] = $row;
            }


            //For Admin...
            if ($this->tank_auth->get_user_id() == 1) {

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = isset($value->unitname) ? $value->unitname : '';
                $row[] = isset($value->letter_date) ? $value->letter_date : '';
                $row[] = isset($value->from_received) ? $value->from_received : '';
                $row[] = isset($value->company_name) ? $value->company_name : '';
                $row[] = isset($pincod) ? $pincod : '';
                $row[] = isset($value->subject) ? $value->subject : '';
                $row[] = isset($value->agency_reference) ? $value->agency_reference : '';
                $row[] = isset($value->officer_filepass) ? $value->officer_filepass : '';
                $row[] = isset($value->emp1) ? $value->emp1 : '';
                $row[] = isset($value->emp2) ? $value->emp2 : '';
                $row[] = '<a href="' . base_url('Courier_controller/inwardcourieredit/' . $value->id) . '">Edit</a>';
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Inward_model->count_all(),
            "recordsFiltered" => $this->Inward_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	
	public function getbunitbyloginid() {

        if ($this->tank_auth->get_user_id() == '432') {
            return '2';
        }
        $this->db->select('businessunit_id');
        $this->db->from('main_employees_summary');
        $this->db->where('user_id', $this->tank_auth->get_user_id());
        $query = $this->db->get();
        $result = $query->result();
        return ($result) ? $result[0]->businessunit_id : '1';
    }
	
	public function editcourier($id) {
		//$projId = $this->uri->segment(4);
		//print_r($projId);
		//die;
		$data['error'] = array();
        $data['title'] = "Edit Courier Details";
		$datacourier = $this->Courier_model->getCourierDetails1(array('status' => '1', 'id' => $id));
		$senderempid = $datacourier['senderempid'];
		$businessunit = $this->Courier_model->get_list_businessunit();
        $companyname = $this->Courier_model->get_list_companyname();
		$pin = $this->Courier_model->get_pincode();
		$deptname = $this->Courier_model->getDepartmentDetails();
		$employename = $this->Courier_model->get_list_empname();
		$empdetails = $this->Courier_model->getContactDetails($senderempid);
		$courier = $this->Courier_model->getCourierservice();
		$country = $this->Courier_model->getCountryDetails();
		$data['datacourier'] = $datacourier;
		$data['form_businessunit'] = $businessunit;
		$data['companyname'] = $companyname;
		$data['pin'] = $pin;
		$data['deptname'] = $deptname;
		$data['empdetails'] = $empdetails;
		$data['employename'] = $employename;
		$data['courier'] = $courier;
		$data['country'] = $country;
		$data['projId'] = $id;
	    if ($_REQUEST) {
            $this->form_validation->set_rules('businessunitid', 'Business Unit', 'required');
            $this->form_validation->set_rules('dept_id', 'Department', 'required');
            $this->form_validation->set_rules('senderempid', 'Employee', 'required');
            $this->form_validation->set_rules('typer_cate_id', 'Type', 'required');
            $this->form_validation->set_rules('rcv_buss_unit', 'Business Unit');
            $this->form_validation->set_rules('receivername', 'Employee Name', 'required');
            $this->form_validation->set_rules('receivercompanyid', 'Company Name');
            $this->form_validation->set_rules('receiverpincode', 'Office Location');
            $this->form_validation->set_rules('is_comp_other', 'Project Status');
            $this->form_validation->set_rules('comp_other_info_details', 'Project Status');
            $this->form_validation->set_rules('receiver_mobile', 'Mobile');
            $this->form_validation->set_rules('receiveremail', 'Email');
            $this->form_validation->set_rules('dispatch', 'Dispatch No');
            $this->form_validation->set_rules('courier_service', 'Courier Service');

            if ($this->form_validation->run() == TRUE) {
                //$this->load->view('back_end/courier/edit_dispatch_view',$data);
            } else {
                if ($this->input->post('receivercompanyid') != 'new') {
                    $update = array(
                        'businessunitid' => $this->input->post('businessunitid'),
                        'rcv_buss_unit' => $this->input->post('rcvbusinessunitnames'),
                        'dept_id' => $this->input->post('dept_id'),
                        'typer_cate_id' => $this->input->post('typer_categ_courier'),
                        'senderempid' => $this->input->post('senderempid'),
                        'receivername' => $this->input->post('receivername'),
                        'receivercompanyid' => $this->input->post('receivercompanyid'),
                        'receiverpincode' => $this->input->post('receiverpincode'),
                        'receiver_mobile' => $this->input->post('receiver_mobile'),
                        'receiveremail' => $this->input->post('receiveremail'),
                        'dispatch' => $this->input->post('dispatch'),
                        'courier_service' => $this->input->post('courier_service'),
                        'entryby' => $this->session->userdata('user_id')
                    );
                } else {
                    $comp_other_info_detailsArr = array('add1' => $this->input->post('add1'), 'add2' => $this->input->post('add2'), 'add3' => $this->input->post('add3'), 'city' => $this->input->post('city'), 'state' => $this->input->post('state'), 'country' => $this->input->post('country'));
                    $update = array('businessunitid' => $this->input->post('businessunitid'), 'rcv_buss_unit' => $this->input->post('rcvbusinessunitnames'),
                        'dept_id' => $this->input->post('dept_id'),
                        'typer_cate_id' => $this->input->post('typer_categ_courier'),
                        'senderempid' => $this->input->post('senderempid'),
                        'receivername' => $this->input->post('receivername'),
                        'is_comp_other' => '1',
                        'comp_other_info_details' => json_encode($comp_other_info_detailsArr),
                        'receiver_mobile' => $this->input->post('receiver_mobile'),
                        'receiveremail' => $this->input->post('receiveremail'),
                        'dispatch' => $this->input->post('dispatch'),
                        'courier_service' => $this->input->post('courier_service'),
                        'entryby' => $this->session->userdata('user_id'));
                }
				//$projId = $this->uri->segment(4);
                $where = array('id' => $id);
				//print_r($where);die;
                $respon = $this->Courier_model->UpdataRecord('main_courierdetail', $update, $where);
                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Record Updated successfully');
                    redirect(base_url('courier'));
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    redirect(base_url('courier'));
                endif;
            }
        }
        $this->load->view('courier/edit_dispatch_view', $data);
    }
	
	public function editcompany() {
        $projId = $this->uri->segment(3);
		// echo $projId; die;
        $companyname = $this->Courier_model->get_list_companyname();
        $country = $this->Courier_model->getCountryname();
        $state = $this->Courier_model->getStatename();
        $city = $this->Courier_model->getCityname();
        $data['new'] = $this->Courier_model->getnewDetails(array('is_active' => '1', 'id' => $projId));
		// echo "<pre>"; print_r($data['new']); die;
        $data['country'] = $country;
        $data['state'] = $state;
        $data['city'] = $city;
        $data['companyname'] = $companyname;
        $data['projId'] = $projId;
        if ($_REQUEST) {
            $update = array(
                'company_id' => $this->input->post('company_id'),
                'pincode' => $this->input->post('pincode'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'address3' => $this->input->post('address3'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country')
            );
            $where = array('id' => $projId);
            $respon = $this->Courier_model->UpdataRecord('tbl_comapnyinfo', $update, $where);
            if ($respon):
                $this->session->set_flashdata('success_msg', 'Record Updated successfully');
                redirect(base_url('add_company'));
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(base_url('add_company'));
            endif;
        }
        $data['title'] = "Edit/Update Company For Courier Details";
        $this->load->view('courier/editcompany_view', $data);
    }
	
	//Update C Sevice,,,
    public function updatecservice() {
        if ($_REQUEST) {
            $this->db->set('courier_service', $_REQUEST['courier_serviceupd']);
            $this->db->where(array('id' => $_REQUEST['updider']));
            $this->db->update('main_courierdetail');
            $this->session->set_flashdata('success_msg', 'Record Updated successfully');
        }
        redirect(base_url('courier'));
    }
	
	
	
}
