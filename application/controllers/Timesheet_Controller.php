<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Timesheet_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Timesheet/Timesheet_Model', 'timesheetmodel');
        $this->load->model('Timesheet/TimesheetAsRo_Model');
        $this->load->library('form_validation');
        $this->load->library('calendar');
        $this->load->helper('date');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

//Timesheet entry first Layout...
    public function timesheet_entry() {
        $data['error'] = '';
        $data['title'] = 'Enter Timesheet';
        $this->load->helper('date');
        $loginUserId = $this->session->userdata('loginid');

        if (@$_REQUEST['flag']):
            $data['AllProjRecdArr'] = $this->timesheetmodel->GetAllRecentProjListByUserID($loginUserId);
            $data['TsFlag'] = true;
        else:
            $data['AllProjRecdArr'] = $this->timesheetmodel->GetAllProjListByUserID($loginUserId);
            $data['TsFlag'] = null;
        endif;

        @$GotWeek = $this->uri->segment(2);
        @$GotMonth = $this->uri->segment(3);
        @$GotYear = $this->uri->segment(4);

        //##### If Curent Week * Code Start * ##########
        //Cuurent Date Week Month Year Default..
        if (@$GotWeek == "" || @$GotMonth == "" || @$GotYear == ""):
            $data['thisMonth'] = date("m");
            $data['thisMonthName'] = date("F");
            $data['thisYear'] = date("Y");
            $data['todayDate'] = date("d-m-Y");
            $todayDate = date("d-m-Y");
            $data['MonthWeek'] = $this->weekOfMonth($todayDate);
            $data['yearWeek'] = date("W", strtotime($todayDate));
            $data['weekDateRangeArr'] = $this->rangeWeek($todayDate);
            $data['DateRangeArr'] = date_range($data['weekDateRangeArr']['start'], $data['weekDateRangeArr']['end']);
            $data['FirstDate_Month'] = "01-" . date("m-Y");
            $data['LastDate_Month'] = date('t-m-Y');
            $data['SumTsDurationArr'] = $this->timesheetmodel->SumTsWeekDayDuration($loginUserId, $data['thisYear'], $data['thisMonth'], $data['MonthWeek'], $data['yearWeek']);
        endif;
        //##### If Curent Week * Code End * ##########
        //######## **If Week Change ** ##########
        if (@$GotWeek || @$GotMonth || @$GotYear):
            $month = $GotMonth;
            $year = $GotYear;
            $data['thisMonth'] = $GotMonth;
            $data['thisYear'] = $GotYear;
            $MonthNameFull = FullMonthName($GotMonth);
            $sunday = strtotime("$GotWeek Sunday $MonthNameFull $GotMonth");
            $startdateofweek = date('d-m-Y', strtotime('last monday', $sunday));
            $enddateofweek = date('d-m-Y', $sunday);
            $data['thisMonthName'] = $MonthNameFull;
            $data['MonthWeek'] = $GotWeek;
            $data['yearWeek'] = date("W", strtotime($startdateofweek));
            $data['weekDateRangeArr'] = array("start" => $startdateofweek, "end" => $enddateofweek);
            $data['DateRangeArr'] = date_range($startdateofweek, $enddateofweek);
            $data['FirstDate_Month'] = "01-" . $GotMonth . "-" . $GotYear;
            $data['LastDate_Month'] = date('t-m-Y', strtotime($data['FirstDate_Month']));
            $data['SumTsDurationArr'] = $this->timesheetmodel->SumTsWeekDayDuration($loginUserId, $data['thisYear'], $data['thisMonth'], $data['MonthWeek'], $data['yearWeek']);
        endif;
        $data['TsAnotherArr'] = $this->GetAllExtraActivitiesTimesheetByUserID();
        //Got TS Notes Of This Week..
        $tSNotesWhereArr = ["a.is_active" => "1", "a.emp_id" => $loginUserId, "a.ts_year" => $data['thisYear'], "a.ts_month" => $data['thisMonth'], "a.ts_week" => $data['MonthWeek'], "a.cal_week" => $data['yearWeek']];
        $data['tSNotesArr'] = $this->timesheetmodel->GetWeeklyTSNoteArr($tSNotesWhereArr);
        $this->load->view("timesheet/timesheet_entry_view", $data);
    }

//Save Or Update Time Sheets.. Code By Ashe....
    public function ts_savesubmit() {
        error_reporting(0);
        $fromDate = $_REQUEST['from_date'];
        $toDate = $_REQUEST['to_date'];
        $currentDateTime = date('Y-m-d h:i:s');
        $timeAddSum = array();
        $storeProjIDsArr = array();
        $TsupdArr_Status = array();
        $TsupdArr2 = array();

        $LoginUserId = $this->session->userdata('loginid');
        $weekDateRangeArr = date_range($fromDate, $toDate);

        //Save ..########
        if ($_REQUEST['taskidarr'] && @$_REQUEST['save']) {

            foreach ($_REQUEST['taskidarr'] as $keyT => $rewT) {
                $projTaskRec = TaskProjIDByprojTaskId($rewT);
                $MonDuration = ($_REQUEST['mondur'][$keyT]) ? $_REQUEST['mondur'][$keyT] : null;
                $TueDuration = ($_REQUEST['tuedur'][$keyT]) ? $_REQUEST['tuedur'][$keyT] : null;
                $WedDuration = ($_REQUEST['weddur'][$keyT]) ? $_REQUEST['weddur'][$keyT] : null;
                $ThuDuration = ($_REQUEST['thudur'][$keyT]) ? $_REQUEST['thudur'][$keyT] : null;
                $FriDuration = ($_REQUEST['fridur'][$keyT]) ? $_REQUEST['fridur'][$keyT] : null;
                $SatDuration = ($_REQUEST['satdur'][$keyT]) ? $_REQUEST['satdur'][$keyT] : null;
                $SunDuration = ($_REQUEST['sundur'][$keyT]) ? $_REQUEST['sundur'][$keyT] : null;

                if (($MonDuration || $TueDuration || $WedDuration || $ThuDuration || $FriDuration || $SatDuration || $SunDuration) AND ( $projTaskRec->project_id)):

                    $MonDuration = ($_REQUEST['mondur'][$keyT]) ? $_REQUEST['mondur'][$keyT] : "00:00";
                    $TueDuration = ($_REQUEST['tuedur'][$keyT]) ? $_REQUEST['tuedur'][$keyT] : "00:00";
                    $WedDuration = ($_REQUEST['weddur'][$keyT]) ? $_REQUEST['weddur'][$keyT] : "00:00";
                    $ThuDuration = ($_REQUEST['thudur'][$keyT]) ? $_REQUEST['thudur'][$keyT] : "00:00";
                    $FriDuration = ($_REQUEST['fridur'][$keyT]) ? $_REQUEST['fridur'][$keyT] : "00:00";
                    $SatDuration = ($_REQUEST['satdur'][$keyT]) ? $_REQUEST['satdur'][$keyT] : "00:00";
                    $SunDuration = ($_REQUEST['sundur'][$keyT]) ? $_REQUEST['sundur'][$keyT] : "00:00";

                    $timeAddSum = array();
                    $timeAddSum[] = $MonDuration;
                    $timeAddSum[] = $TueDuration;
                    $timeAddSum[] = $WedDuration;
                    $timeAddSum[] = $ThuDuration;
                    $timeAddSum[] = $FriDuration;
                    $timeAddSum[] = $SatDuration;
                    $timeAddSum[] = $SunDuration;

                    //Chk Ts Fill Already Exist..
                    $tsCondWhereArr = ["is_active" => '1', "emp_id" => $LoginUserId, "project_task_id" => $rewT, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek']];
                    $NumRows = $this->ChkTs_existing($tsCondWhereArr);
                    //Where Cond Arr..
                    $tsCondWhereStatusArr = ["is_active" => '1', "emp_id" => $LoginUserId, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek']];

                    //Insert
                    if ($NumRows < 1) {
                        $insertArr = array("emp_id" => $LoginUserId, "project_task_id" => $rewT, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek'], "sun_date" => $weekDateRangeArr[6], "sun_duration" => $SunDuration, "mon_date" => $weekDateRangeArr[0], "mon_duration" => $MonDuration, "tue_date" => $weekDateRangeArr[1], "tue_duration" => $TueDuration, "wed_date" => $weekDateRangeArr[2], "wed_duration" => $WedDuration, "thu_date" => $weekDateRangeArr[3], "thu_duration" => $ThuDuration, "fri_date" => $weekDateRangeArr[4], "fri_duration" => $FriDuration, "sat_date" => $weekDateRangeArr[5], "sat_duration" => $SatDuration, "week_duration" => $this->AddPlayTime($timeAddSum), "created_by" => $LoginUserId, "modified_by" => $LoginUserId, "is_active" => '1', "created" => $currentDateTime, "modified" => $currentDateTime);
                        $this->db->insert("tm_emp_timesheets", $insertArr);
                        $insert_id = $this->db->insert_id();
                        //Time Sheet Status..
                        if ($insert_id):
                            $NumRowsStatus = $this->ChkTsStatus_existing($tsCondWhereStatusArr);
                            if ($NumRowsStatus < 1) {
                                $tmts_statusArr = array("emp_id" => $LoginUserId, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek'], "mon_date" => $weekDateRangeArr[0], "tue_date" => $weekDateRangeArr[1], "wed_date" => $weekDateRangeArr[2], "thu_date" => $weekDateRangeArr[3], "fri_date" => $weekDateRangeArr[4], "sat_date" => $weekDateRangeArr[5], "sun_date" => $weekDateRangeArr[6], "sun_status_date" => $currentDateTime, "mon_status_date" => $currentDateTime, "tue_status_date" => $currentDateTime, "wed_status_date" => $currentDateTime, "thu_status_date" => $currentDateTime, "fri_status_date" => $currentDateTime, "sat_status_date" => $currentDateTime, "week_status" => 'saved', "created_by" => $LoginUserId, "modified_by" => $LoginUserId, "is_active" => '1', "created" => $currentDateTime, "modified" => $currentDateTime);
                                //Mon..
                                if ($MonDuration != "00:00" and $MonDuration != "") {
                                    $tmts_statusArr["mon_project_status"] = "saved";
                                    $tmts_statusArr["mon_status"] = "saved";
                                }
                                //Tue..
                                if ($TueDuration != "00:00" and $TueDuration != "") {
                                    $tmts_statusArr["tue_project_status"] = "saved";
                                    $tmts_statusArr["tue_status"] = "saved";
                                }
                                //web..
                                if ($WedDuration != "00:00" and $WedDuration != "") {
                                    $tmts_statusArr["wed_project_status"] = "saved";
                                    $tmts_statusArr["wed_status"] = "saved";
                                }
                                //thu..
                                if ($ThuDuration != "00:00" and $ThuDuration != "") {
                                    $tmts_statusArr["thu_project_status"] = "saved";
                                    $tmts_statusArr["thu_status"] = "saved";
                                }
                                //fri
                                if ($FriDuration != "00:00" and $FriDuration != "") {
                                    $tmts_statusArr["fri_project_status"] = "saved";
                                    $tmts_statusArr["fri_status"] = "saved";
                                }
                                //sat
                                if ($SatDuration != "00:00" and $SatDuration != "") {
                                    $tmts_statusArr["sat_project_status"] = "saved";
                                    $tmts_statusArr["sat_status"] = "saved";
                                }
                                //sun
                                if ($SunDuration != "00:00" and $SunDuration != "") {
                                    $tmts_statusArr["sun_project_status"] = "saved";
                                    $tmts_statusArr["sun_status"] = "saved";
                                }
                                $this->db->insert("tm_ts_status", $tmts_statusArr);
                                $tm_Status_insert_id = $this->db->insert_id();
                            } else {
                                //Status Update..
                                //Mon..
                                if ($MonDuration != "00:00" and $MonDuration != "") {
                                    $TsupdArr_Status["mon_project_status"] = "saved";
                                    $TsupdArr_Status["mon_status"] = "saved";
                                }
                                //Tue..
                                if ($TueDuration != "00:00" and $TueDuration != "") {
                                    $TsupdArr_Status["tue_project_status"] = "saved";
                                    $TsupdArr_Status["tue_status"] = "saved";
                                }
                                //web..
                                if ($WedDuration != "00:00" and $WedDuration != "") {
                                    $TsupdArr_Status["wed_project_status"] = "saved";
                                    $TsupdArr_Status["wed_status"] = "saved";
                                }
                                //thu..
                                if ($ThuDuration != "00:00" and $ThuDuration != "") {
                                    $TsupdArr_Status["thu_project_status"] = "saved";
                                    $TsupdArr_Status["thu_status"] = "saved";
                                }
                                //fri..
                                if ($FriDuration != "00:00" and $FriDuration != "") {
                                    $TsupdArr_Status["fri_project_status"] = "saved";
                                    $TsupdArr_Status["fri_status"] = "saved";
                                }
                                //sat..
                                if ($SatDuration != "00:00" and $SatDuration != "") {
                                    $TsupdArr_Status["sat_project_status"] = "saved";
                                    $TsupdArr_Status["sat_status"] = "saved";
                                }
                                //sun..
                                if ($SunDuration != "00:00" and $SunDuration != "") {
                                    $TsupdArr_Status["sun_project_status"] = "saved";
                                    $TsupdArr_Status["sun_status"] = "saved";
                                }
                                $updResp = $this->timesheetmodel->UpdateTableData("tm_ts_status", $tsCondWhereStatusArr, $TsupdArr_Status);
                            }
                        endif;
                    } else {
                        //Update..
                        $TsupdArr = array("sun_duration" => $SunDuration, "mon_duration" => $MonDuration, "tue_duration" => $TueDuration, "wed_duration" => $WedDuration, "thu_duration" => $ThuDuration, "fri_duration" => $FriDuration, "sat_duration" => $SatDuration, "week_duration" => $this->AddPlayTime($timeAddSum), "modified_by" => $LoginUserId, "modified" => $currentDateTime);
                        $updResp = $this->timesheetmodel->UpdateTableData("tm_emp_timesheets", $tsCondWhereArr, $TsupdArr);
                        //Time Sheet Status Update Cond.. 2
                        //Status Update.. 2
                        //Mon..
                        if ($MonDuration != "00:00" and $MonDuration != "") {
                            $TsupdArr2["mon_project_status"] = "saved";
                            $TsupdArr2["mon_status"] = "saved";
                        }
                        //Tue..
                        if ($TueDuration != "00:00" and $TueDuration != "") {
                            $TsupdArr2["tue_project_status"] = "saved";
                            $TsupdArr2["tue_status"] = "saved";
                        }
                        //web..
                        if ($WedDuration != "00:00" and $WedDuration != "") {
                            $TsupdArr2["wed_project_status"] = "saved";
                            $TsupdArr2["wed_status"] = "saved";
                        }
                        //thu..
                        if ($ThuDuration != "00:00" and $ThuDuration != "") {
                            $TsupdArr2["thu_project_status"] = "saved";
                            $TsupdArr2["thu_status"] = "saved";
                        }
                        //fri..
                        if ($FriDuration != "00:00" and $FriDuration != "") {
                            $TsupdArr2["fri_project_status"] = "saved";
                            $TsupdArr2["fri_status"] = "saved";
                        }
                        //sat..
                        if ($SatDuration != "00:00" and $SatDuration != "") {
                            $TsupdArr2["sat_project_status"] = "saved";
                            $TsupdArr2["sat_status"] = "saved";
                        }
                        //sun..
                        if ($SunDuration != "00:00" and $SunDuration != "") {
                            $TsupdArr2["sun_project_status"] = "saved";
                            $TsupdArr2["sun_status"] = "saved";
                        }
                        $updResp = $this->timesheetmodel->UpdateTableData("tm_ts_status", $tsCondWhereStatusArr, $TsupdArr2);
                    }
                endif;
            }
            if ((@$insert_id) || ($updResp)):
                $this->session->set_flashdata('successmsg', 'Week work Details Saved successfully.');
            else:
                $this->session->set_flashdata('errormsg', 'Something went wrong');
            endif;
        }

        //Lock Or Submit..
        if ($_REQUEST['taskidarr'] && @$_REQUEST['submit']) {
            foreach ($_REQUEST['taskidarr'] as $keyT => $rewT) {
                $projTaskRec = TaskProjIDByprojTaskId($rewT);
                $MonDuration = ($_REQUEST['mondur'][$keyT]) ? $_REQUEST['mondur'][$keyT] : null;
                $TueDuration = ($_REQUEST['tuedur'][$keyT]) ? $_REQUEST['tuedur'][$keyT] : null;
                $WedDuration = ($_REQUEST['weddur'][$keyT]) ? $_REQUEST['weddur'][$keyT] : null;
                $ThuDuration = ($_REQUEST['thudur'][$keyT]) ? $_REQUEST['thudur'][$keyT] : null;
                $FriDuration = ($_REQUEST['fridur'][$keyT]) ? $_REQUEST['fridur'][$keyT] : null;
                $SatDuration = ($_REQUEST['satdur'][$keyT]) ? $_REQUEST['satdur'][$keyT] : null;
                $SunDuration = ($_REQUEST['sundur'][$keyT]) ? $_REQUEST['sundur'][$keyT] : null;

                if (($MonDuration || $TueDuration || $WedDuration || $ThuDuration || $FriDuration || $SatDuration || $SunDuration) AND ( $projTaskRec->project_id)):
                    $MonDuration = ($_REQUEST['mondur'][$keyT]) ? $_REQUEST['mondur'][$keyT] : "00:00";
                    $TueDuration = ($_REQUEST['tuedur'][$keyT]) ? $_REQUEST['tuedur'][$keyT] : "00:00";
                    $WedDuration = ($_REQUEST['weddur'][$keyT]) ? $_REQUEST['weddur'][$keyT] : "00:00";
                    $ThuDuration = ($_REQUEST['thudur'][$keyT]) ? $_REQUEST['thudur'][$keyT] : "00:00";
                    $FriDuration = ($_REQUEST['fridur'][$keyT]) ? $_REQUEST['fridur'][$keyT] : "00:00";
                    $SatDuration = ($_REQUEST['satdur'][$keyT]) ? $_REQUEST['satdur'][$keyT] : "00:00";
                    $SunDuration = ($_REQUEST['sundur'][$keyT]) ? $_REQUEST['sundur'][$keyT] : "00:00";

                    $timeAddSum = array();
                    $timeAddSum[] = $MonDuration;
                    $timeAddSum[] = $TueDuration;
                    $timeAddSum[] = $WedDuration;
                    $timeAddSum[] = $ThuDuration;
                    $timeAddSum[] = $FriDuration;
                    $timeAddSum[] = $SatDuration;
                    $timeAddSum[] = $SunDuration;

                    //Chk Ts Fill Already Exist..
                    $tsCondWhereArr = ["is_active" => '1', "emp_id" => $LoginUserId, "project_task_id" => $rewT, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek']];
                    $NumRows = $this->ChkTs_existing($tsCondWhereArr);
                    //Where Cond Arr..
                    $tsCondWhereStatusArr = ["is_active" => '1', "emp_id" => $LoginUserId, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek']];

                    //Insert
                    if ($NumRows < 1) {
                        $insertArr = array("emp_id" => $LoginUserId, "project_task_id" => $rewT, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek'], "sun_date" => $weekDateRangeArr[6], "sun_duration" => $SunDuration, "mon_date" => $weekDateRangeArr[0], "mon_duration" => $MonDuration, "tue_date" => $weekDateRangeArr[1], "tue_duration" => $TueDuration, "wed_date" => $weekDateRangeArr[2], "wed_duration" => $WedDuration, "thu_date" => $weekDateRangeArr[3], "thu_duration" => $ThuDuration, "fri_date" => $weekDateRangeArr[4], "fri_duration" => $FriDuration, "sat_date" => $weekDateRangeArr[5], "sat_duration" => $SatDuration, "week_duration" => $this->AddPlayTime($timeAddSum), "created_by" => $LoginUserId, "modified_by" => $LoginUserId, "is_active" => '1', "created" => $currentDateTime, "modified" => $currentDateTime);
                        $this->db->insert("tm_emp_timesheets", $insertArr);
                        $insert_id = $this->db->insert_id();
                        //Time Sheet Status..
                        if ($insert_id):
                            $NumRowsStatus = $this->ChkTsStatus_existing($tsCondWhereStatusArr);
                            if ($NumRowsStatus < 1) {
                                $tmts_statusArr = array("emp_id" => $LoginUserId, "project_id" => $projTaskRec->project_id, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek'], "mon_date" => $weekDateRangeArr[0], "tue_date" => $weekDateRangeArr[1], "wed_date" => $weekDateRangeArr[2], "thu_date" => $weekDateRangeArr[3], "fri_date" => $weekDateRangeArr[4], "sat_date" => $weekDateRangeArr[5], "sun_date" => $weekDateRangeArr[6], "sun_status_date" => date('Y-m-d h:i:s'), "mon_status_date" => date('Y-m-d h:i:s'), "tue_status_date" => date('Y-m-d h:i:s'), "wed_status_date" => date('Y-m-d h:i:s'), "thu_status_date" => date('Y-m-d h:i:s'), "fri_status_date" => date('Y-m-d h:i:s'), "sat_status_date" => date('Y-m-d h:i:s'), "week_status" => 'submitted', "created_by" => $LoginUserId, "modified_by" => $LoginUserId, "is_active" => '1', "created" => $currentDateTime, "modified" => $currentDateTime);
                                //Mon..
                                if ($MonDuration != "00:00" and $MonDuration != "") {
                                    $tmts_statusArr["mon_project_status"] = "submitted";
                                    $tmts_statusArr["mon_status"] = "submitted";
                                }
                                //Tue..
                                if ($TueDuration != "00:00" and $TueDuration != "") {
                                    $tmts_statusArr["tue_project_status"] = "submitted";
                                    $tmts_statusArr["tue_status"] = "submitted";
                                }
                                //web..
                                if ($WedDuration != "00:00" and $WedDuration != "") {
                                    $tmts_statusArr["wed_project_status"] = "submitted";
                                    $tmts_statusArr["wed_status"] = "submitted";
                                }
                                //thu..
                                if ($ThuDuration != "00:00" and $ThuDuration != "") {
                                    $tmts_statusArr["thu_project_status"] = "submitted";
                                    $tmts_statusArr["thu_status"] = "submitted";
                                }
                                //fri
                                if ($FriDuration != "00:00" and $FriDuration != "") {
                                    $tmts_statusArr["fri_project_status"] = "submitted";
                                    $tmts_statusArr["fri_status"] = "submitted";
                                }
                                //sat
                                if ($SatDuration != "00:00" and $SatDuration != "") {
                                    $tmts_statusArr["sat_project_status"] = "submitted";
                                    $tmts_statusArr["sat_status"] = "submitted";
                                }
                                //sun
                                if ($SunDuration != "00:00" and $SunDuration != "") {
                                    $tmts_statusArr["sun_project_status"] = "submitted";
                                    $tmts_statusArr["sun_status"] = "submitted";
                                }
                                $this->db->insert("tm_ts_status", $tmts_statusArr);
                                $tm_Status_insert_id = $this->db->insert_id();
                            } else {
                                //Status Update..
                                //Mon..
                                if ($MonDuration != "00:00" and $MonDuration != "") {
                                    $TsupdArr_Status["mon_project_status"] = "submitted";
                                    $TsupdArr_Status["mon_status"] = "submitted";
                                }
                                //Tue..
                                if ($TueDuration != "00:00" and $TueDuration != "") {
                                    $TsupdArr_Status["tue_project_status"] = "submitted";
                                    $TsupdArr_Status["tue_status"] = "submitted";
                                }
                                //web..
                                if ($WedDuration != "00:00" and $WedDuration != "") {
                                    $TsupdArr_Status["wed_project_status"] = "submitted";
                                    $TsupdArr_Status["wed_status"] = "submitted";
                                }
                                //thu..
                                if ($ThuDuration != "00:00" and $ThuDuration != "") {
                                    $TsupdArr_Status["thu_project_status"] = "submitted";
                                    $TsupdArr_Status["thu_status"] = "submitted";
                                }
                                //fri..
                                if ($FriDuration != "00:00" and $FriDuration != "") {
                                    $TsupdArr_Status["fri_project_status"] = "submitted";
                                    $TsupdArr_Status["fri_status"] = "submitted";
                                }
                                //sat..
                                if ($SatDuration != "00:00" and $SatDuration != "") {
                                    $TsupdArr_Status["sat_project_status"] = "submitted";
                                    $TsupdArr_Status["sat_status"] = "submitted";
                                }
                                //sun..
                                if ($SunDuration != "00:00" and $SunDuration != "") {
                                    $TsupdArr_Status["sun_project_status"] = "submitted";
                                    $TsupdArr_Status["sun_status"] = "submitted";
                                }
                                $updResp = $this->timesheetmodel->UpdateTableData("tm_ts_status", $tsCondWhereStatusArr, $TsupdArr_Status);
                            }
                        endif;
                    } else {
                        //Update..
                        $TsupdArr = array("sun_duration" => $SunDuration, "mon_duration" => $MonDuration, "tue_duration" => $TueDuration, "wed_duration" => $WedDuration, "thu_duration" => $ThuDuration, "fri_duration" => $FriDuration, "sat_duration" => $SatDuration, "week_duration" => $this->AddPlayTime($timeAddSum), "modified_by" => $LoginUserId, "modified" => $currentDateTime);
                        $updResp = $this->timesheetmodel->UpdateTableData("tm_emp_timesheets", $tsCondWhereArr, $TsupdArr);
                        //Time Sheet Status Update Cond.. 2
                        //Status Update.. 2
                        //Mon..
                        if ($MonDuration != "00:00" and $MonDuration != "") {
                            $TsupdArr2["mon_project_status"] = "submitted";
                            $TsupdArr2["mon_status"] = "submitted";
                        }
                        //Tue..
                        if ($TueDuration != "00:00" and $TueDuration != "") {
                            $TsupdArr2["tue_project_status"] = "submitted";
                            $TsupdArr2["tue_status"] = "submitted";
                        }
                        //web..
                        if ($WedDuration != "00:00" and $WedDuration != "") {
                            $TsupdArr2["wed_project_status"] = "submitted";
                            $TsupdArr2["wed_status"] = "submitted";
                        }
                        //thu..
                        if ($ThuDuration != "00:00" and $ThuDuration != "") {
                            $TsupdArr2["thu_project_status"] = "submitted";
                            $TsupdArr2["thu_status"] = "submitted";
                        }
                        //fri..
                        if ($FriDuration != "00:00" and $FriDuration != "") {
                            $TsupdArr2["fri_project_status"] = "submitted";
                            $TsupdArr2["fri_status"] = "submitted";
                        }
                        //sat..
                        if ($SatDuration != "00:00" and $SatDuration != "") {
                            $TsupdArr2["sat_project_status"] = "submitted";
                            $TsupdArr2["sat_status"] = "submitted";
                        }
                        //sun..
                        if ($SunDuration != "00:00" and $SunDuration != "") {
                            $TsupdArr2["sun_project_status"] = "submitted";
                            $TsupdArr2["sun_status"] = "submitted";
                        }
                        $updResp = $this->timesheetmodel->UpdateTableData("tm_ts_status", $tsCondWhereStatusArr, $TsupdArr2);
                    }
                endif;
            }
            if (($insert_id) || ($updResp)):
                $this->session->set_flashdata('successmsg', 'Week work Details Submitted successfully.');
            else:
                $this->session->set_flashdata('errormsg', 'Something went wrong');
            endif;
        }
        //Time Sheet Notes.. Save & Submit Both..
        //Time Sheet Notes Section..
        $TsNotesWhereArr = ["is_active" => '1', "emp_id" => $LoginUserId, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek']];
        $TsNotesRows = $this->ChkTsNotes_existing($TsNotesWhereArr);
        if ($TsNotesRows < 1) {
            //Ts Notes Inser Query ..
            $insertNotesArr = array("emp_id" => $LoginUserId, "ts_year" => $_REQUEST['thisYear'], "ts_month" => $_REQUEST['thismonth'], "ts_week" => $_REQUEST['MonthWeek'], "cal_week" => $_REQUEST['yearWeek'], "sun_date" => $weekDateRangeArr[6], "sun_note" => $this->input->post("dnote_sun"), "sun_reject_note" => "", "mon_date" => $weekDateRangeArr[0], "mon_note" => $this->input->post("dnote_mon"), "mon_reject_note" => "", "tue_date" => $weekDateRangeArr[1], "tue_note" => $this->input->post("dnote_tue"), "tue_reject_note" => "", "wed_date" => $weekDateRangeArr[2], "wed_note" => $this->input->post("dnote_wed"), "wed_reject_note" => "", "thu_date" => $weekDateRangeArr[3], "thu_note" => $this->input->post("dnote_thu"), "thu_reject_note" => "", "fri_date" => $weekDateRangeArr[4], "fri_note" => $this->input->post("dnote_fri"), "fri_reject_note" => "", "sat_date" => $weekDateRangeArr[5], "sat_note" => $this->input->post("dnote_sat"), "sat_reject_note" => "", "week_note" => $this->input->post("weeklynote"), "created_by" => $LoginUserId, "modified_by" => $LoginUserId, "is_active" => "1", "created" => $currentDateTime, "modified" => $currentDateTime);
            $this->db->insert("tm_emp_ts_notes", $insertNotesArr);
        } else {
            //Ts Notes Update Query ..
            $updTsNotesArr = array("sun_note" => $this->input->post("dnote_sun"), "mon_note" => $this->input->post("dnote_mon"), "tue_note" => $this->input->post("dnote_tue"), "wed_note" => $this->input->post("dnote_wed"), "thu_note" => $this->input->post("dnote_thu"), "fri_note" => $this->input->post("dnote_fri"), "sat_note" => $this->input->post("dnote_sat"), "week_note" => $this->input->post("weeklynote"));
            $updResp = $this->timesheetmodel->UpdateTableData("tm_emp_ts_notes", $TsNotesWhereArr, $updTsNotesArr);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    //Save Update Extra Activities..
    public function save_extra_actvt() {
        $LoginUserId = $this->session->userdata('loginid');
        $inserterArr = Array("emp_id" => $LoginUserId,
            "project_id" => $_REQUEST['ext_project_id'],
            "week_no" => $this->weekOfMonth($_REQUEST['ext_actdate']),
            "year_week" => date("W", strtotime($_REQUEST['ext_actdate'])),
            "month_no" => date("m", strtotime($_REQUEST['ext_actdate'])),
            "Year" => date("Y", strtotime($_REQUEST['ext_actdate'])),
            "entry_date" => $_REQUEST['ext_actdate'],
            "working_hour" => $_REQUEST['ext_duration'],
            "details" => $_REQUEST['ext_workdetails'],
            "created_by" => $LoginUserId,
            "action_by" => $LoginUserId);
        $this->db->insert("emp_projectanother_task_activities", $inserterArr);
        $tm_Status_insert_id = $this->db->insert_id();
        if ($tm_Status_insert_id):
            $this->session->set_flashdata('successmsg', 'Another Activity submitted Successfully.');
        endif;
        redirect($_SERVER['HTTP_REFERER']);
    }

    //Time Sheet Calender.. 05-03-2020
    public function calendar_timesheet() {
        $data['error'] = '';
        $data['title'] = "Timesheet Calendar Monthly View";
        $data['LeaveAllRecJson'] = $this->GetCalJsonLeaveVal();
        $data['TourAllRecJson'] = $this->GetCalJsonTourVal();
        $data['HoliDaysAllRecJsonPL'] = $this->GetCalJsonHoliDaysValPL();
        $data['HoliDaysAllRecJson'] = $this->GetCalJsonHoliDaysVal();

        $data['TsMonRecJson'] = $this->GetAllTimeSheetJSON_Mon();
        $data['TsTueRecJson'] = $this->GetAllTimeSheetJSON_Tue();
        $data['TsWedRecJson'] = $this->GetAllTimeSheetJSON_Wed();
        $data['TsThuRecJson'] = $this->GetAllTimeSheetJSON_Thu();
        $data['TsFriRecJson'] = $this->GetAllTimeSheetJSON_Fri();
        $data['TsSatRecJson'] = $this->GetAllTimeSheetJSON_Sat();
        $data['TsSunRecJson'] = $this->GetAllTimeSheetJSON_Sun();

        $this->load->view("timesheet/timesheet_calendar_view", $data);
    }

    //All Leave JSON ..
    public function GetCalJsonLeaveVal() {
        $leaveDayArr = array("1" => "Full-Day", "2" => "Half-Day", "3" => "Short L");
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.from_date as start,leaveday, DATE_ADD(a.to_date, INTERVAL 1 DAY) as end ");
        $this->db->from("main_leaverequest_summary as a");
        $this->db->where(["a.isactive" => "1", "a.user_id" => $LoginUserId]);
        $this->db->where('a.leavestatus !=', "Cancel");
        $this->db->order_by("a.id", "DESC");
        $RecordArr = $this->db->get()->result();

        $RecArr = array();
        if ($RecordArr) {
            foreach ($RecordArr as $reCd) {
                $reCd->title = "Leave " . $leaveDayArr[$reCd->leaveday];
                $RecArr[] = $reCd;
            }
        }
        $JsonString = json_encode($RecordArr, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return ($JsonString3) ? $JsonString3 : '{}';
    }

    //All Tour Json..
    public function GetCalJsonTourVal() {
        $loginID = $this->session->userdata('loginid');
        $this->db->select('start_date as start,DATE_ADD(end_date, INTERVAL 1 DAY) as end');
        $this->db->from('emp_tourapply');
        $this->db->where(array('is_active' => '1', 'emp_id' => $loginID));
        $this->db->where("approved_bypmanager !=", "2");
        $this->db->where("approved_bylmanager !=", "2");
        $TourReqList = $this->db->get()->result();
        //Loop Cond..
        $RecordArr = array();
        //All Leaves Dates..
        if ($TourReqList) {
            foreach ($TourReqList as $TourRow) {
                $TourRow->title = "Tour";
                $TourRow->color = "#965555";
                $RecordArr[] = $TourRow;
            }
        }
        $JsonString = json_encode($RecordArr, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return ($JsonString3) ? $JsonString3 : '{}';
    }

    //All HoliDays Arr JSON.. RH
    public function GetCalJsonHoliDaysVal() {
        $this->db->select('holidaydate as start,holidayname as title');
        $this->db->from('main_holidaydates as a');
        $this->db->where(array('a.isactive' => '1', 'a.groupid' => '2'));
        $HoliDayReqList = $this->db->get()->result();
        $RecordArr = array();
        if ($HoliDayReqList) {
            foreach ($HoliDayReqList as $HolidRow) {
                $HolidRow->color = "#d1cc69";
                $HolidRow->title = $HolidRow->title . " (RH)";
                $RecordArr[] = $HolidRow;
            }
        }
        $JsonString = json_encode($RecordArr, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return ($JsonString3) ? $JsonString3 : '{}';
    }

    //All HoliDays Arr JSON.. PL..
    public function GetCalJsonHoliDaysValPL() {
        $this->db->select('holidaydate as start,holidayname as title');
        $this->db->from('main_holidaydates as a');
        $this->db->where(array('a.isactive' => '1', 'a.groupid' => '1'));
        $HoliDayReqList = $this->db->get()->result();
        $RecordArr = array();
        if ($HoliDayReqList) {
            foreach ($HoliDayReqList as $HolidRow) {
                $HolidRow->color = "#49c5b6";
                $RecordArr[] = $HolidRow;
            }
        }
        $JsonString = json_encode($RecordArr, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return ($JsonString3) ? $JsonString3 : '{}';
    }

    //################################# Master Function User ##############################
    //Global Function .. $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    //Time Sheet Sum Duration By Date,, MON
    public function TimeSheetSumDuration_Mon($Date_mon_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.mon_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.mon_date" => $Date_mon_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->mon_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "00:00";
    }

    //Time Sheet Sum Duration By Date,, Tue
    public function TimeSheetSumDuration_Tue($Date_tue_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.tue_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.tue_date" => $Date_tue_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->tue_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //Time Sheet Sum Duration By Date,, Wed
    public function TimeSheetSumDuration_Wed($Date_wed_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.wed_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.wed_date" => $Date_wed_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->wed_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //Time Sheet Sum Duration By Date,, Thu
    public function TimeSheetSumDuration_Thu($Date_thu_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.thu_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.thu_date" => $Date_thu_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->thu_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //Time Sheet Sum Duration By Date,, Fri
    public function TimeSheetSumDuration_Fri($Date_fri_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.fri_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.fri_date" => $Date_fri_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->fri_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //Time Sheet Sum Duration By Date,, Sat
    public function TimeSheetSumDuration_Sat($Date_sat_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.sat_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.sat_date" => $Date_sat_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->sat_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //Time Sheet Sum Duration By Date,, Sun
    public function TimeSheetSumDuration_Sun($Date_sun_duration) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.sun_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId, "a.sun_date" => $Date_sun_duration]);
        $reCordArr = $this->db->get()->result();
        $allhh_mmArr = array();
        if ($reCordArr) {
            foreach ($reCordArr as $ReDs) {
                $allhh_mmArr[] = $ReDs->sun_duration;
            }
        }
        $resultHourVal = $this->AddPlayTime($allhh_mmArr);
        return ($resultHourVal) ? $resultHourVal : "";
    }

    //////////%%%%%%%%%%%%%%%%%%%%%%%%% Close Global Function %%%%%%%%%%%%%%%
    //()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
//Ts...... Table Exist or Not...
    public function ChkTs_existing($TsWhereArr) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select('id');
        $this->db->where($TsWhereArr);
        $query = $this->db->get('tm_emp_timesheets');
        $NumRows = $query->num_rows();
        return ($NumRows) ? $NumRows : null;
    }

    //Ts Status...... Table Exist or Not...
    public function ChkTsStatus_existing($TsWhereArr) {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select('id');
        $this->db->where($TsWhereArr);
        $query = $this->db->get('tm_ts_status');
        $NumRows = $query->num_rows();
        return ($NumRows) ? $NumRows : null;
    }

    //Ts......tm_emp_ts_notes Table Exist or Not...
    public function ChkTsNotes_existing($TsWhereArr) {
        $this->db->select('id');
        $this->db->where($TsWhereArr);
        $query = $this->db->get('tm_emp_ts_notes');
        $NumRows = $query->num_rows();
        return ($NumRows) ? $NumRows : null;
    }

    public function weekOfMonth($date) {
        $firstOfMonth = date("Y-m-01", strtotime($date));
        return intval(date("W", strtotime($date))) - intval(date("W", strtotime($firstOfMonth)));
    }

    public function rangeMonth($datestr) {
        $dt = strtotime($datestr);
        return array(
            "start" => date('Y-m-d', strtotime('first day of this month', $dt)),
            "end" => date('Y-m-d', strtotime('last day of this month', $dt))
        );
    }

    public function rangeWeek($datestr) {
        $dt = strtotime($datestr);
        return array(
            "start" => date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt)),
            "end" => date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('this sunday', $dt))
        );
    }

//################################## 25-02-2020 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//@@@@@@@@@@@@@%%%%%%%%%%%%&&&&&&&&&&&&&&&@@@@@@@@@@@@@@@@@@()()()()()()()()()()()
//Convert Minute to hour
    public function minutetohour($nm, $lZ = true) { //tm = to military (time), lZ = leading zero (if true it returns 510 as 08:30, if false 8:30
        $mins = $nm % 60;
        if ($mins == 0)
            $mins = "0$mins"; //adds a zero, so it doesn't return 08:0, but 08:00
        $hour = floor($nm / 60);
        if ($lZ) {
            if ($hour < 10)
                return "0$hour:$mins";
        }
        return "$hour:$mins";
    }

//Convert Hour To Minute
    public function hourtominute($v1, $v2) {
        $v1arr = explode(":", $v1);
        $hour = $v1arr[0];
        $minute = $v1arr[1];
//V2 pro
        $v1arr2 = explode(":", $v2);
        $hour2 = $v1arr2[0];
        $minute2 = $v1arr2[1];
        $htom = ($hour * 60);
        $htom = $htom + $minute;
        $htom2 = ($hour2 * 60);
        $htom2 = $htom2 + $minute2;
        return array('0' => $htom,
            '1' => $htom2);
    }

//Dates Between Two Dates..
    public function createDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

//Important Function..
    public function createDateRangeMonthArray($strDateFrom, $strDateTo) {
        $aryRange = array();
        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));
        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 2592000; // add 24 hours
                array_push($aryRange, date('Y-m', $iDateFrom));
            }
        }
        array_pop($aryRange);
        $result = array_unique($aryRange);
        return $result;
    }

//Sum Sultiple Hour Minute Val..
    public function AddPlayTime($times) {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }
        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;
        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    public function getStartAndEndDate($week, $year) {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('d-m-Y');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('d-m-Y');
        return $ret;
    }

    //##############################################
    public function test() {
        $this->load->helper('date');
        echo "GT";
        echo $this->weeks(10, 2020) . "<br/>";
        //  echo $this->weeks(9,2020)."<br/>";
    }

    public function weeks($month, $year) {
        $num_of_days = date("t", mktime(0, 0, 0, $month, 1, $year));
        $lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
        $no_of_weeks = 0;
        $count_weeks = 0;
        while ($no_of_weeks < $lastday) {
            $no_of_weeks += 7;
            $count_weeks++;
        }
        return $count_weeks;
    }

    //Get All Extra Activities ...
    public function GetAllExtraActivitiesTimesheetByUserID() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select('a.working_hour,a.entry_date,a.details,a.approved_bypmanager,b.project_name');
        $this->db->from("emp_projectanother_task_activities as a");
        $this->db->join("tm_projects as b", "a.project_id=b.id", "LEFT");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $LoginUserId]);
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("20");

        $AnotherTsRecArr = $this->db->get()->result();
        return ($AnotherTsRecArr) ? $AnotherTsRecArr : null;
    }

    //Get All Time Sheet Feel Details..
    //Mon..
    public function GetAllTimeSheetJSON_Mon() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.mon_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("650");
        $TimeSheetArr = $this->db->get()->result();
        $allMonArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allMonArr[] = $rOwS->mon_date;
            }
        }
        //Layer 2..
        $allMonArr2 = array_unique($allMonArr);
        //Layer 3..
        $FinalRecArr_Mon = array();
        @$singleArr = array();
        if ($allMonArr2) {
            foreach ($allMonArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Mon($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Mon[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Mon, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Tue..
    public function GetAllTimeSheetJSON_Tue() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.tue_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allTueArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allTueArr[] = $rOwS->tue_date;
            }
        }
        //Layer 2..
        $allTueArr2 = array_unique($allTueArr);
        //Layer 3..
        $FinalRecArr_Tue = array();
        @$singleArr = array();
        if ($allTueArr2) {
            foreach ($allTueArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Tue($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Tue[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Tue, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Wed..
    public function GetAllTimeSheetJSON_Wed() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.wed_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allWedArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allWedArr[] = $rOwS->wed_date;
            }
        }
        //Layer 2..
        $allWedArr2 = array_unique($allWedArr);
        //Layer 3..
        $FinalRecArr_Wed = array();
        @$singleArr = array();
        if ($allWedArr2) {
            foreach ($allWedArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Wed($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Wed[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Wed, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Thu..
    public function GetAllTimeSheetJSON_Thu() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.thu_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allThuArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allThuArr[] = $rOwS->thu_date;
            }
        }
        //Layer 2..
        $allThuArr2 = array_unique($allThuArr);
        //Layer 3..
        $FinalRecArr_Thu = array();
        @$singleArr = array();
        if ($allThuArr2) {
            foreach ($allThuArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Thu($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Thu[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Thu, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Fri..
    public function GetAllTimeSheetJSON_Fri() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.fri_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allFriArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allFriArr[] = $rOwS->fri_date;
            }
        }
        //Layer 2..
        $allFriArr2 = array_unique($allFriArr);
        //Layer 3..
        $FinalRecArr_Fri = array();
        @$singleArr = array();
        if ($allFriArr2) {
            foreach ($allFriArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Fri($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Fri[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Fri, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Sat..
    public function GetAllTimeSheetJSON_Sat() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.sat_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allSatArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allSatArr[] = $rOwS->sat_date;
            }
        }
        //Layer 2..
        $allSatArr2 = array_unique($allSatArr);
        //Layer 3..
        $FinalRecArr_Sat = array();
        @$singleArr = array();
        if ($allSatArr2) {
            foreach ($allSatArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Sat($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Sat[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Sat, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

    //Sun..
    public function GetAllTimeSheetJSON_Sun() {
        $LoginUserId = $this->session->userdata('loginid');
        $this->db->select("a.sun_date");
        $this->db->from('tm_emp_timesheets as a');
        $this->db->where(array('a.is_active' => '1', 'a.emp_id' => $LoginUserId));
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("600");
        $TimeSheetArr = $this->db->get()->result();
        $allSunArr = array();
        if ($TimeSheetArr) {
            foreach ($TimeSheetArr as $kEy => $rOwS) {
                $allSunArr[] = $rOwS->sun_date;
            }
        }
        //Layer 2..
        $allSunArr2 = array_unique($allSunArr);
        //Layer 3..
        $FinalRecArr_Sun = array();
        @$singleArr = array();
        if ($allSunArr2) {
            foreach ($allSunArr2 as $KeYY => $rOws) {
                @$singleArr['color'] = "#de3d83";
                @$singleArr['title'] = "TS : " . $this->TimeSheetSumDuration_Sun($rOws);
                @$singleArr['start'] = $rOws;
                $weekMonth = $this->weekOfMonth($rOws);
                $weekMonth = ($weekMonth == "0") ? "1" : $weekMonth;
                @$singleArr['url'] = base_url("timesheet_entry/" . $weekMonth . "/" . date("m", strtotime($rOws)) . "/" . date("Y", strtotime($rOws)));
                $FinalRecArr_Sun[] = $singleArr;
            }
        }
        $JsonString = json_encode($FinalRecArr_Sun, true);
        $JsonString2 = str_replace("[", "", $JsonString);
        $JsonString3 = str_replace("]", "", $JsonString2);
        return $JsonString3;
    }

}
