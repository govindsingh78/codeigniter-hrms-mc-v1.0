<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisal_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->model('Appraisal_new_model', 'appraisalmodel');
        $this->load->model('Edititrprofiledetail_model', 'eipm');
        $this->load->model('Appraisal_model', 'appraisal');
        $this->load->model('Appraisalio_model', 'appraisalio');
        $this->load->model('Appraisalreporthr_model', 'appraisalreport');
        $this->load->model('Appraisalreporthrcegth_model', 'appraisalreportcegth');
        $this->load->model('Appraisalreportajax_model', 'appraisalreportajax');
        $this->load->model('Appraisalreportajaxcegth_model', 'appraisalreportajaxcegth');

        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    //Step 1 Appraisal..
    public function index() {
        $data['error'] = '';
        $loginID = $this->session->userdata('loginid');
        $data['title'] = "My Appraisal List Details";
        $data['ApprRecListArr'] = $this->appraisalmodel->GetBasicRecLoginUser();
        //Check If Assign Project Office Appraisal..
        $data['AssignProj_Appraisal'] = $this->Check_AppraisalForm_AssignorNot_Project();
        $this->load->view("appraisal/appraisal_stepone_view", $data);
    }

    public function Check_AppraisalForm_AssignorNot_Project() {
        //Check If Assign Project Office Appraisal..
        $loginID = $this->session->userdata('loginid');
        $this->db->select("a.*");
        $this->db->from("appr_proj_assignbyhr as a");
        $this->db->where(["a.status" => "1", "a.appr_session" => "2020", "a.rep_manager_id" => $loginID]);
        $ReDsArr = $this->db->get()->row();
        return ($ReDsArr) ? $ReDsArr : null;
    }

    //Fill Appraisal Form By Employee .. 
    //Appraisal Form..
    public function appraisal_index($actid) {
        $data['error'] = '';
        $data['title'] = "Manage Appraisal Form";
        $id = $this->session->userdata('loginid');
        $data['projectListArr'] = $this->ts_projectList($id);
        $data["apprSingleRowDetail"] = $this->getSingleRowDetailsByID($actid);
        $data['userBasicDetailsArr'] = $this->EmpDetailsByUserID($id);
        $this->load->view('appraisal/appraisal_view', $data);
    }

    //Employee Details By Employee ID..
    public function EmpDetailsByUserID($userID) {
        $this->db->select('a.profileimg,a.user_id,a.prefix_name,a.userfullname,a.date_of_joining,a.department_name,a.position_name,a.reporting_manager_name,c.prefix_name as romngr_prefix_name,c.userfullname as romngr_userfullname');
        $this->db->from('main_employees_summary as a');
        $this->db->join('emp_otherofficial_data as b', "a.user_id=b.user_id", "LEFT");
        $this->db->join('main_employees_summary as c', "b.reviewing_officer_ro=c.user_id", "LEFT");
        $this->db->where(["a.user_id" => $userID, "a.isactive" => "1"]);
        $RecArr = $this->db->get()->row();
        return ($RecArr) ? $RecArr : null;
    }

    //Get Employee Lock Status..
    public function EmplProfLockStatus() {
        $userID = $this->session->userdata('loginid');
        $this->db->select('update_perm_lock');
        $this->db->from('temp_hr_official_details');
        $this->db->where(["user_id" => $userID, "status" => "1"]);
        $RecArr = $this->db->get()->row();
        return ($RecArr) ? $RecArr->update_perm_lock : "";
    }

    //Check Assigned or Not Appraisal Id For Particular User..
    public function assignappraisal($actID) {
        $login_user_id = $this->session->userdata('loginid');
        $this->db->where(array("emp_id" => $login_user_id, "status" => "1", "id" => $actID));
        return $this->db->count_all_results('appr_mail_appraisal');
    }

    //Assign / Fill On Project Project List By User_id..
    public function ts_projectList($mplId) {
        $this->db->select('a.project_id,b.project_name');
        $this->db->from('tm_emp_timesheets as a');
        $this->db->join('tm_projects as b', "a.project_id=b.id", "LEFT");
        $this->db->where(array("a.is_active" => "1", "a.ts_year" => "2019", "a.emp_id" => $mplId));
        $this->db->group_by("a.project_id");
        $ProjListArr = $this->db->get()->result();
        return ($ProjListArr) ? $ProjListArr : null;
    }

    //Get Single Row Details By ID..
    public function getSingleRowDetailsByID($rowID) {
        $Userid = $this->session->userdata('loginid');
        $this->db->SELECT('a.*,b.cycle_date,c.day_diff_user');
        $this->db->FROM('appr_mail_appraisal as a');
        $this->db->join('appr_increment_cycle_master as b', "a.appraisal_cycle_id=b.fld_id", "LEFT");
        $this->db->join('appr_auto_lockdate as c', "a.appraisal_cycle_id=c.appr_cycle_id", "LEFT");
        $this->db->WHERE(array('a.emp_id' => $Userid, "a.status" => "1", "a.id" => $rowID));
        $recArr = $this->db->get()->row();
        return $recArr;
    }

    //Total No of Late Days in This Financial Year...
    public function TotalNoLateDaysCount($userID) {
        //$userID = '297';
        $fromDate = "2019-04-01";
        $toDate = "2020-01-30";

        $StartFinmmYY = "2019-04";
        $EndFinmmYY = "2020-01";
        $TotalOnLeaveEmp = $this->GetLeaveDayCountTotal($userID, $fromDate, $toDate);
        $TotalTsFillHours = $this->GetFillTsHour($userID, $StartFinmmYY, $EndFinmmYY);
        $totWorkingDaysPresentDay = $this->GetTotWorkingDaysPresentDay($userID);
        $allInTimeOutTimeArr = $this->GetInTimeOutArr($userID);


        $LaTeTotalMinute = 0;
        if ($allInTimeOutTimeArr) {
            foreach ($allInTimeOutTimeArr as $rowAttPunch):
                $FInTim = date("h:i:s a", strtotime($rowAttPunch->FirstIn));
                $FOutTim = date("h:i:s a", strtotime($rowAttPunch->LastOut));
                $echoV = date("Y-m-d", strtotime($rowAttPunch->FirstIn));
                $dAyNm = date('D', strtotime($echoV));

                $intmL = '09:30:00 am';
                $intmL2 = '10:31:00 am';
                if ((strtotime($FInTim) > strtotime($intmL)) and ( strtotime($FInTim) < strtotime($intmL2)) and ( $dAyNm != "Sun") and ( $dAyNm != "Sat")) {
                    $FIn7 = date("h:i:s a", strtotime($rowAttPunch->FirstIn));
                    $FInTim = strtotime($FIn7);
                    $FOutTim = strtotime("09:30:00 am");
                    $LaTeTotalMinute += round(abs($FOutTim - $FInTim) / 60, 2);
                }
            endforeach;
        }
        $reTurArr = array("TotalOnLeaveEmp" => $TotalOnLeaveEmp, "TotalTsFillHours" => $TotalTsFillHours, "totWorkingDaysPresentDay" => $totWorkingDaysPresentDay, "TotalMinuteLate" => $LaTeTotalMinute);
        return ($reTurArr) ? $reTurArr : null;
    }

    //Get Leave Day Count Total In Financial Date...
    public function GetLeaveDayCountTotal($userID, $fromDate, $toDate) {
        $this->db->select_sum("appliedleavescount");
        $this->db->from("main_leaverequest_summary");
        $this->db->where(array("user_id" => $userID, "isactive" => "1"));
        $where_date = "(from_date>='$fromDate' AND to_date <= '$toDate')";
        $this->db->where($where_date);
        $LeaveStatus_where = "(leavestatus!='Cancel' AND leavestatus!='Rejected')";
        $this->db->where($LeaveStatus_where);
        $LeaveCountFinYear = $this->db->get()->row();
        return ($LeaveCountFinYear) ? $LeaveCountFinYear->appliedleavescount : "0";
    }

    //Get Total TS Fill Hour...
    public function count_totalhours($user_id, $project_id, $year, $month) {
        $this->db->select('sum(total_hours) as total_hours');
        $this->db->from('tm_emp_timesheet_detail');
        $where = array('emp_id' => $user_id, 'ts_year' => $year, 'ts_month' => $month, 'project_id' => $project_id);
        $this->db->where($where);
        $res = $this->db->get()->result_array();
        //echo '<pre>'; print_r($res);
        return ($res) ? $res[0] : false;
    }

//Total Ts Fill Hour...
    public function GetFillTsHour($userID, $StartFinmmYY, $EndFinmmYY) {
        $allAssignProjArr = $this->allassignProj($userID);
        $dateRanArr = $this->getDatesFromRange($StartFinmmYY, $EndFinmmYY);
        $totHour = 0;
        if ($dateRanArr) {
            foreach ($dateRanArr as $recDate) {
                $DcMonth = date("m", strtotime($recDate));
                $DcYear = date("Y", strtotime($recDate));
                if ($allAssignProjArr):
                    foreach ($allAssignProjArr as $prjRow) {
                        $projID = $prjRow->project_id;
                        //echo $projID;
                        $respTsHour = $this->count_totalhours($userID, $projID, $DcYear, $DcMonth);
                        $totHour += $respTsHour['total_hours'];
                    }
                endif;
            }
        }
        //Get Other Official Activity..
        $this->db->SELECT('b.working_hour');
        $this->db->FROM('emp_projectanother_task_activities as b');
        $this->db->WHERE(array('b.emp_id' => $userID, 'b.is_active' => '1'));
        $this->db->where("(b.approved_bypmanager='1' OR b.approved_bypmanager='0')", NULL, FALSE);
        $OtherTsArr = $this->db->get()->result();
        $working_hour = 0;
        if ($OtherTsArr):
            foreach ($OtherTsArr as $TsreC):
                $working_hour += substr($TsreC->working_hour, 0, 2);
            endforeach;
        endif;
        return($totHour) ? ($totHour + $working_hour) : null;
    }

    //Date Range..
    public function getDatesFromRange($startDate, $endDate) {
        $return = array($startDate);
        $start = $startDate;
        $i = 1;
        if (strtotime($startDate) < strtotime($endDate)) {
            while (strtotime($start) < strtotime($endDate)) {
                $start = date('Y-m', strtotime($startDate . '+' . $i . ' days'));
                $return[] = $start;
                $i++;
            }
        }
        return array_unique($return);
    }

    //Get Tot Working Days / Present Day Number...
    public function GetTotWorkingDaysPresentDay($userID) {
        $thumCode = thumbCodeById($userID);
        $query = $this->db->query("SELECT STR_TO_DATE(FirstIn, '%m/%d/%Y') FROM thumb_attendance WHERE (STR_TO_DATE(FirstIn, '%m/%d/%Y')>'2019-04-01' AND STR_TO_DATE(FirstIn, '%m/%d/%Y')<'2019-09-30') AND EmployeeID='$thumCode' and `Status`='1'");
        $totWorkDays = $query->num_rows();
        return ($totWorkDays) ? $totWorkDays : "0";
    }

    //Get Tot Working Days / Present Day Number...
    public function GetInTimeOutArr($userID) {
        $thumCode = thumbCodeById($userID);
        $query = $this->db->query("SELECT FirstIn,LastOut FROM thumb_attendance WHERE (STR_TO_DATE(FirstIn, '%m/%d/%Y')>'2019-04-01' AND STR_TO_DATE(FirstIn, '%m/%d/%Y')<'2019-09-30') AND EmployeeID='$thumCode' and `Status`='1'");
        $totWorkDays = $query->result();
        return ($totWorkDays) ? $totWorkDays : "0";
    }

    //All Assign Project,,,...
    public function allassignProj($userID) {
        $this->db->select('a.project_id');
        $this->db->from('tm_project_employees as a');
        $this->db->where(array('a.emp_id' => $userID, 'a.is_active' => '1'));
        $ProjIdArr = $this->db->get()->result();
        return ($ProjIdArr) ? $ProjIdArr : false;
    }

    public function appraisalsave() {
        //Table 1 Insert in Master...
        $allSubCategMaster = AllAppraisalMasterSubcat();
        $project_task_row2 = $_REQUEST['project_task_row2'];
        $actvperform_row2 = $_REQUEST['actvperform_row2'];
        $projorother_row = $_REQUEST['proj_other'];

        $insrtArr = array('user_id' => $this->session->userdata('loginid'),
            'quest_answ_1' => $this->input->post('quest1'),
            'quest_answ_2' => $this->input->post('quest2'),
            'quest_answ_3' => $this->input->post('quest3'),
            'appraisal_cycle_id' => $this->input->post('actionID'));
        $this->db->insert('appr_action_master', $insrtArr);
        $LastretId = $this->db->insert_id();
        //Section 1...
        if (($allSubCategMaster) and ( $LastretId)) {
            foreach ($allSubCategMaster as $rowM) {
                $rec11 = @$_REQUEST['performance_rating_self_' . $rowM->fld_id];
                if ($rec11) {
                    $iNsErtArr = array('emp_id' => $this->session->userdata('loginid'),
                        'appr_parent_id' => $LastretId,
                        'perform_fact_masterid' => $rowM->fld_id, 'perform_fact_rating' => $rec11);
                    $this->db->insert('appr_performfactor', $iNsErtArr);
                }
            }
        }
        //Section 2..
        if (($project_task_row2) and ( $LastretId)) {
            foreach ($project_task_row2 as $kr2 => $rRow2):
                if ($rRow2):
                    //echo $rRow2."<br>";
                    $insertRow3 = array(
                        'emp_id' => $this->session->userdata('loginid'),
                        'appr_parent_id' => $LastretId,
                        'project_special_taskassign' => $project_task_row2[$kr2],
                        'proj_other' => $projorother_row[$kr2],
                        'activ_perform' => $actvperform_row2[$kr2]);
                    $this->db->insert('appr_perform_poten_eval', $insertRow3);
                endif;
            endforeach;
        }
        if ($LastretId) {
            $this->session->set_flashdata('successmsg', 'Appraisal Details submitted successfully.');
        } else {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
        }
        redirect(base_url('myappraisal'));
    }

    //Edit By User Appraisal...
    public function editappraisalbyuser($edtID) {
        $Userid = $this->session->userdata('loginid');
        $data["error"] = "";
        $data['title'] = "Edit/View/Update My Appraisal Form Details";
        $data['singleRowData'] = $this->GetApprDetailsById($edtID);
        if ($data['singleRowData']["singleBaseRec"]->emp_self_lock) {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
            redirect(base_url('myappraisal'));
        }
        //Secure Cond..
        if ($data['singleRowData']["singleBaseRec"]->user_id != $Userid) {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
            redirect(base_url('myappraisal'));
        }

        $this->load->view('appraisal/edit_appraisalbyuser_view', $data);
    }

    public Function GetApprDetailsById($fldID) {
        $this->db->SELECT('a.*,b.userfullname,b.date_of_joining,b.department_name,b.position_name,b.reporting_manager_name as rouserfullname,d.userfullname as iouserfullname,e.profileimg,bb.emp_grace_period,bb.emp_last_date,cc.cycle_date,dd.day_diff_user,dd.day_diff_io,dd.day_diff_ro');
        $this->db->FROM('appr_action_master as a');
        $this->db->WHERE(array('a.fld_id' => $fldID, 'a.status' => '1'));
        $this->db->join("main_employees_summary as b", "a.user_id=b.user_id", "LEFT");
        $this->db->join("emp_otherofficial_data as c", "a.user_id=c.user_id", "LEFT");
        $this->db->join("main_employees_summary as d", "d.user_id=c.reviewing_officer_ro", "LEFT");
        $this->db->join("main_users as e", "a.user_id=e.id", "LEFT");
        $this->db->join('appr_mail_appraisal as bb', "a.appraisal_cycle_id=bb.appraisal_cycle_id", "LEFT");
        $this->db->join('appr_increment_cycle_master as cc', "a.appraisal_cycle_id=cc.fld_id", "LEFT");
        $this->db->join('appr_auto_lockdate as dd', "a.appraisal_cycle_id=dd.appr_cycle_id", "LEFT");
        $ReturnArr = $this->db->get()->row();
        $reTurnFinal = array();
        $reTurnFinal['singleBaseRec'] = $ReturnArr;

        //Row 2 Get Rec..
        $this->db->SELECT('a.*');
        $this->db->FROM('appr_perform_poten_eval as a');
        $this->db->WHERE(array('a.appr_parent_id' => $fldID, 'a.status' => '1'));
        $reTurnFinal['RecRow2'] = $this->db->get()->result();
        // Row 3 Get Rec..
        $this->db->SELECT('a.*');
        $this->db->FROM('appr_performfactor as a');
        $this->db->WHERE(array('a.appr_parent_id' => $fldID, 'a.status' => '1'));
        $reTurnFinal['RecRow3'] = $this->db->get()->result();
        return $reTurnFinal;
    }

    //Self Update...
    public function apprsaveupdateas_self() {
        $masterfld_id = $this->input->post('masterfld_id');
        //2..
        if ($this->input->post('chkagree3')) {
            foreach ($this->input->post('chkagree3') as $rOwR3) {
                @$perform_fact_rating = @$_REQUEST['performance_rating_self_' . $rOwR3];
                $updData3 = array('perform_fact_rating' => $perform_fact_rating);
                $this->db->where(array('fld_id' => $rOwR3, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_performfactor', $updData3);
            }
        }
        if ($this->input->post('chkagree2')) {
            foreach ($this->input->post('chkagree2') as $rOwR2) {
                $project_special_taskassign = $_REQUEST['project_special_taskassign_' . $rOwR2];
                $activ_perform = $_REQUEST['activ_perform_' . $rOwR2];
                $updData2 = array('project_special_taskassign' => $project_special_taskassign, 'activ_perform' => $activ_perform);
                $this->db->where(array('fld_id' => $rOwR2, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_perform_poten_eval', $updData2);
            }
        }
        if ($masterfld_id) {
            $updData7 = array('quest_answ_1' => $this->input->post('quest1'),
                'quest_answ_2' => $this->input->post('quest2'),
                'quest_answ_3' => $this->input->post('quest3'));
            $this->db->where(array('fld_id' => $masterfld_id));
            $this->db->update('appr_action_master', $updData7);
        }
        if ($masterfld_id) {
            $this->session->set_flashdata('successmsg', 'Appraisal Details Updated successfully.');
        }
        redirect(base_url("editappraisalbyuser/" . $masterfld_id));
    }

    //Row Increase of "Performance and Potential Evaluation";
    public function appr_numrowsincre() {
        if ($_REQUEST) {
            $NumRows = $this->input->post('numrowss');
            for ($i = 1; $i <= $NumRows; $i++) {
                $inserT = array("emp_id" => $this->input->post('user_id'), "appr_parent_id" => $this->input->post('masterfld_id'));
                $this->db->insert('appr_perform_poten_eval', $inserT);
            }
            $this->session->set_flashdata('successmsg', 'Performance and Potential Evaluation: Row Added successfully.');
        }
        redirect(base_url("editappraisalbyuser/" . $this->input->post('masterfld_id')));
    }

    //Delete Single Row...
    public function deletesinglerow() {
        $delID = $this->uri->segment(2);
        $masterID = $this->uri->segment(3);
        if ($delID && $masterID) {
            $updData2 = array('status' => "0");
            $this->db->where(array('fld_id' => $delID));
            $this->db->update('appr_perform_poten_eval', $updData2);
            $this->session->set_flashdata('successmsg', 'Row Deleted successfully.');
        }
        redirect(base_url('editappraisalbyuser/' . $masterID));
    }

    // Get IO Name And Email By LoginId..
    public function GetIODetailsByLoginId() {
        $login_user_id = $this->session->userdata('loginid');
        $this->db->select('a.reporting_manager_name,a.prefix_name as prefix_name_emp,a.emailaddress as emailaddress_emp,a.userfullname as userfullname_emp,b.emailaddress as emailaddress_io,b.prefix_name as prefix_name_io,b.userfullname as userfullname_io');
        $this->db->from('main_employees_summary as a');
        $this->db->join('main_employees_summary as b', "a.reporting_manager=b.user_id", "LEFT");
        $this->db->where(array('a.user_id' => $login_user_id, 'a.isactive' => '1'));
        $querArr = $this->db->get()->row();
        return ($querArr) ? $querArr : false;
    }

    //View Self Appraisal After Lock...
    public function view_selfappraisal($ApprID) {
        $data["error"] = "";
        $data['title'] = "View My Appraisal Form Details";
        $data['singleRowData'] = $this->GetApprDetailsById($ApprID);
        $user_id = $data['singleRowData']['singleBaseRec']->user_id;
        $data['performRec'] = $this->TotalNoLateDaysCount($user_id);
        $this->load->view('appraisal/view_selfappraisal', $data);
    }

    //Employee Appraisal List..
    public function empappraisal_list() {
        error_reporting(0);
        $data = array();
        $data['title'] = "Team Appraisal List";
        $data['Emplylist'] = $this->appraisal->get_datatables();
        $this->load->view('appraisal/emplist_view', $data);
    }

    //View Edit Appraisal Details..
    public Function appraisalview($aprID) {
        $data = array();
        $data['title'] = "Employee Appraisal Form List";
        $RecArrbyID = $this->GetUserIDByApprId($aprID);
        $data['allAppraisal'] = $RecArrbyID;
        $id = $this->session->userdata('user_id');
        $this->db->select('mes.*');
        $this->db->from('main_employees_summary as mes');
        $this->db->where('mes.user_id', $id);
        $query = $this->db->get()->row_array();
        $data['apprisal_profile'] = ($query) ? $query : null;
        //Lock Condition..
        if (count($RecArrbyID) < 1) {
            $singleRowData = $this->GetApprDetailsById($aprID);
            //Cond For Auto Lock..
            $ToDaYdate = date("Y-m-d");
            $dayDiff = $singleRowData["singleBaseRec"]->day_diff_user;
            $LaStDate = $singleRowData["singleBaseRec"]->emp_last_date;
            $LockDate = date('Y-m-d', strtotime($LaStDate . "+$dayDiff days"));
            if (strtotime($LockDate) < strtotime($ToDaYdate)) {
                $this->db->where(array('fld_id' => $aprID));
                $UpdRespo = $this->db->update('appr_action_master', array('emp_self_lock' => "1"));
                redirect(base_url('appraisalview/' . $aprID));
            }
        }
        $this->load->view('appraisal/appraisalviewedit_view', $data);
    }

    //Get All Appraisal Details By Login Id..
    public Function GetUserIDByApprId($fldID) {
        $this->db->select('user_id');
        $this->db->from('appr_action_master');
        $this->db->where('fld_id', $fldID);
        $RecRow = $this->db->get()->row();
        if ($RecRow->user_id):
            $this->db->select('a.*,b.userfullname,b.reporting_manager_name,d.userfullname as iouserfullname,f.cycle_date');
            $this->db->from('appr_action_master as a');
            $this->db->join("main_employees_summary as b", "a.user_id=b.user_id", "LEFT");
            $this->db->join("emp_otherofficial_data as c", "a.user_id=c.user_id", "LEFT");
            $this->db->join("main_employees_summary as d", "d.user_id=c.reviewing_officer_ro", "LEFT");
            $this->db->join("appr_mail_appraisal as e", "a.appraisal_cycle_id=e.id", "LEFT");
            $this->db->join("appr_increment_cycle_master as f", "a.appraisal_cycle_id=f.fld_id", "LEFT");
            $this->db->where(array('a.user_id' => $RecRow->user_id, "a.emp_self_lock" => "1"));
            $this->db->order_by("a.fld_id", "DESC");

            $RECC = $this->db->get()->result();
            return ($RECC) ? $RECC : null;
        else:
            return null;
        endif;
    }

    //View Details A Single Row By IO...
    public Function appraisalviewdetails($fldID) {
        $id = $this->session->userdata('user_id');
        $data['title'] = "Employee Appraisal Form";
        $this->db->select('mes.*');
        $this->db->from('main_employees_summary as mes');
        $this->db->where('mes.user_id', $id);
        $query = $this->db->get()->row_array();
        $data['apprisal_profile'] = ($query) ? $query : null;
        //Single Employee Details By Id...
        $data['singleRowData'] = $this->GetApprDetailsById($fldID);
        //Auto Lock Proceess For IO..
        $ToDaYdate = date("Y-m-d");
        $dayDiff = $data['singleRowData']["singleBaseRec"]->day_diff_io;
        $LaStDate = $data['singleRowData']["singleBaseRec"]->emp_last_date;
        $LockDate = date('Y-m-d', strtotime($LaStDate . "+$dayDiff days"));

//        if (strtotime($LockDate) < strtotime($ToDaYdate)) {
//            $this->db->where(array('fld_id' => $fldID));
//            $UpdRespo = $this->db->update('appr_action_master', array('emp_io_lock' => "1"));
//            if ($UpdRespo) {
//                redirect(base_url('appraisalview/' . $fldID));
//            }
//        }
        //Condition Chk..
        if ($data["singleRowData"]["singleBaseRec"]->emp_io_lock == "1") {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
            redirect(base_url('appraisalview/' . $fldID));
        }
        $Duser_id = $data['singleRowData']["singleBaseRec"]->user_id;
        $this->load->view('appraisal/apprviewdetails_view', $data);
    }

    //Appraisal Save As IO Name Mistake...
    public function apprsaveupdateas_ro() {
        $masterfld_id = $this->input->post('masterfld_id');
        //Row 1 Section Updated close Code Section..
        //Row 2 Section Updated Start..
        if ($this->input->post('chkagree2')) {
            foreach ($this->input->post('chkagree2') as $rOwR2) {
                $Ro_row2 = $_REQUEST['royesorno2_' . $rOwR2];
                $updData2 = array('ro_yes_no' => $Ro_row2);
                $this->db->where(array('fld_id' => $rOwR2, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_perform_poten_eval', $updData2);
            }
        }
        if (@$this->input->post('final_ro_reviewer')) {
            $updData7 = array('final_ro_reviewer' => @$this->input->post('final_ro_reviewer'), 'interviewcond_date_io' => @$this->input->post('interviewcond_date_io'));
            $this->db->where(array('fld_id' => $masterfld_id));
            $this->db->update('appr_action_master', $updData7);
        }
        //Row 2 Section Updated close Code Section..
        //Row 3 Section Updated Start..
        if ($this->input->post('chkagree3')) {
            foreach ($this->input->post('chkagree3') as $rOwR3) {
                $Ro_row3 = $_REQUEST['perf_ratingro_' . $rOwR3];
                $Ro_remark = $_REQUEST['perf_remarkbyro_' . $rOwR3];
                $updData3 = array("ro_rating" => $Ro_row3, "ro_remark" => $Ro_remark);
                $this->db->where(array('fld_id' => $rOwR3, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_performfactor', $updData3);
            }
        }
        if ($masterfld_id) {
            $this->session->set_flashdata('successmsg', 'Appraisal Details Updated successfully.');
        }
        redirect(base_url("appraisalviewdetails/" . $masterfld_id));
    }

    //Get IO Details By TL..
    public function GetIODetailsByTeamId($login_user_id) {
        //$login_user_id = $this->session->userdata('user_id');
        $this->db->select('a.reporting_manager_name,a.prefix_name as prefix_name_emp,a.emailaddress as emailaddress_emp,a.userfullname as userfullname_emp,b.emailaddress as emailaddress_io,b.prefix_name as prefix_name_io,b.userfullname as userfullname_io,d.emailaddress as emailaddress_ro,d.prefix_name as prefix_name_ro,d.userfullname as userfullname_ro');
        $this->db->from('main_employees_summary as a');
        $this->db->join('main_employees_summary as b', "a.reporting_manager=b.user_id", "LEFT");
        $this->db->join('emp_otherofficial_data as c', "a.user_id=c.user_id", "LEFT");
        $this->db->join('main_employees_summary as d', "c.reviewing_officer_ro=d.user_id", "LEFT");
        $this->db->where(array('a.user_id' => $login_user_id, 'a.isactive' => '1'));
        $querArr = $this->db->get()->row();
        return ($querArr) ? $querArr : false;
    }

    //Final Lock By IO...
    public function appraisal_finalockbyio() {
        $lockID = base64_decode($_REQUEST['lockd']);
        if ($lockID) {
            $this->db->SELECT('ab.*');
            $this->db->from('appr_action_master as ab');
            $this->db->where(array('ab.fld_id' => $lockID, 'ab.status' => '1'));
            $DquerArr = $this->db->get()->row();
            $userID = $DquerArr->user_id;
        }
        $IOrecArr = $this->GetIODetailsByTeamId($userID);
        if ($lockID):
            $updData3 = array('emp_io_lock' => "1");
            $this->db->where(array('fld_id' => $lockID));
            $this->db->update('appr_action_master', $updData3);
            $this->session->set_flashdata('successmsg', 'Appraisal Details Locked successfully.');
            //Mail To RO..
            $data = array();
            $data["IOrecArr"] = $IOrecArr;
            $to = $IOrecArr->emailaddress_ro;
            // $to = "asheesh9308@gmail.com";
            $subject = "Team Appraisal Forms Pending with Reviewer (RO)";
            $msgDetails = $this->load->view('email/apprform_lockbyio_email', $data, true);
            $this->sendMail($to, $subject, $msgDetails);
        endif;
        redirect(base_url("appraisalview/" . $lockID));
    }

    public function employee_appraisal_io() {
        error_reporting(0);
        $id = $this->session->userdata('user_id');
        $this->db->select('mes.*');
        $this->db->from('main_employees_summary as mes');
        $this->db->where('mes.user_id', $id);
        $query = $this->db->get()->row_array();
        $data['apprisal_profile'] = ($query) ? $query : null;
        $data['title'] = "Employee Appraisal List View as RO";
        $data['RecAsRolist'] = $this->appraisalio->get_datatables();
        $this->load->view('appraisal/checkappraisal_asio_view', $data);
    }

    //View as IO Edit Appraisal Details..
    public Function appraisalviewasio($aprID) {
        $data = array();
        $RecArrbyID = $this->GetUserIDByApprId($aprID);
        $data['allAppraisal'] = $RecArrbyID;
        $id = $this->session->userdata('user_id');
        $this->db->select('mes.*');
        $this->db->from('main_employees_summary as mes');
        $this->db->where('mes.user_id', $id);
        $query = $this->db->get()->row_array();
        $data['apprisal_profile'] = ($query) ? $query : null;
        //Lock Condition..
        if ($RecArrbyID[0]->emp_io_lock < 1) {
            $singleRowData = $this->GetApprDetailsById($aprID);
            $ToDaYdate = date("Y-m-d");
            $dayDiff = $singleRowData["singleBaseRec"]->day_diff_io;
            $LaStDate = $singleRowData["singleBaseRec"]->emp_last_date;
            $LockDate = date('Y-m-d', strtotime($LaStDate . "+$dayDiff days"));
            if (strtotime($LockDate) < strtotime($ToDaYdate)) {
                $this->db->where(array('fld_id' => $aprID));
                $UpdRespo = $this->db->update('appr_action_master', array('emp_io_lock' => "1"));
                redirect(base_url('appraisalviewasio/' . $aprID));
            }
        }
        $data['title'] = "Employee Form List Check as RO";
        $this->load->view('appraisal/appraisalvieweditasio_view', $data);
    }

    // View / Edit As RO...
    public Function appraisalviewdetailasio($fldID) {
        $data['title'] = "Employee Form Details Check as RO";
        $id = $this->session->userdata('user_id');
        $this->db->select('mes.*');
        $this->db->from('main_employees_summary as mes');
        $this->db->where('mes.user_id', $id);
        $query = $this->db->get()->row_array();
        $data['apprisal_profile'] = ($query) ? $query : null;
        //Single Employee Details By Id...
        $data['singleRowData'] = $this->GetApprDetailsById($fldID);
        //Lock Condition..
        //Lock Condition..
//        if ($data["singleRowData"]["singleBaseRec"]) {
//            $singleRowData = $this->GetApprDetailsById($fldID);
//            //Cond For Auto Lock..
//            $ToDaYdate = date("Y-m-d");
//            $dayDiff = $singleRowData["singleBaseRec"]->day_diff_ro;
//            $LaStDate = $singleRowData["singleBaseRec"]->emp_last_date;
//            $LockDate = date('Y-m-d', strtotime($LaStDate . "+$dayDiff days"));
//            if (strtotime($LockDate) < strtotime($ToDaYdate)) {
//                $this->db->where(array('fld_id' => $fldID));
//                $UpdRespo = $this->db->update('appr_action_master', array('emp_ro_lock' => "1"));
//                redirect(base_url('appraisalviewasio/' . $fldID));
//            }
//        }
        if (($data["singleRowData"]["singleBaseRec"]->emp_ro_lock == "1") or ( $data["singleRowData"]["singleBaseRec"]->emp_io_lock == "") or ( $data["singleRowData"]["singleBaseRec"]->emp_io_lock == "")) {
            $this->session->set_flashdata('errormsg', 'Something went wrong');
            redirect(base_url('appraisalviewasio/' . $fldID));
        }
        $Duser_id = $data['singleRowData']["singleBaseRec"]->user_id;
        // $data['performRec'] = $this->TotalNoLateDaysCount($Duser_id); 
        $this->load->view('appraisal/apprviewdetailasio_view', $data);
    }

    //Save By IO Section View...
    public function apprsaveupdateas_io() {
        $masterfld_id = $this->input->post('masterfld_id');
        if ($this->input->post('chkagree2')) {
            foreach ($this->input->post('chkagree2') as $rOwR2) {
                $Ro_row2 = $_REQUEST['ioyesorno_' . $rOwR2];
                $updData2 = array('io_yes_no' => $Ro_row2);
                $this->db->where(array('fld_id' => $rOwR2, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_perform_poten_eval', $updData2);
            }
        }
        //2..
        if ($this->input->post('chkagree3')) {
            foreach ($this->input->post('chkagree3') as $rOwR3) {
                $Ro_row3 = $_REQUEST['perf_ratingio_' . $rOwR3];
                $Io_remark = $_REQUEST['perf_remarkbyio_' . $rOwR3];
                $updData3 = array('io_rating' => $Ro_row3, 'io_remark' => $Io_remark);
                $this->db->where(array('fld_id' => $rOwR3, 'appr_parent_id' => $masterfld_id));
                $this->db->update('appr_performfactor', $updData3);
            }
        }
        if ($masterfld_id) {
            $updData7 = array('final_io_reviewer' => $this->input->post('final_io_reviewer'), 'interviewcond_date_ro' => $this->input->post('interviewcond_date_ro'));
            $this->db->where(array('fld_id' => $masterfld_id));
            $this->db->update('appr_action_master', $updData7);
        }
        if ($masterfld_id) {
            $this->session->set_flashdata('successmsg', 'Appraisal Details Updated successfully.');
        }
        redirect(base_url("appraisalviewdetailasio/" . $masterfld_id));
    }

    //Final Lock By RO..
    public function appraisal_finalockbyro() {
        $lockID = base64_decode($_REQUEST['lockd']);
        if ($lockID):
            $updData3 = array('emp_ro_lock' => "1");
            $this->db->where(array('fld_id' => $lockID));
            $this->db->update('appr_action_master', $updData3);
            $this->session->set_flashdata('successmsg', 'Appraisal Details Locked successfully.');
        endif;
        redirect(base_url("appraisalviewasio/" . $lockID));
    }

    //Final Lock By User..
    public function appraisal_finalockbyuser() {
        $lockID = base64_decode($_REQUEST['lockd']);
        $Login_id = $this->session->userdata('loginid');
        $IOrecArr = $this->GetIODetailsByLoginId();
        if ($lockID and $Login_id):
            $updData3 = array('emp_self_lock' => "1");
            $this->db->where(array('fld_id' => $lockID, 'user_id' => $Login_id));
            $this->db->update('appr_action_master', $updData3);
            $this->session->set_flashdata('successmsg', 'Appraisal Details Locked successfully.');
            //Send Mail To IO..
            $data = array();
            $data["IOrecArr"] = $IOrecArr;
            $to = $IOrecArr->emailaddress_io;
            $subject = "Team Appraisal Pending with Appraiser";
            $msgDetails2 = $this->load->view('email/apprform_lockbyuser_email', $data, true);
            $this->sendMail($to, $subject, $msgDetails2);
        endif;
        redirect(base_url("myappraisal"));
    }

    //Mail Lebraray
    function sendMail($to, $subject, $msgDetails) {
        // return true;
        // $to = "ts.admin@cegindia.com";
        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->initialize(array('protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $CI->email->from('hrms.admin@cegindia.com', 'CEG-HRMS');
        $CI->email->to($to);
        $CI->email->bcc('marketing@cegindia.com');
        $CI->email->subject($subject);
        $CI->email->message($msgDetails);
        $resp = $CI->email->send();
        return ($resp) ? $resp : $CI->email->print_debugger();
    }

}
