<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile_Control extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mastermodel');
        $this->load->library('form_validation');
        if (($this->session->userdata('loginid') == "") or ( $this->session->userdata('assign_role') == "")) {
            redirect(base_url(""));
        }
    }

    public function myprofile() {
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['ofcRecArr'] = $this->mastermodel->GetOfficialDataByUserId($id);

        $data['otherDetailsRecArr'] = $this->mastermodel->GetOtherDetailsRecByID($id);
        
        $data['loginUsersRelatives'] = $this->mastermodel->loginUsersRelatives();
        
        $data['EmplExprRecArr'] = $this->mastermodel->GetExperianceDetailByID($id);
        $data['EmplSkillRecArr'] = $this->mastermodel->GetSkillRecordDetailByID($id);
        $data['PersonalDetailRecArr'] = $this->mastermodel->GetPersonalDetailsRecByID($id);
        $data['ContactDetailsRecArr'] = $this->mastermodel->GetContactDetailRecByID($id);
        $data['EduDetailsRecArr'] = $this->mastermodel->GetEducationDetailRecByID($id);
        $data['FamilyDetailsRecArr'] = $this->mastermodel->GetFamilyDetailRecByID($id);



         $data['ReferencesDetailsRecArr'] = $this->mastermodel->GetReferencesDetailRecByID($id);


        $data['EmployeeDocsDetailRec'] = $this->mastermodel->GetEmployeeDocsDetailRecByID($id);
        $data['title'] = "My Profile";

        $data['loginMarital'] = $this->mastermodel->GetloginMarital();
        $data['loginNationality'] = $this->mastermodel->GetloginNationality();
        $data['loginLanguage'] = $this->mastermodel->GetloginLanguage();


        $data['loginCountries'] = $this->mastermodel->GetloginCountries();
        $data['loginStates'] = $this->mastermodel->GetloginStates();
        $data['loginCities'] = $this->mastermodel->GetloginCities();

        $data['loginEducationLevel'] = $this->mastermodel->GetEducationLevel();

        $data['getOtherDocType'] = $this->mastermodel->getOtherDocType();


        

        // $this->db->select('a.*,b.maritalstatusname,c.nationalitycode,d.languagename');
        // $this->db->from('main_emppersonaldetails as a');
        // $this->db->join('main_maritalstatus as b', "a.maritalstatusid=b.id", "LEFT");
        // $this->db->join('main_nationality as c', "a.nationalityid=c.id", "LEFT");
        // $this->db->join('main_language as d', "a.languageid=d.id", "LEFT");
        // $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        // $RecRows = $this->db->get()->row();

        $this->load->view("userprofile/profile_view", $data);
    }


    public function post()

    {

        $data = $_POST['image'];
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $imageName = 'profile-pic-'.time().'.png';
        file_put_contents('public/uploads/profile/'.$imageName, $data);

        // script to add file name to db profileimg
        $id = $this->session->userdata('loginid');
        $data = array(
            'profileimg' => $imageName,
        );

        $this->db->where('user_id', $id);
        $updateUserPic = $this->db->update('main_employees_summary', $data);

        if($updateUserPic){
            $message = 'Image uploaded successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );



        echo json_encode($arrayResp);

    }

public function addExperience(){
 
        $id = $this->session->userdata('loginid');
 
        $comp_name = $_REQUEST['comp_name'];
        $comp_website = $_REQUEST['comp_website'];
        $designation = $_REQUEST['designation'];
        $from_date = $_REQUEST['from_date'];
        $to_date = $_REQUEST['to_date'];


        $last_salary_drawn = $_REQUEST['last_salary_drawn'];
        $reason_for_leaving = $_REQUEST['reason_for_leaving'];
        $comp_location = $_REQUEST['comp_location'];


         
       
         
        
 
        $data = array(
            'user_id' => $id,
            'comp_name' => $comp_name,
            'comp_website' => $comp_website,
            'designation' => $designation,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'last_salary_drawn' => $last_salary_drawn,
            'reason_for_leaving' => $reason_for_leaving,
            'comp_location' => $comp_location,
            
        );

        
       
        $updateUser = $this->db->insert('main_empexperiancedetails', $data);
 


        if($updateUser){
            $message = 'Experience Detail Added Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


public function updateExperience(){
 
        $id = $this->session->userdata('loginid');
 
        $comp_name = $_REQUEST['comp_name'];
        $comp_website = $_REQUEST['comp_website'];
        $designation = $_REQUEST['designation'];
        $from_date = $_REQUEST['from_date'];
        $to_date = $_REQUEST['to_date'];
        $table_id = $_REQUEST['table_id'];

        $last_salary_drawn = $_REQUEST['last_salary_drawn'];
        $reason_for_leaving = $_REQUEST['reason_for_leaving'];
        $comp_location = $_REQUEST['comp_location'];
        
         
        
 
        $data = array(
            'comp_name' => $comp_name,
            'comp_website' => $comp_website,
            'designation' => $designation,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'last_salary_drawn' => $last_salary_drawn,
            'reason_for_leaving' => $reason_for_leaving,
            'comp_location' => $comp_location,
            
        );

        
        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empexperiancedetails', $data);
 


        if($updateUser){
            $message = 'Experience Details Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

// Add Update Education

public function addEducation(){

        // echo "<pre>"; print_r($_REQUEST); die;
 
        $id = $this->session->userdata('loginid');
        $education_type = $_REQUEST['education_type'];

        if($education_type == "professional_membership"){

        $institution_name = $_REQUEST['membership_institution_name'];
        
        $membership_type = $_REQUEST['membership_type'];
        $valid_upto = $_REQUEST['valid_upto'];
         
        
        $data = array(
            'user_id' => $id,
            'institution_name' => $institution_name,
            'education_type' => $education_type,
            'membership_type' => $membership_type,
            'valid_upto' => $valid_upto,
            
        );



        }else{

        $educationlevel = $_REQUEST['educationlevel'];
        $institution_name = $_REQUEST['institution_name'];
        $course = $_REQUEST['course'];
        $spc_location = $_REQUEST['spc_location'];
        $specialization = $_REQUEST['specialization'];
        $from_date = $_REQUEST['from_date'];
        $percentage = $_REQUEST['percentage'];

        $to_date = $_REQUEST['to_date'];
        $marks_obtained = $_REQUEST['marks_obtained'];
        $total_marks = $_REQUEST['total_marks'];
        
         
        
        $data = array(
            'user_id' => $id,
            'educationlevel' => $educationlevel,
            'institution_name' => $institution_name,
            'course' => $course,
            'spc_location' => $spc_location,
            'specialization' => $specialization,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'marks_obtained' => $marks_obtained,
            'total_marks' => $total_marks,
            'education_type' => $education_type,
            'percentage' => $percentage,
            
        );



        }




        
        
       
        $updateUser = $this->db->insert('main_empeducationdetails', $data);
 


        if($updateUser){
            $message = 'Educational Detail Added Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


public function updateEducation(){

        // echo "<pre>"; print_r($_REQUEST); die;
        // Array
        // (
        // [educationlevel] => 1
        // [institution_name] => Rajasthan Board
        // [course] => 10t
        // [spc_location] => Ajmer
        // [specialization] => All
        // [from_date] => 2000
        // [percentage] => 60
        // [table_id] => 377
        // )
 
        $id = $this->session->userdata('loginid');
 
       $education_type = $_REQUEST['education_type'];
       $table_id = $_REQUEST['table_id'];

        if($education_type == "professional_membership"){

        $institution_name = $_REQUEST['membership_institution_name'];
        
        $membership_type = $_REQUEST['membership_type'];
        $valid_upto = $_REQUEST['valid_upto'];
         
        
        $data = array(
             
            'institution_name' => $institution_name,
            'education_type' => $education_type,
            'membership_type' => $membership_type,
            'valid_upto' => $valid_upto,
            
        );



        }else{

        $educationlevel = $_REQUEST['educationlevel'];
        $institution_name = $_REQUEST['institution_name'];
        $course = $_REQUEST['course'];
        $spc_location = $_REQUEST['spc_location'];
        $specialization = $_REQUEST['specialization'];
        $from_date = $_REQUEST['from_date'];
        $percentage = $_REQUEST['percentage'];

        $to_date = $_REQUEST['to_date'];
        $marks_obtained = $_REQUEST['marks_obtained'];
        $total_marks = $_REQUEST['total_marks'];
        
         
        
        $data = array(
             
            'educationlevel' => $educationlevel,
            'institution_name' => $institution_name,
            'course' => $course,
            'spc_location' => $spc_location,
            'specialization' => $specialization,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'marks_obtained' => $marks_obtained,
            'total_marks' => $total_marks,
            'education_type' => $education_type,
            'percentage' => $percentage,
            
        );



        }

        
        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empeducationdetails', $data);
 


        if($updateUser){
            $message = 'Educational Details Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

// Add Update Education


    // main_employeedocuments
    // Add Update Family

public function addDocument(){
     
        $id = $this->session->userdata('loginid');
        $doc_name = $_REQUEST['doc_name'];


        $doc_type = $_REQUEST['doc_type'];
        if($doc_type == 0){
            $doc_type_id = $_REQUEST['educationlevel'];
         
        }else{
           
        $doc_type_id = $_REQUEST['other_doc'];
        }
        

          

        $original_name = $_FILES['doc_file']['name'];


        $config = array(
        'upload_path' => "public/uploads/employeedocs/",
        'allowed_types' => "gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|ppt|pptx|csv",
        'overwrite' => TRUE,
        'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        'encrypt_name' => FALSE,
        
        );



        $new_name = time().'_'.$id.'_'.$doc_name.'_'.$_FILES["doc_file"]['name'];
        $config['file_name'] = $new_name;
        


        $this->load->library('upload', $config);
        if($this->upload->do_upload('doc_file'))
        {
        $dataArray = array('original_name' => $original_name, 'new_name' => $this->upload->data('file_name'));




        //print_r(json_encode($dataArray)); die;

        $data = array(
        'user_id' => $id,
        'doc_type' => $doc_type,
        'doc_type_id' => $doc_type_id,
        'name' => $doc_name,
        'attachments' => '['.json_encode($dataArray).']',

        );




        $updateUser = $this->db->insert('main_employeedocuments', $data);


        // print_r($this->db->last_query()); die;


        if($updateUser){
        $message = 'Document Added Successfully !';
        }else{
        $message = 'Some issue occured !';
        }
 




        
         
        }
        else
        {
        $error = array('error' => $this->upload->display_errors());

        $message = "Document Upload Failed !!";


         
        }


         
         
         
        
 
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


public function updateDocument(){


        $id = $this->session->userdata('loginid');
        $doc_name = $_REQUEST['doc_name'];

        $doc_type = $_REQUEST['doc_type'];
        if($doc_type == 0){
            $doc_type_id = $_REQUEST['educationlevel'];
         
        }else{
           
        $doc_type_id = $_REQUEST['other_doc'];
        }
        



        $original_name = $_FILES['doc_file']['name'];
        $table_id = $_REQUEST['table_id'];


        $config = array(
        'upload_path' => "public/uploads/employeedocs/",
        'allowed_types' => "gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|ppt|pptx|csv",
        'overwrite' => TRUE,
        'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        'encrypt_name' => FALSE,
        
        );



        $new_name = time().'_'.$id.'_'.$doc_name.'_'.$_FILES["doc_file"]['name'];
        $config['file_name'] = $new_name;
        


        $this->load->library('upload', $config);
        if($this->upload->do_upload('doc_file'))
        {
        $dataArray = array('original_name' => $original_name, 'new_name' => $this->upload->data('file_name'));




        //print_r(json_encode($dataArray)); die;

        $data = array(
        'doc_type' => $doc_type,
        'doc_type_id' => $doc_type_id,
        'name' => $doc_name,
        'attachments' => '['.json_encode($dataArray).']',

        );




        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_employeedocuments', $data);
 


        // print_r($this->db->last_query()); die;


        if($updateUser){
        $message = 'Document Updated Successfully !';
        }else{
        $message = 'Some issue occured !';
        }
 




        
         
        }
        else
        {
        $error = array('error' => $this->upload->display_errors());

        $message = "Document Upload Failed !!";


         
        }


         
         
         
        
  

        
       


        
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }



    // Add Update Family

public function addFamily(){

      
        $id = $this->session->userdata('loginid');
 
        $dependent_name = $_REQUEST['dependent_name'];
        $dependent_relation = $_REQUEST['dependent_relation'];
        $dependent_dob = $_REQUEST['dependent_dob'];
        $aadhar_no = $_REQUEST['aadhar_no'];
        $dependent_select = $_REQUEST['dependent_select'];
         
         
        
 
        $data = array(
            'user_id' => $id,
            'dependent_name' => $dependent_name,
            'dependent_relation' => $dependent_relation,
            'dependent_dob' => $dependent_dob,
            'aadhar_no' => $aadhar_no,
            'dependent_select' => $dependent_select,
            
        );


        
       
        $updateUser = $this->db->insert('main_empdependencydetails', $data);
 


        if($updateUser){
            $message = 'Family Detail Added Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


      public function updateFamily(){

        
 
       $id = $this->session->userdata('loginid');
 
        $dependent_name = $_REQUEST['dependent_name'];
        $dependent_relation = $_REQUEST['dependent_relation'];
        $dependent_dob = $_REQUEST['dependent_dob'];
        $aadhar_no = $_REQUEST['aadhar_no'];
        $dependent_select = $_REQUEST['dependent_select'];
        $table_id = $_REQUEST['table_id'];
         
         
        
 
        $data = array(
             
            'dependent_name' => $dependent_name,
            'dependent_relation' => $dependent_relation,
            'dependent_dob' => $dependent_dob,
            'aadhar_no' => $aadhar_no,
            'dependent_select' => $dependent_select,
            
        );


       
         
        
 
        

        
        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empdependencydetails', $data);
 


        if($updateUser){
            $message = 'Family Details Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


    public function addReferences(){

      
        $id = $this->session->userdata('loginid');
  


        $reference_name = $_REQUEST['reference_name'];
        $organisation = $_REQUEST['organisation'];
        $designation = $_REQUEST['designation'];
        $contact_no = $_REQUEST['contact_no'];
        $reference_type = $_REQUEST['reference_type'];
       
         
         
        
 
        $data = array(
             'user_id' => $id,
            'reference_name' => $reference_name,
            'organisation' => $organisation,
            'designation' => $designation,
            'contact_no' => $contact_no,
            'reference_type' => $reference_type,
            
        );




        
       
        $updateUser = $this->db->insert('personnel_professional_reference', $data);
 


        if($updateUser){
            $message = 'Reference  Added Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }




    public function updateReferences(){

        
 
       $id = $this->session->userdata('loginid');
 
        $reference_name = $_REQUEST['reference_name'];
        $organisation = $_REQUEST['organisation'];
        $designation = $_REQUEST['designation'];
        $contact_no = $_REQUEST['contact_no'];
        $reference_type = $_REQUEST['reference_type'];
        $table_id = $_REQUEST['table_id'];
         
         
        
 
        $data = array(
             
            'reference_name' => $reference_name,
            'organisation' => $organisation,
            'designation' => $designation,
            'contact_no' => $contact_no,
            'reference_type' => $reference_type,
            
        );


       
         
        
 
        

        
        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('personnel_professional_reference', $data);
 


        if($updateUser){
            $message = 'Reference Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

// Add Update Education


    public function addSkills(){
 
        $id = $this->session->userdata('loginid');

        // echo "<pre>"; print_r($_REQUEST); die;
        // Array
        // (
        // [skillname] => Hacking Plus
        // [yearsofexp] => 2.3
        // [competencylevelid] => 2
        // [year_skill_last_used] => 2020-02-24
        // [table_id] => 22
        // )
 
        $skillname = $_REQUEST['skillname'];
        $yearsofexp = $_REQUEST['yearsofexp'];
        $competencylevelid = $_REQUEST['competencylevelid'];
        $year_skill_last_used = $_REQUEST['year_skill_last_used'];
      
         
        
 
        $data = array(
            'user_id' => $id,
            'skillname' => $skillname,
            'yearsofexp' => $yearsofexp,
            'competencylevelid' => $competencylevelid,
            'year_skill_last_used' => $year_skill_last_used,
             
            
        );

        
       
        $updateUser = $this->db->insert('main_empskills', $data);
 


        if($updateUser){
            $message = 'Skills Added Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

public function updateSkills(){
 
        $id = $this->session->userdata('loginid');

        // echo "<pre>"; print_r($_REQUEST); die;
        // Array
        // (
        // [skillname] => Hacking Plus
        // [yearsofexp] => 2.3
        // [competencylevelid] => 2
        // [year_skill_last_used] => 2020-02-24
        // [table_id] => 22
        // )
 
        $skillname = $_REQUEST['skillname'];
        $yearsofexp = $_REQUEST['yearsofexp'];
        $competencylevelid = $_REQUEST['competencylevelid'];
        $year_skill_last_used = $_REQUEST['year_skill_last_used'];
        
        $table_id = $_REQUEST['table_id'];
         
        
 
        $data = array(
            'skillname' => $skillname,
            'yearsofexp' => $yearsofexp,
            'competencylevelid' => $competencylevelid,
            'year_skill_last_used' => $year_skill_last_used,
             
            
        );

        
        $this->db->where('id', $table_id);
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empskills', $data);
 


        if($updateUser){
            $message = 'Skills Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

 public function updateContactAddress()
    {
 
        $id = $this->session->userdata('loginid');

        $perm_streetaddress = $_REQUEST['perm_streetaddress'];
        $perm_country_name = $_REQUEST['perm_country_name'];
        $perm_state_name = $_REQUEST['perm_state_name'];
        $perm_city_name = $_REQUEST['perm_city_name'];
        $perm_pincode = $_REQUEST['perm_pincode'];
        $current_streetaddress = $_REQUEST['current_streetaddress'];
        $current_country_name = $_REQUEST['current_country_name'];
        $current_state_name = $_REQUEST['current_state_name'];
        $current_city_name = $_REQUEST['current_city_name'];
        $current_pincode = $_REQUEST['current_pincode'];
        $emergency_name = $_REQUEST['emergency_name'];
        $emergency_namerelation = $_REQUEST['emergency_namerelation'];
        $emergency_email = $_REQUEST['emergency_email'];
        $emergency_number = $_REQUEST['emergency_number'];
        




        // echo "<pre>";
        // print_r($_REQUEST); die;
        $data = array(
            'perm_streetaddress' => $perm_streetaddress,
            'perm_country' => $perm_country_name,
            'perm_state' => $perm_state_name,
            'perm_city' => $perm_city_name,
            'perm_pincode' => $perm_pincode,
            'current_streetaddress' => $current_streetaddress,
            'current_country' => $current_country_name,
            'current_state' => $current_state_name,
            'current_city' => $current_city_name,
            'current_pincode' => $current_pincode,

            'emergency_name' => $emergency_name,
            'emergency_namerelation' => $emergency_namerelation,
            'emergency_email' => $emergency_email,
            'emergency_number' => $emergency_number,
        );

        
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_empcommunicationdetails', $data);
 


        if($updateUser){
            $message = 'Contact Address Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }

// Update Other Details
    public function updateOtherDetails()
    {
 
        $id = $this->session->userdata('loginid');

        $candidate_source = $_REQUEST['candidate_source'];
        $candidate_reference = $_REQUEST['candidate_reference'];
        $criminal_offense = $_REQUEST['criminal_offense'];
        $drug_alcohal_abuse = $_REQUEST['drug_alcohal_abuse'];



        $criminal_offense_detail = $_REQUEST['criminal_offense_detail'];
        $drug_abuse_detail = $_REQUEST['drug_abuse_detail'];

        $pre_existing_illness = $_REQUEST['pre_existing_illness'];
        $partial_disabilities = $_REQUEST['partial_disabilities'];
        $pre_existing_illness_detail = $_REQUEST['pre_existing_illness_detail'];
        $partial_disabilities_detail = $_REQUEST['partial_disabilities_detail'];
        $relative_employed_ceg = $_REQUEST['relative_employed_ceg'];
        $employed_ceg_relation = $_REQUEST['employed_ceg_relation'];
        $employed_ceg_user_id = $_REQUEST['employed_ceg_user_id'];
        $previous_employed_with_ceg = $_REQUEST['previous_employed_with_ceg'];
        $previous_reason_for_leaving_ceg = $_REQUEST['previous_reason_for_leaving_ceg'];
        $previous_employee_code = $_REQUEST['previous_employee_code'];
        
        




       
        $data = array(
            'user_id' => $id,
            'candidate_source' => $candidate_source,
            'candidate_reference' => $candidate_reference,
            'criminal_offense' => $criminal_offense,
            'drug_alcohal_abuse' => $drug_alcohal_abuse,
            'criminal_offense_detail' => $criminal_offense_detail,
            'drug_abuse_detail' => $drug_abuse_detail,
            'pre_existing_illness' => $pre_existing_illness,
            'partial_disabilities' => $partial_disabilities,
            'pre_existing_illness_detail' => $pre_existing_illness_detail,
            'partial_disabilities_detail' => $partial_disabilities_detail,

            'relative_employed_ceg' => $relative_employed_ceg,
            'employed_ceg_relation' => $employed_ceg_relation,
            'employed_ceg_user_id' => $employed_ceg_user_id,
            'previous_employed_with_ceg' => $previous_employed_with_ceg,
            'previous_reason_for_leaving_ceg' => $previous_reason_for_leaving_ceg,
            'previous_employee_code' => $previous_employee_code,
        );

        $this->db->where('user_id',$id);
        $query = $this->db->get('other_user_record');
        if ($query->num_rows() > 0){
            $this->db->where('user_id', $id);
            $updateUser = $this->db->update('other_user_record', $data);
        }
        else{
            $this->db->where('user_id', $id);
            $updateUser =   $respon = $this->db->insert('other_user_record', $data);
        }
       
 


        if($updateUser){
            $message = 'Other Details Saved Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }



    public function updatePersonnelData()
    {

        // main_emppersonaldetails >> mother_nm, father_nm, maritalstatusname, 
        // genderid, nationalitycode, languagename, dob, bloodgroup, aadhar_no_enrolment, skypee_id

        
         
        $id = $this->session->userdata('loginid');

        $mother_nm = $_REQUEST['mother_nm'];
        $father_nm = $_REQUEST['father_nm'];
        $maritalstatusname = $_REQUEST['maritalstatusname'];
        $genderid = $_REQUEST['genderid'];
        $nationalitycode = $_REQUEST['nationalitycode'];
        $languagename = $_REQUEST['languagename'];
        $dob = $_REQUEST['dob'];
        $bloodgroup = $_REQUEST['bloodgroup'];

        $aadhar_no_enrolment = $_REQUEST['aadhar_no_enrolment'];
        $skypee_id = $_REQUEST['skypee_id'];

        // //height_in_cm
        $height_in_cm = $_REQUEST['height_in_cm'];
        // //weight
        $weight = $_REQUEST['weight'];
        // //religion
        $religion = $_REQUEST['religion'];
        // passport_no
        $passport_no = $_REQUEST['passport_no'];
        // pan_no
        $pan_no = $_REQUEST['pan_no'];
        // driving_license_no
        $driving_license_no = $_REQUEST['driving_license_no'];
        // ration_card
        $ration_card = $_REQUEST['ration_card'];
        // //fathers_contact_no
        $fathers_contact_no = $_REQUEST['fathers_contact_no'];
        // //mothers_contact_no
        $mothers_contact_no = $_REQUEST['mothers_contact_no'];
        // marriage_date
        $marriage_date = $_REQUEST['marriage_date'];
        // spouse_name
        $spouse_name = $_REQUEST['spouse_name'];
        // spouse_occupation
        $spouse_occupation = $_REQUEST['spouse_occupation'];
        // spouse_contact_no
        $spouse_contact_no = $_REQUEST['spouse_contact_no'];




        // echo "<pre>";
        // print_r($_REQUEST); die;

        $data = array(
            'mother_nm' => $mother_nm,
            'father_nm' => $father_nm,
            'genderid' => $genderid,
            'dob' => $dob,
            'bloodgroup' => $bloodgroup,
            'aadhar_no_enrolment' => $aadhar_no_enrolment,
            'skypee_id' => $skypee_id,
            'maritalstatusid' => $maritalstatusname,
            'nationalityid' => $nationalitycode,
            'languageid' => $languagename,
            'height_in_cm' => $height_in_cm,
            'weight' => $weight,
            'religion' => $religion,
            'passport_no' => $passport_no,
            'pan_no' => $pan_no,
            'driving_license_no' => $driving_license_no,
            'ration_card' => $ration_card,
            'fathers_contact_no' => $fathers_contact_no,
            'mothers_contact_no' => $mothers_contact_no,
            'marriage_date' => $marriage_date,
            'spouse_name' => $spouse_name,
            'spouse_occupation' => $spouse_occupation,
            'spouse_contact_no' => $spouse_contact_no,

        );

        
        $this->db->where('user_id', $id);
        $updateUser = $this->db->update('main_emppersonaldetails', $data);
 


        if($updateUser){
            $message = 'Personnel Details Updated Successfully !';
        }else{
            $message = 'Some issue occured !';
        }
        

        $arrayResp = array(
            "msg" => $message

        );


        echo json_encode($arrayResp);

    }


    //My Team Details..
    public function myteam() {
        $data['error'] = '';
        $id = $this->session->userdata('loginid');
        $data['myTeamDetailsRecArr'] = $this->mastermodel->GetMyTeamDetailsRecByID($id);
        $data['title'] = "My Team Details";
        $this->load->view("self-services/myteam_list_view", $data);
    }

    //My Holidays Section...
    public function myholidays() {
        $data['error'] = '';
        $data['title'] = "My Holidays Details";
        $hgroupID = '';
        $hyear = date("Y");
        if (@$_REQUEST['filter']) {
            $hgroupID = $_REQUEST['holidaystype'];
            $hyear = $_REQUEST['holidaysyear'];
            $data['holidaystype'] = $hgroupID;
            $data['holidaysyear'] = $hyear;
            $data['HolidaysRecArr'] = $this->mastermodel->GetAllHolidaysListRec($hgroupID, $hyear);
        } else {
            $data['HolidaysRecArr'] = $this->mastermodel->GetAllHolidaysListRec($hgroupID, $hyear);
        }
        $data['holidaysyear'] = $hyear;
        $this->load->view("self-services/myholidays_list_view", $data);
    }

    //Apply Tour Details..
    public function applytour() {
        $this->form_validation->set_rules('project_id', 'Project Name', 'required|trim');
        $this->form_validation->set_rules('tour_type', 'Tour Type', 'required|trim');
        $this->form_validation->set_rules('expectedout_time', 'Expected Out Time', 'trim|callback_chkTourTypeCond');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required|trim|callback_compareDate');
        $this->form_validation->set_rules('end_date', 'End Date', 'required|trim|callback_compareDate');
        $this->form_validation->set_rules('tour_location', 'Comment', 'required|min_length[2]|max_length[100]');
        $this->form_validation->set_rules('description', 'Tour Description', 'required|trim|min_length[4]|max_length[200]');

        if ($this->form_validation->run() == FALSE) {
            $data['error'] = '';
            $data['title'] = "My Tour Details";
            $id = $this->session->userdata('loginid');
            $data['AppliedTourRecArr'] = $this->mastermodel->GetAllAppliedTourListRec($id);
            $data['ProjectListRecArr'] = $this->mastermodel->GelAllActiveProjectList();
            $this->load->view("self-services/applytour_list_view", $data);
        } else {
            $startDate = $this->input->post('start_date');
            $endDate = $this->input->post('end_date');
            $insertedArr = array('emp_id' => $this->session->userdata('loginid'),
                'project_id' => $this->input->post('project_id'),
                'week_no' => weekOfMonth($startDate),
                'year_week' => (int) date("W", strtotime($startDate)),
                'month_no' => (int) date("m", strtotime($startDate)),
                'Year' => (int) date("Y", strtotime($startDate)),
                'start_date' => date("Y-m-d", strtotime($startDate)),
                'end_date' => date("Y-m-d", strtotime($endDate)),
                'tour_location' => $this->input->post('tour_location'),
                'description' => $this->input->post('description'),
                'tour_type' => $this->input->post('tour_type'),
                'expected_out_time' => $this->input->post('expectedout_time'),
                'created_by' => $this->session->userdata('loginid'));
            $respon = $this->db->insert('emp_tourapply', $insertedArr);
            $LastinsertID = $this->db->insert_id();

            if ($respon):
                //Tour Mail..
                $this->tourmail($LastinsertID);
                $this->session->set_flashdata('success_msg', 'Tour Apply successfully');
                redirect(thisurl());
            else:
                $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                redirect(thisurl());
            endif;
        }
    }

    //Call Back Function ..
    function compareDate() {
        $startDate = strtotime($_POST['start_date']);
        $endDate = strtotime($_POST['end_date']);

        $start_date1 = $_POST['start_date'];
        $end_date1 = $_POST['end_date'];
        $id = $this->session->userdata('loginid');

        $extRec = $this->mastermodel->tour_already_exists_or_not($start_date1, $end_date1, $id);
        if ($extRec > 0) {
            $this->form_validation->set_message('compareDate', 'Tour Request already taken on this Date Combination');
            return False;
        }
        if ($endDate >= $startDate)
            return True;
        else {
            $this->form_validation->set_message('compareDate', 'Start Date & End Date Combination Error');
            return False;
        }
    }

    //Call Back Function 2 .. Tour Type ..
    public function chkTourTypeCond() {
        $tour_type = $_POST['tour_type'];
        $expectedout_time = $_POST['expectedout_time'];
        $startDate = strtotime($_POST['start_date']);
        $endDate = strtotime($_POST['end_date']);

        //Condition 1..
        if (($tour_type == "2") AND ( $expectedout_time == "")):
            $this->form_validation->set_message('chkTourTypeCond', 'Enter Expected Out Time of Short Tour.');
            return False;
        endif;
        //Condition 2..
        if (($tour_type == "2") AND ( $startDate != $endDate)):
            $this->form_validation->set_message('chkTourTypeCond', 'Start & End Date will be same in case of short Tour.');
            return False;
        endif;
        return true;
    }

    //User Profile My Leave..
    public function myleave() {
        $data['error'] = '';
        $data['title'] = "My Leave Details";
        $id = $this->session->userdata('loginid');
        $data['loginUserRecd'] = $this->mastermodel->GetBasicRecLoginUser();
        $data['myallAppliedLeaveRecArr'] = $this->mastermodel->GetMyAllAppliedLeaves($id);
        $data['AvlLeaveRecd'] = $this->mastermodel->GetAvlLeaveBalance($id);
        $this->load->view("self-services/myleave_list_view", $data);
    }

    //ajax User RH check Before Apply Rh Leave..
    public function ajax_checkrhleave() {
        $recRhDate = @$_REQUEST["rhapplydate"];
        $id = $this->session->userdata('loginid');
        $numRows = 0;
        $totApplyRh = 0;
        if ($recRhDate) {
            $dtFormated = date("Y-m-d", strtotime($recRhDate));
            $this->db->select('RH_date');
            $this->db->from('rh_leave_summery');
            $this->db->where("RH_date", $dtFormated);
            $numRows = $this->db->get()->num_rows();
        }
        //Check Leave Limit..
        if (($numRows > 0) and ( $recRhDate)) {
            $cYear = date("Y");
            $this->db->select('id');
            $this->db->from('main_leaverequest_summary');
            $this->db->where(array("user_id" => $id, "isactive" => "1"));
            $this->db->where("leavestatus !=", "Cancel");
            $this->db->where("leavestatus !=", "Rejected");
            $this->db->where("from_date LIKE '%$cYear%'");
            $totApplyRh = $this->db->get()->num_rows();
        }
        echo json_encode(array("isrh" => $numRows, "rhleavelimit" => $totApplyRh));
    }

    //Apply PL ..
    public function ajax_savesubmit_leaverequest_pl() {
        $id = $this->session->userdata('loginid');
        $leave_type = $this->input->post('leave_type');
        $leave_for = $this->input->post('leave_for');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $reason = $this->input->post('reason');
        $count_leave = $this->input->post('count_leave');

        if ($count_leave > 0):
            $returnArr = array("status" => "0", "error_msg" => "Some Thing went wrong.", "success_msg" => "");
        else:
            $returnArr = array("status" => "0", "error_msg" => "Your Selected Date already exists Holidays Date.", "success_msg" => "");
            echo json_encode($returnArr);
        endif;

        if ($leave_type and $leave_for and $start_date and $end_date and $reason and ( $count_leave > 0)):
            $insertArr = array("user_id" => $id, "reason" => $reason,
                "leavetypeid" => "3",
                "leaveday" => "1",
                "from_date" => date("Y-m-d", strtotime($start_date)),
                "to_date" => date("Y-m-d", strtotime($end_date)),
                "rep_mang_id" => $this->input->post('reporting_manager'),
                "hr_id" => "221",
                "appliedleavescount" => $count_leave,
                "createddate" => date("Y-m-d h:i:s"),
                "modifieddate" => date("Y-m-d h:i:s"),
                "createdby" => $id);

            $numRecs = $this->mastermodel->leave_already_exists_or_not($start_date, $end_date, $id);
            if ($numRecs < 1) {
                $respon = $this->db->insert('main_leaverequest', $insertArr);
                $leaveReqID = $this->db->insert_id();
                //Mail To User self Who Applied Leave.
                $this->leavereqmailsms($leaveReqID);
                //Mail To IO/Tl of Employee.
                $this->leavereqmailsmstoio($leaveReqID);

                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Leave request added successfully.');
                    $returnArr = array("status" => "1");
                    echo json_encode($returnArr);
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    echo json_encode($returnArr);
                endif;
            } else {
                $returnArr = array("status" => "0", "error_msg" => "Leave Request already taken on This Date");
                echo json_encode($returnArr);
            }
        endif;
    }

    //Apply PL Half Day..
    //Apply PL ..
    public function ajax_savesubmit_leaverequest_hd() {
        $id = $this->session->userdata('loginid');
        $leave_type = $this->input->post('leave_type');
        $leave_for = $this->input->post('leave_for');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $reason = $this->input->post('reason');
        $count_leave = $this->input->post('count_leave');
        $half_day_type = $this->input->post('half_day_type');
        $returnArr = array("status" => "0", "error_msg" => "Some Thing went wrong", "success_msg" => "");

        if ($leave_type and $leave_for and $start_date and $end_date and $reason and $count_leave and $half_day_type):
            $insertArr = array("user_id" => $id, "reason" => $reason,
                "leavetypeid" => "3",
                "leaveday" => "2",
                "from_date" => date("Y-m-d", strtotime($start_date)),
                "to_date" => date("Y-m-d", strtotime($end_date)),
                "rep_mang_id" => $this->input->post('reporting_manager'),
                "hr_id" => "221",
                "half_day_type" => $half_day_type,
                "appliedleavescount" => $count_leave,
                "createddate" => date("Y-m-d h:i:s"),
                "modifieddate" => date("Y-m-d h:i:s"),
                "hd_pre_post_lunch" => $half_day_type,
                "createdby" => $id);

            $numRecs = $this->mastermodel->leave_already_exists_or_not($start_date, $end_date, $id);
            if ($numRecs < 1) {
                $respon = $this->db->insert('main_leaverequest', $insertArr);
                $leaveReqID = $this->db->insert_id();
                $this->leavereqmailsms($leaveReqID);
                $this->leavereqmailsmstoio($leaveReqID);
                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Leave request added successfully.');
                    $returnArr = array("status" => "1");
                    echo json_encode($returnArr);
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    echo json_encode($returnArr);
                endif;
            } else {
                $returnArr = array("status" => "0", "error_msg" => "Leave Request already taken on This Date");
                echo json_encode($returnArr);
            }
        endif;
    }

    //Short Leave Apply..
    public function ajax_savesubmit_leaverequest_sortl() {
        $id = $this->session->userdata('loginid');
        $leave_type = $this->input->post('leave_type');
        $leave_for = $this->input->post('leave_for');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $reason = $this->input->post('reason');
        $count_leave = $this->input->post('count_leave');
        $sort_leave_exptime = $this->input->post('sort_leave_exptime');
        $returnArr = array("status" => "0", "error_msg" => "Some Thing went wrong", "success_msg" => "");
        //**(leaveday) 1 = Full Day, 2 Half Day, 3 Short Leave.

        if ($leave_type and $leave_for and $start_date and $end_date and $reason and $count_leave and $sort_leave_exptime):
            $insertArr = array("user_id" => $id, "reason" => $reason,
                "leavetypeid" => "3",
                "leaveday" => "3",
                "from_date" => date("Y-m-d", strtotime($start_date)),
                "to_date" => date("Y-m-d", strtotime($end_date)),
                "rep_mang_id" => $this->input->post('reporting_manager'),
                "hr_id" => "221",
                "sort_leave_exptime" => $sort_leave_exptime,
                "appliedleavescount" => $count_leave,
                "createddate" => date("Y-m-d h:i:s"),
                "modifieddate" => date("Y-m-d h:i:s"),
                "createdby" => $id);

            $numRecs = $this->mastermodel->leave_already_exists_or_not($start_date, $end_date, $id);
            if ($numRecs < 1) {
                $respon = $this->db->insert('main_leaverequest', $insertArr);
                $leaveReqID = $this->db->insert_id();
                $this->leavereqmailsms($leaveReqID);
                $this->leavereqmailsmstoio($leaveReqID);

                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Leave request added successfully.');
                    $returnArr = array("status" => "1");
                    echo json_encode($returnArr);
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    echo json_encode($returnArr);
                endif;
            } else {
                $returnArr = array("status" => "0", "error_msg" => "Leave Request already taken on This Date");
                echo json_encode($returnArr);
            }
        endif;
    }

    //Leave Request RH..
    public function ajax_savesubmit_leaverequest_rh() {
        $id = $this->session->userdata('loginid');
        $leave_type = $this->input->post('leave_type');
        $leave_for = $this->input->post('leave_for');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $reason = $this->input->post('reason');
        $count_leave = $this->input->post('count_leave');
        $returnArr = array("status" => "0", "error_msg" => "Some Thing went wrong", "success_msg" => "");

        if ($leave_type and $leave_for and $start_date and $end_date and $reason and $count_leave):
            $insertArr = array("user_id" => $id, "reason" => $reason,
                "leavetypeid" => "5",
                "leaveday" => "1",
                "from_date" => date("Y-m-d", strtotime($start_date)),
                "to_date" => date("Y-m-d", strtotime($end_date)),
                "rep_mang_id" => $this->input->post('reporting_manager'),
                "hr_id" => "221",
                "appliedleavescount" => $count_leave,
                "createddate" => date("Y-m-d h:i:s"),
                "modifieddate" => date("Y-m-d h:i:s"),
                "createdby" => $id);

            $numRecs = $this->mastermodel->leave_already_exists_or_not($start_date, $end_date, $id);
            if ($numRecs < 1) {
                $respon = $this->db->insert('main_leaverequest', $insertArr);
                $leaveReqID = $this->db->insert_id();
                $this->leavereqmailsms($leaveReqID);
                $this->leavereqmailsmstoio($leaveReqID);

                if ($respon):
                    $this->session->set_flashdata('success_msg', 'Leave request added successfully.');
                    $returnArr = array("status" => "1");
                    echo json_encode($returnArr);
                else:
                    $this->session->set_flashdata('error_msg', 'Something went gone wrong');
                    echo json_encode($returnArr);
                endif;
            } else {
                $returnArr = array("status" => "0", "error_msg" => "Leave Request already taken on This Date");
                echo json_encode($returnArr);
            }
        endif;
    }

    function getDatesFromRange($startDate, $endDate) {
        $return = array($startDate);
        $start = $startDate;
        $i = 1;
        if (strtotime($startDate) < strtotime($endDate)) {
            while (strtotime($start) < strtotime($endDate)) {
                $start = date('Y-m-d', strtotime($startDate . '+' . $i . ' days'));
                $return[] = $start;
                $i++;
            }
        }
        return $return;
    }

    //User Profile PL..
    public function ajax_chkdatecomb_pl() {
        $id = $this->session->userdata('loginid');
        $LeaveType = $this->input->post('pleavetype');
        $LeaveFor = $this->input->post('pleavefor');
        $StartDate = $this->input->post('pstart_date');
        $endDate = $this->input->post('pend_date');
        $AvlLeaveRecd = $this->mastermodel->GetAvlLeaveBalance($id);

        $noticeV = "";
        $returnArr = array("status" => "0", "error_msg" => "Something went gone wrong", "countday" => "", "notice" => "");

        if (($LeaveType) and ( $LeaveFor) and ( $StartDate) and ( $endDate)) {
            $StartDate = date("Y-m-d", strtotime($StartDate));
            $endDate = date("Y-m-d", strtotime($endDate));
            if (strtotime($endDate) < strtotime($StartDate)) {
                $returnArr = array("status" => "0", "error_msg" => "End Date should be greater than Start date", "countday" => "", "notice" => "");
            } else {
                $dateArr = $this->getDatesFromRange($StartDate, $endDate);
                $realLvCont = count($dateArr);
                //In Sandwich Leaves Rule Condition..
                if (count($dateArr) > 5) {
                    //If Leave Greater Than 12..
                    if (count($dateArr) > 12):
                        $noticeV = 1;
                    endif;
                    //if Leave Greater than Earn Leave..
                    if ($AvlLeaveRecd < count($dateArr)):
                        $noticeV = 1;
                    endif;
                    $returnArr = array("status" => "1", "error_msg" => "", "countday" => $realLvCont, "notice" => $noticeV);
                }
                if (count($dateArr) < 6) {
                    //Sandwich leaves Rule Free Condition..
                    $AllHolidaysArr = $this->firs_third_Sat_AllSun_AllHolidays_ListArr();
                    $HdyMinusDays = array_intersect($AllHolidaysArr, $dateArr);
                    $realLvCont = count($dateArr) - count($HdyMinusDays);
                    $returnArr = array("status" => "1", "error_msg" => "", "countday" => $realLvCont, "notice" => "");
                }
            }
        }
        echo json_encode($returnArr);
    }

    //Get First And Third Sat of current Year..
    public function firs_third_Sat_AllSun_AllHolidays_ListArr() {
        $firsThirArr = '';
        $array = array();
        $YeArPrev = date("Y") - 1;
        $YeAr = date("Y");
        $YeArNext = date("Y") + 1;

        //For Prevoius
        for ($i = 1; $i < 13; $i++) {
            $actdate = "$YeArPrev-$i-01";
            $firstReturn = date("Y-m-d", strtotime("first saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $thirdReturn = date("Y-m-d", strtotime("third saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //All Sunday..
            $firstSunDReturn1 = date("Y-m-d", strtotime("first sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn2 = date("Y-m-d", strtotime("second sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn3 = date("Y-m-d", strtotime("third sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn4 = date("Y-m-d", strtotime("fourth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn5 = date("Y-m-d", strtotime("fifth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn6 = date("Y-m-d", strtotime("sixth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //Sund..
            array_push($array, $firstSunDReturn1);
            array_push($array, $firstSunDReturn2);
            array_push($array, $firstSunDReturn3);
            array_push($array, $firstSunDReturn4);
            array_push($array, $firstSunDReturn5);
            array_push($array, $firstSunDReturn6);
            //Satd..
            array_push($array, $firstReturn);
            array_push($array, $thirdReturn);
        }

        //For Current Year..
        for ($i = 1; $i < 13; $i++) {
            $actdate = "$YeAr-$i-01";
            $firstReturn = date("Y-m-d", strtotime("first saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $thirdReturn = date("Y-m-d", strtotime("third saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //All Sunday..
            $firstSunDReturn1 = date("Y-m-d", strtotime("first sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn2 = date("Y-m-d", strtotime("second sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn3 = date("Y-m-d", strtotime("third sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn4 = date("Y-m-d", strtotime("fourth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn5 = date("Y-m-d", strtotime("fifth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn6 = date("Y-m-d", strtotime("sixth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //Sund..
            array_push($array, $firstSunDReturn1);
            array_push($array, $firstSunDReturn2);
            array_push($array, $firstSunDReturn3);
            array_push($array, $firstSunDReturn4);
            array_push($array, $firstSunDReturn5);
            array_push($array, $firstSunDReturn6);
            //Satd..
            array_push($array, $firstReturn);
            array_push($array, $thirdReturn);
        }

        //For Next Year..
        for ($i = 1; $i < 13; $i++) {
            $actdate = "$YeArNext-$i-01";
            $firstReturn = date("Y-m-d", strtotime("first saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $thirdReturn = date("Y-m-d", strtotime("third saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //All Sunday..
            $firstSunDReturn1 = date("Y-m-d", strtotime("first sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn2 = date("Y-m-d", strtotime("second sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn3 = date("Y-m-d", strtotime("third sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn4 = date("Y-m-d", strtotime("fourth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn5 = date("Y-m-d", strtotime("fifth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            $firstSunDReturn6 = date("Y-m-d", strtotime("sixth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
            //Sund..
            array_push($array, $firstSunDReturn1);
            array_push($array, $firstSunDReturn2);
            array_push($array, $firstSunDReturn3);
            array_push($array, $firstSunDReturn4);
            array_push($array, $firstSunDReturn5);
            array_push($array, $firstSunDReturn6);
            //Satd..
            array_push($array, $firstReturn);
            array_push($array, $thirdReturn);
        }
        //Got All Holidays List..
        $recHolidaysList = $this->holidayCount("$YeArPrev-01-01", "$YeArNext-03-01");
        if ($recHolidaysList) {
            foreach ($recHolidaysList as $rechday) {
                array_push($array, $rechday);
            }
        }
        asort($array);
        return array_unique($array);
    }

    //Get All Holidays List..
    public function holidayCount($startDate, $endDate) {
        $ReturnHolidateArr = array();
        $this->db->select('id,holidaydate');
        $this->db->from('main_holidaydates');
        $where_date = "(holidaydate>='$startDate' AND holidaydate <= '$endDate')";
        $this->db->where($where_date);
        $arr = array('isactive' => '1', 'groupid' => '1');
        $this->db->where($arr);
        $res = $this->db->get()->result();
        if ($res) {
            foreach ($res as $val) {
                $ReturnHolidateArr[] = $val->holidaydate;
            }
        }
        return $ReturnHolidateArr ? $ReturnHolidateArr : '';
    }

    public function leavereqmailsms($leaveID) {
        // $leaveID = '5575';
        $this->db->select('a.*,b.user_name,c.firstname,c.emailaddress,b.department_name,b.buss_unit_name,b.leavetype_name,b.rep_mang_id,b.rep_manager_name,c.contactnumber');
        $this->db->from('main_leaverequest as a');
        $this->db->join('main_leaverequest_summary as b', 'a.id=b.leave_req_id', 'LEFT');
        $this->db->join('main_employees_summary as c', 'a.user_id=c.user_id', 'LEFT');
        $this->db->where(array('a.isactive' => '1', 'a.id' => $leaveID));
        $data['singleRecLeave'] = $this->db->get()->row();
        //Send SMS To Self Start Code..
        $to = $data['singleRecLeave']->emailaddress;
        $subject = "Leave Request For Approval";
        $content = "Dear " . $data['singleRecLeave']->firstname . ",\n";
        $content .= " You have successfully applied for a leave from " . date('d-m-y', strtotime($data['singleRecLeave']->from_date)) . " To " . date('d-m-y', strtotime($data['singleRecLeave']->to_date)) . "\n";
        $content .= " Pending for approval by your IO. \n";
        $content .= "CEG HRMS";
        $smscontent = urlencode($content);
        $mob = $data['singleRecLeave']->contactnumber;
        $respon = file_get_contents("http://sms.getonesms.com/sendsms.jsp?password=f4fd42ad3fXX&sms=" . $smscontent . "&user=cegltd&senderid=CEGLTD&mobiles=" . $mob);
        //Send Mail To Self Start Code..
        $msgDetails = $this->load->view('email/leave_request_approval', $data, true);
        //Send Mail Close To Self Start Code..
        $returVaL = $this->sendMail($to, $subject, $msgDetails);
        return ($returVaL) ? $returVaL : true;
    }

    //Leave Request By EMP.. Time Mail And SMS To IO... 
    public function leavereqmailsmstoio($leaveID) {
        $this->db->select('a.*,b.user_name,c.firstname,c.userfullname,c.prefix_name,c.emailaddress,d.emailaddress as emailaddress_io,d.contactnumber as contactnumber_io,b.leavetype_name,b.rep_mang_id,b.rep_manager_name,c.contactnumber');
        $this->db->from('main_leaverequest as a');
        $this->db->join('main_leaverequest_summary as b', 'a.id=b.leave_req_id', 'LEFT');
        $this->db->join('main_employees_summary as c', 'a.user_id=c.user_id', 'LEFT');
        $this->db->join('main_employees_summary as d', 'c.reporting_manager=d.user_id', 'LEFT');
        $this->db->where(array('a.isactive' => '1', 'a.id' => $leaveID));
        $data['singleRecLeave'] = $this->db->get()->row();

        //Send SMS To IO Start Code..
        $to = $data['singleRecLeave']->emailaddress_io;
        $subject = "Leave Request For Approval";
        $content = "Dear " . $data['singleRecLeave']->rep_manager_name . "\n";
        $content .= " " . $data['singleRecLeave']->prefix_name . " " . $data['singleRecLeave']->userfullname . " has Applied leave from " . date('d-m-y', strtotime($data['singleRecLeave']->from_date)) . " To " . date('d-m-y', strtotime($data['singleRecLeave']->to_date)) . "\n";
        $content .= " Awaiting your approval. \n";
        $content .= "CEG HRMS";
        $smscontent = urlencode($content);
        $mob = $data['singleRecLeave']->contactnumber_io;
        $respon = file_get_contents("http://sms.getonesms.com/sendsms.jsp?password=f4fd42ad3fXX&sms=" . $smscontent . "&user=cegltd&senderid=CEGLTD&mobiles=" . $mob);
        //Send SMS To IO Close Code...
        $msgDetails = $this->load->view('email/leave_request_approval_io', $data, true);
        //Send Mail Close To IO Start Code..
        $returVaL = $this->sendMail($to, $subject, $msgDetails);
        return ($returVaL) ? $returVaL : true;
    }

    //Tour Mail Dummy Code Byu Asheesh..
    public function tourmail($tourID) {
        $data['error'] = '';
        $data['singleRecTour'] = $this->mastermodel->GetSingleTourDetailsByID($tourID);
        //########## Tour Self ##################
        $to = $data['singleRecTour']->emailaddress;
        $subject = "Tour Request For Approval";
        $msgDetails = $this->load->view('email/tour_request_approval', $data, true);
        $returVaL = $this->sendMail($to, $subject, $msgDetails);
        //########## Tour Mail IO ###############
        $to_IO = $data['singleRecTour']->reporting_manager_email;
        $subject_IO = "Tour Request For Approval";
        $msgDetails_IO = $this->load->view('email/tour_request_approval_io', $data, true);
        $returVaL_IO = $this->sendMail($to_IO, $subject_IO, $msgDetails_IO);
        return ($returVaL) ? $returVaL : true;
    }

    //Delete Cancel Tour ..
    public function deletetourbytid($tourID) {
        $user_ID = $this->session->userdata('loginid');
        $UpdData = array('is_active' => '0');
        $this->db->where(['id' => $tourID, 'is_active' => '1', 'emp_id' => $user_ID, 'approved_bypmanager' => '0']);
        $returnResp = $this->db->update('emp_tourapply', $UpdData);
        if ($returnResp):
            //Tour Delete Mail..
            $this->TourDeleteMailByLreqID($tourID);
            $this->session->set_flashdata('success_msg', 'Tour Delete Successfully.');
        endif;
        redirect(base_url("applytour"));
    }

    //Leave Cancel Delete ..
    public function deleteleavebytid($leaveID) {
        $user_ID = $this->session->userdata('loginid');
        $updateArr = array("leavestatus" => "Cancel");
        $this->db->where(['id' => $leaveID, 'isactive' => '1', 'user_id' => $user_ID]);
        $returnResp = $this->db->update('main_leaverequest', $updateArr);
        if ($returnResp):
            //Leave Delete Mail..
            $this->LeaveDeleteMailByLreqID($leaveID);
            $this->session->set_flashdata('success_msg', 'Leave Delete/Cancelled Successfully.');
        endif;
        redirect(base_url("myleave"));
    }

    //Get Tour By Id.. Ajx..
    public function ajax_getsingletourby_tid() {
        $tourID = $_REQUEST['tourtblid'];
        if ($tourID) {
            $singleRecTour = $this->mastermodel->GetSingleTourDetailsByID($tourID);
        }
        echo json_encode($singleRecTour);
    }

    //Get Single Req..
    public function ajax_getsingleleaveby_tid() {
        $leavetblID = $_REQUEST['leavetblid'];
        if ($leavetblID) {
            $singleRecLeave = $this->mastermodel->LeaveReqSummaryDetailsByReqID($leavetblID);
        }
        echo json_encode($singleRecLeave);
    }

    //Update Rec Tour Single Rows..
    public function toureditupdate() {
        $tourID = $_REQUEST['tour_id'];
        $updtour_projectid = $_REQUEST['updtour_projectid'];
        $updtour_location = $_REQUEST['updtour_location'];
        $upddescription = $_REQUEST['upddescription'];
        if ($tourID and $updtour_projectid and $updtour_location and $upddescription) {
            $updArr = array("project_id" => $updtour_projectid, "tour_location" => $updtour_location, "description" => $upddescription);
            $this->db->where(['id' => $tourID, 'is_active' => '1']);
            $returnResp = $this->db->update('emp_tourapply', $updArr);
        }
        if ($returnResp):
            //Tour Delete Mail..
            $this->session->set_flashdata('success_msg', 'Tour Edit/Updated Successfully.');
        endif;
        redirect(base_url("applytour"));
    }

    //Update Leave Rec Popup..
    public function leave_editupdate() {
        $leaveID = $_REQUEST['leave_id'];
        $upd_reason = $_REQUEST['upd_reason'];
        if ($leaveID and $upd_reason) {
            $updArr = array("reason" => $upd_reason);
            $this->db->where(['id' => $leaveID, 'isactive' => '1']);
            $returnResp = $this->db->update('main_leaverequest', $updArr);
        }
        if ($returnResp):
            //Tour Delete Mail..
            $this->session->set_flashdata('success_msg', 'Leave Edit/Updated Successfully.');
        endif;
        redirect(base_url("myleave"));
    }

    //Leave Delete Mail By Leave ID..
    public function LeaveDeleteMailByLreqID($leaveReqID) {
        //$leaveReqID = '7890';
        $singleRecLeave = $this->mastermodel->LeaveReqSummaryDetailsByReqID($leaveReqID);
        $data['singleRecLeave'] = $singleRecLeave;
        if ($singleRecLeave and $leaveReqID) {
            //Self mail..
            $to = $data['singleRecLeave']->emailaddress;
            $subject = "Leave Request Cancelled";
            $msgDetails = $this->load->view('email/leave_delete_mail_self', $data, true);
            $returVaL = $this->sendMail($to, $subject, $msgDetails);
            //IO Mail..
            $to_IO = $data['singleRecLeave']->emailaddress;
            $subject_IO = "Leave Request Cancelled";
            $msgDetails_IO = $this->load->view('email/leave_delete_mail_io', $data, true);
            $return = $this->sendMail($to_IO, $subject_IO, $msgDetails_IO);
        }
        return true;
    }

    //Tour Delete Mail By Leave ID..
    public function TourDeleteMailByLreqID($tourID) {
        $singleRecTour = $this->mastermodel->GetSingleTourDetailsByTourID($tourID);
        $data['singleRecTour'] = $singleRecTour;
        if ($singleRecTour and $tourID) {
            //Mail To Self..
            $to = $data['singleRecTour']->emailaddress;
            $subject = "Tour Request Cancelled";
            $msgDetails = $this->load->view('email/tour_delete_self', $data, true);
            $returVaL = $this->sendMail($to, $subject, $msgDetails);
            //Mail To IO..
            $to_IO = $data['singleRecTour']->reporting_manager_email;
            $subject_IO = "Tour Request Cancelled";
            $msgDetails_IO = $this->load->view('email/tour_delete_io', $data, true);
            $return = $this->sendMail($to_IO, $subject_IO, $msgDetails_IO);
        }
        return true;
    }

    //Send Mail..
    function sendMail($to, $subject, $msgDetails) {
        //$to = "ts.admin@cegindia.com";
        //return true;
        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->initialize(array('protocol' => 'smtp',
            'smtp_host' => 'mail.cegindia.com',
            'smtp_user' => 'marketing@cegindia.com',
            'smtp_pass' => 'MARK-2015ceg',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ));
        $CI->email->from('hrms.admin@cegindia.com', 'CEG-HRMS');
        $CI->email->to($to);
        $CI->email->bcc('marketing@cegindia.com');
        $CI->email->subject($subject);
        $CI->email->message($msgDetails);
        $resp = $CI->email->send();
        return ($resp) ? $resp : $CI->email->print_debugger();
    }

}
