<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Tank_auth
 *
 * Authentication library for Code Igniter.
 *
 * @package		Tank_auth
 * @author		Ilya Konyukhov (http://konyukhov.com/soft/)
 * @version		1.0.9
 * @based on	DX Auth by Dexcell (http://dexcell.shinsengumiteam.com/dx_auth)
 * @license		MIT License Copyright (c) 2008 Erick Hartanto
 */
class Tank_auth {

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('tank_auth/users');
    }

    /*     * * Get user_id @return string * * */

    function get_user_id() {
        return $this->ci->session->userdata('user_id');
    }

    /*     * * get user role * * */

    function get_role($id) {
        $role = $this->ci->users->get_role($id);
        return ($role) ? $role : null;
    }
	function get_user_bunit_by_id($id) {
	return $this->ci->users->get_userbunit_by_id($id);
   
}
    /*     * * get emp feedback department id * * */

    function get_feedback_dept($id) {
        $dept = $this->ci->users->get_feedback_dept($id);
        return ($dept) ? $dept : null;
    }

    /*     * * get emp feedback subdepartment id * * */

    function get_feedback_subdept($id) {
        $subdept = $this->ci->users->get_feedback_subdept($id);
        return ($subdept) ? $subdept : null;
    }

    /*     * * check emp feedback by  time slot * * */

    function check_feedback_data_exist($id) {
        $check_feedback = $this->ci->users->check_feedback_data_exist($id);
        return ($check_feedback) ? $check_feedback : null;
    }

}
