<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisalreporthr_model extends CI_Model {

    var $table = 'main_employees_summary as a';
    // var $project_table = 'tm_projects as p';
    var $column_order = array(null, 'a.userfullname');
    var $column_search = array('a.userfullname');
    var $order = array('a.user_id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        if ($this->input->post('appraisal_cycle')) {
            $this->db->where('b.appraisal_cycle_id', $this->input->post('appraisal_cycle'));
        }
        if ($this->input->post('businessunit_name')) {
            $this->db->where('a.businessunit_id', $this->input->post('businessunit_name'));
        }
        if ($this->input->post('userfullname')) {
            $this->db->like('a.userfullname', $this->input->post('userfullname'));
        }

//        if ($this->input->post('assignstatus')) {
//            if ($this->input->post('assignstatus') == "1") {
//                $this->db->where('b.appraisal_cycle_id', "1");
//            }
//            if ($this->input->post('assignstatus') == "2") {
//                $this->db->where('b.appraisal_cycle_id', "");
//            }
//        }

        $this->db->select('a.user_id,a.reporting_manager_name,a.employeeId,a.userfullname,a.contactnumber,b.appraisal_cycle_id,c.emp_self_lock,c.user_id as selfentry,c.emp_io_lock,c.emp_ro_lock,e.userfullname as nameof_ro');
        $this->db->from($this->table);
        $this->db->join("appr_mail_appraisal as b", 'a.user_id = b.emp_id', 'LEFt');
        $this->db->join("appr_action_master as c", "a.user_id=c.user_id", 'LEFt');
        $this->db->join("emp_otherofficial_data as d", "a.user_id=d.user_id", 'LEFt');
        $this->db->join("main_employees_summary as e", "d.reviewing_officer_ro=e.user_id", 'LEFt');

        $this->db->where('a.isactive', '1');
        $this->db->group_by('a.id');
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
//        if ($this->input->post('start_dates') and ( $this->input->post('end_dates'))) {
//            $start_date = $this->input->post('start_dates');
//            $end_date = $this->input->post('end_dates');
//            $where_date = "(l.from_date>='$start_date' AND l.to_date <= '$end_date')";
//            $this->db->where($where_date);
//        }
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
//        if ($this->input->post('start_dates') and ( $this->input->post('end_dates'))) {
//            $start_date = $this->input->post('start_dates');
//            $end_date = $this->input->post('end_dates');
//            $where_date = "(l.from_date>='$start_date' AND l.to_date <= '$end_date')";
//            $this->db->where($where_date);
//        }
        $this->db->from($this->table);
        $this->db->where('a.isactive', '1');
        return $this->db->count_all_results();
    }

}
