<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisalonproject_model extends CI_Model {

    var $table = 'main_employees_summary as a';
    var $column_order = array(null, 'a.userfullname');
    var $column_search = array('a.userfullname');
    var $order = array('a.userfullname' => 'ASC');

    public function __construct() {
        parent::__construct();
        $this->load->database();
  
    }

    private function _get_datatables_query() {
        $this->db->select('c.fld_id,c.current_status,a.reporting_manager,a.reporting_manager_name,b.employeeId,b.emailaddress,b.department_name,b.position_name,b.jobtitle_name');
        $this->db->from($this->table);
        $this->db->join("main_employees_summary as b", "a.reporting_manager=b.user_id", "LEFT");
        $this->db->join("appr_proj_assignbyhr as c", "c.rep_manager_id=a.reporting_manager", "LEFT");
        $this->db->where(array("a.isactive" => "1", "a.businessunit_id" => "3"));
        $this->db->group_by('a.reporting_manager');
        
        $userfullnameID = $this->input->post('userfullnameID');
        $employeeID = $this->input->post('employeeID');
        
        if ($this->input->post('jobtitgroupID')) {
            $this->db->where('b.jobtitle_id', $this->input->post('jobtitgroupID'));
        }
        
        if ($this->input->post('employeeID')) {
            $this->db->like('b.employeeId', $employeeID);
        }

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
