<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Team_function_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function tour_atten_report_ceg1($userID) {
		$today = date("Y-m-d"); 
		$this->db->select("a.*");
        $this->db->from('main_employees_summary as a');
        $this->db->join("emp_tourapply as b","a.user_id=b.emp_id","left");
        $this->db->join("emp_otherofficial_data as c","c.user_id=a.user_id","left");
        $this->db->join("tbl_ofclocation as d","d.id=c.company_location","left");
        $this->db->where(array('a.isactive'=>'1','start_date'=>$today));
		$this->db->where_in("a.user_id",$userID);
        $this->db->order_by("a.jobtitle_id",'ASC');
        $this->db->group_by('a.user_id');
		$recArr = $this->db->get()->result();
        // return ($recArr) ? $recArr : null;
        return $recArr;
    }
	
	//Get My Team Details..
    public function GetMyTeamDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_employees_summary as a');
        // $this->db->join("emp_otherofficial_data as b","b.user_id=a.user_id","left");
        // $this->db->where(array('a.reporting_manager' => $userID, 'a.isactive' => '1'));
        $this->db->where_in('a.user_id', $userID);
        $this->db->where(array('a.isactive' => '1'));
        $RecRows = $this->db->get()->result();
        // $RecRows = $this->db->get()->num_rows();
        return ($RecRows) ? $RecRows : '0';
		// return $RecRows;
    }
	
	//Get My Team Details..
    public function GetMyTeamDetailsRecByID_io_ro($userID) {
        $this->db->select('a.*');
        $this->db->from('main_employees_summary as a');
        // $this->db->join("emp_otherofficial_data as b","b.user_id=a.user_id","left");
        $this->db->where(array('a.reporting_manager' => $userID, 'a.isactive' => '1'));
        // $this->db->where(array("b.reviewing_officer_ro" => $userID));
        $RecRows = $this->db->get()->result();
        // $RecRows = $this->db->get()->num_rows();
        return $RecRows;
    }
	
	public function leave_atten_report_ceg_list($userID) {
		$today = date("Y-m-d"); 
		$this->db->select("a.*");
        $this->db->from('main_employees_summary as a');
        $this->db->join("main_leaverequest as b","a.user_id=b.user_id","left");
        $this->db->join("emp_otherofficial_data as c","c.user_id=a.user_id","left");
        $this->db->join("tbl_ofclocation as d","d.id=c.company_location","left");
        $this->db->where(array('a.isactive'=>'1','b.from_date'=>$today));
        $this->db->where_in("a.user_id",$userID);
		$this->db->order_by("a.jobtitle_id",'ASC');
        $this->db->group_by('a.user_id');
		$recArr = $this->db->get()->result();
        // return ($recArr) ? $recArr : null;
        return $recArr;
    }
	
	public function count_present_atten_report_ceg_list($userID) {
		$today = date("Y-m-d"); 
		// $this->db->select("a.user_id,c.*");
		$this->db->select("a.*");
		$this->db->from("main_employees_summary as a");
        $this->db->join("emp_otherofficial_data as b", "a.user_id=b.user_id", "LEFT");
        $this->db->join("devicelogs_processed as c", "c.UserId=b.machine_id", "LEFT");
		// $this->db->where(array("a.reporting_manager" => $userID,"a.isactive" => "1", "b.status" => "1"));
		$this->db->where(array("a.isactive" => "1", "b.status" => "1"));
		$this->db->where_in("a.user_id", $userID);
		$this->db->where("c.LogDate LIKE '%$today%'");
        // $this->db->or_where(array("b.reviewing_officer_ro" => $userID));
        // $this->db->where("c.Direction", "in");
        $this->db->order_by("c.DeviceLogId", "ASC");
        $this->db->group_by("c.UserId");
        $recArr = $this->db->get()->result();
        // $recArr = $this->db->get()->num_rows();
        return $recArr;
    }
	
	public function tour_atten_report_ceg1_for_absent($userID) {
		$today = date("Y-m-d"); 
		$this->db->select("a.user_id");
        $this->db->from('main_employees_summary as a');
        $this->db->join("emp_tourapply as b","a.user_id=b.emp_id","left");
        $this->db->join("emp_otherofficial_data as c","c.user_id=a.user_id","left");
        $this->db->join("tbl_ofclocation as d","d.id=c.company_location","left");
        $this->db->where(array('a.isactive'=>'1','start_date'=>$today));
		$this->db->where_in("a.user_id",$userID);
        $this->db->order_by("a.jobtitle_id",'ASC');
        $this->db->group_by('a.user_id');
		$recArr = $this->db->get()->result();
        // return ($recArr) ? $recArr : null;
        return $recArr;
    }
	
	public function leave_atten_report_ceg_list_for_absent($userID) {
		$today = date("Y-m-d"); 
		$this->db->select("a.user_id");
        $this->db->from('main_employees_summary as a');
        $this->db->join("main_leaverequest as b","a.user_id=b.user_id","left");
        $this->db->join("emp_otherofficial_data as c","c.user_id=a.user_id","left");
        $this->db->join("tbl_ofclocation as d","d.id=c.company_location","left");
        $this->db->where(array('a.isactive'=>'1','b.from_date'=>$today));
        $this->db->where_in("a.user_id",$userID);
		$this->db->order_by("a.jobtitle_id",'ASC');
        $this->db->group_by('a.user_id');
		$recArr = $this->db->get()->result();
        // return ($recArr) ? $recArr : null;
        return $recArr;
    }
	
	public function count_present_atten_report_ceg_list_absent($userID) {
		$today = date("Y-m-d"); 
		// $this->db->select("a.user_id,c.*");
		$this->db->select("a.user_id");
		$this->db->from("main_employees_summary as a");
        $this->db->join("emp_otherofficial_data as b", "a.user_id=b.user_id", "LEFT");
        $this->db->join("devicelogs_processed as c", "c.UserId=b.machine_id", "LEFT");
		// $this->db->where(array("a.reporting_manager" => $userID,"a.isactive" => "1", "b.status" => "1"));
		$this->db->where(array("a.isactive" => "1", "b.status" => "1"));
		$this->db->where_in("a.user_id", $userID);
		$this->db->where("c.LogDate LIKE '%$today%'");
        // $this->db->or_where(array("b.reviewing_officer_ro" => $userID));
        // $this->db->where("c.Direction", "in");
        $this->db->order_by("c.DeviceLogId", "ASC");
        $this->db->group_by("c.UserId");
        $recArr = $this->db->get()->result();
        // $recArr = $this->db->get()->num_rows();
        return $recArr;
    }
	
	public function GetBasicRecLoginUser_machineid($id) {
        // $id = $this->session->userdata('loginid');
        $this->db->select('user.*,b.thumbcode,b.payroll_with_name,b.machine_id');
        $this->db->from('main_employees_summary as user');
        $this->db->join("emp_otherofficial_data b", "user.user_id=b.user_id", "LEFT");
        $this->db->where(array("user.user_id" => $id, "user.isactive" => "1"));
        $this->db->where_in("user.user_id", $id);
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }
	
	//Get My Team Details..
    public function check_self_IO_RO($userID) {
        $this->db->select('a.user_id,a.reporting_manager');
        $this->db->from('main_employees_summary as a');
        // $this->db->join("emp_otherofficial_data as b","b.user_id=a.user_id","left");
        // $this->db->where(array('a.reporting_manager' => $userID, 'a.isactive' => '1'));
        // $this->db->where('a.reporting_manager !=', $userID);
        $this->db->where(array('a.isactive' => '1'));
        $RecRows = $this->db->get()->result();
        // $RecRows = $this->db->get()->num_rows();
        return ($RecRows) ? $RecRows : '0';
		// return $RecRows;
    }
	
	public function count_present_atten_report_list_logTime($userID) {
		$today = date("Y-m-d"); 
		// $this->db->select("a.user_id,c.*");
		$this->db->select("a.*,b.machine_id");
		$this->db->from("main_employees_summary as a");
        $this->db->join("emp_otherofficial_data as b", "a.user_id=b.user_id", "LEFT");
        $this->db->join("devicelogs_processed as c", "c.UserId=b.machine_id", "LEFT");
		// $this->db->where(array("a.reporting_manager" => $userID,"a.isactive" => "1", "b.status" => "1"));
		$this->db->where(array("a.isactive" => "1", "b.status" => "1"));
		$this->db->where_in("a.user_id", $userID);
		$this->db->where("c.LogDate LIKE '%$today%'");
        // $this->db->or_where(array("b.reviewing_officer_ro" => $userID));
        // $this->db->where("c.Direction", "in");
        $this->db->order_by("c.DeviceLogId", "ASC");
        $this->db->group_by("c.UserId");
        $recArr = $this->db->get()->result();
        // $recArr = $this->db->get()->num_rows();
        return $recArr;
    }

}

?>