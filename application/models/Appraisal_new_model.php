<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appraisal_new_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //######### New Hrms 06-02-2020 ###########
    public function GetBasicRecLoginUser() {
        $id = $this->session->userdata('loginid');
        $this->db->select('a.id,a.emp_id,a.emp_last_date,a.emp_grace_period,a.appraisal_cycle_id,b.cycle_date,c.emp_self_lock,c.emp_io_lock,c.emp_ro_lock,c.fld_id as appr_action_masterID');
        $this->db->from('appr_mail_appraisal as a');
        $this->db->join("appr_increment_cycle_master as b", "a.appraisal_cycle_id=b.fld_id", "LEFT");
        $this->db->join("appr_action_master as c", "(a.appraisal_cycle_id=c.appraisal_cycle_id AND c.user_id=a.emp_id)", "LEFT");
        $this->db->where(array("a.emp_id" => $id, "a.status" => "1"));
        $this->db->order_by("a.id","DESC");
        
        $ApprRecList = $this->db->get()->result();
        return ($ApprRecList) ? $ApprRecList : null;
    }

}

?>