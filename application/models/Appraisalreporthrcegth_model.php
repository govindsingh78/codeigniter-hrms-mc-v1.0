<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisalreporthrcegth_model extends CI_Model {

    var $table = 'appr_mail_appraisal as a';
    // var $project_table = 'tm_projects as p';
    var $column_order = array(null, 'a.emp_last_date');
    var $column_search = array('a.emp_last_date');
    var $order = array('a.id' => 'ASC'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        if ($this->input->post('userfullname')) {
            $this->db->like('b.userfullname', $this->input->post('userfullname'));
        }

        $this->db->select('a.emp_last_date,b.userfullname,b.employeeId,b.contactnumber,c.self_lock,c.io_lock,c.ro_lock,c.appraisal_cycle_id');
        $this->db->from($this->table);
        $this->db->join("main_employees_summary as b", 'a.emp_id = b.user_id', 'LEFt');
        $this->db->join("appr_action_master_cegth as c", "a.emp_id=c.user_id", 'LEFt');

        $this->db->where(array('a.status' => '1', 'a.assign_group_id' => '2'));
        $this->db->group_by('a.id');
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        $this->db->where('a.isactive', '1');
        return $this->db->count_all_results();
    }

}
