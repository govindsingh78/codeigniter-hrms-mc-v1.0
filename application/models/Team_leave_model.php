<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Team_leave_model extends CI_Model {

    var $table = 'main_leaverequest_summary as l';
    var $summery_table = 'main_employees_summary as b';
    var $project_table = 'tm_projects as p';
    var $column_order = array(null, 'l.from_date', 'l.to_date', 'l.no_of_days', 'l.leavetype_name', 'l.leavestatus', 'b.businessunit_name', 'b.userfullname', 'b.department_name', 'b.reporting_manager_name'); //set column field database for datatable orderable
    var $column_search = array('l.from_date', 'l.to_date', 'l.no_of_days', 'l.leavetype_name', 'l.leavestatus', 'b.businessunit_name', 'b.userfullname', 'b.department_name', 'b.reporting_manager_name'); //set column field database for datatable searchable 
    var $order = array('l.user_id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $user_id = $this->session->userdata('loginid');
        $this->db->select('l.*,b.businessunit_name,b.userfullname,b.department_name,b.reporting_manager_name');
        $this->db->from($this->table);
        $this->db->join($this->summery_table, 'l.user_id = b.user_id', 'left');
        $this->db->where(array('l.rep_mang_id' => $user_id, 'l.isactive' => '1', 'b.isactive' => '1'));
		$this->db->order_by("l.id", "desc");
        $this->db->group_by('l.id');

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) { // first loop
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
