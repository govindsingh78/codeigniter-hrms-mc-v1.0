<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appraisalio_model extends CI_Model {

    var $table = 'emp_otherofficial_data as a';
    var $table_summary = 'main_employees_summary as b';
    var $table_appr_mail_appraisal = 'appr_mail_appraisal as c';
    var $table_appr_increment_cycle_master = 'appr_increment_cycle_master as d';
    var $table_appr_action_master = 'appr_action_master as e';
    var $column_order = array(null, 'b.userfullname', 'b.employeeId', 'b.emailaddress', 'b.contactnumber');
    var $column_search = array('b.userfullname', 'b.employeeId', 'b.emailaddress', 'b.contactnumber');
    var $order = array('b.id' => 'ASC');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $user_id = $this->session->userdata('loginid');
        $this->db->select('b.userfullname,b.employeeId,b.emailaddress,b.contactnumber,e.fld_id,e.emp_io_lock,d.cycle_date,c.assign_group_id,a.user_id');
        $this->db->from("emp_otherofficial_data as a");
        $this->db->join("main_employees_summary as b", "a.user_id=b.user_id", "Left");
        $this->db->join("appr_mail_appraisal as c", "a.user_id=c.emp_id", "INNER");
        $this->db->join("appr_increment_cycle_master as d", "c.appraisal_cycle_id=d.fld_id", "LEFT");
        $this->db->join("appr_action_master as e", "b.user_id=e.user_id", "LEFT");
        $this->db->where(array("a.reviewing_officer_ro" => $user_id));
        $this->db->group_by('a.id');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
