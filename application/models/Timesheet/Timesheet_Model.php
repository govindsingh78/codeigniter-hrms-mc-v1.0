<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Timesheet_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Get All Assign Project By User ID..
    public function GetAllProjListByUserID($userID) {
        $this->db->select("a.id,b.project_name,a.project_id");
        $this->db->from("tm_project_employees as a");
        $this->db->join("tm_projects as b", "a.project_id=b.id", "LEFT");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $userID]);
        $this->db->order_by("b.project_name", "ASC");
        $recordProjArr = $this->db->get()->result_array();

        $AllReturnArr = array();
        if ($recordProjArr) {
            foreach ($recordProjArr as $rEcd) {
                $rEcd['taskDetailsArr'] = $this->GetAllTaskArr($userID, $rEcd['project_id']);
                if ($rEcd['taskDetailsArr']):
                    $AllReturnArr[] = $rEcd;
                endif;
            }
        }
        return ($AllReturnArr) ? $AllReturnArr : "";
    }

    //Recent Project List..
    public function GetAllRecentProjListByUserID($userID) {
        $this->db->select("b.project_name,a.project_id");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->join("tm_projects as b", "a.project_id=b.id", "LEFT");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $userID]);
        $this->db->group_by("a.project_id");
        $this->db->order_by("a.id", "DESC");
        $this->db->limit("5");
        $recordProjArr = $this->db->get()->result_array();

        $AllReturnArr = array();
        if ($recordProjArr) {
            foreach ($recordProjArr as $rEcd) {
                $rEcd['taskDetailsArr'] = $this->GetAllTaskArr($userID, $rEcd['project_id']);
                if ($rEcd['taskDetailsArr']):
                    $AllReturnArr[] = $rEcd;
                endif;
            }
        }
        return ($AllReturnArr) ? $AllReturnArr : "";
    }

    //All Task By Proj ID..
    public function GetAllTaskArr($userID, $projID) {
        $this->db->select("a.id,b.task,a.project_task_id");
        $this->db->from("tm_project_task_employees as a");
        $this->db->join("tm_tasks as b", "a.task_id=b.id", "LEFT");
        $this->db->where(["a.is_active" => "1", "a.emp_id" => $userID, "a.project_id" => $projID]);
        $recordProjTaskArr = $this->db->get()->result_array();
        return ($recordProjTaskArr) ? $recordProjTaskArr : null;
    }

    //Update Table Record...
    public function UpdateTableData($tblRec, $WhereArr, $updRecArr) {
        $this->db->where($WhereArr);
        return $this->db->update($tblRec, $updRecArr);
    }

    //Time Sheet Details..
    public function weeklyTsRecByprojTaskID($WhereArr) {
        $this->db->select("a.id,a.sun_date,a.sun_duration,a.mon_date,a.mon_duration,a.tue_date,a.tue_duration,a.wed_date,a.wed_duration,a.thu_date,a.thu_duration,a.fri_date,a.fri_duration,a.sat_date,a.sat_duration,a.week_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1"]);
        $this->db->where($WhereArr);
        $recordArr = $this->db->get()->row();
        return ($recordArr) ? $recordArr : null;
    }

    //Get Weekly TS Notes arr..
    public function GetWeeklyTSNoteArr($WhereArr){
        $this->db->select("a.*");
        $this->db->from("tm_emp_ts_notes as a");
        $this->db->select($WhereArr);
        $tSNotesArr = $this->db->get()->row();
        return ($tSNotesArr) ? $tSNotesArr : null;
    }
    
    
    //Time Sheet Weekly Duration Sum..
    public function SumTsWeekDayDuration($emp_id, $ts_year, $ts_month, $ts_week, $cal_week) {
        $sun_durationArr = array();
        $mon_durationArr = array();
        $tue_durationArr = array();
        $wed_durationArr = array();
        $thu_durationArr = array();
        $fri_durationArr = array();
        $sat_durationArr = array();
        $Week_durationArr = array();
        $reurnArr = array();
        $WhereArr = ["emp_id" => $emp_id, "ts_year" => $ts_year, "ts_month" => $ts_month, "ts_week" => $ts_week, "cal_week" => $cal_week];
        $this->db->select("a.sun_duration,a.mon_duration,a.tue_duration,a.wed_duration,a.thu_duration,a.fri_duration,a.sat_duration,a.week_duration");
        $this->db->from("tm_emp_timesheets as a");
        $this->db->where(["a.is_active" => "1"]);
        $this->db->where($WhereArr);
        $recordArr = $this->db->get()->result();
        if ($recordArr) {
            foreach ($recordArr as $kKeY => $rOws) {
                $sun_durationArr[] = $rOws->sun_duration;
                $mon_durationArr[] = $rOws->mon_duration;
                $tue_durationArr[] = $rOws->tue_duration;
                $wed_durationArr[] = $rOws->wed_duration;
                $thu_durationArr[] = $rOws->thu_duration;
                $fri_durationArr[] = $rOws->fri_duration;
                $sat_durationArr[] = $rOws->sat_duration;
                $Week_durationArr[] = $rOws->week_duration;
            }
            //SumAll..
            $reurnArr = array("Sun_DurationSum" => $this->AddDurationTime($sun_durationArr), "Mon_DurationSum" => $this->AddDurationTime($mon_durationArr), "Tue_DurationSum" => $this->AddDurationTime($tue_durationArr), "Wed_DurationSum" => $this->AddDurationTime($wed_durationArr), "Thu_DurationSum" => $this->AddDurationTime($thu_durationArr), "Fri_DurationSum" => $this->AddDurationTime($fri_durationArr), "Sat_DurationSum" => $this->AddDurationTime($sat_durationArr), "Week_DurationSum" => $this->AddDurationTime($Week_durationArr));
        }
        return ($reurnArr) ? $reurnArr : null;
    }

    //Sum Sultiple Hour Minute Val..
    public function AddDurationTime($times) {
        $minutes = 0;
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }
        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;
        return sprintf('%02d:%02d', $hours, $minutes);
    }

}
