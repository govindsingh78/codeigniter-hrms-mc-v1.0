<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TimesheetAsRo_Model extends CI_Model {

    var $timesheet_table = 'tm_emp_timesheets as a';
    var $project_table = 'tm_projects as b';
    var $summery_table = 'main_employees_summary as c';
    var $project_task_table = 'tm_project_task_employees as d';
    var $task_table = 'tm_tasks as e';
    var $timesheet_status = 'tm_ts_status as f';
    var $column_order = array(null, 'b.project_name'); //set column field database for datatable orderable
    var $column_search = array('b.project_name'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        if ($this->input->post('ro_project')) {
            $this->db->where('a.project_id', $this->input->post('ro_project'));
        }
        if ($this->input->post('emp_name')) {
            $this->db->where('c.user_id', $this->input->post('emp_name'));
        }
        if ($this->input->post('from_date') and $this->input->post('to_date')) {
            $fromdate = date("Y-m-d", strtotime($this->input->post('from_date')));
            $todate = date("Y-m-d", strtotime($this->input->post('to_date')));
            $this->db->where('a.created >=', $fromdate);
            $this->db->where('a.created <=', $todate);
        }
        if ($this->input->post('status')) {
            $this->db->where('f.week_status', $this->input->post('status'));
        }
        $user_id = $this->session->userdata('loginid');
        $this->db->select('SUM(a.week_duration) as wd, f.week_status,a.id,a.emp_id,a.project_task_id,a.project_id,a.sun_date,a.sun_duration,a.mon_date,a.mon_duration,a.tue_date,a.tue_duration,a.wed_date,a.wed_duration,a.thu_date,a.thu_duration,a.fri_date,a.fri_duration,a.sat_date,a.sat_duration,a.week_duration,b.project_name,c.userfullname,c.employeeId');
        $this->db->from($this->summery_table);
        $this->db->where(array('c.isactive' => '1', 'c.reporting_manager' => $user_id));
        $this->db->join($this->timesheet_table, 'a.emp_id = c.user_id', 'inner');
        $this->db->join($this->timesheet_status, 'f.emp_id = a.emp_id AND f.sun_date = a.sun_date AND f.project_id=a.project_id', 'inner');
        $this->db->join($this->project_table, 'a.project_id = b.id', 'inner');
        $this->db->order_by("a.id", "desc");
        $this->db->group_by('a.sun_date');


        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return FALSE;
        }
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->timesheet_table);
        return $this->db->count_all_results();
    }

    public function getuserlist() {
        $user_id = $this->session->userdata('loginid');
        $this->db->select('c.userfullname,c.user_id');
        $this->db->from($this->summery_table);
        $this->db->where(array('c.isactive' => '1', 'c.reporting_manager' => $user_id));
        $this->db->join($this->timesheet_table, 'c.user_id=a.emp_id', 'inner');
        $this->db->order_by("a.emp_id", "ASC");
        $this->db->group_by('a.emp_id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function getprojectlist() {
        $user_id = $this->session->userdata('loginid');
        $this->db->select('b.id,b.project_name,e.task');
        $this->db->from($this->summery_table);
        $this->db->where(array('c.isactive' => '1', 'c.reporting_manager' => $user_id));
        $this->db->join($this->timesheet_table, 'c.user_id=a.emp_id', 'inner');
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id OR d.emp_id=a.emp_id OR d.project_id=a.project_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.project_id", "ASC");
        $this->db->group_by('a.project_id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function getusertimesheet($user_id) {
        $this->db->select('f.sun_project_status,f.mon_project_status,f.tue_project_status,f.wed_project_status,f.thu_project_status,f.fri_project_status,f.sat_project_status,a.emp_id,a.sun_date, SUM(a.sun_duration) as sun_dur, a.mon_date, SUM(a.mon_duration) as mon_dur,a.tue_date,SUM(a.tue_duration) as tue_dur,a.wed_date,SUM(a.wed_duration) as wed_dur,a.thu_date,SUM(a.thu_duration) as thu_dur,a.fri_date,SUM(a.fri_duration) as fri_dur,a.sat_date,SUM(a.sat_duration) as sat_dur');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $user_id));
        $this->db->join($this->timesheet_status, 'f.emp_id = a.emp_id AND f.sun_date = a.sun_date AND f.project_id=a.project_id', 'inner');
        $this->db->order_by("a.id", "desc");
        $this->db->group_by("a.sun_date,a.mon_date,a.tue_date,a.wed_date,a.thu_date,a.fri_date,a.sat_date");
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_time_report($user_id) {
        $this->db->select('c.userfullname,c.employeeId,c.contactnumber,c.emailaddress,c.date_of_joining,c.reporting_manager_name,c.department_name,c.position_name,c.profileimg,c.isactive');
        $this->db->from($this->summery_table);
        $this->db->where(array('c.user_id' => $user_id));
        $this->db->order_by("c.user_id", "desc");
        $this->db->group_by('c.user_id');
        $result = $this->db->get()->row_array();
        return ($result) ? $result : null;
    }

    public function get_sun_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.sun_date,a.sun_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.sun_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_mon_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.mon_date,a.mon_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.mon_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_tue_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.tue_date,a.tue_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.tue_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_wed_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.wed_date,a.wed_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.wed_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_thu_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.thu_date,a.thu_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.thu_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_fri_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.fri_date,a.fri_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.fri_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_sat_time_report($emp_id, $date) {
        $this->db->select('b.id,b.project_name,e.task,a.emp_id,a.sat_date,a.sat_duration');
        $this->db->from($this->timesheet_table);
        $this->db->where(array('a.emp_id' => $emp_id, 'a.sat_date' => $date));
        $this->db->join($this->project_table, 'a.project_id=b.id', 'inner');
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $this->db->order_by("a.id", "ASC");
        $this->db->group_by('a.id');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_list_timesheet($emp_id) {
        $this->db->select('e.task,a.project_id,a.project_task_id,a.sun_date,a.mon_date,a.tue_date,a.wed_date,a.thu_date,a.fri_date,a.sat_date');
        $this->db->from($this->timesheet_table);
        $this->db->where('a.emp_id', $emp_id);
        $this->db->join($this->project_task_table, 'd.project_task_id=a.project_task_id', 'inner');
        $this->db->join($this->task_table, 'e.id=d.task_id', 'inner');
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function get_list_timesheet_status($emp_id) {
        $this->db->select('f.emp_id,f.sun_date,f.mon_date,f.tue_date,f.wed_date,f.thu_date,f.fri_date,f.sat_date');
        $this->db->from($this->timesheet_status);
        $this->db->where('f.emp_id', $emp_id);
        $result = $this->db->get()->result();
        return ($result) ? $result : null;
    }

    public function update_timesheet_status($tablename, $updateArr, $where) {
        $this->db->where($where);
        $result = $this->db->update($tablename, $updateArr);
        if ($this->db->affected_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }

}
