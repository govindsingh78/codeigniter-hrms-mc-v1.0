<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appraisal_acc_part_model extends CI_Model {
	
	
	// var $table = 'main_users as a';
    var $table = 'main_employees_summary as a';
    var $edudetail_table = 'main_empeducationdetails as b';
    var $appraisal_acc_table = 'annual_appraisal_account as c';
    // var $otherofficial_table = 'emp_otherofficial_data as g';
    var $column_order = array(null, 'a.userfullname');
    var $column_search = array('a.userfullname');
    var $order = array('a.userfullname' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        if ($this->input->post('businessunit_name')) {
            $this->db->where('a.businessunit_id', $this->input->post('businessunit_name'));
        }
        if ($this->input->post('department_id')) {
            $this->db->where('a.department_id', $this->input->post('department_id'));
        }

        $this->db->select('c.billRate,c.origBillrate,c.remark,a.user_id,a.userfullname,a.position_name,a.date_of_joining,a.years_exp');
        $this->db->from($this->table);
        $this->db->where(array('a.isactive' => '1'));
        $this->db->join($this->appraisal_acc_table, 'c.user_id = a.user_id', 'left');
        // $this->db->join($this->otherofficial_table, 'a.id = g.user_id', 'left');
        $this->db->group_by('a.user_id');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
         $this->_get_datatables_query();
         $query = $this->db->get();
         return $query->num_rows();
        
    }

    public function count_all() {
         $this->db->from($this->table);
        return $this->db->count_all_results();
        
    }
	
	
	
    


}

?>