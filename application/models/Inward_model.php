<?php

class Inward_model extends CI_Model {

    var $table = 'main_inwardcourier as a';
    var $user_table = 'main_users as b';
    var $user_table1 = 'main_users as d';
	var $pincode_tbl = 'tbl_comapnyinfo as t';
	var $company_table = 'tbl_companylist as p';
    var $businessunits_table = 'main_businessunits as c';
    var $column_order = array(null, 'b.userfullname', 'd.emp2', 'c.unitname', 'p.company_id', 't.pincode', 'a.letter_date', 'a.officer_filepass', 'a.letter_date');
    var $column_search = array('b.userfullname', 'd.emp2', 'c.unitname',  'a.letter_date', 'p.company_id', 't.pincode', 'a.agency_reference', 'a.officer_filepass');

    //var $order = array('t.emp_id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
	   if($this->input->post('businessunit_name')) {
            $this->db->where('a.businessunitid', $this->input->post('businessunit_name'));
       }
        if($this->input->post('company_name')) {
            $this->db->where('a.company_id', $this->input->post('company_name'));
       }
	   if(($this->input->post('from_date')) and ($this->input->post('to_date'))) {
           $from_date = date("Y-m-d", strtotime($this->input->post('from_date')));
           $to_date = date("Y-m-d", strtotime($this->input->post('to_date')));
           $this->db->where("a.letter_date >=",$from_date);
           $this->db->where("a.letter_date <=",$to_date);
        }
        $this->db->select('a.*,b.userfullname as emp1,d.userfullname as emp2, p.company_name, t.pincode, c.unitname');
        $this->db->from($this->table);
        $this->db->join($this->user_table, 'a.forwardemp_id1 = b.id', 'LEFT');
        $this->db->join($this->user_table1, 'a.forwardemp_id2 = d.id', 'LEFT');
		$this->db->join($this->pincode_tbl, 'a.receiverpincode1 = t.id', 'LEFT');
		$this->db->join($this->company_table, 'a.company_id = p.company_id', 'LEFT');
		$this->db->join($this->businessunits_table, 'a.businessunitid = c.id', 'LEFT');
        $this->db->order_by('a.id', 'DESC');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
        return 0;
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
        return 0;
    }

    public function get_list_businessunit() {
        $this->db->select('*');
        $this->db->from('main_businessunits');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        $businessunit = $result;
        return $businessunit;
    }

    public function get_list_companyname() {
        $this->db->select('*');
        $this->db->from('tbl_companylist');
        $this->db->where('is_active', '1');
        $this->db->order_by('company_id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getCompanyRows($company_id) {
        $this->db->select('c.id, c.pincode');
        $this->db->from('tbl_comapnyinfo as c');
        $this->db->where('c.company_id', $company_id);
        $this->db->where('c.is_active', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getDetails($company_id) {
        $this->db->select('d.city_name, a.country_name, b.state_name, c.address1, c.address2, c.address3');
        $this->db->from('tbl_comapnyinfo as c');
        $this->db->join('countries as a', 'c.country = a.country_id', 'left');
        $this->db->join('states as b', 'c.state = b.state_id', 'left');
        $this->db->join('cities as d', 'c.city = d.city_id', 'left');
        $this->db->where('c.id', $company_id);
        $this->db->where('c.is_active', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getdeptnames($id) {
        $this->db->select('c.id, c.deptname');
        $this->db->from('main_departments as c');
        $this->db->where('c.unitid', $id);
        $this->db->where('c.isactive', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getempidbydept($id) {
        $this->db->select('c.user_id, u.userfullname');
        $this->db->from('main_employees_summary as c');
        $this->db->JOIN('main_users as u', 'c.user_id = u.id', 'left');
        $this->db->where('c.department_id', $id);


        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        return $result;
    }

    public function get_list_empdetails($id) {
        $this->db->select('c.emailaddress, c.contactnumber');
        $this->db->from('main_users as c');
        $this->db->where('c.id', $id);

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    public function insert($data) {
        if ($this->db->insert("tbl_comapnyinfo", $data)) {
            return true;
        }
    }

    public function insert2($data) {
        if ($this->db->insert("tbl_companylist", $data)) {
            return true;
        }
    }

    public function insertData($tablename, $data) {
        if ($this->db->insert($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCountryname() {
        $this->db->select('country_id,country_name');
        $this->db->from('countries');
        $this->db->where('status', '1');
        $this->db->order_by('country_name', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getstateBycountryID($countryID) {
        $this->db->select('state_id,state_name');
        $this->db->from('states');
        $this->db->where('status', '1');
        $this->db->order_by('state_name', 'asc');
        $this->db->where('country_id', $countryID);
        $query = $this->db->get();
        $result = $query->result();
        return isset($result) ? $result : false;
    }

    public function getcityBystateID($stateID) {
        $this->db->select('city_id,city_name');
        $this->db->from('cities');
        $this->db->where('status', '1');
        $this->db->order_by('city_name', 'asc');
        $this->db->where('state_id', $stateID);
        $query = $this->db->get();
        $result = $query->result();
        return isset($result) ? $result : false;
    }

}

?>