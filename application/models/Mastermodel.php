<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mastermodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //######### New Hrms 07-02-1992 ###########


     public function GetloginMarital() {
        
        $this->db->select('*');
        $this->db->from('main_maritalstatus');
         
        $Rec = $this->db->get()->result();
        return ($Rec) ? $Rec : null;
    }

     public function GetloginNationality() {
        $this->db->select('*');
        $this->db->from('main_nationality');
         
        $Rec = $this->db->get()->result();
        return ($Rec) ? $Rec : null;
    }

     public function GetloginLanguage() {
         
        $this->db->select('*');
        $this->db->from('main_language');

        $Rec = $this->db->get()->result();
        return ($Rec) ? $Rec : null;
    }


    public function GetBasicRecLoginUser() {
        $id = $this->session->userdata('loginid');
        $this->db->select('user.*,b.thumbcode,b.payroll_with_name,b.machine_id');
        $this->db->from('main_employees_summary as user');
        $this->db->join("emp_otherofficial_data b", "user.user_id=b.user_id", "LEFT");
        $this->db->where(array("user.user_id" => $id, "user.isactive" => "1"));
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }

    //Get Official Rec By UserId.. 
    public function GetOfficialDataByUserId($userID) {
        $this->db->select('user.*,c.prefix_name as prefix_name_ro,c.userfullname as userfullname_ro,d.subdepartment,e.company_name,b.probation_period_no,b.noticeperiod');
        $this->db->from('main_employees_summary as user');
        $this->db->join("emp_otherofficial_data b", "user.user_id=b.user_id", "LEFT");
        $this->db->join("main_employees_summary c", "b.reviewing_officer_ro=c.user_id", "LEFT");
        $this->db->join("main_subdepartments d", "b.sub_department=d.id", "LEFT");
        $this->db->join("tbl_companyname e", "b.company_name=e.id", "LEFT");
        $this->db->where(array("user.user_id" => $userID, "user.isactive" => "1"));
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }

    //Get Experiance Details..
    public function GetExperianceDetailByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empexperiancedetails as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }


     public function GetOtherDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('other_user_record as a');
        $this->db->where(array("a.user_id" => $userID, "a.status" => "0"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }


    

    //Skill..
    public function GetSkillRecordDetailByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empskills as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    public function GetEducationDetailRecByID($userID) {
        $this->db->select('a.*,b.educationlevelcode');
        $this->db->from('main_empeducationdetails as a');
        $this->db->join('main_educationlevelcode as b', "a.educationlevel=b.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    public function GetEducationLevel() {
            $this->db->select('*');
            $this->db->from('main_educationlevelcode');
            $Rec = $this->db->get()->result();
            return ($Rec) ? $Rec : null;

            
    }

    public function getOtherDocType() {
            $this->db->select('*');
            $this->db->from('main_doc_type');
            $Rec = $this->db->get()->result();
            return ($Rec) ? $Rec : null;
      
    }

    //Family Details..
    public function GetFamilyDetailRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_empdependencydetails as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }


    public function GetReferencesDetailRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('personnel_professional_reference as a');
        $this->db->where(array("a.user_id" => $userID, "a.status" => "0"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Employee Docs..
    public function GetEmployeeDocsDetailRecByID($userID) {
        // , b.doc_name as otherDoc, c.educationlevelcode as educationDoc
        $this->db->select('a.*');
        $this->db->from('main_employeedocuments as a');

        // $this->db->join('main_doc_type as b', "a.doc_type_id=b.id", "LEFT");
        // $this->db->join('main_educationlevelcode as c', "a.educationlevel=c.id", "LEFT");

        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

//Get Personnal Details..
    public function GetPersonalDetailsRecByID($userID) {
        $this->db->select('a.*,b.maritalstatusname,c.nationalitycode,d.languagename');
        $this->db->from('main_emppersonaldetails as a');
        $this->db->join('main_maritalstatus as b', "a.maritalstatusid=b.id", "LEFT");
        $this->db->join('main_nationality as c', "a.nationalityid=c.id", "LEFT");
        $this->db->join('main_language as d', "a.languageid=d.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

//Contact Details..
    public function GetContactDetailRecByID($userID) {
        $this->db->select('a.*,b.country_name as per_country_name,c.country_name as current_country_name,d.state_name as perm_state_name,e.state_name as current_state_name,f.city_name as perm_city_name,g.city_name as current_city_name');
        $this->db->from('main_empcommunicationdetails as a');
        $this->db->join('tbl_countries as b', "a.perm_country=b.id", "LEFT");
        $this->db->join('tbl_countries as c', "a.current_country=c.id", "LEFT");
        $this->db->join('tbl_states as d', "a.perm_state=d.id", "LEFT");
        $this->db->join('tbl_states as e', "a.current_state=e.id", "LEFT");
        $this->db->join('tbl_cities as f', "a.perm_city=f.id", "LEFT");
        $this->db->join('tbl_cities as g', "a.current_city=g.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }


     


    public function GetloginCountries() {
        $this->db->select('*');
        $this->db->from('tbl_countries');
        $Rec = $this->db->get()->result();
        return ($Rec) ? $Rec : null;
    }

    public function GetloginStates() {
        $this->db->select('*');
        $this->db->from('tbl_states');
        $Rec = $this->db->get()->result();
        return ($Rec) ? $Rec : null;
    }

    public function GetloginCities() {
            $this->db->select('*');
            $this->db->from('tbl_cities');
            $Rec = $this->db->get()->result();
            return ($Rec) ? $Rec : null;
    }
     public function loginUsersRelatives() {
            $this->db->select('id,userfullname,employeeId');
            $this->db->from('main_users');
            $Rec = $this->db->get()->result();
            return ($Rec) ? $Rec : null;
    }

     



    //Get Previous Day In Time And Out Time..
    public function GetInOutTimeDetails($userThumbID) {
        $this->db->select('a.*');
        $this->db->from('thumb_attendance as a');
        $this->db->where(array("a.EmployeeID" => $userThumbID));
        $this->db->where(array("a.Status" => "1"));
        $this->db->order_by("a.thumbid", "DESC");
        $this->db->limit(1);
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

    //Get Employee Salary Details..
    public function GetEmpSalaryDetailRecByID($userID) {
        $this->db->select('a.*,b.currencyname,c.freqtype');
        $this->db->from('main_empsalarydetails as a');
        $this->db->join('main_currency as b', "a.currencyid=b.id", "LEFT");
        $this->db->join('main_payfrequency as c', "a.salarytype=c.id", "LEFT");
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->row();
        return ($RecRows) ? $RecRows : null;
    }

    //Pay Slips Details..
    public function GetEmpPayslipsRecByuserID($payroll_with_name, $year = '') {
        $this->db->select('a.*');
        $this->db->from('payslip_filepath as a');
        $this->db->where(array("a.payroll_with_name" => $payroll_with_name, "a.is_active" => "1"));
        if ($year) {
            $this->db->where("a.year", $year);
        }
        $this->db->order_by("a.id", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get ITR Form Record By User Id..
    public function GetItrFormDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_itr as a');
        $this->db->where(array("a.user_id" => $userID, "a.is_active" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get My Team Details..
    public function GetMyTeamDetailsRecByID($userID) {
        $this->db->select('a.*');
        $this->db->from('main_employees_summary as a');
        $this->db->where(array("a.reporting_manager" => $userID, "a.isactive" => "1"));
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Applied Leave..
    public function GetMyAllAppliedLeaves($userID) {
        $this->db->select('a.*');
        $this->db->from('main_leaverequest_summary as a');
        $this->db->where(array("a.user_id" => $userID, "a.isactive" => "1"));
        $this->db->order_by("a.id", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Holidays List..
    public function GetAllHolidaysListRec($hgroupID = '', $hyear = '') {
        $this->db->select('a.*,b.groupname');
        $this->db->from('main_holidaydates as a');
        $this->db->join('main_holidaygroups as b', "a.groupid=b.id", "LEFT");
        $this->db->where(array("a.isactive" => "1"));
        if ($hgroupID) {
            $this->db->where(array("a.groupid" => $hgroupID));
        }
        if ($hyear) {
            $this->db->where(array("a.holidayyear" => $hyear));
        }
        $this->db->order_by("a.holidaydate", "ASC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Tour List By UserID..
    public function GetAllAppliedTourListRec($userID) {
        $this->db->select('a.*,b.project_name,c.prefix_name,c.userfullname');
        $this->db->from('emp_tourapply as a');
        $this->db->join('tm_projects as b', "a.project_id=b.id", "LEFT");
        $this->db->join('main_employees_summary as c', "a.emp_id=c.user_id", "LEFT");
        $this->db->where(array("a.emp_id" => $userID, "a.is_active" => "1"));
        $this->db->order_by("a.start_date", "DESC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

    //Get All Project List..
    public function GelAllActiveProjectList() {
        $this->db->select('a.id,a.project_name');
        $this->db->from('tm_projects as a');
        $this->db->where(array("a.is_active" => "1"));
        $this->db->order_by("a.project_name", "ASC");
        $RecRows = $this->db->get()->result();
        return ($RecRows) ? $RecRows : null;
    }

//check login
    public function login_user_action($LoginCredentialArr) {
        $Where = array('employeeId' => $LoginCredentialArr['username'], 'emppassword' => md5($LoginCredentialArr['password']), 'isactive' => '1');
        $this->db->select('*');
        $this->db->from('main_users');
        $this->db->where($Where);
        $recArr = $this->db->get()->result();
        if ($recArr):
            return $recArr;
        else:
            return false;
        endif;
    }

    //Get Avl. Leave Balance..
    public function GetAvlLeaveBalance($user_id) {
        $Where = array('user_id' => $user_id, 'isactive' => '1');
        $this->db->select('emp_leave_limit');
        $this->db->from('main_employeeleaves');
        $this->db->where($Where);
        $recArr = $this->db->get()->row();
        return ($recArr) ? $recArr->emp_leave_limit : "";
    }

    //Get Leave All ready Exist or not on this date..
    public function leave_already_exists_or_not($start_date, $end_date, $loginID) {
        $fromdate = date("Y-m-d", strtotime($start_date));
        $todate = date("Y-m-d", strtotime($end_date));
        $this->db->select('id');
        $this->db->from('main_leaverequest');
        $this->db->where(array('isactive' => '1', 'user_id' => $loginID));
        $where_date = "((`from_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`to_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`from_date`<='" . $fromdate . "' AND `to_date`>='" . $todate . "'))";
        $this->db->where($where_date);
        $this->db->where("leavestatus !=", "Cancel");
        $this->db->where("leavestatus !=", "Rejected");
        $ChKExist = $this->db->get()->num_rows();
        return ($ChKExist) ? $ChKExist : "";
    }

    //Get Tour All Ready Exist or not on this date..
    public function tour_already_exists_or_not($start_date, $end_date, $loginID) {
        $fromdate = date("Y-m-d", strtotime($start_date));
        $todate = date("Y-m-d", strtotime($end_date));

        $this->db->select('id');
        $this->db->from('emp_tourapply');
        $this->db->where(array('is_active' => '1', 'emp_id' => $loginID));
        $where_date = "((`start_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`end_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`start_date`<='" . $fromdate . "' AND `end_date`>='" . $todate . "'))";
        $this->db->where($where_date);
        $this->db->where("approved_bypmanager !=", "2");
        $this->db->where("approved_bylmanager !=", "2");
        $ChKExist = $this->db->get()->num_rows();
        return ($ChKExist) ? $ChKExist : "";
    }

    //other Ofc Data Rec.. 
    public function GetOtherOfcDataByUserID($userID) {
        $this->db->select('a.machine_id,a.company_location');
        $this->db->from('emp_otherofficial_data as a');
        $this->db->where(array("a.user_id" => $userID, "a.status" => "1"));
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow : null;
    }

    //Get Yesterday Out Time By User ID..
    public function GetYesterdayOutTime($userID) {
        $todayDate = date("d-m-Y");
        $this->db->select('a.LogDate');
        $this->db->from('devicelogs_processed as a');
        $this->db->where(array("a.UserId" => $userID));
        $WhereCond = "DATE_FORMAT(`LogDate`,'%d-%m-%Y')<'" . $todayDate . "'";
        $this->db->where($WhereCond);
        $this->db->order_by("a.DeviceLogId", "DESC");
        $this->db->limit("1");
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow->LogDate : null;
    }

    //Get Today Out Time By User ID..
    public function GetTodayInTime($userID) {
        $todayDate = date("d-m-Y");
        $this->db->select('a.LogDate');
        $this->db->from('devicelogs_processed as a');
        $this->db->where(array("a.UserId" => $userID));
        $WhereCond = "DATE_FORMAT(`LogDate`,'%d-%m-%Y')='" . $todayDate . "'";
        $this->db->where($WhereCond);
        $this->db->order_by("a.DeviceLogId", "ASC");
        $this->db->limit("1");
        $RecSingleRow = $this->db->get()->row();
        return ($RecSingleRow) ? $RecSingleRow->LogDate : null;
    }

    //Get First And Third Sat of current Year..
    public function firs_third_Sat_AllSun_Holidays_ListCurrentMonth() {
        $firsThirArr = '';
        $array = array();
        $YeAr = date("Y");
        $month = date("m");

        //For Current Year-Month.
        $actdate = "$YeAr-$month-01";
        $firstReturn = date("Y-m-d", strtotime("first saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $firstSunDReturn1 = date("Y-m-d", strtotime("first sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $firstSunDReturn2 = date("Y-m-d", strtotime("second sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $thirdReturn = date("Y-m-d", strtotime("third saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        //All Sunday..

        $firstSunDReturn3 = date("Y-m-d", strtotime("third sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $firstSunDReturn4 = date("Y-m-d", strtotime("fourth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $firstSunDReturn5 = date("Y-m-d", strtotime("fifth sunday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));

        //Sund..
        array_push($array, $firstReturn);
        array_push($array, $firstSunDReturn1);
        array_push($array, $firstSunDReturn2);
        array_push($array, $thirdReturn);
        array_push($array, $firstSunDReturn3);
        array_push($array, $firstSunDReturn4);
        array_push($array, $firstSunDReturn5);

        //Got All Holidays List..
        $recHolidaysList = $this->holidayCount("$YeAr-$month-01", "$YeAr-$month-31");
        if ($recHolidaysList) {
            foreach ($recHolidaysList as $rechday) {
                array_push($array, $rechday);
            }
        }
        asort($array);
        return array_unique($array);
    }

    //Get First And Third Sat of current Year..
    public function firs_third_Sat_Holidays_ListCurrentMonth() {
        $firsThirArr = '';
        $array = array();
        $YeAr = date("Y");
        $month = date("m");
        //For Current Year-Month.
        $actdate = "$YeAr-$month-01";
        $firstReturn = date("d-m-Y", strtotime("first saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        $thirdReturn = date("d-m-Y", strtotime("third saturday of " . date('M', strtotime($actdate)) . " " . date('Y', strtotime($actdate)) . ""));
        //All Sunday..
        //Sund..
        array_push($array, $firstReturn);
        array_push($array, $thirdReturn);

        //Got All Holidays List..
        $recHolidaysList = $this->holidayCount("$YeAr-$month-01", "$YeAr-$month-31");
        if ($recHolidaysList) {
            foreach ($recHolidaysList as $rechday) {
                array_push($array, $rechday);
            }
        }
        asort($array);
        return array_unique($array);
    }

    //Get All Holidays List..
    public function holidayCount($startDate, $endDate) {
        $ReturnHolidateArr = array();
        $this->db->select('id,holidaydate');
        $this->db->from('main_holidaydates');
        $where_date = "(holidaydate>='$startDate' AND holidaydate <= '$endDate')";
        $this->db->where($where_date);
        $arr = array('isactive' => '1', 'groupid' => '1');
        $this->db->where($arr);
        $res = $this->db->get()->result();
        if ($res) {
            foreach ($res as $val) {
                $ReturnHolidateArr[] = date("d-m-Y", strtotime($val->holidaydate));
            }
        }
        return $ReturnHolidateArr ? $ReturnHolidateArr : '';
    }

    //################## Get All Leave Request List #####################
    public function GetAllLeaveListArr($start_date, $end_date, $loginID) {
        $fromdate = date("Y-m-d", strtotime($start_date));
        $todate = date("Y-m-d", strtotime($end_date));
        $this->db->select('from_date,to_date');
        $this->db->from('main_leaverequest');
        $this->db->where(array('isactive' => '1', 'user_id' => $loginID));
        $where_date = "((`from_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`to_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`from_date`<='" . $fromdate . "' AND `to_date`>='" . $todate . "'))";
        $this->db->where($where_date);
        $this->db->where("leavestatus !=", "Cancel");
        $this->db->where("leavestatus !=", "Rejected");
        $LeaveReqList = $this->db->get()->result();

        //Loop Cond..
        $AllDatesArr = array();
        //All Leaves Dates..
        if ($LeaveReqList) {
            foreach ($LeaveReqList as $LvDate) {
                $recDDarr = $this->getDatesFromRange($LvDate->from_date, $LvDate->to_date);
                if ($recDDarr):
                    foreach ($recDDarr as $rEdRows):
                        $AllDatesArr[] = date("d-m-Y", strtotime($rEdRows));
                    endforeach;
                endif;
            }
        }
        return ($AllDatesArr) ? $AllDatesArr : $AllDatesArr;
    }

    //################## Get All Tour Request List #####################
    public function GetAllTourListArr($start_date, $end_date, $loginID) {
        $fromdate = date("Y-m-d", strtotime($start_date));
        $todate = date("Y-m-d", strtotime($end_date));

        $this->db->select('start_date as from_date,end_date as to_date');
        $this->db->from('emp_tourapply');
        $this->db->where(array('is_active' => '1', 'emp_id' => $loginID));
        $where_date = "((`start_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`end_date` BETWEEN '" . $fromdate . "' AND '" . $todate . "') OR (`start_date`<='" . $fromdate . "' AND `end_date`>='" . $todate . "'))";
        $this->db->where($where_date);
        $this->db->where("approved_bypmanager !=", "2");
        $this->db->where("approved_bylmanager !=", "2");
        $TourReqList = $this->db->get()->result();

        //Loop Cond..
        $AllDatesArr = array();
        //All Leaves Dates..
        if ($TourReqList) {
            foreach ($TourReqList as $LvDate) {
                $recDDarr = $this->getDatesFromRange($LvDate->from_date, $LvDate->to_date);
                if ($recDDarr):
                    foreach ($recDDarr as $rEdRows):
                        $AllDatesArr[] = date("d-m-Y", strtotime($rEdRows));
                    endforeach;
                endif;
            }
        }
        return ($AllDatesArr) ? $AllDatesArr : $AllDatesArr;
    }

    function getDatesFromRange($startDate, $endDate) {
        $return = array($startDate);
        $start = $startDate;
        $i = 1;
        if (strtotime($startDate) < strtotime($endDate)) {
            while (strtotime($start) < strtotime($endDate)) {
                $start = date('Y-m-d', strtotime($startDate . '+' . $i . ' days'));
                $return[] = $start;
                $i++;
            }
        }
        return $return;
    }

    //Get New In Time Out Punch Data,,,,
    Public function GetAllPunchData($UserId, $AllHolidLeaveTourDateArr = '') {
        $first_dateM = date('d-m-Y', strtotime('first day of this month'));
        $last_dateM = date('d-m-Y', strtotime('last day of this month'));
        $mmdd = date('m-Y', strtotime('last day of this month'));

        $this->db->select("min(DATE_FORMAT(`LogDate`,'%d-%m-%Y %H:%i:%s %p')) as firstintime,min(`DeviceLogId`) as devicetbl_id, DAYNAME(`LogDate`) as dayname");
        $this->db->from('devicelogs_processed');
        $this->db->where("(`UserId`='" . $UserId . "' AND DAYNAME(`LogDate`)!='Sunday') AND (DATE_FORMAT(`LogDate`,'%d-%m-%Y')>='" . $first_dateM . "' AND DATE_FORMAT(`LogDate`,'%d-%m-%Y')<'" . $last_dateM . "')", NULL, FALSE);
        $this->db->where("DATE_FORMAT(`LogDate`,'%m-%Y')>='" . $mmdd . "'");

        if (count($AllHolidLeaveTourDateArr) > 0):
            $this->db->where_not_in("DATE_FORMAT(`LogDate`,'%d-%m-%Y')", $AllHolidLeaveTourDateArr, NULL, FALSE);
        endif;
        $this->db->group_by("DATE_FORMAT(`LogDate`,'%d-%m-%Y')");
        $PunchReqList = $this->db->get()->result();
        return ($PunchReqList) ? $PunchReqList : null;
    }

    //Get Single Date Atten Details By User ID..
    Public function getSingleDateAttByUserID($punchDate, $userID) {
        //$punchDate = "01-01-2020";
        // $userID = "127";

        $this->db->select("LogDate,DATE_FORMAT(`LogDate`,'%H:%i:%s %p') as punchtime");
        $this->db->from('devicelogs_processed');
        $this->db->where("(`UserId` = '" . $userID . "' and DATE_FORMAT(`LogDate`,'%d-%m-%Y')='" . $punchDate . "')");
        $this->db->order_by("LogDate", "ASC");
        $this->db->group_by("LogDate");
        $PunchRecData = $this->db->get()->result();
        if (@$PunchRecData) {
            $recArrReturn["intime"] = $PunchRecData[0]->punchtime;
            $FunllInTim = date("h:i:s A", strtotime($PunchRecData[0]->punchtime));
            if ((strtotime($FunllInTim) > strtotime("09:30:00 AM")) AND ( strtotime($FunllInTim) < strtotime("10:01:00 AM"))) {
                $recArrReturn["lateby"] = round(abs(strtotime($FunllInTim) - strtotime("09:30:00 AM")) / 60, 2);
            }
            //Out Time..
            if (count($PunchRecData) > 0):
                foreach ($PunchRecData as $Pkey => $rows) {
                    $return = ($Pkey % 2);
                    if ($return == 1):
                        $recArrReturn["outtime"] = $rows->punchtime;
                    endif;
                }
            endif;
            return ($recArrReturn) ? $recArrReturn : null;
        } else {
            return null;
        }
    }

    public function GetTotalWorkingHourWithPunch($punchDate, $userID) {
        $this->db->select("DATE_FORMAT(`LogDate`,'%H:%i:%s') as punchtime");
        $this->db->from('devicelogs_processed');
        $this->db->where("(`UserId` = '" . $userID . "' and DATE_FORMAT(`LogDate`,'%d-%m-%Y')='" . $punchDate . "')");
        $this->db->order_by("LogDate", "ASC");
        $this->db->group_by("LogDate");

        $PunchRecData = $this->db->get()->result();
        $In_timeArr = array();
        $Out_timeArr = array();
        $workingHourInMinute = 0;

        if ($PunchRecData) {
            foreach ($PunchRecData as $kKeY => $rEcd) {
                $returnM = ($kKeY % 2);
                if ($returnM == 0) {
                    array_push($In_timeArr, $rEcd->punchtime);
                }
                if ($returnM == 1) {
                    array_push($Out_timeArr, $rEcd->punchtime);
                }
            }
        }

        foreach ($In_timeArr as $kkEy => $rOwS) {
            $inTime = $rOwS;
            $outTime = @$Out_timeArr[$kkEy];
            if (($inTime) and ( $outTime)) {
                $workingHourInMinute += ((strtotime($outTime) - strtotime($inTime)) / 60);
            }
        }
        return $workingHourInMinute;
    }

    //Get All Punch
    public function GetAllPunchDetail($punchDate, $userID) {
        $this->db->select("DATE_FORMAT(`LogDate`,'%H:%i:%s %p') as punchtime");
        $this->db->from('devicelogs_processed');
        $this->db->where("(`UserId` = '" . $userID . "' and DATE_FORMAT(`LogDate`,'%d-%m-%Y')='" . $punchDate . "')");
        $this->db->order_by("LogDate", "ASC");
        $this->db->group_by("LogDate");
        $PunchRecData = $this->db->get()->result();
        $In_timeArr = array();
        $Out_timeArr = array();

        if ($PunchRecData) {
            foreach ($PunchRecData as $kKeY => $rEcd) {
                $returnM = ($kKeY % 2);
                if ($returnM == 0) {
                    array_push($In_timeArr, $rEcd->punchtime);
                }
                if ($returnM == 1) {
                    array_push($Out_timeArr, $rEcd->punchtime);
                }
            }
            return array("intime" => $In_timeArr, "outtime" => $Out_timeArr);
        } else {
            return null;
        }
    }

    //Get New In Time Out Punch Data,,,,
    Public function GetAllPunchDataByDateRange($UserId, $AllHolidLeaveTourDateArr = '', $first_dateM, $last_dateM) {
        $first_dateM = date('d-m-Y', strtotime($first_dateM));
        $last_dateM = date('d-m-Y', strtotime($last_dateM));
        $mmdd = date('m-Y', strtotime($last_dateM));

        $this->db->select("min(DATE_FORMAT(`LogDate`,'%d-%m-%Y %H:%i:%s %p')) as firstintime,min(`DeviceLogId`) as devicetbl_id, DAYNAME(`LogDate`) as dayname,b.leave_act_status,b.leave_count");
        $this->db->from('devicelogs_processed');
        $this->db->join("short_leave as b", "devicelogs_processed.DeviceLogId=b.devicelog_id", "LEFT");
        $this->db->where("(`UserId`='" . $UserId . "' AND DAYNAME(`LogDate`)!='Sunday') AND (DATE_FORMAT(`LogDate`,'%d-%m-%Y')>='" . $first_dateM . "' AND DATE_FORMAT(`LogDate`,'%d-%m-%Y')<'" . $last_dateM . "')", NULL, FALSE);
        $this->db->where("DATE_FORMAT(`LogDate`,'%m-%Y')='" . $mmdd . "'");
        $this->db->group_by("LogDate");

        if (count($AllHolidLeaveTourDateArr) > 0):
            $this->db->where_not_in("DATE_FORMAT(`LogDate`,'%d-%m-%Y')", $AllHolidLeaveTourDateArr, NULL, FALSE);
        endif;
        $this->db->group_by("DATE_FORMAT(`LogDate`,'%d-%m-%Y')");
        $PunchReqList = $this->db->get()->result();
        return ($PunchReqList) ? $PunchReqList : null;
    }

    //Get Total Short Day Leave...
    Public function CountTotShortDayLeave($year, $month, $userID) {
        $this->db->select("a.id");
        $this->db->from('short_leave as a');
        $this->db->where(["a.user_id" => $userID, "a.status" => '1', "a.month" => $month, "a.year" => $year, "a.leave_count" => "short_leave"]);
        $numRowData = $this->db->get()->num_rows();
        return ($numRowData) ? $numRowData : 0;
    }

    //Leave Request Summary By Leave Req Id..
    Public function LeaveReqSummaryDetailsByReqID($leaveReqID) {
        if ($leaveReqID) {
            $this->db->select('a.*,b.user_name,b.department_id,b.department_name,b.bunit_id,b.buss_unit_name,b.leavetypeid,b.leavetype_name,b.leavestatus,b.rep_manager_name,b.appliedleavescount,c.emailaddress,c.reporting_manager_name as rep_manager_name');
            $this->db->from('main_leaverequest as a');
            $this->db->join('main_leaverequest_summary as b', "a.id=b.leave_req_id", "LEFT");
            $this->db->join('main_employees_summary as c', "a.user_id=c.user_id", "LEFT");
            $this->db->where(["a.id" => $leaveReqID, "a.isactive" => "1"]);
            $record = $this->db->get()->row();
        }
        return ($record) ? $record : null;
    }

    //Single Tour Details By ID..
    Public function GetSingleTourDetailsByID($tourID) {
        $this->db->select('a.emp_id,a.project_id,a.month_no,a.Year,a.start_date,a.end_date,a.expected_out_time,a.tour_type,a.approved_bypmanager,a.rejected_notes,a.description,a.tour_location,b.project_name,c.prefix_name,c.userfullname,c.emailaddress,c.contactnumber,c.reporting_manager,c.reporting_manager_name,d.emailaddress as reporting_manager_email,d.contactnumber as reporting_manager_contact,f.userfullname as RO_userfullname,f.emailaddress as RO_emailaddress,f.contactnumber as RO_contactnumber');
        $this->db->from('emp_tourapply as a');
        $this->db->join('tm_projects as b', "a.project_id=b.id", "LEFT");
        $this->db->join('main_employees_summary as c', "a.emp_id=c.user_id", "LEFT");
        $this->db->join('main_employees_summary as d', "c.reporting_manager=d.user_id", "LEFT");
        $this->db->join('temp_hr_otherofc_details as e', "a.emp_id=e.user_id", "LEFT");
        $this->db->join('main_employees_summary as f', "e.reviewing_officer_ro=f.user_id", "LEFT");
        $this->db->where(["a.id" => $tourID, "a.is_active" => "1"]);
        $TourRecord = $this->db->get()->row();
        return ($TourRecord) ? $TourRecord : null;
    }

    //Single Tour Details By ID..
    Public function GetSingleTourDetailsByTourID($tourID) {
        $this->db->select('a.emp_id,a.project_id,a.month_no,a.Year,a.start_date,a.end_date,a.expected_out_time,a.tour_type,a.approved_bypmanager,a.rejected_notes,a.description,a.tour_location,b.project_name,c.prefix_name,c.userfullname,c.emailaddress,c.contactnumber,c.reporting_manager,c.reporting_manager_name,d.emailaddress as reporting_manager_email,d.contactnumber as reporting_manager_contact,f.userfullname as RO_userfullname,f.emailaddress as RO_emailaddress,f.contactnumber as RO_contactnumber');
        $this->db->from('emp_tourapply as a');
        $this->db->join('tm_projects as b', "a.project_id=b.id", "LEFT");
        $this->db->join('main_employees_summary as c', "a.emp_id=c.user_id", "LEFT");
        $this->db->join('main_employees_summary as d', "c.reporting_manager=d.user_id", "LEFT");
        $this->db->join('temp_hr_otherofc_details as e', "a.emp_id=e.user_id", "LEFT");
        $this->db->join('main_employees_summary as f', "e.reviewing_officer_ro=f.user_id", "LEFT");
        $this->db->where(["a.id" => $tourID]);
        $TourRecord = $this->db->get()->row();
        return ($TourRecord) ? $TourRecord : null;
    }

    //Get Table Data Record.
    public function GetTableData($cegth_table, $Where) {
        $this->db->select('*');
        $this->db->from($cegth_table);
        $this->db->order_by("id", "DESC");
        $this->db->where($Where);
        $recArr = $this->db->get()->result();
        return $recArr;
    }
    
    
}

?>