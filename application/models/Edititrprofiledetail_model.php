<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Edititrprofiledetail_model extends CI_Model {

    public function showAllItr() {
        $id = $this->session->userdata('loginid');
        $this->db->select('mi.*');
        $this->db->from('main_itr as mi');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}

?>