<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appraisal_acc_part_model extends CI_Model {
	
	
	// var $table = 'main_users as a';
    var $table = 'main_empsalarydetails as a';
    var $emp_sum_table = 'main_employees_summary as d';
    var $edudetail_table = 'main_empeducationdetails as b';
    var $appraisal_acc_table = 'annual_appraisal_account as c';
    var $tm_project_table = 'tm_projects as h';
    var $otherofficial_table = 'emp_otherofficial_data as i';
    // var $otherofficial_table = 'emp_otherofficial_data as g';
    var $column_order = array(null, 'd.userfullname');
    var $column_search = array('d.userfullname');
    var $order = array('d.userfullname' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        if ($this->input->post('businessunit_name')) {
            $this->db->where('d.businessunit_id', $this->input->post('businessunit_name'));
        }
        if ($this->input->post('department_id')) {
            $this->db->where('d.department_id', $this->input->post('department_id'));
        }
		if ($this->input->post('project_id')) {
			// $data = ','.$this->input->post('project_id').',';
			$data = $this->input->post('project_id');
            $this->db->where("i.on_project LIKE '%$data%'");
            // $this->db->where('i.on_project', $this->input->post('project_id'));
        }
		
        $this->db->select('i.on_project,c.billRate,c.origBillrate,c.remark,d.*');
        $this->db->from($this->table);
        $this->db->where(array('d.isactive' => '1','a.appraisalduedate' => 'April 2020'));
        $this->db->join($this->appraisal_acc_table, 'c.user_id = a.user_id', 'left');
        $this->db->join($this->emp_sum_table, 'd.user_id = a.user_id', 'left');
        $this->db->join($this->otherofficial_table, 'i.user_id = d.user_id', 'left');
		$this->db->where('d.jobtitle_name !=', 'F');
        // $this->db->join($this->otherofficial_table, 'a.id = g.user_id', 'left');
        $this->db->group_by('a.user_id');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
         $this->_get_datatables_query();
         $query = $this->db->get();
         return $query->num_rows();
        
    }

    public function count_all() {
         $this->db->from($this->table);
        return $this->db->count_all_results();
        
    }
	
	
	
    


}

?>