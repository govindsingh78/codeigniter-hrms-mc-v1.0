<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travelticket_model extends CI_Model {

    var $table = 'travelticket_tbl as a';
    var $column_order = array(null, 'a.trav_from', 'a.trav_to', 'a.dateoftravel', 'a.dateofbooking', 'a.tickcost', 'a.trip_invoiceno');
    var $column_search = array('a.trav_from', 'a.trav_to', 'a.dateoftravel', 'a.dateofbooking', 'a.tickcost', 'a.trip_invoiceno');
    var $order = array('a.fld_id' => 'ASC');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

        $user_id = $this->tank_auth->get_user_id();

        $this->db->select('a.*,b.userfullname,b.department_name,b.emailaddress,b.businessunit_name,b.contactnumber,c.userfullname as aprvuserfullname,d.userfullname as entrybyuserfullname');
        $this->db->from($this->table);
        $this->db->join('main_employees_summary as b', 'a.travel_empid=b.user_id', "LEFT");
        $this->db->join('main_employees_summary as c', 'a.approvedby=c.user_id', "LEFT");
        $this->db->join('main_employees_summary as d', 'a.entry_by=d.user_id', "LEFT");
        $this->db->group_by('a.travel_empid');

        if ($this->input->post('bunit')) {
            $this->db->where('b.businessunit_id', $this->input->post('bunit'));
        }
        if ($this->input->post('traveluserid')) {
            $this->db->where('a.travel_empid', $this->input->post('traveluserid'));
        }
        if ($this->input->post('travelproject')) {
            $this->db->like('a.tickproject', $this->input->post('travelproject'));
        }
        if ($this->input->post('travdatefrom') and ( $this->input->post('travdateto'))) {
            $fromdate = date("Y-m-d", strtotime($this->input->post('travdatefrom')));
            $todate = date("Y-m-d", strtotime($this->input->post('travdateto')));
            $this->db->where('a.dateoftravel >=', $fromdate);
            $this->db->where('a.dateoftravel <=', $todate);
        }

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $ReturnArr = array();
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $recResp = $query->result();
        return $recResp;
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	    //Insert All Master
    public function InsertMasterData($recArr, $tablename) {
        if ($recArr && $tablename):
            $this->db->insert($tablename, $recArr);
            return $this->db->insert_id();
        else:
            return false;
        endif;
    }
	

}
