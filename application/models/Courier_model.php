<?php

class Courier_model extends CI_Model {

    var $table = 'main_courierdetail as a';
    var $user_table = 'main_users as b';
    var $businessunits_table = 'main_businessunits as c';
    var $company_table = 'tbl_companylist as d';
    var $companyinfo = 'tbl_comapnyinfo as e';
    var $city = 'cities as f';
    var $state = 'states as l';
    var $country = 'countries as m';
    var $courierservice = 'main_courierservice as g';
    var $column_order = array(null, 'b.userfullname', 'b.contactnumber', 'c.unitname', 'd.company_name', 'e.pincode', 'f.city_name', 'g.courier_service');
    var $column_search = array('b.userfullname', 'b.contactnumber', 'c.unitname', 'd.company_name', 'e.pincode', 'f.city_name', 'g.courier_service');
  //  var $order = array('t.emp_id' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
	  if($this->input->post('businessunit_name')) {
            $this->db->where('a.businessunitid', $this->input->post('businessunit_name'));
       }
        if($this->input->post('courier_service')) {
            $this->db->where('a.courier_service', $this->input->post('courier_service'));
       }
	   if(($this->input->post('from_date')) and ($this->input->post('to_date'))) {
           $from_date = date("Y-m-d", strtotime($this->input->post('from_date')));
           $to_date = date("Y-m-d", strtotime($this->input->post('to_date')));
           $this->db->where("a.created >=",$from_date);
           $this->db->where("a.created <=",$to_date);
        }
        $this->db->select('a.*,b.userfullname,b.contactnumber,c.unitname,d.company_name,e.pincode,f.city_name,g.courier_service,a.courier_service as courier_serviceid,a.courier_weight');
        $this->db->from($this->table);
        $this->db->join($this->user_table, 'a.senderempid = b.id', 'left');
        $this->db->join($this->businessunits_table, 'a.businessunitid = c.id', 'left');
        $this->db->join($this->company_table, 'a.receivercompanyid = d.company_id', 'left');
        $this->db->join($this->companyinfo, 'a.receiverpincode = e.id', 'left');
        $this->db->join($this->city, 'e.city = f.city_id', 'left');
        $this->db->join($this->courierservice, 'a.courier_service = g.id', 'left');
        $this->db->order_by('a.id', 'DESC');

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_company_data() {
        $this->db->select('e.id,e.pincode, e.address1, e.address2, e.address3, d.company_name, f.city_name, l.state_name, m.country_name');
        $this->db->from($this->companyinfo);
        $this->db->join($this->company_table, 'e.company_id = d.company_id', 'left');
        $this->db->join($this->country, 'e.country = m.country_id', 'left');
        $this->db->join($this->state, 'e.state = l.state_id', 'left');
        $this->db->join($this->city, 'e.city = f.city_id', 'left');
        $this->db->where('e.is_active', '1');
        $this->db->order_by('e.id', 'DESC');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        $recArr = $query->result();
        $returnerA = array();
        if ($recArr) {
            foreach ($recArr as $row) {
                if ($row->is_comp_other == '1') {
                    if ($row->comp_other_info_details):
                        $comDetArr = json_decode($row->comp_other_info_details);
                        $row->city_name = $comDetArr->city;
                    endif;
                }
                $returnerA[] = $row;
            }
            return $returnerA;
        }
    }

    function get_company() {
        $this->_get_company_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
         $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows(); 
        //return 0;
    }

    public function count_all() {
         $this->db->from($this->table);
          return $this->db->count_all_results();
       // return 0;
    }

    public function get_list_businessunit() {
        $this->db->select('*');
        $this->db->from('main_businessunits');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        $businessunit = $result;
        return $businessunit;
    }

    public function getCourierDetails($c_id) {
        $this->db->select('*');
        $this->db->from('main_courierdetail');
        $this->db->order_by('id', 'asc');
        $this->db->where(array('id'=>$c_id));
        $query = $this->db->get()->row_array();
        return $query;
    }
	
	 public function getCourierDetails1($where) {
        $this->db->select('*');
        $this->db->from('main_courierdetail');
        $this->db->order_by('id', 'asc');
        $this->db->where($where);
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function getnewDetails($Where) {
        $this->db->select('*');
        $this->db->from('tbl_comapnyinfo');
        $this->db->order_by('id', 'asc');
        $this->db->where($Where);
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_list_companyname() {
        $this->db->select('*');
        $this->db->from('tbl_companylist');
        $this->db->where('is_active', '1');
        $this->db->order_by('company_id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_pincode() {
        $this->db->select('pincode, id, city, country, state, address1, address2, address3');
        $this->db->from('tbl_comapnyinfo');
        //$this->db->join('main_courierdetail as b', 'tbl_comapnyinfo.id = b.receiverpincode', 'left');
        $this->db->where('is_active', '1');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getDepartmentDetails() {
        $this->db->select('id, deptname');
        $this->db->from('main_departments');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_list_empname() {
        $this->db->select('main_users.id, main_users.userfullname, main_users.contactnumber, main_users.emailaddress');
        $this->db->from('main_users');
        $this->db->where('isactive', '1');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getContactDetails($senderempid) {
        $this->db->select(' main_users.userfullname, main_users.contactnumber, main_users.emailaddress');
        $this->db->from('main_users');
        $this->db->where('id', $senderempid);
        $this->db->where('isactive', '1');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function getCountryDetails() {
        $this->db->select(' c.country_name, k.state_name, n.city_name, tbl_comapnyinfo.address1, tbl_comapnyinfo.address2, tbl_comapnyinfo.address3');
        $this->db->from('tbl_comapnyinfo');
        $this->db->join('countries as c', 'tbl_comapnyinfo.country = c.country_id', 'left');
        $this->db->join('states as k', 'tbl_comapnyinfo.state = k.state_id');
        $this->db->join('cities as n', 'tbl_comapnyinfo.city = n.city_id');
        $this->db->join('main_courierdetail as p', 'tbl_comapnyinfo.id = p.receiverpincode', 'left');
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    function getCompanyRows($company_id) {
        $this->db->select('c.id, c.pincode');
        $this->db->from('tbl_comapnyinfo as c');
        $this->db->where('c.company_id', $company_id);
        $this->db->where('c.is_active', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getDetails($company_id) {
        $this->db->select('d.city_name, a.country_name, b.state_name, c.address1, c.address2, c.address3');
        $this->db->from('tbl_comapnyinfo as c');
        $this->db->join('countries as a', 'c.country = a.country_id', 'left');
        $this->db->join('states as b', 'c.state = b.state_id', 'left');
        $this->db->join('cities as d', 'c.city = d.city_id', 'left');
        $this->db->where('c.id', $company_id);
        $this->db->where('c.is_active', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getdeptnames($id) {
        $this->db->select('c.id, c.deptname');
        $this->db->from('main_departments as c');
        $this->db->where('c.unitid', $id);
        $this->db->where('c.isactive', '1');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    function getempidbydept($id) {
        $this->db->select('c.user_id, u.userfullname');
        $this->db->from('main_employees_summary as c');
        $this->db->JOIN('main_users as u', 'c.user_id = u.id', 'left');
        $this->db->where('c.department_id', $id);
        $this->db->where('u.isactive', 1);
        $this->db->order_by('u.userfullname', 'ASC');


        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        return $result;
    }

    public function get_list_empdetails($id) {
        $this->db->select('c.emailaddress, c.contactnumber');
        $this->db->from('main_users as c');
        $this->db->where('c.id', $id);
        $this->db->where('c.isactive', 1);

        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        return $result;
    }

    public function insert($data) {
        if ($this->db->insert("tbl_comapnyinfo", $data)) {
            return true;
        }
    }

    public function insert2($data) {
        if ($this->db->insert("tbl_companylist", $data)) {
            return true;
        }
    }

    public function insertData($tablename, $data) {
        if ($this->db->insert($tablename, $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCountryname() {
        $this->db->select('country_id,country_name');
        $this->db->from('countries');
        $this->db->where('status', '1');
        $this->db->order_by('country_id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getStatename() {
        $this->db->select('state_id,state_name');
        $this->db->from('states');
        $this->db->where('status', '1');
        $this->db->order_by('state_id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getCityname() {
        $this->db->select('city_id,city_name');
        $this->db->from('cities');
        $this->db->where('status', '1');
        $this->db->order_by('city_id', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getCourierservice() {
        $this->db->select('id,courier_service');
        $this->db->from('main_courierservice');
        $this->db->where('status', '1');
        $this->db->order_by('courier_service', 'asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getstateBycountryID($countryID) {
        $this->db->select('state_id,state_name');
        $this->db->from('states');
        $this->db->where('status', '1');
        $this->db->order_by('state_name', 'asc');
        $this->db->where('country_id', $countryID);
        $query = $this->db->get();
        $result = $query->result();
        return isset($result) ? $result : false;
    }

    public function getcityBystateID($stateID) {
        $this->db->select('city_id,city_name');
        $this->db->from('cities');
        $this->db->where('status', '1');
        $this->db->order_by('city_name', 'asc');
        $this->db->where('state_id', $stateID);
        $query = $this->db->get();
        $result = $query->result();
        return isset($result) ? $result : false;
    }
	
	    //Update Record From Table..
    public function UpdataRecord($table_name, $upd_data, $where) {
        $this->db->where($where);
        return $this->db->update($table_name, $upd_data);
        // return true;
    }

}

?>