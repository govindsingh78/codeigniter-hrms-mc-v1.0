<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model {

    private $table_name = 'main_users';   // user accounts
    private $table_emp_feedback_group_name = 'main_employees_summary';   // user accounts
    private $table_emp_feedback_subdept_name = ' emp_otherofficial_data';   // user accounts
    private $table_emp_feedback = 'emp_feedback'; // employee feedback

    function __construct() {
        parent::__construct();
        $ci = & get_instance();
        $this->table_name = $ci->config->item('db_table_prefix', 'tank_auth') . $this->table_name;
    }

// Get user role by access  created by durgesh....(23 feb 2019)
    function get_role($id) {
        $this->db->select('emprole');
        $this->db->where('id', $id);
        $query = $this->db->get($this->table_name);
        $data = $query->row_array();
        return ($data['emprole']) ? $data['emprole'] : null;
    }
	
	function get_userbunit_by_id($id) {
	$this->db->select('businessunit_id');
	$this->db->where('user_id', $id);
	$query = $this->db->get('main_employees_summary');
	$data = $query->row_array();
	return $data;
    }

//  Get emp feedback dept by access created by durgesh....(23 feb 2019)    
    function get_feedback_dept($id) {
        $this->db->select('department_id');
        $this->db->where('user_id', $id);
        $query = $this->db->get($this->table_emp_feedback_group_name);
        $data = $query->row_array();
        if (!empty($data)):
            return $data['department_id'];
        endif;
    }

// Get emp feedback subdept by access created by durgesh....(23 feb 2019)    
    function get_feedback_subdept($id) {
        $this->db->select('sub_department');
        $this->db->where('user_id', $id);
        $query = $this->db->get($this->table_emp_feedback_subdept_name);
        $data = $query->row_array();
        if (!empty($data)):
            return $data['sub_department'];
        endif;
    }

    /*     * * check emp feedback by  time slot * * */

    function check_feedback_data_exist($id) {
        $this->db->select('created_at, group_name');
        $this->db->where(array('user_id' => $id));
        $query = $this->db->get($this->table_emp_feedback);
        $data = $query->row_array();
        return $data;
    }

}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */