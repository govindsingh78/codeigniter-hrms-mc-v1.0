<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */


//################# HRMS Route ##########################
//******************** 07-01-2020 Ash *******************
//Basic Controller
$route['default_controller'] = 'admin';
$route['404_override'] = '';
$route['userdashboard'] = 'userdashboard/home';
$route['logout'] = 'userdashboard/logout';
//Attendance..
$route['myattendance'] = 'Account_Controller/myattendance';
$route['atest'] = 'Account_Controller/atest';
$route['payslip_sendonmail/(:num)'] = 'Account_Controller/payslip_sendonmail/$1';
//short leave Section Route..
$route['myshort_leave_list'] = 'Shortleave_Controller/myshort_leave_list';
$route['ajax_alldetails_bydevicelogid'] = 'Shortleave_Controller/ajax_alldetails_bydevicelogid';
$route['shortleave_formsubmit'] = 'Shortleave_Controller/shortleave_formsubmit';
$route['ash'] = 'Userdashboard/testash';

//User Profile
$route['myprofile'] = 'userProfile_control/myprofile';
$route['my-form-upload']['post'] = "userProfile_control/post";
$route['mypayslip'] = 'Account_controller/mypayslip';
$route['mysalarydetails'] = 'Account_controller/mysalarydetails';
$route['payslips_data_ajax'] = 'Account_controller/payslips_data_ajax';
$route['myformitr'] = 'Account_controller/myformitr';
$route['myteam'] = 'userProfile_control/myteam';
$route['myleave'] = 'userProfile_control/myleave';
$route['myholidays'] = 'userProfile_control/myholidays';
$route['applytour'] = 'userProfile_control/applytour';
$route['ajax_checkrhleave'] = 'userProfile_control/ajax_checkrhleave';
$route['ajax_savesubmit_leaverequest_rh'] = 'userProfile_control/ajax_savesubmit_leaverequest_rh';
$route['ajax_chkdatecomb_pl'] = 'userProfile_control/ajax_chkdatecomb_pl';
$route['test'] = 'userProfile_control/test';
$route['ajax_savesubmit_leaverequest_pl'] = 'userProfile_control/ajax_savesubmit_leaverequest_pl';
$route['ajax_savesubmit_leaverequest_hd'] = 'userProfile_control/ajax_savesubmit_leaverequest_hd';
$route['ajax_savesubmit_leaverequest_shortl'] = 'userProfile_control/ajax_savesubmit_leaverequest_shortl';

//appraisal User Secion Code By Asheesh.. 06-02-2020.
$route['myappraisal'] = 'Appraisal_controller/index';
$route['appraisal/(:num)'] = 'Appraisal_controller/appraisal_index/$1';
$route['appraisalsave'] = 'Appraisal_controller/appraisalsave';
$route['editappraisalbyuser/(:num)'] = 'Appraisal_controller/editappraisalbyuser/$1';
$route['apprsaveupdateas_self'] = 'Appraisal_controller/apprsaveupdateas_self';
$route['appr_numrowsincre'] = 'Appraisal_controller/appr_numrowsincre';
$route['deletesinglerow/(:num)/(:num)'] = 'Appraisal_controller/deletesinglerow/$1/$1';
$route['appraisal_finalockbyuser'] = 'Appraisal_controller/appraisal_finalockbyuser';
$route['view_selfappraisal/(:num)'] = 'Appraisal_controller/view_selfappraisal/$1';
$route['employee-appraisal'] = 'Appraisal_controller/empappraisal_list';
$route['appraisalview/(:num)'] = 'Appraisal_controller/appraisalview/$1';
$route['appraisalviewdetails/(:num)'] = 'Appraisal_controller/appraisalviewdetails/$1';
$route['apprsaveupdateas_ro'] = 'Appraisal_controller/apprsaveupdateas_ro';
$route['appraisal_finalockbyio'] = 'Appraisal_controller/appraisal_finalockbyio';
$route['employee_appraisal_io'] = 'Appraisal_controller/employee_appraisal_io';
$route['appraisalviewasio/(:num)'] = 'Appraisal_controller/appraisalviewasio/$1';
$route['appraisalviewdetailasio/(:num)'] = 'Appraisal_controller/appraisalviewdetailasio/$1';
$route['apprsaveupdateas_io'] = 'Appraisal_controller/apprsaveupdateas_io';
$route['appraisal_finalockbyro'] = 'Appraisal_controller/appraisal_finalockbyro';
//Test Mail Send Dynamic ...
$route['leavemailsend/(:num)'] = 'Userprofile_control/leavemailsend/$1';
$route['ajax_savesubmit_leaverequest_sortl'] = 'Userprofile_control/ajax_savesubmit_leaverequest_sortl';
$route['tourmail'] = 'Userprofile_control/tourmail';
$route['deletetourbytid/(:num)'] = 'Userprofile_control/deletetourbytid/$1';
$route['ajax_getsingletourby_tid'] = 'Userprofile_control/ajax_getsingletourby_tid';
$route['toureditupdate'] = 'Userprofile_control/toureditupdate';
$route['deleteleavebytid/(:num)'] = 'Userprofile_control/deleteleavebytid/$1';
$route['ajax_getsingleleaveby_tid'] = 'Userprofile_control/ajax_getsingleleaveby_tid';
$route['leave_editupdate'] = 'Userprofile_control/leave_editupdate';
$route['mail1'] = 'Userprofile_control/TourDeleteMailByLreqID';


//Appraisal Annual CRU part(Gaurav)
$route['annual_appraisal_cru_part'] = 'Appraisal_cru_acc_Controller/annual_appraisal_cru_part';
$route['annual_appraisal_acc_part'] = 'Appraisal_cru_acc_Controller/annual_appraisal_acc_part';
$route['ajax_list_appraisal_cru'] = 'Appraisal_cru_acc_Controller/ajax_list_appraisal_cru';
$route['ajax_list_appraisal_acc'] = 'Appraisal_cru_acc_Controller/ajax_list_appraisal_acc';
//Team under IO and RO
$route['team_under_officers'] = 'Team_fun_Controller/team_under_officers';
$route['team_under_officers_list'] = 'Team_fun_Controller/team_under_officers_list';
$route['tour_under_officers_list'] = 'Team_fun_Controller/tour_under_officers_list';
$route['leave_under_officers_list'] = 'Team_fun_Controller/leave_under_officers_list';
$route['absent_under_officers_list'] = 'Team_fun_Controller/absent_under_officers_list';
//Team Leave Status //date===03-03-2020
$route['team_leavestatus_list'] = 'Team_fun_Controller/team_leavestatus_list';
//Courier Status======coded on 13-03-2020 ===================
$route['courier_dashboard'] = 'Courier_Controller/courier_dashboard';
$route['courier'] = 'Courier_controller';
$route['add_company'] = 'Courier_Controller/add_company';
$route['inwardcourier'] = 'Courier_controller/inwardcourier';
//Travel ticket code by Gaurav.===========13-03-2020 ==================
$route['travelticket'] = 'travelticket_controller/add';
$route['getemppdetailbyid_ajax/(:any)'] = 'travelticket_controller/getemppdetailbyid_ajax/$1';
$route['travelticket_save'] = 'travelticket_controller/travelticket_save';
$route['travelticket_list'] = 'travelticket_controller/travelticket_list';
$route['ajax_list_tickets'] = 'travelticket_controller/ajax_list_tickets';
$route['editticket/(:any)'] = 'travelticket_controller/editticket/$1';
$route['travelticket_update'] = 'travelticket_controller/travelticket_update';



//###############--------------------------------------##################
// ************* Appraisal on Project  By Asheesh 21-02-2020 ************
//###############--------------------------------------##################
$route['apprai_projsiteofc'] = 'appraisal_on_project/Appraisalon_project_controller/apprai_projsiteofc';
$route['single_emp_projappr/(:any)'] = 'appraisal_on_project/Appraisalon_project_controller/single_emp_projappr/$1';
$route['apprai_projsiteofc_saveorupd'] = 'appraisal_on_project/Appraisalon_project_controller/apprai_projsiteofc_saveorupd';
$route['apprai_projsiteofcbcform_saveorupd'] = 'appraisal_on_project/Appraisalon_project_controller/apprai_projsiteofcbcform_saveorupd';
$route['lockprojappra_bc'] = 'appraisal_on_project/Appraisalon_project_controller/lockprojappra_bc';
$route['lockprojappra_de'] = 'appraisal_on_project/Appraisalon_project_controller/lockprojappra_de';
$route['exp'] = 'appraisal_on_project/Appraisalon_project_controller/exp';
//###############--------------------------------------##################
//************** Timesheet Entry By Asheesh 24-02-2020 ******************
//###############--------------------------------------##################
$route['timesheet_entry/(:num)/(:num)/(:num)'] = 'Timesheet_controller/timesheet_entry/$1/$1/$1';
$route['timesheet_entry'] = 'Timesheet_controller/timesheet_entry';
$route['test'] = 'Timesheet_controller/test';
$route['ts_savesubmit'] = 'Timesheet_controller/ts_savesubmit';
$route['save_extra_actvt'] = 'Timesheet_controller/save_extra_actvt';
$route['calendar_timesheet'] = 'Timesheet_controller/calendar_timesheet';