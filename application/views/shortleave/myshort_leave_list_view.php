<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$$punchLateMinute = 0;
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?> </h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>                            
                                <li class="breadcrumb-item active">Home / <?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body">  
                                <form method="post" id="mform" action="<?= thisurl(); ?>">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="email">Month : </label>
                                            <select class="form-control" id="filtermonth" name="filtermonth">
                                                <option <?= ($filterrecdata['filtermonth'] == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "01") ? "selected" : ""; ?> value="01"> January </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "02") ? "selected" : ""; ?> value="02"> February </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "03") ? "selected" : ""; ?> value="03"> March </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "04") ? "selected" : ""; ?> value="04"> April </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "05") ? "selected" : ""; ?> value="05"> May </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "06") ? "selected" : ""; ?> value="06"> June </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "07") ? "selected" : ""; ?> value="07"> July </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "08") ? "selected" : ""; ?> value="08"> August </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "09") ? "selected" : ""; ?> value="09"> September </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "10") ? "selected" : ""; ?> value="10"> October </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "11") ? "selected" : ""; ?> value="11"> November </option>
                                                <option <?= ($filterrecdata['filtermonth'] == "12") ? "selected" : ""; ?> value="12"> December </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="email">Year : </label>
                                            <select class="form-control" id="filteryear" name="filteryear">
                                                <option <?= ($filterrecdata['filteryear'] == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                <?php for ($i = 2019; $i < 2031; $i++) { ?>
                                                    <option <?= ($filterrecdata['filteryear'] == $i) ? "selected" : ""; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="pwd">&nbsp; </label> <br>
                                            <button type="submit" name="submit" value="Submit" class="btn btn-success">Submit</button>
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="pwd">&nbsp; </label> <br>
                                            <a href="<?= base_url("userdashboard"); ?>">
                                                <button type="button" class="btn btn-primary"> << Back </button>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                                <hr>


                                <div class="row">
                                    <?php
                                    if (@$allPunchDataArr) {
                                        foreach (@$allPunchDataArr as $punchRow) {
                                            $FunllInTim = date("H:i:s A", strtotime($punchRow->firstintime));
                                            if ((strtotime($FunllInTim) > strtotime("09:30:59 AM")) AND ( strtotime($FunllInTim) < strtotime("10:01:00 AM"))) {
                                                $tOtLateMinute += round(abs(strtotime($FunllInTim) - strtotime("09:30:00 AM")) / 60, 2);
                                                if (($tOtLateMinute) > TATALDELAY) {
                                                    ?>
                                                    <div class="col-sm-3">
                                                        <div class="card">
                                                            <div class="header">
                                                                <p style="margin:0 0 0 0;" ><span style="color:#c3406d;"><?= date("d-m-Y", strtotime($punchRow->firstintime)); ?> (<?= $punchRow->dayname; ?>) <?= date("h:i:s A", strtotime($punchRow->firstintime)); ?></span></p>
                                                                <?php if ($punchRow->leave_count == "") { ?>
                                                                    <b id="lvstatus_<?= $punchRow->devicetbl_id ?>"><?= ((strtotime($FunllInTim) > strtotime("11:30:59 AM")) or ( $CountShortLeaveInMonth > 7)) ? "Half Leave" : "Short Leave"; ?></b>
                                                                <?php } else { ?>
                                                                    <b id="lvstatus_<?= $punchRow->devicetbl_id ?>"><?= str_replace("_", " ", $punchRow->leave_count); ?></b>
                                                                <?php } ?>
                                                                <br><br>
                                                                <?php
                                                                if ($punchRow->leave_act_status) {
                                                                    $shortLeaveS = ShortLeaveStatusRec();
                                                                    echo "<p>" . $shortLeaveS[$punchRow->leave_act_status] . "</p>";
                                                                } else {
                                                                    ?>
                                                                    <p><button onclick="ShortLeaveCond('<?= $punchRow->devicetbl_id; ?>')" data-toggle="modal" data-target="#myModal" class="btn btn-info"> Quick Apply Leave </button></p>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                    ?>

                                    <!-- Condition 2 !-->
                                    <?php
                                    $punchRow = '';
                                    if (@$allPunchDataArr) {
                                        foreach (@$allPunchDataArr as $punchRow) {
                                            $FunllInTim = date("H:i:s A", strtotime($punchRow->firstintime));
                                            if ((strtotime($FunllInTim) > strtotime("10:00:59 AM")) AND ( strtotime($FunllInTim) < strtotime("01:00:00 PM"))) {
                                                ?>
                                                <div class="col-sm-3">
                                                    <div class="card">
                                                        <div class="header">
                                                            <p style="margin:0 0 0 0;" ><span style="color:#c3406d;"><?= date("d-m-Y", strtotime($punchRow->firstintime)); ?> (<?= $punchRow->dayname; ?>) <?= date("h:i:s A", strtotime($punchRow->firstintime)); ?></span></p>
                                                            <?php if ($punchRow->leave_count == "") { ?>
                                                                <b id="lvstatus_<?= $punchRow->devicetbl_id ?>"><?= ((strtotime($FunllInTim) > strtotime("11:30:59 AM")) or ( $CountShortLeaveInMonth > 7)) ? "Half Leave" : "Short Leave"; ?></b>
                                                            <?php } else { ?>
                                                                <b id="lvstatus_<?= $punchRow->devicetbl_id ?>"><?= str_replace("_", " ", $punchRow->leave_count); ?></b>
                                                            <?php } ?>
                                                            <br><br>
                                                            <?php
                                                            if ($punchRow->leave_act_status) {
                                                                $shortLeaveS = ShortLeaveStatusRec();
                                                                echo "<p>" . $shortLeaveS[$punchRow->leave_act_status] . "</p>";
                                                            } else {
                                                                ?>
                                                                <p><button onclick="ShortLeaveCond('<?= $punchRow->devicetbl_id; ?>')" data-toggle="modal" data-target="#myModal" class="btn btn-info"> Quick Apply Leave </button></p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>


                                <div class="row">
                                    <div class="col-sm-5"><hr></div>
                                    <div class="col-sm-2" style="color:green"> ----All In & Out Time---- </div>
                                    <div class="col-sm-5"><hr></div>
                                    <?php
                                    $FunllInTim = 0;
                                    if (@$allPunchDataArr) {
                                        foreach (@$allPunchDataArr as $punRow) {
                                            $FunllInTim = date("H:i:s A", strtotime($punRow->firstintime));
                                            $punchLateMinute = round(abs(strtotime($FunllInTim) - strtotime("09:30:00 AM")) / 60, 2);
                                            ?>
                                            <div style="border:solid 1px #f3eeee8f" class="col-sm-2">
                                                <p><?= $punRow->firstintime; ?></p>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>


    <script>
        function ShortLeaveCond(device_tblID) {
            
            var lvstatus = $("#lvstatus_" + device_tblID).html();
            if (device_tblID) {
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url('ajax_alldetails_bydevicelogid'); ?>",
                    data: {'devicelog_id': device_tblID},
                    success: function (responData) {
                        var parsed = jQuery.parseJSON(responData);
                        if (parsed) {
                            $("#leave_date").val(parsed.leaveDate);
                            $("#leave_type").val(lvstatus);
                            $("#full_intime").val(parsed.intime);
                            $("#reporting_manager").val(parsed.reporting_manager_name);
                            $("#reporting_mngr_id").val(parsed.reporting_manager);
                            $("#devicelog_id").val(device_tblID);
                        }
                    },
                });
            }
        }
    </script>


    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">Apply for Short Leave </h5>
                    <button style="color:red" type="button" class="close" data-dismiss="modal"> X </button>
                </div>
                <div class="modal-body">
                    <form id="shortleave_form" name="shortleave_form" action="<?= base_url("shortleave_formsubmit"); ?>" method="post">
                        <div class="row">
                            <div class="form-group col-md-6">  
                                <label class="email"> Leave Date : </label>
                                <input type="date" class="form-control" id="leave_date" name="leave_date" readonly="readonly" value="">
                            </div>
                            <div class="form-group col-md-6">  
                                <label class="email"> Leave Type : </label>
                                <input type="text" class="form-control" id="leave_type" name="leave_type" readonly="readonly" value="">
                            </div>
                            <div class="form-group col-md-6">  
                                <label class="email"> In Time : </label>
                                <input type="text" class="form-control" id="full_intime" name="full_intime" readonly="readonly" value="">
                            </div>
                            <div class="form-group col-md-6">  
                                <label class="email"> Reporting Manager : </label>
                                <input type="text" class="form-control" id="reporting_manager" name="reporting_manager" readonly="readonly" value="">
                            </div>
                            <div class="form-group required col-md-6">
                                <input type="hidden" name="devicelog_id" id="devicelog_id" >
                                <input type="hidden" name="reporting_mngr_id" id="reporting_mngr_id" >
                                <input type="hidden" name="short_lv_month" value="<?= @$filterrecdata['filtermonth']; ?>" >
                                <input type="hidden" name="short_lv_year" value="<?= @$filterrecdata['filteryear']; ?>" >
                                <input type="submit" class="btn btn-success" value="Apply">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>

