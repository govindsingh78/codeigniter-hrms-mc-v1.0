<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
         $this->load->view('admin/includes/sidebar');
        ?>
        
        <div id="main-content">
            <div class="container-fluid">
                
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                       <?php if (@$rep_manager_assignbyhr) { ?>
                            <div class="card">
                                <div class="body">
                                    <div class="table-responsive">
                                        <h5> Project Appraisal - List of Employee : Check as IO </h5>
                                        <div class="panel-body">
                                            <a title="Guideline" target="_blank" href="<?= HOSTNAME; ?>/public/PMS-Guideline.pdf">
                                                <i class="fa fa-paperclip fa-2x"></i> <h4> Download Manual </h4>
                                            </a>
                                        </div>
                                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No. </th>
                                                    <th>Employee Name </th>
                                                    <th>EmployeeID </th>
                                                    <th>Email </th>
                                                    <th>Job Group </th>
                                                    <th>Department </th>
                                                    <th>Designation </th>
                                                    <th>Status </th>
                                                    <th>Action </th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($EmplListArr) {
                                                    foreach ($EmplListArr as $kEyy => $recRow) {
                                                        $checkStatus = chkApprProjofcStatus($recRow->user_id);
                                                        ?>
                                                        <tr>
                                                            <td><?= $kEyy + 1; ?></td>
                                                            <td><?= $recRow->prefix_name . " " . $recRow->userfullname; ?></td>
                                                            <td><?= $recRow->employeeId . " <br>( " . date("d-m-Y", strtotime($recRow->date_of_joining)) . " )"; ?></td>
                                                            <td><?= $recRow->emailaddress; ?></td>
                                                            <td><?= $recRow->jobtitle_name; ?></td>
                                                            <td><?= $recRow->department_name; ?></td>
                                                            <td><?= $recRow->position_name; ?></td>
                                                            <td><?= $checkStatus; ?></td>
                                                            <td>
                                                                <b>
                                                                    <a href="<?= base_url("single_emp_projappr/" . $recRow->user_id); ?>"> Fill in as IO </a>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                       <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr. No. </th>
                                                    <th>Employee Name </th>
                                                    <th>EmployeeID </th>
                                                    <th>Email </th>
                                                    <th>Job Group </th>
                                                    <th>Department </th>
                                                    <th>Designation </th>
                                                    <th>Status </th>
                                                    <th>Action </th> 
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>


                       <?php if (@$echeck_as_roDataArr) { ?>
                            <div class="card">
                                <div class="body">
                                    <div class="table-responsive">
                                        <h5> Project Appraisal - List of Employee : Check as RO </h5>
                                        <a title="Guideline" target="_blank" href="<?= HOSTNAME; ?>/public/PMS-Guideline.pdf">
                                            <i class="fa fa-paperclip fa-2x"></i>
                                        </a>
                                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th>Sr. No. </th>
                                                    <th>Employee Name </th>
                                                    <th>EmployeeID </th>
                                                    <th>Email </th>
                                                    <th>Job Group </th>
                                                    <th>Department </th>
                                                    <th>Designation </th>
                                                    <th>Status </th>
                                                    <th>Action </th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($echeck_as_roDataArr) {
                                                    foreach ($echeck_as_roDataArr as $kEey => $recasRO) {
                                                        $checkStatus2 = chkApprProjofcStatus($recasRO->user_id);
                                                        ?>
                                                        <tr>
                                                            <td><?= $kEey + 1; ?></td>
                                                            <td><?= $recasRO->prefix_name . " " . $recasRO->userfullname; ?></td>
                                                            <td><?= $recasRO->employeeId . " <br>( " . date("d-m-Y", strtotime($recasRO->date_of_joining)) . " )"; ?></td>
                                                            <td><?= $recasRO->emailaddress; ?></td>
                                                            <td><?= $recasRO->jobtitle_name; ?></td>
                                                            <td><?= $recasRO->department_name; ?></td>
                                                            <td><?= $recasRO->position_name; ?></td>
                                                            <td><?= $checkStatus2; ?></td>
                                                            <td>
                                                                <b>
                                                                    <a href="<?= base_url("single_emp_projappr/" . $recasRO->user_id); ?>"> Fill in as RO </a>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }

                                                //DE Form Type Data...
                                                if ($form_deRODataArr) {
                                                    foreach ($form_deRODataArr as $kEeyde => $recasROde) {
                                                        $checkStatus3 = chkApprProjofcStatus($recasROde->user_id);
                                                        ?>
                                                        <tr>
                                                            <td><?= $kEeyde + 1; ?></td>
                                                            <td><?= $recasROde->prefix_name . " " . $recasROde->userfullname; ?></td>
                                                            <td><?= $recasROde->employeeId . " <br>( " . date("d-m-Y", strtotime($recasROde->date_of_joining)) . " )"; ?></td>
                                                            <td><?= $recasROde->emailaddress; ?></td>
                                                            <td><?= $recasROde->jobtitle_name; ?></td>
                                                            <td><?= $recasROde->department_name; ?></td>
                                                            <td><?= $recasROde->position_name; ?></td>
                                                            <td><?= $checkStatus3; ?></td>
                                                            <td>
                                                                <b>
                                                                    <a href="<?= base_url("single_emp_projappr/" . $recasROde->user_id); ?>"> Fill in as RO </a>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr. No. </th>
                                                    <th>Employee Name </th>
                                                    <th>EmployeeID </th>
                                                    <th>Email </th>
                                                    <th>Job Group </th>
                                                    <th>Department </th>
                                                    <th>Designation </th>
                                                    <th>Status </th>
                                                    <th>Action </th> 
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
      <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>