<?PHP
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
?>
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/jquery.dataTables.min.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/dataTables.colVis.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/buttons.dataTables.min.css' ?>" rel="stylesheet">

<body>
    <div class="page-wrapper">
        <div class="content container-fluid">	
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" > Assign Appraisal on Project : </h3>
                </div>
                <div class="panel-body"> 
                    <div class="panel-body">
                        <form id="form-filter" class="form-horizontal">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="deptinfo" id="deptinfo" style="margin-bottom:15px;">
                                    <label class="email"> Emp. Group </label>
                                    <select id="jobtitgroup" class="form-control">
                                        <option value="" selected="selected"> -- All -- </option>
                                        <?php foreach ($jobgroupArr as $row) { ?>
                                            <option value="<?= $row->id; ?>"><?= $row->jobtitlename; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="deptinfo" id="deptinfo" style="margin-bottom:15px;">
                                    <label class="email"> Employee ID </label>
                                    <input type="text" class="form-control" id="employeeId">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4"> <br>
                                    <button type="button" id="btn-filter" class="btn btn-primary"> Filter </button> &nbsp;&nbsp;&nbsp;
                                    <button type="button" id="btn-reset" class="btn btn-primaryt"> Reset </button>
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>

            </div>	

            <div class="panel panel-default" style="overflow-x: scroll;">
                <div class="row">
                    <div class="col-md-12">
                        <div id="colvis"></div>
                    </div>
                </div>
                <table id="table" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sr. No. </th>
                            <th>Employee Name </th>
                            <th>EmployeeID </th>
                            <th>Email </th>
                            <th>Job Group </th>
                            <th>Department </th>
                            <th>Designation </th>
                            <th>Status </th>
                            <th>Action </th> 
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Sr. No. </th>
                            <th>Employee Name </th>
                            <th>EmployeeID </th>
                            <th>Email </th>
                            <th>Job Group </th>
                            <th>Department </th>
                            <th>Designation </th>
                            <th>Status </th>
                            <th>Action </th> 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?PHP $this->load->view("back_end/includes/footer"); ?>
    <style>
        .datahidden{display:none;}
        .showdata{display:block;}
    </style>

    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jquery.dataTables.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.bootstrap.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.colVis.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.buttons.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.flash.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/pdfmake.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jszip.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/vfs_fonts.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.html5.min.js'; ?>"></script>
    <script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.print.min.js'; ?>"></script>

    <script type="text/javascript">
        var table;
        $(document).ready(function () {
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= base_url('appronproj_employeelistajax_list'); ?>",
                    "type": "POST",
                    "data": function (data) {
                        data.jobtitgroupID = $('#jobtitgroup').val();
                        data.userfullnameID = $('#userfullname').val();
                        data.employeeID = $('#employeeId').val();

                    },
                },
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'csv',
                            'pdf',
                            'print'
                        ]
                    }
                ],
                //Set column definition initialisation properties.
                "columnDefs": [{
                        "targets": [0], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            });
            var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
            $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
            $('#btn-filter').click(function () { //button filter event click
                table.ajax.reload();  //just reload table
            });
            $('#btn-reset').click(function () { //button reset event click
                $('#form-filter')[0].reset();
                table.ajax.reload();  //just reload table
            });
        });
    </script>

    <script>
        function assignbyhrajax(userid) {
            $("#asssign_" + userid).html("Done");
            $("#asssign_" + userid).css("background", "Green");
            $.ajax({
                type: 'POST',
                url: '<?= base_url('appronproj_assigninsert'); ?>',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'uid': userid},
                success: function (res) {
                    // var result = JSON.parse(res);
                    $("#asssign_" + res).html("Done");
                    $("#asssign_" + res).css("background", "Green");
                },
            });
        }
    </script>
</body>	
</html>