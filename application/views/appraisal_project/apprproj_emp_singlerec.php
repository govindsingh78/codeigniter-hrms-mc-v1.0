<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
error_reporting(0);
$IoLock = '';

//All Cond For BC...
if ((($SingleRecArr->identityro_io == "IO") and ( $SingleappraisalRecBC->current_status > 1)) or ( $SingleRecArr->identityro_io == "RO")) {
    $IoLock = 'disabled="disabled"';
}
if (($SingleRecArr->identityro_io == "BOTH") and ( $SingleappraisalRecBC->current_status > 1)) {
    $IoLock = 'disabled="disabled"';
}
if (( $SingleappraisalRecBC->current_status > 3) and ( $SingleRecArr->identityro_io == "BOTH")) {
    $RoLock = 'disabled="disabled"';
}
if (( $SingleappraisalRecBC->current_status > 3) and ( $SingleRecArr->identityro_io == "RO")) {
    $RoLock = 'disabled="disabled"';
}
////////////DE/////////////////////
//All Cpond For DE...
//********************************

if ((($SingleRecArr->identityro_io == "IO") and ( $SingleappraisalRec->current_status > 1)) or ( $SingleRecArr->identityro_io == "RO")) {
    $IoLockDE = 'disabled="disabled"';
}
if (($SingleRecArr->identityro_io == "BOTH") and ( $SingleappraisalRec->current_status > 1)) {
    $IoLockDE = 'disabled="disabled"';
}
if (( $SingleappraisalRec->current_status > 3) and ( $SingleRecArr->identityro_io == "BOTH")) {
    $RoLockDE = 'disabled="disabled"';
}
if (( $SingleappraisalRec->current_status > 3) and ( $SingleRecArr->identityro_io == "RO")) {
    $RoLockDE = 'disabled="disabled"';
}
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <?php if ($this->session->flashdata('successmsg')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('errormsg')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong> Error ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                    </div>
                <?php } ?>


                <div class="row clearfix">
                    <div class="col-lg-12">


                        <div class="card">
                            <div class="body">
                                <div class="panel panel-default">
                                    <div class="row" style="background:#7777771f">
                                        <div class="col-sm-12" >
                                            Fill Employee Appraisal - Form :  &nbsp; &nbsp; <a class="btn btn-success" href="<?= base_url("apprai_projsiteofc"); ?>"> &nbsp; Back </a>
                                        </div>
                                    </div><br>
                                    <div class="panel-body"> 
                                        <div class="panel-body">

                                            <form id="form-filter" method="post" action="<?= base_url("apprai_projsiteofcbcform_saveorupd"); ?>" class="form-horizontal">
                                                <?php if (( $SingleRecArr->jobtitle_id == "9") OR ( $SingleRecArr->jobtitle_id == "10")) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading" style="background:#337ab7; color:#fff; padding: 10px">
                                                            <h5 align="center"> Annual Performance Appraisal cum Training Need Identification Form </h5>
                                                            <h6 align="center"> Middle And Senior Management Cadre (Group B & C) </h6>
                                                            <b align="center"> Performance Appraisal - April 2020 </b> <br><br>
                                                            <H6 align="center"> NOTE : - PLEASE SAVE YOUR DATA BEFORE YOU CLICK ON SUBMIT & LOCK. OTHERWISE YOU WILL LOOSE DATA AND WILL BE REQUIRED TO REFILLED.</H6>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> Personal Information : </b></li>
                                                                </div>
                                                            </div>
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <th> Employee Full Name : </th>
                                                                        <th> Date of Joining : </th>
                                                                        <th> Department : </th>
                                                                        <th> Designation : </th>
                                                                        <th> Project : </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> <?= ($SingleRecArr->userfullname) ? $SingleRecArr->prefix_name . " " . $SingleRecArr->userfullname : ""; ?></td>
                                                                        <td> <?= ($SingleRecArr->date_of_joining) ? date("d-m-Y", strtotime($SingleRecArr->date_of_joining)) : ""; ?> </td>
                                                                        <td> <?= ($SingleRecArr->department_name) ? $SingleRecArr->department_name : ""; ?> </td>
                                                                        <td> <?= ($SingleRecArr->position_name) ? $SingleRecArr->position_name : ""; ?> </td>
                                                                        <td><?= (@$SingleRecArr->allprojName) ? $SingleRecArr->allprojName : ""; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th> Educational Qualification : </th>
                                                                        <th> Total Experience : </th>
                                                                        <th> Appraisal  Cycle : </th>
                                                                        <th> Due Date of Increment : </th>
                                                                        <th> Name of IO & RO : </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b><?php
                                                                                if ($recEducationalDetail) {
                                                                                    foreach ($recEducationalDetail as $recEdu) {
                                                                                        echo $recEdu->course . " --> " . $recEdu->institution_name;
                                                                                        echo "<br>";
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b><?= "<span style='color:green'> CEG :-  </span>" . $totCegExpr; ?><br>
                                                                                <?= "<span style='color:green'> Other :-  </span>" . $totOtherExpr; ?></b>
                                                                        </td>
                                                                        <td>Apr-2020</td>
                                                                        <td>Apr-2020</td>
                                                                        <td>
                                                                            IO : <?= ($SingleRecArr->reporting_manager_name) ? $SingleRecArr->reporting_manager_name : ""; ?> <br>
                                                                            RO : <?= ($SingleRecArr->reviewing_officer_roname) ? $SingleRecArr->reviewing_officer_roprefixname . " " . $SingleRecArr->reviewing_officer_roname : ""; ?> <br>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <p>
                                                                <b> PERFORMANCE FACTOR RATINGS: Using the following description please give rating that most closely describes the employee's performance for each of the required performance factors mentioned below </b>
                                                            </p>

                                                            <hr>
                                                            <h4 align="center"> 5: Excellent | 4: V.Good | 3: Good: 2 Average | 1: Poor </h4>
                                                            <h5 align="center"> Rating on Performance Factors below mentioned on 1-5 scale - evaluating each parameter </h5>
                                                            <hr>

                                                            <div class="row">
                                                                <div class="col-sm-2" >
                                                                    <label><b> Sr. No. </b></label>
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label><b> Performance Factors </b></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <label><b> Rating by IO (1-5) </b></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <label><b> Rating by RO (1-5) </b></label>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> A. JOB KNOWLEDGE </b></li>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    1. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label class="email">  Technical / Functional Knowledge. <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_jobkno_techfunc_ratingIO" name="bcform_jobkno_techfunc_ratingIO">
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_jobkno_techfunc_ratingRO" name="bcform_jobkno_techfunc_ratingRO">
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_jobkno_techfunc_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> B. PERSONAL SKILLS </b></li>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    2. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label>  Communication Skills. <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_person_communskill_ratingIO" name="bcform_person_communskill_ratingIO">
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_person_communskill_ratingRO" name="bcform_person_communskill_ratingRO">
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_communskill_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    3. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Team Management and Interpersonal Skills. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_person_teammanageinterpskill_ratingIO" name="bcform_person_teammanageinterpskill_ratingIO">
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_person_teammanageinterpskill_ratingRO" name="bcform_person_teammanageinterpskill_ratingRO">
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_person_teammanageinterpskill_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> C. MANAGERIAL SKILLS </b></li>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    4. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Delegation Skills. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_managerialskill_delegationskill_ratingIO" name="bcform_managerialskill_delegationskill_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_managerialskill_delegationskill_ratingRO" name="bcform_managerialskill_delegationskill_ratingRO">
                                                                            <option value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_delegationskill_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    5. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Problem Solving and Process Development. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_managerialskill_probsolv_ratingIO" name="bcform_managerialskill_probsolv_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_managerialskill_probsolv_ratingRO" name="bcform_managerialskill_probsolv_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_probsolv_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    6. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Planning, Budgeting and Monitoring. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_managerialskill_planningmonit_ratingIO" name="bcform_managerialskill_planningmonit_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_managerialskill_planningmonit_ratingRO" name="bcform_managerialskill_planningmonit_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_planningmonit_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    7. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Negotiation / Commercial Skills. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_managerialskill_negotiation_ratingIO" name="bcform_managerialskill_negotiation_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_managerialskill_negotiation_ratingRO" name="bcform_managerialskill_negotiation_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_managerialskill_negotiation_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> D. LEADERSHIP SKILLS </b></li>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    8. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Conflict Management. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_leadership_conflictmanag_ratingIO" name="bcform_leadership_conflictmanag_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_leadership_conflictmanag_ratingRO" name="bcform_leadership_conflictmanag_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_conflictmanag_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    9. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Strategic  Orientation. <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_leadership_strategic_ratingIO" name="bcform_leadership_strategic_ratingIO">
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_leadership_strategic_ratingRO" name="bcform_leadership_strategic_ratingRO">
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_strategic_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    10. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Customer Orientation. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_leadership_customerorient_ratingIO" name="bcform_leadership_customerorient_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_leadership_customerorient_ratingRO" name="bcform_leadership_customerorient_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_customerorient_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    11. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label>  Training, Developing and Team Building. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_leadership_trainingdevteambuild_ratingIO" name="bcform_leadership_trainingdevteambuild_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_leadership_trainingdevteambuild_ratingRO" name="bcform_leadership_trainingdevteambuild_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_leadership_trainingdevteambuild_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> E. ATTITUDES & ATTRIBUTES </b></li>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    12. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Initiative & Proactivity. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_attitude_initiativproactiv_ratingIO" name="bcform_attitude_initiativproactiv_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_attitude_initiativproactiv_ratingRO" name="bcform_attitude_initiativproactiv_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_initiativproactiv_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    13. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Result Orientation And Delivery. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_attitude_resultorient_ratingIO" name="bcform_attitude_resultorient_ratingIO">
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_attitude_resultorient_ratingRO" name="bcform_attitude_resultorient_ratingRO">
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_resultorient_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    14. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Openness To New Ideas. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLock; ?> class="form-control" id="bcform_attitude_openideas_ratingIO" name="bcform_attitude_openideas_ratingIO" >
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLock; ?> class="form-control" id="bcform_attitude_openideas_ratingRO" name="bcform_attitude_openideas_ratingRO" >
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRecBC->bcform_attitude_openideas_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>

                                                            <hr>
                                                            <h4 align="center"> Section 3: Contribution to Organization </h4>
                                                            <h5 align="center"> Assessment by Individual regarding his/ her 12 months activities/ performance including current Job Description, Goals/ Target achieved (Separate Sheet should be attached) </h5>
                                                            <hr> 
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-12" >
                                                                    <label class="email"> Please highlight significant achievements, initiatives and contributions made by appraisee during the year. (IO) <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-12" >
                                                                    <textarea <?= $IoLock; ?> class="form-control" name="bcform_describe_highlight_significant_achievementIO" ><?= ($SingleappraisalRecBC->bcform_describe_highlight_significant_achievementIO) ? $SingleappraisalRecBC->bcform_describe_highlight_significant_achievementIO : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-12" >
                                                                    <label class="email"> Please indicate constraints and limitations faced by appraisee in completing his/her responsibilities. (IO) <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-12" >
                                                                    <textarea <?= $IoLock; ?> class="form-control" name="bcform_describe_constraints_limitations_facedIO" ><?= ($SingleappraisalRecBC->bcform_describe_constraints_limitations_facedIO) ? $SingleappraisalRecBC->bcform_describe_constraints_limitations_facedIO : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                <div class="row"> 
                                                                    <div class="col-sm-12" >
                                                                        <label class="email"> Please highlight significant achievements, initiatives and contributions made by appraisee during the year. (RO) </label>
                                                                    </div>
                                                                    <div class="col-sm-12" >
                                                                        <textarea <?= $RoLock; ?> class="form-control" name="bcform_describe_highlight_significant_achievementRO" ><?= (@$SingleappraisalRecBC->bcform_describe_highlight_significant_achievementRO) ? @$SingleappraisalRecBC->bcform_describe_highlight_significant_achievementRO : ""; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="row"> 
                                                                    <div class="col-sm-12" >
                                                                        <label class="email"> Please indicate constraints and limitations faced by appraisee in completing his/her responsibilities. (RO) </label>
                                                                    </div>
                                                                    <div class="col-sm-12" >
                                                                        <textarea <?= $RoLock; ?> class="form-control" name="bcform_describe_constraints_limitations_facedRO" ><?= (@$SingleappraisalRecBC->bcform_describe_constraints_limitations_facedRO) ? @$SingleappraisalRecBC->bcform_describe_constraints_limitations_facedRO : ""; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php } ?> 

                                                            <h4 align="center"> Section 4: Learning & Development </h4>
                                                            <h5 align="center"> IO to indicate the Employee's Strength & Weakness, and to identify the areas of improvement /Training Needs </h5>
                                                            <hr>
                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> Technical Training </label>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> Behavioral Training  </label>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . <span class="reqfield"> * </span></label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control"  name="bcform_technical_trainingIO_01" ><?= ($SingleappraisalRecBC->bcform_technical_trainingIO_01) ? $SingleappraisalRecBC->bcform_technical_trainingIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . <span class="reqfield"> * </span></label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control"  name="bcform_behaviour_trainingIO_01" ><?= ($SingleappraisalRecBC->bcform_behaviour_trainingIO_01) ? $SingleappraisalRecBC->bcform_behaviour_trainingIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_technical_trainingIO_02"><?= ($SingleappraisalRecBC->bcform_technical_trainingIO_02) ? $SingleappraisalRecBC->bcform_technical_trainingIO_02 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_behaviour_trainingIO_02"><?= ($SingleappraisalRecBC->bcform_behaviour_trainingIO_02) ? $SingleappraisalRecBC->bcform_behaviour_trainingIO_02 : ""; ?></textarea>
                                                                </div>

                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_technical_trainingIO_03"><?= ($SingleappraisalRecBC->bcform_technical_trainingIO_03) ? $SingleappraisalRecBC->bcform_technical_trainingIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_behaviour_trainingIO_03"><?= ($SingleappraisalRecBC->bcform_behaviour_trainingIO_03) ? $SingleappraisalRecBC->bcform_behaviour_trainingIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_technical_trainingIO_04"><?= ($SingleappraisalRecBC->bcform_technical_trainingIO_04) ? $SingleappraisalRecBC->bcform_technical_trainingIO_04 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_behaviour_trainingIO_04" ><?= ($SingleappraisalRecBC->bcform_behaviour_trainingIO_04) ? $SingleappraisalRecBC->bcform_behaviour_trainingIO_04 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 5 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_technical_trainingIO_05"><?= ($SingleappraisalRecBC->bcform_technical_trainingIO_05) ? $SingleappraisalRecBC->bcform_technical_trainingIO_05 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 5 > . </label>
                                                                    &nbsp; <textarea <?= $IoLock; ?> class="form-control" name="bcform_behaviour_trainingIO_05" ><?= ($SingleappraisalRecBC->bcform_behaviour_trainingIO_05) ? $SingleappraisalRecBC->bcform_behaviour_trainingIO_05 : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <hr>
                                                            <h4 align="center">  Please mention your suggestions for improving functioning of the department and Organization </h4>
                                                            <hr>

                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . </label>
                                                                    &nbsp; <textarea  class="form-control" <?= $IoLock; ?> name="bcform_ideasuggestionIO_01" ><?= ($SingleappraisalRecBC->bcform_ideasuggestionIO_01) ? $SingleappraisalRecBC->bcform_ideasuggestionIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea  class="form-control" <?= $IoLock; ?> name="bcform_ideasuggestionIO_02" ><?= ($SingleappraisalRecBC->bcform_ideasuggestionIO_02) ? $SingleappraisalRecBC->bcform_ideasuggestionIO_02 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea class="form-control" <?= $IoLock; ?> name="bcform_ideasuggestionIO_03" ><?= ($SingleappraisalRecBC->bcform_ideasuggestionIO_03) ? $SingleappraisalRecBC->bcform_ideasuggestionIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea class="form-control" <?= $IoLock; ?> name="bcform_ideasuggestionIO_04" ><?= ($SingleappraisalRecBC->bcform_ideasuggestionIO_04) ? $SingleappraisalRecBC->bcform_ideasuggestionIO_04 : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-2" >
                                                                    <input type="hidden" name="formio_rotype" id="formio_rotype" value="<?= $SingleRecArr->identityro_io; ?>">
                                                                    <input type="hidden" name="io_userid" id="io_userid" value="<?= $SingleRecArr->reporting_manager; ?>">
                                                                    <input type="hidden" name="ro_userid" id="ro_userid" value="<?= $SingleRecArr->reviewing_officer_ro; ?>"> 
                                                                    <input type="hidden" name="user_id" id="user_id" value="<?= $SingleRecArr->user_id; ?>">

                                                                    <?php if (($SingleRecArr->identityro_io == "IO") and ( $SingleappraisalRecBC->current_status == "1")) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" > 
                                                                    <?php } ?>

                                                                    <?php if (($SingleRecArr->identityro_io == "IO") and ( $SingleappraisalRecBC->current_status == "") and ( $SingleappraisalRecBC->current_status != "4")) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" > 
                                                                    <?php } ?>

                                                                    <?php if (($SingleRecArr->identityro_io == "RO") and ( $SingleappraisalRecBC->current_status > 1) and ( $SingleappraisalRecBC->current_status != "4")) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" > 
                                                                    <?php } ?>     

                                                                    <?php if (($SingleRecArr->identityro_io == "BOTH") and ( $SingleappraisalRecBC->current_status != "4")) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" >
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="col-sm-10" >
                                                                    <small style="color:red"> NOTE : - PLEASE SAVE YOUR DATA BEFORE YOU CLICK ON SUBMIT & LOCK. OTHERWISE YOU WILL LOOSE DATA AND WILL BE REQUIRED TO REFILLED. </small> &nbsp;&nbsp; 
                                                                    <label class="email"> 
                                                                        <?php if (($SingleappraisalRecBC->current_status == 1) and ( $SingleRecArr->identityro_io == "BOTH" OR $SingleRecArr->identityro_io == "IO")) { ?>
                                                                            <a title="Lock" href="<?= base_url("lockprojappra_bc?id=" . $SingleRecArr->user_id . "&Loginiden=" . $SingleRecArr->identityro_io); ?>">
                                                                                <button type="button" class="btn btn-success"> Submit & Lock </button>
                                                                            </a>
                                                                        <?php } ?>
                                                                        <?php if (($SingleappraisalRecBC->current_status == 3) and ( $SingleRecArr->identityro_io == "RO")) { ?>
                                                                            <a title="Lock" href="<?= base_url("lockprojappra_bc?id=" . $SingleRecArr->user_id . "&Loginiden=" . $SingleRecArr->identityro_io); ?>">
                                                                                <button type="button" class="btn btn-success"> Submit & Lock </button>
                                                                            </a>
                                                                        <?php } ?>


                                                                        <?php //echo $SingleappraisalRecBC->current_status; ?>

                                                                    </label>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                <?php } ?>
                                            </form>



                                            <!-- ###########################################FOR 2 Start################################## -->
                                            <!-- ###########################################FOR 2 Start################################## -->
                                            <!-- ###########################################FOR 2 Start################################## -->
                                            <!-- ###########################################FOR 2 Start################################## -->

                                            <form id="form-filter" method="post" action="<?= base_url("apprai_projsiteofc_saveorupd"); ?>" class="form-horizontal">
                                                <?php if (($SingleRecArr->jobtitle_id == "11") OR ( $SingleRecArr->jobtitle_id == "12")) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading" style="background:#337ab7; color:#fff; padding: 10px"> 
                                                            <h5 align="center"> Annual Performance Appraisal cum Training Need Identification Form </h5>
                                                            <h6 align="center"> Middle And Senior Management Cadre (Group D & E) </h6>
                                                            <b align="center"> Performance Appraisal - April 2020 </b> <br><br>
                                                            <h6 align="center"> NOTE : - PLEASE SAVE YOUR DATA BEFORE YOU CLICK ON SUBMIT & LOCK</h6>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> Personal Information : </b></li>
                                                                </div>
                                                            </div>
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <th> Employee Full Name : </th>
                                                                        <th> Date of Joining : </th>
                                                                        <th> Department : </th>
                                                                        <th> Designation : </th>
                                                                        <th> Project : </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> <?= ($SingleRecArr->userfullname) ? $SingleRecArr->prefix_name . " " . $SingleRecArr->userfullname : ""; ?></td>
                                                                        <td> <?= ($SingleRecArr->date_of_joining) ? date("d-m-Y", strtotime($SingleRecArr->date_of_joining)) : ""; ?> </td>
                                                                        <td> <?= ($SingleRecArr->department_name) ? $SingleRecArr->department_name : ""; ?> </td>
                                                                        <td> <?= ($SingleRecArr->position_name) ? $SingleRecArr->position_name : ""; ?> </td>
                                                                        <td> <?= ($SingleRecArr->allprojName) ? $SingleRecArr->allprojName : ""; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th> Educational Qualification : </th>
                                                                        <th> Total Experience : </th>
                                                                        <th> Appraisal  Cycle : </th>
                                                                        <th> Due Date of Increment : </th>
                                                                        <th> Name of IO/RO : </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b><?php
                                                                                if ($recEducationalDetail) {
                                                                                    foreach ($recEducationalDetail as $recEdu) {
                                                                                        echo $recEdu->course . " --> " . $recEdu->institution_name;
                                                                                        echo "<br>";
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </b>
                                                                        </td>
                                                                        <td>
                                                                            <b><?= "<span style='color:green'> CEG :-  </span>" . $totCegExpr; ?><br>
                                                                                <?= "<span style='color:green'> Other :-  </span>" . $totOtherExpr; ?></b>
                                                                        </td>
                                                                        <td>Apr-2020</td>
                                                                        <td>Apr-2020</td>
                                                                        <td>
                                                                            IO : <?= ($SingleRecArr->reporting_manager_name) ? $SingleRecArr->reporting_manager_name : ""; ?> <br>
                                                                            RO : <?= ($SingleRecArr->reviewing_officer_roname) ? $SingleRecArr->reviewing_officer_roprefixname . " " . $SingleRecArr->reviewing_officer_roname : ""; ?> <br>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <p> 
                                                                <b> PERFORMANCE FACTOR RATINGS: Using the following description please give rating that most closely describes the employee's performance for each of the required performance factors mentioned below </b>
                                                            </p>
                                                            <hr>
                                                            <h4 align="center"> 5: Excellent | 4: V.Good | 3: Good: 2 Average | 1: Poor </h4>
                                                            <h5 align="center"> Rating on Performance Factors below mentioned on 1-5 scale - evaluating each parameter </h5>
                                                            <hr>

                                                            <div class="row">
                                                                <div class="col-sm-2" >
                                                                    <label><b> Sr. No. </b></label>
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label><b> Performance Factors </b></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <label><b> Rating by IO (1-5) </b></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <label><b> Rating by RO (1-5) </b></label>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> A. JOB KNOWLEDGE </b></li>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    1. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label class="email">  Technical / Functional Knowledge. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" id="deform_jobkno_techfunc_ratingIO" name="deform_jobkno_techfunc_ratingIO" >
                                                                        <option <?= (@$SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>

                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" id="deform_jobkno_techfunc_ratingRO" name="deform_jobkno_techfunc_ratingRO">
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_jobkno_techfunc_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> B. PERSONAL SKILLS </b></li>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    2. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label>  Communication Skills. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_communskill_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_communskill_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_communskill_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    3. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Quality Conciousness. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_qualityconc_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>

                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_qualityconc_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_qualityconc_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    4. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Self Management. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_selfmanagement_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_selfmanagement_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_selfmanagement_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    5. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Commercial Knowledge. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_commercialknowledge_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_commercialknowledge_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_commercialknowledge_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    6. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Problem Solving & Result Orientation. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_problemsolv_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_problemsolv_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_problemsolv_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    7. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Interpersonal Skills & Team Spirit. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_interpersskills_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_interpersskills_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_interpersskills_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    8. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Resource Mobilization & Utilization. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_resourcemobilization_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_resourcemobilization_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_resourcemobilization_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    9. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Process Orientation. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_persnskill_processorient_ratingIO">
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_persnskill_processorient_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_persnskill_processorient_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-12" >
                                                                    <li class="list-group-item"><b> C. ATTITUDES & ATTRIBUTES </b></li>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    10. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Initiative & Proactivity. <span class="reqfield"> * </span></label>
                                                                </div>

                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_attitude_initiativeproact_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_attitude_initiativeproact_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_initiativeproact_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>

                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    11. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Cost Conciousness and Timeliness. <span class="reqfield"> * </span></label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_attitude_costconcious_ratingIO" >
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_attitude_costconcious_ratingRO" >
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_costconcious_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <br>


                                                            <div class="row">
                                                                <div class="col-sm-1" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-sm-1" >
                                                                    12. &nbsp;&nbsp;
                                                                </div>
                                                                <div class="col-sm-4" >
                                                                    <label> Integrity. </label>
                                                                </div>
                                                                <div class="col-sm-3" >
                                                                    <select <?= $IoLockDE; ?> class="form-control" name="deform_attitude_integrity_ratingIO">
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                        <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingIO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                    </select>
                                                                </div>
                                                                <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                    <div class="col-sm-3" >
                                                                        <select <?= $RoLockDE; ?> class="form-control" name="deform_attitude_integrity_ratingRO">
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                            <option <?= ($SingleappraisalRec->deform_attitude_integrity_ratingRO == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                        </select>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>

                                                            <hr>
                                                            <h4 align="center"> Section 3 : Contribution to Organization </h4>
                                                            <h5 align="center"> Assessment by Individual regarding his/ her 12 months activities/ performance including current Job Description, Goals/ Target achieved (Separate Sheet should be attached) </h5>
                                                            <hr> 
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-12" >
                                                                    <label class="email"> Please highlight significant achievements, initiatives and Appraisee contributions made during the year. (IO) <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-12" >
                                                                    <textarea <?= $IoLockDE; ?> class="form-control" name="deform_describe_highlight_significant_achievementIO"><?= ($SingleappraisalRec->deform_describe_highlight_significant_achievementIO) ? $SingleappraisalRec->deform_describe_highlight_significant_achievementIO : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-12" >
                                                                    <label class="email"> Please indicate constraints and limitations faced by Appraisee in completing his responsibilities. (IO) <span class="reqfield"> * </span> </label>
                                                                </div>
                                                                <div class="col-sm-12" >
                                                                    <textarea <?= $IoLockDE; ?> class="form-control" name="deform_describe_constraints_limitations_facedIO" ><?= ($SingleappraisalRec->deform_describe_constraints_limitations_facedIO) ? $SingleappraisalRec->deform_describe_constraints_limitations_facedIO : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <?php if ($SingleRecArr->identityro_io != "IO") { ?>
                                                                <div class="row"> 
                                                                    <div class="col-sm-12" >
                                                                        <label class="email"> Please highlight significant achievements, initiatives and your contributions made during the year. (RO) </label>
                                                                    </div>
                                                                    <div class="col-sm-12" >
                                                                        <textarea <?= $RoLockDE; ?> class="form-control" name="deform_describe_highlight_significant_achievementRO" ><?= (@$SingleappraisalRec->deform_describe_highlight_significant_achievementRO) ? @$SingleappraisalRec->deform_describe_highlight_significant_achievementRO : ""; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="row"> 
                                                                    <div class="col-sm-12" >
                                                                        <label class="email"> Please indicate constraints and limitations faced by you in completing your responsibilities. (RO) </label>
                                                                    </div>
                                                                    <div class="col-sm-12" >
                                                                        <textarea <?= $RoLockDE; ?> class="form-control" name="deform_describe_constraints_limitations_facedRO"><?= (@$SingleappraisalRec->deform_describe_constraints_limitations_facedRO) ? @$SingleappraisalRec->deform_describe_constraints_limitations_facedRO : ""; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>

                                                            <hr>
                                                            <h4 align="center"> Section 4: Learning & Development </h4>
                                                            <h5 align="center"> IO to indicate the Employee's Strength & Weakness, and to identify the areas of improvement /Training Needs </h5>
                                                            <hr>

                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> Technical Training </label>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> Behavioral Training </label>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . <span class="reqfield"> * </span> </label>
                                                                    &nbsp; <textarea  <?= $IoLockDE; ?> class="form-control" name="deform_technical_trainingIO_01" ><?= ($SingleappraisalRec->deform_technical_trainingIO_01) ? $SingleappraisalRec->deform_technical_trainingIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . <span class="reqfield"> * </span> </label>
                                                                    &nbsp; <textarea  <?= $IoLockDE; ?> class="form-control" name="deform_behaviour_trainingIO_01" ><?= ($SingleappraisalRec->deform_behaviour_trainingIO_01) ? $SingleappraisalRec->deform_behaviour_trainingIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_technical_trainingIO_02" ><?= ($SingleappraisalRec->deform_technical_trainingIO_02) ? $SingleappraisalRec->deform_technical_trainingIO_02 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_behaviour_trainingIO_02" ><?= ($SingleappraisalRec->deform_behaviour_trainingIO_02) ? $SingleappraisalRec->deform_behaviour_trainingIO_02 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_technical_trainingIO_03" ><?= ($SingleappraisalRec->deform_technical_trainingIO_03) ? $SingleappraisalRec->deform_technical_trainingIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_behaviour_trainingIO_03" ><?= ($SingleappraisalRec->deform_behaviour_trainingIO_03) ? $SingleappraisalRec->deform_behaviour_trainingIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_technical_trainingIO_04" ><?= ($SingleappraisalRec->deform_technical_trainingIO_04) ? $SingleappraisalRec->deform_technical_trainingIO_04 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_behaviour_trainingIO_04" ><?= ($SingleappraisalRec->deform_behaviour_trainingIO_04) ? $SingleappraisalRec->deform_behaviour_trainingIO_04 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 5 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_technical_trainingIO_05" ><?= ($SingleappraisalRec->deform_technical_trainingIO_05) ? $SingleappraisalRec->deform_technical_trainingIO_05 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 5 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_behaviour_trainingIO_05" ><?= ($SingleappraisalRec->deform_behaviour_trainingIO_05) ? $SingleappraisalRec->deform_behaviour_trainingIO_05 : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <hr>
                                                            <h5 align="center"> Please mention your suggestions for improving functioning of the department and Organization </h5>
                                                            <hr>

                                                            <div class="row"> 
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 1 > . </label>
                                                                    &nbsp; <textarea  <?= $IoLockDE; ?> class="form-control" name="deform_ideasuggestionIO_01" ><?= ($SingleappraisalRec->deform_ideasuggestionIO_01) ? $SingleappraisalRec->deform_ideasuggestionIO_01 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 2 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_ideasuggestionIO_02" ><?= ($SingleappraisalRec->deform_ideasuggestionIO_02) ? $SingleappraisalRec->deform_ideasuggestionIO_02 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 3 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_ideasuggestionIO_03" ><?= ($SingleappraisalRec->deform_ideasuggestionIO_03) ? $SingleappraisalRec->deform_ideasuggestionIO_03 : ""; ?></textarea>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <label class="email"> 4 > . </label>
                                                                    &nbsp; <textarea <?= $IoLockDE; ?> class="form-control" name="deform_ideasuggestionIO_04" ><?= ($SingleappraisalRec->deform_ideasuggestionIO_04) ? $SingleappraisalRec->deform_ideasuggestionIO_04 : ""; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <br>

                                                            <div class="row"> 
                                                                <div class="col-sm-2" >
                                                                    <input type="hidden" name="formio_rotype" id="formio_rotype" value="<?= $SingleRecArr->identityro_io; ?>">
                                                                    <input type="hidden" name="io_userid" id="io_userid" value="<?= $SingleRecArr->reporting_manager; ?>">
                                                                    <input type="hidden" name="ro_userid" id="ro_userid" value="<?= $SingleRecArr->reviewing_officer_ro; ?>"> 
                                                                    <input type="hidden" name="user_id" id="user_id" value="<?= $SingleRecArr->user_id; ?>">

                                                                    <?php if (($SingleRecArr->identityro_io == "IO") and ( $SingleappraisalRec->current_status != "2") and ( $SingleappraisalRec->current_status != 4)) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" > 
                                                                    <?php } ?>

                                                                    <?php if (($SingleRecArr->identityro_io == "RO") and ( $SingleappraisalRec->current_status > 1) and ( $SingleappraisalRec->current_status != 4)) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" > 
                                                                    <?php } ?>

                                                                    <?php if ($SingleRecArr->identityro_io == "BOTH" and $SingleappraisalRec->current_status != 4) { ?>
                                                                        <input type="submit" value="Save" name="submit" class="btn btn-success btn-block" >
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="col-sm-10" >
                                                                    <small style="color:red"> NOTE : - PLEASE SAVE YOUR DATA BEFORE YOU CLICK ON SUBMIT & LOCK </small>&nbsp;&nbsp; 
                                                                    <label class="email">
                                                                        <?php if (($SingleappraisalRec->current_status == 1) and ( $SingleRecArr->identityro_io == "BOTH" or $SingleRecArr->identityro_io == "IO" or $SingleRecArr->identityro_io == "RO")) { ?>
                                                                            <a title="Lock" href="<?= base_url("lockprojappra_de?id=" . $SingleRecArr->user_id . "&Loginiden=" . $SingleRecArr->identityro_io); ?>">
                                                                                <button type="button" class="btn btn-success"> Submit & Lock </button>
                                <!--                                                        <i class="fa fa-lock fa-2x"></i>-->
                                                                            </a>
                                                                        <?php } ?>

                                                                        <?php if (($SingleappraisalRec->current_status == 3) and ( $SingleRecArr->identityro_io == "RO")) { ?>
                                                                            <a title="Lock" href="<?= base_url("lockprojappra_de?id=" . $SingleRecArr->user_id . "&Loginiden=" . $SingleRecArr->identityro_io); ?>">
                                                                                <button type="button" class="btn btn-success"> Submit & Lock </button>
                                <!--                                                        <i class="fa fa-lock fa-2x"></i>-->
                                                                            </a>
                                                                        <?php } ?>

                                                                    </label>

                                                                </div>

                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                            </form> 

                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <style>
            .datahidden{display:none;}
            .showdata{display:block;}
            .list-group-item {
                background:#7777771f;
            }
            .reqfield { color: red; }
        </style>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>