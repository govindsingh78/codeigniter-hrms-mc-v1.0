<?PHP
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
$courLisArr = list_cour_categ_type();
?>
<div class="page-wrapper">
    <div class="content container-fluid">	

        <?= form_open(base_url('back_end/Courier_controller/editcourier/' . $projId), array("method" => "post")); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" >SENDER'S DETAILS: </h3>
            </div>

            <div class="panel-body">
                <div class="row filter-row">
                    <div class="col-sm-4 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Business Unit</label>
                            <select id="businessunitnames" name="businessunitid" class="form-control" required>
                                <label for="email">Business Unit</label>
                                <?php foreach ($form_businessunit as $dept) { ?>
                                    <option <?= ($datacourier['businessunitid'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Department</label>
                            <label class="email">Department</label>
                            <select id="departmentnames" name="dept_id" id="dept_id" class="form-control" required>
                                <?php foreach ($deptname as $dept) { ?>
                                    <option <?= ($datacourier['dept_id'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->deptname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Employee</label>
                            <select id="empnames" name="senderempid" class="form-control" required>
                                <?php foreach ($employename as $dept) { ?>
                                    <option <?= ($datacourier['senderempid'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->userfullname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Mobile</label>
                            <input type="text" value="<?= $empdetails['contactnumber']; ?>" class="form-control" id="mobile" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" value="<?= $empdetails['emailaddress']; ?>"  class="form-control" id="email" readonly>
                        </div>
                    </div>

                </div>         
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" >RECIEVER'S DETAILS: </h3>
            </div>
            <div class="panel-body">
                <div class="row filter-row">				
                    <div class="col-sm-3 col-xs-12"> 
                        <div class="form-group">
                            <label for="email"> Business Unit </label>
                            <select id="rcvbusinessunitnames" name="rcvbusinessunitnames" onchange="genratedispatch_no()" class="form-control" readonly>
                                <?php foreach ($form_businessunit as $dept) { ?>
                                    <option <?= ($datacourier['rcv_buss_unit'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12"> 
                        <div class="form-group">
                            <label for="email"> Type : </label>
                            <select id="typer_categ_courier" name="typer_categ_courier" class="form-control" required>
                                <option value="" selected="selected">Select Type</option>
                                <?php
                                if ($courLisArr) {
                                    foreach ($courLisArr as $type) {
                                        ?>
                                        <option  <?= ($datacourier['typer_cate_id'] == $type->caur_fld_id) ? 'selected' : ''; ?> value="<?= $type->caur_fld_id; ?>"><?= $type->cour_cat_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12"> 
                        <div class="form-group">
                            <label for="email">Employee Name</label>
                            <input type="text" value="<?= $datacourier['receivername']; ?>" class="form-control" id="receivername" name="receivername" required>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12"> 
                        <div class="form-group">
                            <label for="email"> Company Name</label>
                            <select id="companynames" name="receivercompanyid" class="form-control"  required>
                                <option value="" selected="selected">Select Company</option>
                                <option value="new"> Other </option>
                                <?php foreach ($companyname as $company) { ?>
                                    <option <?= ($datacourier['receivercompanyid'] == $company->company_id) ? 'selected' : ''; ?> value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12"> 
                        <div class="form-group">
                            <label for="email"> Office Location</label>
                            <select id="officelocation" name="receiverpincode" class="form-control"  required>
                                <option value="" selected="selected">Select Pincode</option>
                                <?php foreach ($pin as $company) { ?>
                                    <option <?= ($datacourier['receiverpincode'] == $company->id) ? 'selected' : ''; ?> value="<?= $company->id; ?>"><?= $company->pincode; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>

                <hr>

                <div class="panel-body">
                    <div class="row filter-row">
                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Addresss 1</label>
                                <input type="text" value="<?= $country['address1']; ?>" class="form-control" name="add1" id="add1" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Addresss 2</label>
                                <input type="text" value="<?= $country['address2']; ?>" class="form-control" name="add2" id="add2" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Addresss 3</label>
                                <input type="text" value="<?= $country['address3']; ?>" class="form-control" name="add3" id="add3" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">City</label>
                                <input type="text" value="<?= $country['city_name']; ?>" class="form-control" name="city" id="city" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">State </label>
                                <input type="text" value="<?= $country['state_name']; ?>" class="form-control" name="state" id="state" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Country </label>
                                <input type="text"  value="<?= $country['country_name']; ?>" class="form-control" name="country" id="country" readonly>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Mobile </label>
                                <input type="text" value="<?= $datacourier['receiver_mobile']; ?>" class="form-control" id="receiver_mobile" name="receiver_mobile">
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email">Email </label>
                                <input type="text" value="<?= $datacourier['receiveremail']; ?>" class="form-control" id="receiveremail" name="receiveremail">
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email"> Dispatch No </label>
                                <input type="text" value="<?= $datacourier['dispatch']; ?>" readonly class="form-control" id="dispatch" name="dispatch">
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12"> 
                            <div class="form-group">
                                <label for="email"> Courier Services</label>
                                <label class="email"> Courier Services</label>
                                <select id="courier_service" name="courier_service" class="form-control" >
                                    <option value="" selected="selected">Select Courier</option>
                                    <?php foreach ($courier as $courierval) { ?>
                                        <option <?= ($datacourier['courier_service'] == $courierval->id) ? 'selected' : ''; ?>  value="<?= $courierval->id; ?>"><?= $courierval->courier_service; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2 col-xs-12"> 
                            <br>
                            <input type="submit" id="submit" class="btn btn-success btn-block" value="Update">
                        </div>
                    </div>

                </div>	
            </div>	
        </div>

        <?= form_close(); ?>



    </div>
</div>	


<?PHP $this->load->view("back_end/includes/footer"); ?>

<script>

    function updproccess(updid, courierserviceid) {
        $("#updider").val(updid);
        $("#updider").val(updid);
        $("#courier_serviceupd").val(courierserviceid);

    }

    function setasnewcomp() {
        var comtypeid = $("#companynames").val();
        if (comtypeid == "new") {
            $("#add1").removeAttr("readonly");
            $("#add2").removeAttr("readonly");
            $("#add3").removeAttr("readonly");
            $("#city").removeAttr("readonly");
            $("#state").removeAttr("readonly");
            $("#country").removeAttr("readonly");
            $("#officelocation").removeAttr("required");
        } else {
            $("#add1").attr("readonly", "readonly");
            $("#add2").attr("readonly", "readonly");
            $("#add3").attr("readonly", "readonly");
            $("#city").attr("readonly", "readonly");
            $("#state").attr("readonly", "readonly");
            $("#country").attr("readonly", "readonly");
        }
    }

    $(document).ready(function () {
        $('#companynames').change(function () {
            companyID = $(this).find("option:selected").val();
            if (companyID) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('back_end/Courier_controller/getcompanynames'); ?>',
                    data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'company_id': companyID},
                    success: function (data) {
                        $('#officelocation').html('<option value="">Select pincode</option>');
                        var dataObj = jQuery.parseJSON(data);
                        console.log(dataObj);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                var option = $('<option />');
                                option.attr('value', this.id).text(this.pincode);
                                $('#officelocation').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#officelocation').on('change', function () {
            var pincode = $(this).val();
            if (pincode) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('back_end/Courier_controller/getDetails'); ?>',
                    data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'company_id': pincode},
                    success: function (data) {
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                $('#city').val(this.city_name);
                                $('#country').val(this.country_name);
                                $('#state').val(this.state_name);
                                $('#add1').val(this.address1);
                                $('#add2').val(this.address2);
                                $('#add3').val(this.address3);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#businessunitnames').change(function () {
            bname = $(this).find("option:selected").val();
            if (bname) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('back_end/Courier_controller/getdeptname'); ?>',
                    data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'id': bname},
                    success: function (data) {
                        $('#departmentnames').html('<option value="">Select department</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                var option = $('<option />');
                                option.attr('value', this.id).text(this.deptname);
                                $('#departmentnames').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#departmentnames').on('change', function () {
            var deptID = $(this).val();
            if (deptID) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('back_end/Courier_controller/getempbydept'); ?>',
                    data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'id': deptID},
                    success: function (data) {
                        $('#empnames').html('<option value="">Select Employee Name</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                var option = $('<option />');
                                option.attr('value', this.user_id).text(this.userfullname);
                                $('#empnames').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#empnames').on('change', function () {
            var empname = $(this).val();
            if (empname) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('back_end/Courier_controller/getempdetails'); ?>',
                    data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'id': empname},
                    success: function (data) {
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                $('#mobile').val(this.contactnumber);
                                $('#email').val(this.emailaddress);
                            });
                        }
                    },
                });
            }
        });
    });
</script>

</body>	
</html>


