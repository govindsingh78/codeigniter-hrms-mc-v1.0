<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
// $courLisArr = list_cour_categ_type();
// $GetAllEmplArr = get_all_Activeempls();
$courLisArr = list_cour_categ_type();
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				
				<?= form_open(base_url('Courier_controller/editcourier/' . $projId), array("method" => "post")); ?>
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
							<div class="row">
<div class="col-xs-10">
<h4 class="page-title" >SENDER'S DETAILS:</h4>
</div>
</div>
 
<!--<div class="row filter-row">-->

<div class="row">
<div class="col-lg-3 col-md-6">
	<b>Business Unit :</b>
	<div class="input-group mb-3">
		<select id="businessunitnames" name="businessunitid" class="form-control" required>
                                <?php foreach ($form_businessunit as $dept) { ?>
                                    <option <?= ($datacourier['businessunitid'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Department :</b>
	<div class="input-group mb-3">
		<select id="departmentnames" name="dept_id" id="dept_id" class="form-control" required>
                                <?php foreach ($deptname as $dept) { ?>
                                    <option <?= ($datacourier['dept_id'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->deptname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>EMPLOYEE :</b>
	<div class="input-group mb-3">
		<select id="empnames" name="senderempid" class="form-control" required>
                                <?php foreach ($employename as $dept) { ?>
                                    <option <?= ($datacourier['senderempid'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->userfullname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Mobile :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $empdetails['contactnumber']; ?>" class="form-control" id="mobile" readonly>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Email :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $empdetails['emailaddress']; ?>"  class="form-control" id="email" readonly>
	</div>
</div>



							</div>
						</div>
					</div>          
				</div>
				</div>


<!-- //===============RECIEVER's Details ==============// -->             
<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
							<div class="row">
<div class="col-xs-10">
<h4 class="page-title" >RECIEVER'S DETAILS:</h4>
</div>
</div>
 
<!--<div class="row filter-row">-->

<div class="row">
<div class="col-lg-3 col-md-6">
	<b>Business Unit :</b>
	<div class="input-group mb-3">
		<select id="rcvbusinessunitnames" name="rcvbusinessunitnames" onchange="genratedispatch_no()" class="form-control" readonly>
                                <?php foreach ($form_businessunit as $dept) { ?>
                                    <option <?= ($datacourier['rcv_buss_unit'] == $dept->id) ? 'selected' : ''; ?> value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>TYPE :</b>
	<div class="input-group mb-3">
		<select id="typer_categ_courier" name="typer_categ_courier" class="form-control" required>
                                <option value="" selected="selected">Select Type</option>
                                <?php
                                if ($courLisArr) {
                                    foreach ($courLisArr as $type) {
                                        ?>
                                        <option  <?= ($datacourier['typer_cate_id'] == $type->caur_fld_id) ? 'selected' : ''; ?> value="<?= $type->caur_fld_id; ?>"><?= $type->cour_cat_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>EMPLOYEE NAME :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $datacourier['receivername']; ?>" class="form-control" id="receivername" name="receivername" required>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Company Name :</b>
	<div class="input-group mb-3">
		<select id="companynames" name="receivercompanyid" class="form-control"  required>
                                <option value="" selected="selected">Select Company</option>
                                <option value="new"> Other </option>
                                <?php foreach ($companyname as $company) { ?>
                                    <option <?= ($datacourier['receivercompanyid'] == $company->company_id) ? 'selected' : ''; ?> value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
                                <?php } ?>
                            </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Office Location :</b>
	<div class="input-group mb-3">
		<select id="officelocation" name="receiverpincode" class="form-control"  required>
                                <option value="" selected="selected">Select Pincode</option>
                                <?php foreach ($pin as $company) { ?>
                                    <option <?= ($datacourier['receiverpincode'] == $company->id) ? 'selected' : ''; ?> value="<?= $company->id; ?>"><?= $company->pincode; ?></option>
                                <?php } ?>
                            </select>
	</div>
</div>

							</div>
						</div>
						
						<hr>
						
						<div class="body">

 
<!--<div class="row filter-row">-->

<div class="row">
<div class="col-lg-3 col-md-6">
	<b>Addresss 1 :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $country['address1']; ?>" class="form-control" name="add1" id="add1" readonly>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Addresss 2 :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $country['address2']; ?>" class="form-control" name="add2" id="add2" readonly>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>City :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $country['city_name']; ?>" class="form-control" name="city" id="city" readonly>
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>State:</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $country['state_name']; ?>" class="form-control" name="state" id="state" readonly>
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>Country :</b>
	<div class="input-group mb-3">
		<input type="text"  value="<?= $country['country_name']; ?>" class="form-control" name="country" id="country" readonly>
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>Mobile :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $datacourier['receiver_mobile']; ?>" class="form-control" id="receiver_mobile" name="receiver_mobile">
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>Email :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $datacourier['receiveremail']; ?>" class="form-control" id="receiveremail" name="receiveremail">
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>Dispatch No :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $datacourier['dispatch']; ?>" readonly class="form-control" id="dispatch" name="dispatch">
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<b>Courier Services :</b>
	<div class="input-group mb-3">
		<select id="courier_service" name="courier_service" class="form-control" >
                                    <option value="" selected="selected">Select Courier</option>
                                    <?php foreach ($courier as $courierval) { ?>
                                        <option <?= ($datacourier['courier_service'] == $courierval->id) ? 'selected' : ''; ?>  value="<?= $courierval->id; ?>"><?= $courierval->courier_service; ?></option>
                                    <?php } ?>
                                </select>
	</div>
</div>


<div class="col-lg-3 col-md-6">
	<div class="input-group mb-3">
		<input type="submit" id="submit" class="btn btn-success btn-block" value="Update">
	</div>
</div>



							</div>
						</div>
						
					</div>          
				</div>
				</div>
				<?= form_close(); ?>
				
            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
        
    </div>


<div id="add_courier" class="modal custom-modal fade" role="dialog">
<div class="modal-dialog">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<div class="modal-content modal-lg">
<div class="modal-header">
<h4 class="modal-title">SENDER'S DETAILS: </h4>
</div>

<div class="modal-body">			
<?= form_open(base_url('back_end/Courier_controller/savecompanydetail'), array("method"=>"post")); ?>

<div class="row">

<div class="col-sm-3 col-xs-12">
<label class="control-label"></label>
<img src="<?= HOSTNAME . "assets/avatar.jpg"; ?>" id="userimg" class="img-circle" alt="Cinque Terre" width="100" height="105"> 
</div>

<div class="col-sm-6 col-xs-12">
<label class="control-label" style="color:green"> Details : </label>
<table class="table" style="border:green 1px solid;">
<thead>
<tr>
<th width="24%">Department</th>
<th width="24%">B-Unit </th>
<th width="24%">Email</th>
<th width="24%">Contact</th>
</tr>
</thead>
<tbody>
<tr>
<td id="dvdepartment_name"></td>
<td id="dvbunit"></td>
<td id="dvemail"></td>
<td id="dvcontact"></td>
</tr>
</tbody>
</table>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Employee Name : <span id="lvlspan">*</span> </label>
<select required name="empid" id="empid" onchange="getEmpDetails()"  onchange="getEmpDetailsother()" class="form-control custom-select">
<option value="" selected="selected">-- Select Employee --</option>
<?php
if ($GetAllEmplArr) {
foreach ($GetAllEmplArr as $Emprowrec) {
?>
<option value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
<?php
}
}
?>
<option value="other"> Other </option>
</select>
</div>
</div>
    
</div>

<br/><br/>
<div class="modal-header">
<h4 class="modal-title">RECIEVER'S DETAILS: </h4>
</div>


<div class="row">

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Business Unit</label>
<select id="rcvbusinessunitnames" name="rcvbusinessunitnames" onchange="genratedispatch_no()" class="form-control" required>
<?php foreach ($form_businessunit as $dept) { ?>
<option value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
<?php
}
?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Type : </label>
<select id="typer_categ_courier" name="typer_categ_courier" class="form-control" required>
<option value="" selected="selected">Select Type</option>
<?php
if ($courLisArr) {
foreach ($courLisArr as $type) {
?>
<option value="<?= $type->caur_fld_id; ?>"><?= $type->cour_cat_name; ?></option>
<?php
}
}
?>
</select>
</div>
</div>


<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Employee Name</label>
<input type="text" autocomplete="off" class="form-control" id="receivername" name="receivername" required>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Company Name</label>
<select id="companynames" name="receivercompanyid" class="form-control custom-select" onchange="setasnewcomp()" required>
<option value="" selected="selected">Select Company</option>
<option value="new"> Other </option>
<?php foreach ($companyname as $company) { ?>
<option value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Office Location</label>
<select id="officelocation" name="receiverpincode" class="form-control" required>
<option value="">Select company first</option>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 1</label>
<input type="text" autocomplete="off" class="form-control" name="add1" id="add1" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 2</label>
<input type="text" autocomplete="off" class="form-control" name="add2" id="add2" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 3</label>
<input type="text" autocomplete="off" class="form-control" name="add3" id="add3" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">City</label>
<input type="text" autocomplete="off" class="form-control" name="city" id="city" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">State </label>
<input type="text" autocomplete="off" class="form-control" name="state" id="state" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Country </label>
<input type="text" autocomplete="off" class="form-control" name="country" id="country" readonly>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Mobile </label>
<input type="text" autocomplete="off" class="form-control" id="receiver_mobile" name="receiver_mobile">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Email </label>
<input type="text" autocomplete="off" class="form-control" id="receiveremail" name="receiveremail">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Dispatch No </label>
<input type="text" readonly class="form-control" id="dispatch" name="dispatch">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label class="control-label"> Packing Type </label>
<select id="courier_packtype" name="courier_packtype" class="form-control" >
<?php foreach ($c_packtypeArr as $ckey => $pval) { ?>
<option value="<?= $ckey; ?>"><?= $pval; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Weight </label>
<input type="text" autocomplete="off" class="form-control" id="courier_weight" name="courier_weight">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label"> Courier Services</label>
<select id="courier_service" name="courier_service" class="form-control" >
<option value="" selected="selected">Select Courier</option>
<?php foreach ($courier as $courierval) { ?>
<option value="<?= $courierval->id; ?>"><?= $courierval->courier_service; ?></option>
<?php } ?>
</select>
</div>
</div>
</div>

<div class="m-t-20 text-center">
<input type="submit" class="btn btn-success" value="Submit">
</div>

<?=form_close(); ?>
</div>
</div>	
</div>
</div>


<script>

function updproccess(updid, courierserviceid) {
$("#updider").val(updid);
$("#updider").val(updid);
$("#courier_serviceupd").val(courierserviceid);

}
function genratedispatch_no() {
var rcvbunit = $("#rcvbusinessunitnames").val();

if (rcvbunit) {
$.ajax({
type: 'POST',
url: '<?php echo base_url('getnextdispetchno?rcvbunit='); ?>' + rcvbunit,
success: function (data) {
	console.log(data);
	$("#dispatch").val(data);
},
});
}
}
function setasnewcomp() {
var comtypeid = $("#companynames").val();
if (comtypeid == "new") {
$("#add1").removeAttr("readonly");
$("#add2").removeAttr("readonly");
$("#add3").removeAttr("readonly");
$("#city").removeAttr("readonly");
$("#state").removeAttr("readonly");
$("#country").removeAttr("readonly");
$("#officelocation").removeAttr("required");
} else {
$("#add1").attr("readonly", "readonly");
$("#add2").attr("readonly", "readonly");
$("#add3").attr("readonly", "readonly");
$("#city").attr("readonly", "readonly");
$("#state").attr("readonly", "readonly");
$("#country").attr("readonly", "readonly");
}
}

$(document).ready(function () {
$('#companynames').change(function () {
companyID = $(this).find("option:selected").val();
if (companyID) {
$.ajax({
	type: 'POST',
	url: '<?php echo base_url('back_end/Courier_controller/getcompanynames'); ?>',
	data: {'company_id': companyID},
	success: function (data) {
		$('#officelocation').html('<option value="">Select pincode</option>');
		var dataObj = jQuery.parseJSON(data);
		console.log(dataObj);
		if (dataObj) {
			$(dataObj).each(function () {
				var option = $('<option />');
				option.attr('value', this.id).text(this.pincode);
				$('#officelocation').append(option);
			});
		}
	},
});
}
});
});


$(document).ready(function () {
$('#officelocation').on('change', function () {
var pincode = $(this).val();
if (pincode) {
$.ajax({
	type: 'POST',
	url: '<?php echo base_url('back_end/Courier_controller/getDetails'); ?>',
	data: {'company_id': pincode},
	success: function (data) {
		var dataObj = jQuery.parseJSON(data);
		if (dataObj) {
			$(dataObj).each(function () {
				console.log(this);
				$('#city').val(this.city_name);
				$('#country').val(this.country_name);
				$('#state').val(this.state_name);
				$('#add1').val(this.address1);
				$('#add2').val(this.address2);
				$('#add3').val(this.address3);
			});
		}
	},
});
}
});
});


$(document).ready(function () {
$('#businessunitnames').change(function () {
bname = $(this).find("option:selected").val();
if (bname) {
$.ajax({
	type: 'POST',
	url: '<?php echo base_url('back_end/Courier_controller/getdeptname'); ?>',
	data: {'id': bname},
	success: function (data) {
		$('#departmentnames').html('<option value="">Select department</option>');
		var dataObj = jQuery.parseJSON(data);
		if (dataObj) {
			$(dataObj).each(function () {
				var option = $('<option />');
				option.attr('value', this.id).text(this.deptname);
				$('#departmentnames').append(option);
			});
		}
	},
});
}
});
});


$(document).ready(function () {
$('#departmentnames').on('change', function () {
var deptID = $(this).val();
if (deptID) {
$.ajax({
	type: 'POST',
	url: '<?php echo base_url('back_end/Courier_controller/getempbydept'); ?>',
	data: {'id': deptID},
	success: function (data) {
		$('#empnames').html('<option value="">Select Employee Name</option>');
		var dataObj = jQuery.parseJSON(data);
		if (dataObj) {
			$(dataObj).each(function () {
				console.log(this);
				var option = $('<option />');
				option.attr('value', this.user_id).text(this.userfullname);
				$('#empnames').append(option);
			});
		}
	},
});
}
});
});


$(document).ready(function () {
$('#empnames').on('change', function () {
var empname = $(this).val();
if (empname) {
$.ajax({
	type: 'POST',
	url: '<?php echo base_url('back_end/Courier_controller/getempdetails'); ?>',
	data: {'id': empname},
	success: function (data) {
		var dataObj = jQuery.parseJSON(data);
		if (dataObj) {
			$(dataObj).each(function () {
				console.log(this);
				$('#mobile').val(this.contactnumber);
				$('#email').val(this.emailaddress);
			});
		}
	},
});
}
});
});


function getEmpDetails() {
var empid = $("#empid").val();
if (empid > 0) {
$('#empothersection').hide();
$.ajax({
url: '<?= base_url('getemppdetailbyid_ajax/'); ?>' + empid,
type: "GET",
dataType: "json",
success: function (data) {
	$('#dvdepartment_name').empty();
	$('#dvbunit').empty();
	$('#dvemail').empty();
	$('#dvcontact').empty();
	$('#userimg').attr("src", "<?= HOSTNAME . "assets/avatar.jpg"; ?>");
	$.each(data, function (key, value) {
		$('#dvdepartment_name').html(value.department_name);
		$('#dvbunit').html(value.businessunit_name);
		$('#dvemail').html(value.emailaddress);
		$('#dvcontact').html(value.contactnumber);
//                                            
		if (value.profileimg) {
			$('#userimg').attr("src", "http://hrms.cegindia.com/public/uploads/profile/" + value.profileimg);
		}
	});
}
});
} else {
$('#dvdepartment_name').empty();
$('#dvbunit').empty();
$('#dvemail').empty();
$('#dvcontact').empty();
$('#userimg').attr("src", "<?= HOSTNAME . "assets/avatar.jpg"; ?>");
getEmpDetailsother();
}
}

$(function () {
$("#empid,#project,#approvedby").customselect();
});




var table;
$(document).ready(function () {
	   $("#from_date").datepicker(); 
   $("#to_date").datepicker();
table = $('#tabledata').DataTable({
"processing": true, //Feature control the processing indicator.
"serverSide": true, //Feature control DataTables' server-side processing mode.
"order": [], //Initial no order.
// Load data for the table's content from an Ajax source
"ajax": {
"url": "<?php echo base_url('Courier_controller/ajax_list') ?>",
"type": "POST",
"data": function (data) {
data.businessunit_name = $('#businessunit_name').val();
data.courier_service = $('#courier_service').val();
data.from_date = $('#from_date').val();
data.to_date = $('#to_date').val();
},

},
"dom": 'lBfrtip',
"buttons": [{
	extend: 'collection',
	text: 'Export',
	buttons: [
		'copy',
		'excel',
		'csv',
		'pdf',
		'print'
	]
}
],
//Set column definition initialisation properties.
"columnDefs": [{
	"targets": [0], //first column / numbering column
	"orderable": false, //set not orderable
},
],
"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
});
// var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
// $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
$('#btn-filter').click(function () { //button filter event click
table.ajax.reload();  //just reload table
});
$('#btn-reset').click(function () { //button reset event click
$('#form-filter')[0].reset();
table.ajax.reload();  //just reload table
});
});
</script>

<div class="modal custom-modal fade" id="myModal" role="dialog">
<div class="modal-dialog">
    
<button type="button" class="close" data-dismiss="modal">&times;</button>

<div class="modal-content modal-lg">
    
<div class="modal-header">
<h4 class="modal-title">Update C. Service </h4>
</div>
    
<div class="modal-body">
<?= form_open('back_end/Courier_controller/updatecservice', array("method" => "post")); ?>
 <div class="row">    
<div class="col-sm-12">    
<div class="form-group">
<label for="control-label">Courier Services</label>
<select id="courier_serviceupd" name="courier_serviceupd" class="form-control" required>
<option value="" selected="selected">Select Courier</option>
<?php foreach ($courier as $courierval) { ?>
<option value="<?= $courierval->id; ?>"><?= $courierval->courier_service; ?></option>
<?php } ?>
</select>
</div>
</div>   
    
<div class="m-t-20 text-center">
<input type="hidden" class="" id="updider" name="updider" value="" />
<input type="submit" class="btn btn-primary" value="Update">
<?= form_close(); ?>

</div>
</div>    
</div>
</div>

</div>
</div>
<script type="text/javascript">
//code by durgesh
function getEmpDetails() {
var empid = $("#empid").val();
if (empid > 0) {
$('#empothersection').hide();
$.ajax({
url: '<?= base_url('getemppdetailbyid_ajax'); ?>/' + empid,
type: "GET",
dataType: "json",
success: function (data) {
	$('#dvdepartment_name').empty();
	$('#dvbunit').empty();
	$('#dvemail').empty();
	$('#dvcontact').empty();
	$('#userimg').attr("src", "<?= HOSTNAME . "assets/avatar.jpg"; ?>");
	$.each(data, function (key, value) {
		$('#dvdepartment_name').html(value.department_name);
		$('#dvbunit').html(value.businessunit_name);
		$('#dvemail').html(value.emailaddress);
		$('#dvcontact').html(value.contactnumber);
//                                            
if (value.profileimg) {
$('#userimg').attr("src", "http://hrms.cegindia.com/public/uploads/profile/" + value.profileimg);
}
});
}
});
                                    } else {
                                        $('#dvdepartment_name').empty();
                                        $('#dvbunit').empty();
                                        $('#dvemail').empty();
                                        $('#dvcontact').empty();
                                        $('#userimg').attr("src", "<?= HOSTNAME . "assets/avatar.jpg"; ?>");
                                        getEmpDetailsother();
                                    }
                                }
								


    $(function () {
        $("#empid,#companynames").customselect();
    });
</script>
<style>
    .custom-select{width:auto!important;}
    .custom-select input{width:100%!important;}
	.chosen-container-single .chosen-single{display:none!important;}
</style>
</body>
<?php $this->load->view('admin/includes/footer'); ?>