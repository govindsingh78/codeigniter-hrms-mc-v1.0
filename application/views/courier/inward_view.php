<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$courLisArr = list_cour_categ_type();
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
								<div class="row">
            <div class="col-xs-8">
                <h4 class="page-title" >Inward Courier Details: </h4>
            </div>
        </div>

        <form id="form-filter">   
            <div class="row filter-row">

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label for="control-label">Business Unit : </label>
                        <select id="businessunit_name" class="form-control">
                            <option value="" selected="selected"> Select Business </option>
                            <?php
                            if ($form_businessunit):
                                foreach ($form_businessunit as $unitrow) {
                                    ?>
                                    <option value="<?= $unitrow->id; ?>"><?= $unitrow->unitname; ?></option>
                                    <?php
                                }
                            endif;
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label for="control-label">Company Name : </label>
                        <select id="company_name" class="form-control custom-select">
                            <option value="" selected="selected"> Select Company </option>
                            <?php
                            if ($companyname):
                                foreach ($companyname as $companyrow) {
                                    ?>
                                    <option value="<?= $companyrow->company_id; ?>"><?= $companyrow->company_name; ?></option>
                                    <?php
                                }
                            endif;
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label for="control-label">From Date: </label>
                        <div class="cal-icon"><input autocomplete="off" type="text" name="from_date" class="form-control" id="from_date"></div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label for="control-label">To Date: </label>
                        <div class="cal-icon"><input autocomplete="off" type="text"name="to_date" class="form-control" id="to_date"></div>
                    </div>
                </div>


                <div class="col-sm-2 col-xs-4">  
                    <button type="button" id="btn-filter" class="btn btn-success btn-block"> Filter </button>
                </div>  

                <div class="col-sm-2 col-xs-4"> 
                    <button type="button" id="btn-reset" class="btn btn-primary btn-block"> Reset </button>
                </div>
				
				<div class="col-sm-1 col-xs-4"> 
                    <a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#add_invard"><i class="fa fa-plus"></i>&ensp;Add</a>
                </div>
				
				
            </div>
        </form>  
							</div>
						</div>
					</div>          
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tabledata" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
												<th>BusinessUnit</th>
												<th>Letter Date</th>
												<th>From Whom Received </th>
												<th>Company Name</th>
												<th>Office Location</th>
												<th>Subject</th>
												<th>Agency Reference</th>
												<th>Officer To Whom File Passed</th>
												<th>Forward Emp1</th>
												<th>Forward Emp2</th>
												<th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>

                                        <tfoot>
                                            <tr>
												<th>Sr. No</th>
												<th>BusinessUnit</th>
												<th>Letter Date</th>
												<th>From Whom Received </th>
												<th>Company Name</th>
												<th>Office Location</th>
												<th>Subject</th>
												<th>Agency Reference</th>
												<th>Officer To Whom File Passed</th>
												<th>Forward Emp1</th>
												<th>Forward Emp2</th>
												<th>Action</th>                                          
											</tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
        
    </div>

<script>
var table;

    $(document).ready(function () {
        $("#from_date").datepicker();
        $("#to_date").datepicker();
        table = $('#tabledata').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

// Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('Courier_controller/inwardajax_list') ?>",
                "type": "POST",
                "data": function (data) {
                    data.businessunit_name = $('#businessunit_name').val();
                    data.company_name = $('#company_name').val();
                    data.from_date = $('#from_date').val();
                    data.to_date = $('#to_date').val();
                },
            },
            "dom": 'lBfrtip',
            "buttons": [
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
//Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });
</script>
<div class="modal custom-modal fade" id="add_invard" role="dialog">
<div class="modal-dialog modal-lg">
    
<button type="button" class="close" data-dismiss="modal">&times;</button>

<div class="modal-content">
    
<div class="modal-header">
<h4 class="modal-title">Add INWARD Courier </h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
    
<div class="modal-body">
<?= form_open(base_url('Courier_controller/saveinwardcourier'), array("id" => "form-filter", "method" => "post")); ?>  
 <div class="row">    
<div class="col-sm-4">    
<div class="form-group">
<label for="control-label"> Type <span class="req">*</span></label>
<select id="typer_categ_courier" name="typer_categ_courier" class="form-control" required>
                                <option value="" selected="selected">Select Type</option>
                                <?php
                                if ($courLisArr) {
                                    foreach ($courLisArr as $type) {
                                        ?>
                                        <option value="<?= $type->caur_fld_id; ?>"><?= $type->cour_cat_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
</div>
</div>   
    
<div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Date Of Letter <span class="req">*</span></label>
                            <div class="cal-icon"><input type="text" autocomplete="off" class="form-control" id="letter_date" name="letter_date" required></div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">From Whom Received</label>
                            <input type="text" autocomplete="off" class="form-control" id="from_received" name="from_received">
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Company Name<span class="req">*</span></label>
                            <select id="company_id" name="company_id" class="form-control custom-select" required >
                                <option value="" selected="selected">Select Company</option>
                                <option value="new"> Other </option>
                                <?php foreach ($companyname as $company) { ?>
                                    <option value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Office Location</label>
                            <select id="officelocation" name="receiverpincode1" class="form-control" >
                                <option value="">Select company first</option>
                            </select>
                        </div>
                    </div>

                    <div class=" datahidden col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Pin Code</label>
                            <input type="text" autocomplete="off" class="form-control" id="pincode" name="receiverpincode">
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12 datahidden">
                        <div class="form-group">
                            <label for="control-label">Address</label>
                            <textarea class="form-control" autocomplete="off" id="otheraddress" name="otheraddress"></textarea>
                        </div>
                    </div> 

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Subject</label>
                            <textarea class="form-control" autocomplete="off" id="subject" name="subject"></textarea>
                        </div>
                    </div> 

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Agency Reference</label>
                            <input type="text" autocomplete="off" class="form-control" id="agency_reference" name="agency_reference">
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Officer To Whom File Passed,& Dated Filed</label>
                            <input type="text" autocomplete="off" class="form-control" id="officer_filepass" name="officer_filepass">
                        </div>
                    </div>


                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">File Number</label>
                            <input type="text" readonly class="form-control" id="file_number" name="file_number">
                        </div>
                    </div>

                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Country </label>
                            <input type="text" autocomplete="off" class="form-control" name="country1" id="country" readonly>
                        </div>
                    </div>

                    <div class="datahidden col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label"> Country </label>
                            <select id="country1" name="country" class="form-control custom-select">
                                <option value=""> -- select country -- </option>
                                <?php
                                if ($country):
                                    foreach ($country as $val) {
                                        ?>
                                        <option value="<?= $val->country_id ?>"> <?= $val->country_name ?> </option>
                                        <?php
                                    }
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">State </label>
                            <input type="text" class="form-control" name="state1" id="state" readonly>
                        </div>
                    </div>

                    <div class=" datahidden col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label"> State </label>
                            <select id="state1" name="state" class="form-control">
                                <option value=""> -- select country first -- </option>
                            </select>
                        </div>
                    </div>


                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">City</label>
                            <input type="text" class="form-control" name="city1" id="city" readonly>
                        </div>
                    </div>

                    <div class="datahidden col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label"> City </label>
                            <select id="city1" name="city" class="form-control" >
                                <option value=""> -- select state first -- </option>
                            </select>
                        </div> 
                    </div>

                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Addresss 1</label>
                            <input type="text" class="form-control" name="add1" id="add1" readonly>
                        </div>
                    </div>

                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Addresss 2</label>
                            <input type="text" class="form-control" name="add2" id="add2" readonly>
                        </div>
                    </div>
                    <div class="showdata col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Addresss 3</label>
                            <input type="text" class="form-control" name="add3" id="add3" readonly>
                        </div>
                    </div>


                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Business Unit<span class="req">*</span></label>
                            <select id="businessunitnames" name="businessunitid" onchange="setfilenolast()" class="form-control custom-select" required style="">
                                <?php foreach ($form_businessunit as $dept) { ?>
                                    <option value="<?= $dept->id; ?>"><?= $dept->unitname; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Department<span class="req">*</span></label>
                            <select id="departmentnames" name="dept_id" class="form-control" required >
                                <option value="" selected="selected">Select Business Unit first</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Forward Employee Name 1<span class="req">*</span> </label>
                            <select id="forwardemp_id1" name="forwardemp_id1" class="form-control" required >
                                <option value="" selected="selected">Select Employee</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="control-label">Forward Employee Name 2 </label>
                            <select id="forwardemp_id2" name="forwardemp_id2" class="form-control">
                                <option value="" selected="selected">Select Employee</option>
                            </select>
                        </div> 
                    </div>

                </div> 

                <div class="m-t-20 text-center">
                    <input type="submit" class="btn btn-success" value="submit">    
                </div>
<?= form_close(); ?>

</div>
</div>    
</div>
</div>
<script>
    $('#letter_date').datepicker();
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#company_id').change(function () {
            if ($(this).val() == 'new')
            {
                $(".datahidden").css("display", "block");
                $(".showdata").css("display", "none");

            } else
            {
                $(".showdata").css("display", "block");
                $(".datahidden").css("display", "none");
            }

//alert($(this).val());
            var companyID = $(this).find("option:selected").val();
            if (companyID) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('Courier_controller/getcompanynames'); ?>',
                    data: {'company_id': companyID},
                    success: function (data) {
                        $('#officelocation').html('<option value="">Select pincode</option>');
                        var dataObj = jQuery.parseJSON(data);
                        console.log(dataObj);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                var option = $('<option />');
                                option.attr('value', this.id).text(this.pincode);
                                $('#officelocation').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#officelocation').on('change', function () {
            var pincode = $(this).val();
            if (pincode) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('Courier_controller/getDetails'); ?>',
                    data: {'company_id': pincode},
                    success: function (data) {
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                $('#city').val(this.city_name);
                                $('#country').val(this.country_name);
                                $('#state').val(this.state_name);
                                $('#add1').val(this.address1);
                                $('#add2').val(this.address2);
                                $('#add3').val(this.address3);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#businessunitnames').change(function () {
            var bname = $(this).find("option:selected").val();
            if (bname) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('Courier_controller/getdeptname'); ?>',
                    data: {'id': bname},
                    success: function (data) {
                        $('#departmentnames').html('<option value="">Select department</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                var option = $('<option />');
                                option.attr('value', this.id).text(this.deptname);
                                $('#departmentnames').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#departmentnames').on('change', function () {
            var deptID = $(this).val();
            if (deptID) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('Courier_controller/getempbydept'); ?>',
                    data: {'id': deptID},
                    success: function (data) {
                        $('#forwardemp_id1').html('<option value="">Select Employee Name 1 </option>');
                        $('#forwardemp_id2').html('<option value="">Select Employee Name 2</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                var option = $('<option />');
                                option.attr('value', this.user_id).text(this.userfullname);
                                $('#forwardemp_id1').append(option);
                            });
                            $(dataObj).each(function () {
                                var option = $('<option />');
                                option.attr('value', this.user_id).text(this.userfullname);
                                $('#forwardemp_id2').append(option);
                            });
                        }
                    },
                });
            }
        });
    });


    $(document).ready(function () {
        $('#empnames').on('change', function () {
            var empname = $(this).val();
            if (empname) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('Courier_controller/getempdetails'); ?>',
                    data: {'id': empname},
                    success: function (data) {
                        var dataObj = jQuery.parseJSON(data);
                        if (dataObj) {
                            $(dataObj).each(function () {
                                console.log(this);
                                $('#mobile').val(this.contactnumber);
                                $('#email').val(this.emailaddress);
                            });
                        }
                    },
                });
            }
        });
    });






    $('#country1').on('change', function () {
        var countryID = $(this).val();
        if (countryID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('Courier_controller/getstateBycountryID'); ?>',
                data: {'countryID': countryID},
                success: function (response) {
                    $('#state1').html('<option value="">Choose State</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
//console.log(val.state_name);	
                            $('#state1').append('<option value="' + val.state_id + '">' + val.state_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });
    $(document).on('change', '#state1', function () {
        var stateID = $(this).val();
        if (stateID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('Courier_controller/getcityBystateID'); ?>',
                data: {'stateID': stateID},
                success: function (response) {
                    $('#city1').html('<option value="">Choose City</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
//console.log(val.state_name);	
                            $('#city1').append('<option value="' + val.city_id + '">' + val.city_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });

//Last File No..
    function setfilenolast() {
        var bunit = $("#businessunitnames").val();
        if (bunit) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('Courier_controller/setfilenamelasta_ajax'); ?>',
                data: {'bunit': bunit},
                success: function (response) {
                    $("#file_number").val(response);
                },
            });
        } else {
            alert('Please Select Country');
        }
    }
</script>
<script>
    $(function () {
        $("#company_id,#businessunitnames,#country1,#company_name").customselect();
    });
</script>
</body>
<?php $this->load->view('admin/includes/footer'); ?>