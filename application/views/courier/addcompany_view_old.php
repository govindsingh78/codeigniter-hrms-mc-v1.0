<?PHP
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
?>

<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/jquery.dataTables.min.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/dataTables.colVis.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/buttons.dataTables.min.css' ?>" rel="stylesheet">

<div class="page-wrapper">
<div class="content container-fluid">

<?php if (validation_errors()): ?>
<div class="alert alert-danger alert-dismissable">
<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
<strong>Error ! </strong> <?= validation_errors(); ?>
</div>
<?php endif; ?>


<div class="row">
<div class="col-xs-8">
<h4 class="page-title" >Add Company Name: </h4>
</div>
<div class="col-xs-4">
<a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#add_company"><i class="fa fa-plus"></i>Add Company Info.</a>
</div>
</div>
    

<?= form_open(base_url('add_company'), array("method"=>"post")); ?>
<div class="row filter-row">
    
<div class="col-sm-4 col-xs-12">
<div class="form-group">
<label for="control-label"> Company Name </label>
<input type="text" autocomplete="off" name="CName" class="form-control">
</div>
</div>

<div class="col-sm-2 col-xs-4">
<br/>
<input type="submit" class="btn btn-success btn-block" value="ADD">   
</div>
    
</div>
<?= form_close(); ?>


<br/><br/>

<div class="card-box">
<div class="row">
<div class="col-xs-12">
<?php if ($this->session->flashdata('success_msg')): ?>
<div class="alert alert-info alert-dismissible fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong>Success ! </strong> <?php echo $this->session->flashdata('success_msg'); ?>
</div>
<?php endif; ?> 
<?php if ($this->session->flashdata('error_msg')): ?>
<div class="alert alert-danger alert-dismissible fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong>Error ! </strong> <?php echo $this->session->flashdata('error_msg'); ?>
</div>
<?php endif; ?>
</div>
</div>    
<div class="row">
<div class="col-md-12">
<div id="colvis"></div>
</div>
</div>
<div class="table-responsive">    
<table id="tabledata" class="display" cellspacing="0" width="100%">
<thead>
<tr>
<th>Sr. No</th>
<th>Company Name</th>
<th>Pincode</th>
<th>Country </th>
<th>State</th>
<th>City</th>
<th>Address1</th>
<th>Address2</th>
<th>Address3</th>
<th>Entry Date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
</tbody>
<tfoot>
<tr>
<th>Sr. No</th>
<th>Company Name</th>
<th>Pincode</th>
<th>Country </th>
<th>State</th>
<th>City</th>
<th>Address1</th>
<th>Address2</th>
<th>Address3</th>
<th>Entry Date</th>
<th>Action</th>
</tr>
</tfoot>
</table>
</div>   
</div>

</div>
</div>



<div id="add_company" class="modal custom-modal fade" role="dialog">
<div class="modal-dialog">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<div class="modal-content modal-lg">
<div class="modal-header">
<h4 class="modal-title">Add Company Info:</h4>
</div>

<div class="modal-body">
    
<?= form_open(base_url('back_end/Courier_controller/savecompanyinfo'), array("method"=>"post")); ?>
<div class="row">
    
<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Company Name</label>
<select id="company_id" name="company_id" class="form-control custom-select" required>
<option value="" selected="selected">Select Company</option>
<?php foreach ($companyname as $company) { ?>
<option value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 1 </label>
<input type="text" autocomplete="off" class="form-control"  id="address1" name="address1" required>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 2</label>
<input type="text" autocomplete="off" class="form-control" id="address2" name="address2">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 3</label>
<input type="text" autocomplete="off" class="form-control" id="address3" name="address3">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Country</label>
<select id="country" name="country" class="form-control custom-select" required>
<option value=""> -- select country -- </option>
<?php
if ($country):
foreach ($country as $val) {
?>
<option value="<?= $val->country_id ?>"> <?= $val->country_name ?> </option>
<?php
}
endif;
?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">State</label>
<select id="state" name="state" class="form-control" required>
<option value=""> -- select country first -- </option>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">City</label>
<select id="city" name="city" class="form-control" required>
<option value=""> -- select state first -- </option>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Pincode</label>
<input type="text" autocomplete="off" class="form-control" id="pincode" name="pincode" required>
</div>
</div>
</div>

<div class="m-t-20 text-center">
<input type="submit" class="btn btn-success" value="submit">
</div>

<?= form_close(); ?>

</div>   
</div>
</div>
</div>
    
<?PHP $this->load->view("back_end/includes/footer"); ?>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.colVis.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.buttons.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.flash.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/pdfmake.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jszip.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/vfs_fonts.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.html5.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.print.min.js'; ?>"></script>

<script type="text/javascript">

//$('#company_id').chosen().change(function () {});
$('#country').on('change', function () {
var countryID = $(this).val();
if (countryID) {
$.ajax({
type: 'POST',
url: '<?php echo base_url('back_end/Courier_controller/getstateBycountryID'); ?>',
data: {'countryID': countryID},
success: function (response) {
$('#state').html('<option value="">Choose State</option>');
var data = jQuery.parseJSON(response);
if (data) {
	$.each(data, function (index, val) {
		//console.log(val.state_name);	
		$('#state').append('<option value="' + val.state_id + '">' + val.state_name + '</option>');
	});
}
},
});
} else {
alert('Please Select Country');
}
});
$(document).on('change', '#state', function () {
var stateID = $(this).val();
if (stateID) {
$.ajax({
type: 'POST',
url: '<?php echo base_url('back_end/Courier_controller/getcityBystateID'); ?>',
data: {'stateID': stateID},
success: function (response) {
$('#city').html('<option value="">Choose City</option>');
var data = jQuery.parseJSON(response);
if (data) {
	$.each(data, function (index, val) {
		//console.log(val.state_name);	
		$('#city').append('<option value="' + val.city_id + '">' + val.city_name + '</option>');
	});
}
},
});
} else {
alert('Please Select Country');
}
});


var table;

$(document).ready(function () {
table = $('#tabledata').DataTable({
"processing": true,
"serverSide": true,
"order": [],
"ajax": {
"url": "<?php echo site_url('back_end/Courier_controller/company_ajax_list') ?>",
"type": "POST",
},
"dom": 'lBfrtip',
"buttons": [
{
extend: 'collection',
text: 'Export',
buttons: [
	'copy',
	'excel',
	'csv',
	'pdf',
	'print'
]
}
],
//Set column definition initialisation properties.
"columnDefs": [
{
"targets": [0], //first column / numbering column
"orderable": false, //set not orderable
},
],
"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
});
var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
$('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
$('#btn-filter').click(function () { //button filter event click
table.ajax.reload();  //just reload table
});
$('#btn-reset').click(function () { //button reset event click
$('#form-filter')[0].reset();
table.ajax.reload();  //just reload table
});
});
</script>
<script type="text/javascript">
    $(function () {
        $("#company_id,#country").customselect();
    });
</script>
<style>
    .custom-select{width:auto!important;}
    .custom-select input{width:100%!important;}
	
</style>
</body>	
</html>
