<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$courLisArr = list_cour_categ_type();
$GetAllEmplArr = get_all_Activeempls();
$courLisArr = list_cour_categ_type();
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				
				<?= form_open(base_url('Courier_controller/editcompany/' . $projId), array("method" => "post")); ?>
				<div class="row clearfix">
					<div class="col-lg-12">
					<?php if (validation_errors()): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= validation_errors(); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-info alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success ! </strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?> 
                <?php if ($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error ! </strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
                        <div class="card">
							<div class="body">
							<div class="row">
<div class="col-xs-10">
<h4 class="page-title">Edit Company Info:</h4>
</div>
</div>
 
<!--<div class="row filter-row">-->

<div class="row">
<div class="col-lg-3 col-md-6">
	<b>Business Unit :</b>
	<?php //echo "<pre>"; print_r($companyname); die;?>
	<div class="input-group mb-3">
		<select id="company_id" name="company_id" class="form-control" required>
                        <option value="" selected="selected">Company Name</option>
                        <?php foreach ($companyname as $company) { ?>
                            <option <?= ($new[0]->company_id == $company->company_id) ? 'selected' : ''; ?> value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
                        <?php } ?>
                    </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Addresss 1 :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $new[0]->address1; ?>" class="form-control"  id="address1" name="address1" required>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Addresss 2 :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $new[0]->address2; ?>" class="form-control" id="address2" name="address2">
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Addresss 3 :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?php $new[0]->address3; ?>" class="form-control" id="mobile" readonly>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Country :</b>
	<div class="input-group mb-3">
		<select id="country" name="country" class="form-control custom-select" required>
                        <option value=""> -- select country -- </option>
                        <?php
                        if ($country):
                            foreach ($country as $val) {
                                ?>
                                <option <?= ($new[0]->country == $val->country_id) ? 'selected' : ''; ?> value="<?= $val->country_id ?>"> <?= $val->country_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>State :</b>
	<div class="input-group mb-3">
		<select id="state" name="state" class="form-control" required>
                        <option value=""> -- select state -- </option>
                        <?php
                        if ($state):
                            foreach ($state as $val) {
                                ?>
                                <option <?= ($new[0]->state == $val->state_id) ? 'selected' : ''; ?> value="<?= $val->state_id ?>"> <?= $val->state_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>City :</b>
	<div class="input-group mb-3">
		<select id="city" name="city" class="form-control" required>
                        <option value=""> -- select city -- </option>
                        <?php
                        if ($city):
                            foreach ($city as $val) {
                                ?>
                                <option <?= ($new[0]->city == $val->city_id) ? 'selected' : ''; ?> value="<?= $val->city_id ?>"> <?= $val->city_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
	</div>
</div>

<div class="col-lg-3 col-md-6">
	<b>Pincode :</b>
	<div class="input-group mb-3">
		<input type="text" value="<?= $new[0]->pincode; ?>" class="form-control" id="pincode" name="pincode" required>
	</div>
</div>

<div class="col-lg-2 col-md-6">
	<div class="input-group mb-3">
		<input type="submit" class="btn btn-success btn-block" value="UPDATE">
	</div>
</div>



							</div>
						</div>
					</div>          
				</div>
				</div>

				
            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
        
    </div>





<script type="text/javascript">

//$('#company_id').chosen().change(function () {});


    $('#country').on('change', function () {
        var countryID = $(this).val();
        if (countryID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('back_end/Courier_controller/getstateBycountryID'); ?>',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'countryID': countryID},
                success: function (response) {
                    $('#state').html('<option value="">Choose State</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
                            //console.log(val.state_name);	
                            $('#state').append('<option value="' + val.state_id + '">' + val.state_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });
    $(document).on('change', '#state', function () {
        var stateID = $(this).val();
        if (stateID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('back_end/Courier_controller/getcityBystateID'); ?>',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'stateID': stateID},
                success: function (response) {
                    $('#city').html('<option value="">Choose City</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
                            //console.log(val.state_name);	
                            $('#city').append('<option value="' + val.city_id + '">' + val.city_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });


</script>
<script>
    $(function () {
        $("#company_id,#country").customselect();
    });
</script>
<style>
    .custom-select{width:auto!important;}
    .custom-select input{width:100%!important;}

</style>
</body>
<?php $this->load->view('admin/includes/footer'); ?>