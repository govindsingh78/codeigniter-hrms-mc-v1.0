<?PHP
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
?>
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/jquery.dataTables.min.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/dataTables.colVis.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/buttons.dataTables.min.css' ?>" rel="stylesheet">

<div class="page-wrapper">
    <div class="content container-fluid">	
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Error ! </strong> <?= validation_errors(); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-info alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success ! </strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?> 
                <?php if ($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error ! </strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title" >Edit Company Info: </h4>
            </div>
        </div>    

        <?= form_open(base_url('back_end/Courier_controller/editcompany/' . $projId), array("method" => "post")); ?>
        <div class="row filter-row">

            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Company Name</label>
                    <select id="company_id" name="company_id" class="form-control custom-select" required>
                        <option value="" selected="selected">Select Company</option>
                        <?php foreach ($companyname as $company) { ?>
                            <option <?= ($new[0]->company_id == $company->company_id) ? 'selected' : ''; ?> value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Addresss 1 </label>
                    <input type="text" value="<?= $new[0]->address1; ?>" class="form-control"  id="address1" name="address1" required>
                </div>
            </div>


            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Addresss 2</label>
                    <input type="text" value="<?= $new[0]->address2; ?>" class="form-control" id="address2" name="address2">
                </div>
            </div>


            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Addresss 3</label>
                    <input type="text" value="<?= $new[0]->address3; ?>" class="form-control" id="address3" name="address3">
                </div>
            </div>

            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Country</label>
                    <select id="country" name="country" class="form-control custom-select" required>
                        <option value=""> -- select country -- </option>
                        <?php
                        if ($country):
                            foreach ($country as $val) {
                                ?>
                                <option <?= ($new[0]->country == $val->country_id) ? 'selected' : ''; ?> value="<?= $val->country_id ?>"> <?= $val->country_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
                </div>
            </div>


            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">State</label>
                    <select id="state" name="state" class="form-control" required>
                        <option value=""> -- select state -- </option>
                        <?php
                        if ($state):
                            foreach ($state as $val) {
                                ?>
                                <option <?= ($new[0]->state == $val->state_id) ? 'selected' : ''; ?> value="<?= $val->state_id ?>"> <?= $val->state_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">City</label>
                    <select id="city" name="city" class="form-control" required>
                        <option value=""> -- select city -- </option>
                        <?php
                        if ($city):
                            foreach ($city as $val) {
                                ?>
                                <option <?= ($new[0]->city == $val->city_id) ? 'selected' : ''; ?> value="<?= $val->city_id ?>"> <?= $val->city_name ?> </option>
                                <?php
                            }
                        endif;
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-3 col-xs-12">
                <div class="form-group">
                    <label for="control-label">Pincode</label>
                    <input type="text" value="<?= $new[0]->pincode; ?>" class="form-control" id="pincode" name="pincode" required>
                </div>
            </div>

            <div class="col-sm-2 col-xs-12">
                <input type="submit" class="btn btn-success btn-block" value="UPDATE">
            </div>

        </div>     
        <?= form_close(); ?>


    </div>
</div>

<?PHP $this->load->view("back_end/includes/footer"); ?>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.colVis.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.buttons.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.flash.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/pdfmake.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jszip.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/vfs_fonts.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.html5.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.print.min.js'; ?>"></script>

<script type="text/javascript">

//$('#company_id').chosen().change(function () {});


    $('#country').on('change', function () {
        var countryID = $(this).val();
        if (countryID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('back_end/Courier_controller/getstateBycountryID'); ?>',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'countryID': countryID},
                success: function (response) {
                    $('#state').html('<option value="">Choose State</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
                            //console.log(val.state_name);	
                            $('#state').append('<option value="' + val.state_id + '">' + val.state_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });
    $(document).on('change', '#state', function () {
        var stateID = $(this).val();
        if (stateID) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('back_end/Courier_controller/getcityBystateID'); ?>',
                data: {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>', 'stateID': stateID},
                success: function (response) {
                    $('#city').html('<option value="">Choose City</option>');
                    var data = jQuery.parseJSON(response);
                    if (data) {
                        $.each(data, function (index, val) {
                            //console.log(val.state_name);	
                            $('#city').append('<option value="' + val.city_id + '">' + val.city_name + '</option>');
                        });
                    }
                },
            });
        } else {
            alert('Please Select Country');
        }
    });


</script>
<script>
    $(function () {
        $("#company_id,#country").customselect();
    });
</script>
<style>
    .custom-select{width:auto!important;}
    .custom-select input{width:100%!important;}

</style>
</body>	
</html>

