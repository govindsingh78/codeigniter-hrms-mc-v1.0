<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
								<div class="row">
									<div class="col-xs-10">
									<h4 class="page-title" >Add Company Name:</h4>
									</div>

									</div>

									<?= form_open(base_url('add_company'), array("method"=>"post")); ?>   
									<div class="row filter-row">
									<div class="col-lg-3 col-md-6">
										<b>Company Name :</b>
										<div class="input-group mb-3">
											<input type="text" autocomplete="off" name="CName" class="form-control">
										</div>
									</div>

									<div class="col-sm-2 col-xs-4">  
									<b style="color:#fff">.</b>
										<div class="input-group mb-3">
											<input type="submit" class="btn btn-success btn-block" value="ADD">
										</div>
									
									</div>  

									<div class="col-sm-2 col-xs-4"> 
									<b style="color:#fff">.</b>
										<div class="input-group mb-3">
											<button type="button" id="btn-reset" class="btn btn-primary btn-block"> Reset </button>
										</div>
									
									</div>
									<div class="col-sm-2 col-xs-2"> 
									<b style="color:#fff">.</b>
									<div class="input-group mb-3">
											<a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#add_company"><i class="fa fa-plus"></i>&nbsp;Add Company Info.</a>
										</div>
									
									</div>
									</div>
									<?= form_close(); ?> 
							</div>
						</div>
					</div>          
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tabledata" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
												<th>Company Name</th>
												<th>Pincode</th>
												<th>Country </th>
												<th>State</th>
												<th>City</th>
												<th>Address1</th>
												<th>Address2</th>
												<th>Address3</th>
												<th>Entry Date</th>
												<th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php /*
											// echo "<pre>"; print_r($myTeamPresentDetailsArr);
                                            if ($myTeamPresentDetailsArr) {
                                                foreach ($myTeamPresentDetailsArr as $kEy => $dataRow) {
													$Dispdate = date("d-m-Y");
													// echo $Dispdate; die;
													$machine_userid = $this->session->userdata('loginid');
													$punchRec = getSingleDateAttByUserID($Dispdate, $dataRow->machine_id);
                                                    $AllPunchDetail = GetAllPunchDetail($Dispdate, $dataRow->machine_id);
                                                    ?>
                                                    <tr>
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->userfullname) ? $dataRow->prefix_name . " " . $dataRow->userfullname : ""; ?></td>
                                                        <td><?= ($dataRow->employeeId) ? $dataRow->employeeId : ""; ?></td>
                                                        <td><?= ($dataRow->contactnumber) ? $dataRow->contactnumber : ""; ?></td>
                                                        <td><?= ($dataRow->position_name) ? $dataRow->position_name : ""; ?></td>
                                                        <td><?= (@$punchRec['intime']) ? @$punchRec['intime'] : ""; ?></td>
                                                        <td><?= (@$punchRec['outtime']) ? @$punchRec['outtime'] : ""; ?></td>
														<td>
                                                                <?php if ($AllPunchDetail['intime']): ?>
                                                                    <ul id="myUL">
                                                                        <li><span class="caret">Details</span>
                                                                            <ul class="nested">
                                                                                <table class="table" style="color:black">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Punch-In</th>
                                                                                            <th>Punch-Out</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if ($AllPunchDetail['intime']):
                                                                                            foreach ($AllPunchDetail['intime'] as $keyAtt => $recAtt) { ?>
                                                                                                <tr>
                                                                                                    <td><?= ($recAtt) ? $recAtt : ""; ?></td>
                                                                                                    <td><?= (@$AllPunchDetail['outtime'][$keyAtt]) ? @$AllPunchDetail['outtime'][$keyAtt] : "<span style='color:red'>In Office</span>"; ?></td>
                                                                                                </tr>
                                                                                            <?php } endif;
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="8"> Record Not Found. </td>
                                                </tr>
                                            <?php } */?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr. No</th>
												<th>Company Name</th>
												<th>Pincode</th>
												<th>Country </th>
												<th>State</th>
												<th>City</th>
												<th>Address1</th>
												<th>Address2</th>
												<th>Address3</th>
												<th>Entry Date</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
        
    </div>
<!--================= Modal start ====================-->

<!-- Modal -->
<div id="add_company" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Add Company Info: </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		
      </div>
      <div class="modal-body">
		<?= form_open(base_url('back_end/Courier_controller/savecompanyinfo'), array("method"=>"post")); ?>
<div class="row">
    
<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Company Name</label>
<select id="company_id" name="company_id" class="form-control custom-select" required>
<option value="" selected="selected">Select Company</option>
<?php foreach ($companyname as $company) { ?>
<option value="<?= $company->company_id; ?>"><?= $company->company_name; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 1 </label>
<input type="text" autocomplete="off" class="form-control"  id="address1" name="address1" required>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 2</label>
<input type="text" autocomplete="off" class="form-control" id="address2" name="address2">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Addresss 3</label>
<input type="text" autocomplete="off" class="form-control" id="address3" name="address3">
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Country</label>
<select id="country" name="country" class="form-control custom-select" required>
<option value=""> -- select country -- </option>
<?php
if ($country):
foreach ($country as $val) {
?>
<option value="<?= $val->country_id ?>"> <?= $val->country_name ?> </option>
<?php
}
endif;
?>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">State</label>
<select id="state" name="state" class="form-control" required>
<option value=""> -- select country first -- </option>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">City</label>
<select id="city" name="city" class="form-control" required>
<option value=""> -- select state first -- </option>
</select>
</div>
</div>

<div class="col-sm-3 col-xs-12">
<div class="form-group">
<label for="control-label">Pincode</label>
<input type="text" autocomplete="off" class="form-control" id="pincode" name="pincode" required>
</div>
</div>
</div>

<div class="m-t-20 text-center">
<input type="submit" class="btn btn-success" value="submit">
</div>
   

<?= form_close(); ?>
      </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>
</div>

<!--================= Modal End ====================-->
<script>
var table;

$(document).ready(function () {
table = $('#tabledata').DataTable({
"processing": true,
"serverSide": true,
"order": [],
"ajax": {
"url": "<?php echo base_url('Courier_Controller/company_ajax_list') ?>",
"type": "POST",
},
"dom": 'lBfrtip',
"buttons": [
{
extend: 'collection',
text: 'Export',
buttons: [
	'copy',
	'excel',
	'csv',
	'pdf',
	'print'
]
}
],
//Set column definition initialisation properties.
"columnDefs": [
{
"targets": [0], //first column / numbering column
"orderable": false, //set not orderable
},
],
"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
});
// var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
// $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
$('#btn-filter').click(function () { //button filter event click
table.ajax.reload();  //just reload table
});
$('#btn-reset').click(function () { //button reset event click
$('#form-filter')[0].reset();
table.ajax.reload();  //just reload table
});
});
</script>
</body>
<?php $this->load->view('admin/includes/footer'); ?>