<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$statusArr = TourStatusRec();
?>

<body class="theme-cyan">
    <div id="pageloader">
        <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
    </div>
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <?php if ($this->session->flashdata('success_msg')): ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong>Success ! </strong> <?= $this->session->flashdata('success_msg'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($this->session->flashdata('error_msg')): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
                                    </div>
                                <?php endif; ?> 
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Apply Tour
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="body">
                                        <form action="<?= thisurl(); ?>" id="basic-form" method="post">
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_project_id"><?= form_error('project_id'); ?></span>
                                                        <label class="text-muted"> Project : <span id="reqd"> * </span></label>
                                                        <select onclick="rmvalidationerror(this.id)" class="form-control show-tick ms select2" name="project_id" id="project_id" data-placeholder="Select" >
                                                            <option value=""> -- Select -- </option>
                                                            <?php
                                                            if ($ProjectListRecArr) {
                                                                foreach ($ProjectListRecArr as $keyy => $recD) {
                                                                    ?>
                                                                    <option <?= set_select('project_id', $recD->id, (!empty($data) && $data == $recD->id ? TRUE : FALSE)); ?> value="<?= $recD->id; ?>"><?= $recD->project_name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_tour_type"><?= form_error('tour_type'); ?></span>
                                                        <label class="text-muted">Tour Type : <span id="reqd"> * </span></label>
                                                        <select onchange="SetExpectedoutTime()" class="form-control" onclick="rmvalidationerror(this.id)" name="tour_type" onchange="set_tourtype()" id="tour_type" data-placeholder="Select" >
                                                            <option <?= set_select('tour_type', "", (!empty($data) && $data == "" ? TRUE : FALSE)); ?> value=""> --Select-- </option>
                                                            <option <?= set_select('tour_type', "1", (!empty($data) && $data == "1" ? TRUE : FALSE)); ?>  value="1"> General </option>
                                                            <option <?= set_select('tour_type', "2", (!empty($data) && $data == "2" ? TRUE : FALSE)); ?> value="2"> Short Tour </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6" id="exptout_time" style="<?= (set_value('tour_type') != '2') ? 'display:none' : ''; ?>">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_expectedout_time"><?= form_error('expectedout_time'); ?></span>
                                                        <label class="text-muted">Expected out Time : <span id="reqd"> * </span></label>
                                                        <input type="time" onclick="rmvalidationerror(this.id)" name="expectedout_time" id="expectedout_time" autocomplete="off" value="<?= set_value("expectedout_time"); ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_start_date"><?= form_error('start_date'); ?></span>
                                                        <label class="text-muted">Start Date : <span id="reqd"> * </span></label>
                                                        <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                            <input onclick="rmvalidationerror(this.id)" type="text" class="form-control" name="start_date" autocomplete="off" id="start_date" value="<?= set_value("start_date"); ?>">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_end_date"><?= form_error('end_date'); ?></span>
                                                        <label class="text-muted">End Date : <span id="reqd"> * </span></label>
                                                        <div class="input-group date" data-date-autoclose="true" data-provide="datepicker" >
                                                            <input type="text" onclick="rmvalidationerror(this.id)" class="form-control" name="end_date" autocomplete="off" id="end_date" value="<?= set_value("end_date"); ?>">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_tour_location"><?= form_error('tour_location'); ?></span>
                                                        <label class="text-muted">Tour/Project Location : <span id="reqd"> * </span></label>
                                                        <input type="text" onclick="rmvalidationerror(this.id)" name="tour_location" id="tour_location" autocomplete="off" value="<?= set_value("tour_location"); ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group">
                                                        <span id="reqd" class="error_description"><?= form_error('description'); ?></span>
                                                        <label class="text-muted">Tour Description : <span id="reqd"> * </span></label>
                                                        <textarea onclick="rmvalidationerror(this.id)" class="form-control" maxlength="200" name="description" autocomplete="off" id="description"><?= set_value("description"); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-2">
                                                        <br><br>
                                                        <input type="submit" class="btn btn-primary">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Project </th>
                                                <th>Tour Type</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Location</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($AppliedTourRecArr) {
                                                foreach ($AppliedTourRecArr as $kEy => $dataRow) {
                                                    ?>
                                                    <tr style="<?= ($dataRow->approved_bylmanager == 2) ? "color:#bd2727" : ""; ?>">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->project_name) ? $dataRow->project_name : ""; ?></td>
                                                        <td style="<?= ($dataRow->tour_type == "2") ? "color:#17a2b8" : ""; ?>">
                                                            <?php
                                                            echo ($dataRow->tour_type == "1" OR $dataRow->tour_type == "") ? "General" : "Short";
                                                            echo ($dataRow->tour_type == "2") ? " (" . $dataRow->expected_out_time . ")" : "";
                                                            ?>
                                                        </td>
                                                        <td><?= ($dataRow->start_date) ? date("d-m-Y", strtotime($dataRow->start_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->end_date) ? date("d-m-Y", strtotime($dataRow->end_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->tour_location) ? $dataRow->tour_location : ""; ?></td>
                                                        <td><?= ($dataRow->description) ? $dataRow->description : ""; ?></td>
                                                        <td><?= $statusArr[$dataRow->approved_bypmanager]; ?></td>
                                                        <td>
                                                            <?php if ($dataRow->approved_bypmanager == "0") { ?>
                                                                <a class="btn" onclick="seteditupdatedata('<?= $dataRow->id; ?>')" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                                                                <a title="Delete/Cancel" class="btn" onclick="TourDeleteAction('<?= $dataRow->id; ?>')" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="9"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Project </th>
                                                <th>Tour Type</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Location</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="container">
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5> Tour View/Edit/Update : </h5>
                                <button type="button" class="close" style="color:red" data-dismiss="modal"> X </button>
                            </div>
                            <div class="modal-body">
                                <?= form_open(base_url('toureditupdate'), array("name" => "frmmDate", "method" => "post", "id" => "frmmDate")) ?>
                                <div class="row">
                                    <div class="form-group required col-md-6">
                                        <label class="email">Project : </label>
                                        <select required name="updtour_projectid" class="form-control" id="updtour_projectid">
                                            <option value="">-Select Project-</option> 
                                            <?php
                                            if ($ProjectListRecArr) {
                                                foreach ($ProjectListRecArr as $keyy => $recD) {
                                                    ?>
                                                    <option value="<?= $recD->id; ?>"><?= $recD->project_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group required col-md-6">
                                        <label class="email">Tour Type : </label>
                                        <select class="form-control" disabled="disabled" name="updtour_type" id="updtour_type" >
                                            <option value=""> --Select-- </option>
                                            <option  value="1"> General </option>
                                            <option value="2"> Short Tour </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group required col-md-6">
                                        <label class="email">Start Date : </label>
                                        <input type="date" disabled="disabled" class="form-control" name="updtour_startdate" id="tour_startdate">
                                    </div>
                                    <div class="form-group required col-md-6">
                                        <label class="email">End Date : </label>
                                        <input type="date" disabled="disabled" class="form-control" name="updtour_enddate" id="tour_enddate">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group required col-md-6">
                                        <label class="email">Tour/Project Location : </label>
                                        <input type="text" required class="form-control" name="updtour_location" id="updtour_location">
                                    </div>
                                    <div class="form-group required col-md-6">
                                        <label class="email">Tour Description : </label>
                                        <textarea class="form-control" required maxlength="200" name="upddescription" autocomplete="off" id="upddescription"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group required col-md-6">
                                        <input type="hidden" id="updtour_id" name="tour_id">
                                        <br/>
                                        <input type="submit" class="btn btn-info" value="Update">
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
                                                                    $(document).ready(function () {
                                                                        $("#basic-form,#frmmDate").on("submit", function () {
                                                                            $("#pageloader").fadeIn();
                                                                        });
                                                                    });
        </script>
        <style>
            #pageloader{
                background: rgba( 255, 255, 255, 0.8 );
                display: none;
                height: 100%;
                position: fixed;
                width: 100%;
                z-index: 9999;
            }
            #pageloader img{
                left: 50%;
                margin-left: -32px;
                margin-top: -32px;
                position: absolute;
                top: 50%;
            }
        </style>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

<script>
    function set_tourtype() {
        var tourcateg = $("#tour_type").val();
        if (tourcateg == "2") {
            var start_date = $("#start_date").val();
            if (start_date) {
                $("#end_date").val(start_date);
                $("#end_date").attr("readonly", "readonly");
            }
        }
        if (tourcateg == "1") {
            $("#end_date").removeAttr("readonly");
        }
    }
    //Validation Error Removed..
    function rmvalidationerror(returnarrg) {
        $('.error_' + returnarrg).html("");
    }
    function SetExpectedoutTime() {
        var tourtype = $("#tour_type").val();
        $("#exptout_time").hide();
        if (tourtype == "2") {
            $("#exptout_time").show();
        } else {
            $("#exptout_time").hide();
        }
    }

    function TourDeleteAction(tourid) {
        if (confirm("Are You sure Delete this ? ")) {
            window.location = "<?= base_url('deletetourbytid/'); ?>" + tourid;
        }
    }
    //Set Edit Val..
    function seteditupdatedata(tourid) {
        $.ajax({
            url: '<?= base_url("ajax_getsingletourby_tid"); ?>',
            data: ({tourtblid: tourid}),
            type: 'post',
            success: function (data) {
                var arr = $.parseJSON(data);
                $("#updtour_projectid").val(arr.project_id);
                $("#updtour_type").val(arr.tour_type);
                $("#tour_startdate").val(arr.start_date);
                $("#tour_enddate").val(arr.end_date);
                $("#updtour_location").val(arr.tour_location);
                $("#upddescription").val(arr.description);
                $("#updtour_id").val(tourid);
            }
        });
    }
</script>