<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">

                                <?php if ($this->session->flashdata('success_msg')): ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong>Success ! </strong> <?= $this->session->flashdata('success_msg'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($this->session->flashdata('error_msg')): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong>Error ! </strong> <?= $this->session->flashdata('error_msg'); ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (validation_errors()): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong>Error ! </strong> <?= validation_errors(); ?>
                                    </div>
                                <?php endif; ?>

                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Apply Leave <i style="font-size:19px" class="fa fa-angle-down"></i>
                                        </button>
                                        &nbsp;&nbsp; Reporting Manager : <span style="color:green"><?= ($loginUserRecd->reporting_manager_name) ? $loginUserRecd->reporting_manager_name : ""; ?></span>
                                        &nbsp;&nbsp; 
                                        <a href="<?= base_url("myleave"); ?>">
                                            <button class="btn btn-primary btn-xs"> <i class="fa fa-refresh fa-spin" style="font-size:24px"></i> &nbsp;&nbsp; Replan Leave</button>
                                        </a>  
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="header">
                                        <span style="color:red" id="sec-error-div"> </span>
                                    </div>

                                    <div class="body">
                                        <form action="#" id="basic-form" id="leaveform" method="post">
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-3">
                                                        <p>Available Leaves : </p>
                                                        <input type="text" class="form-control" name="avl_leave" id="avl_leave" value="<?= ($AvlLeaveRecd > 0) ? $AvlLeaveRecd : ""; ?>" readonly="readonly">    
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-3">
                                                        <p>Leave Type : <span id="reqd"> * </span></p>
                                                        <select class="form-control" name="leave_type" onchange="setcond_leavetype()" id="leave_type" data-placeholder="Select">
                                                            <option value=""> -- Select -- </option>
                                                            <option value="1"> PL </option>
                                                            <option value="2"> RH </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-3">
                                                        <p>Leave For : <span id="reqd"> * </span></p>
                                                        <select class="form-control" name="leave_for" id="leave_for" onchange="setcond_leavefor()" data-placeholder="Select">
                                                            <option value=""> -- Select-- </option>
                                                            <option value="1"> Full Day </option>
                                                            <option value="2"> Half Day </option>
                                                            <option value="3"> Short Leave </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-6" id="halfdaycateg" style="display:none">
                                                    <div class="mb-3">
                                                        <p>Select Half Day Leave : <span id="reqd"> * </span></p>
                                                        <select class="form-control" name="half_day_type" id="half_day_type" onchange="hdayprelunchpost()" data-placeholder="Select">
                                                            <option value=""> -- Select-- </option>
                                                            <option value="1"> Pre Lunch </option>
                                                            <option value="2"> Post Lunch </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6" id="sortleavecateg" style="display:none">
                                                    <div class="mb-3">
                                                        <p>Expected Time : <span id="reqd"> * </span></p>
                                                        <input type="time" class="form-control" name="expected_time" id="expected_time" >    
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <p>From Date : <span id="reqd"> * </span></p>
                                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                        <input type="text" class="form-control" name="start_date" autocomplete="off" id="start_date" onchange="setcond_startdate()" value="<?= set_value("start_date"); ?>">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <p>To Date : <span id="reqd"> * </span></p>
                                                    <div class="input-group date" data-date-autoclose="true" data-provide="datepicker" >
                                                        <input type="text" class="form-control" name="end_date" autocomplete="off" id="end_date" onchange="setcond_enddate()" value="<?= set_value("end_date"); ?>">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="mb-3">
                                                        <p>Reason : <span id="reqd"> * </span></p>
                                                        <textarea maxlength="200" class="form-control" name="reason" id="reason"><?= set_value("reason"); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-3">
                                                        <p>Count Leave : <span id="reqd"> * </span></p>
                                                        <input type="text" class="form-control" name="count_leave" id="count_leave" readonly="readonly">    
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-2">
                                                        <p>&nbsp;</p>
                                                        <input type="button" value="Apply" onclick="gotosubmit()" class="btn btn-round btn-primary">
                                                    </div>
                                                    <input type="hidden" name="reporting_manager" id="reporting_manager" value="<?= ($loginUserRecd->reporting_manager) ? $loginUserRecd->reporting_manager : ""; ?>">
                                                </div>
                                                <span class="js-sweetalert" style="display:none" data-type="with-title"></span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Leave Type </th>
                                                <th>Reason</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Days</th>
                                                <th>Applied On</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if ($myallAppliedLeaveRecArr) {
                                                foreach ($myallAppliedLeaveRecArr as $kEy => $dataRow) {
                                                    ?>
                                                    <tr style="<?= ($dataRow->leavetypeid == 5) ? "color:#bd2727" : ""; ?>">
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->leavetype_name) ? $dataRow->leavetype_name : ""; ?></td>
                                                        <td title="<?= @$dataRow->reason; ?>">
                                                            <?php
                                                            $string1 = ($dataRow->reason) ? $dataRow->reason : "";
                                                            $string = substr($string1, 0, 57) . '...';
                                                            echo (strlen($dataRow->reason) > 56) ? $string : $dataRow->reason;
                                                            ?>
                                                        </td>
                                                        <td><?= ($dataRow->from_date) ? date("d-m-Y", strtotime($dataRow->from_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->to_date) ? date("d-m-Y", strtotime($dataRow->to_date)) : ""; ?></td>
                                                        <td><?= ($dataRow->appliedleavescount) ? $dataRow->appliedleavescount : ""; ?></td>
                                                        <td><?= ($dataRow->createddate) ? date("d-m-Y", strtotime($dataRow->createddate)) : ""; ?></td>
                                                        <td><?= ($dataRow->leavestatus) ? $dataRow->leavestatus : ""; ?></td>
                                                        <td>
                                                            <?php if ($dataRow->leavestatus == "Pending for approval") { ?>
                                                                <a class="btn" onclick="seteditupdatedata('<?= $dataRow->leave_req_id; ?>')" data-toggle="modal" data-target="#myModal" href="javascript:void(0);"><i class="fa fa-edit"></i></a>
                                                                <a title="Delete/Cancel" class="btn" onclick="LeaveDeleteAction('<?= $dataRow->leave_req_id; ?>')" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="9"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Leave Type </th>
                                                <th>Reason</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Days</th>
                                                <th>Applied On</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
        <script src="<?= FRONTASSETS; ?>js/pages/ui/dialogs.js"></script>
    </div>


    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5> Leave View/Edit/Update : </h5>
                        <button type="button" class="close" style="color:red" data-dismiss="modal"> X </button>
                    </div>
                    <div class="modal-body">
                        <?= form_open(base_url('leave_editupdate'), array("name" => "frmmDate", "method" => "post", "id" => "frmmDate")) ?>
                        <div class="row">
                            <div class="form-group required col-md-6">
                                <label class="email">Leave Type : </label>
                                <input type="date" disabled="disabled" class="form-control" name="upd_leavetype" id="upd_leavetype">
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="email">Leave For : </label>
                                <select disabled="disabled" class="form-control" name="upd_leave_for" id="upd_leave_for" data-placeholder="Select">
                                    <option value=""> -- Select-- </option>
                                    <option value="1"> Full Day </option>
                                    <option value="2"> Half Day </option>
                                    <option value="3"> Short Leave </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group required col-md-6">
                                <label class="email">From Date : </label>
                                <input type="date" disabled="disabled" class="form-control" name="upd_fromdate" id="upd_fromdate">
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="email">To Date : </label>
                                <input type="date" disabled="disabled" class="form-control" name="upd_todate" id="upd_todate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group required col-md-6">
                                <label class="email">Count Leave : </label>
                                <input disabled="disabled" type="text" required class="form-control" name="upd_countleave" id="upd_countleave">
                            </div>
                            <div class="form-group required col-md-6">
                                <label class="email">Leave Reason : </label>
                                <textarea class="form-control" required maxlength="200" name="upd_reason" autocomplete="off" id="upd_reason"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group required col-md-6">
                                <input type="hidden" id="updleave_id" name="leave_id">
                                <br/>
                                <input type="submit" class="btn btn-info" value="Update">
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


</body>

<script>
                                                                    //Cond First..
                                                                    function setcond_leavetype() {
                                                                        var leavetype = $("#leave_type").val();
                                                                        $("#leave_for").val("");
                                                                        $("#start_date").val("");
                                                                        $("#end_date").val("");
                                                                        $("#count_leave").val("");
                                                                        $("#leave_for").removeAttr("disabled");
                                                                        $("#sec-error-div").html("");
                                                                        $("#halfdaycateg").hide();
                                                                        if (leavetype) {
                                                                            if (leavetype == "2") {
                                                                                $("#leave_for").val("1");
                                                                                $("#leave_for").attr("disabled", true);
                                                                            } else {
                                                                                $("#leave_for").val("");
                                                                                $("#leave_for").removeAttr("disabled");
                                                                            }
                                                                        }
                                                                    }

                                                                    //Cond Second..
                                                                    function setcond_leavefor() {
                                                                        var leavetype = $("#leave_type").val();
                                                                        var leavefor = $("#leave_for").val();
                                                                        $("#sec-error-div").html("");
                                                                        $("#halfdaycateg").hide();
                                                                        //Code For RH..
                                                                        if (leavetype == "2") {
                                                                            if ((leavetype) && (leavefor)) {
                                                                                $("#sec-error-div").html("");
                                                                                $("#start_date").val("");
                                                                                $("#end_date").val("");
                                                                                $("#count_leave").val("");
                                                                                $("#leave_type").attr("disabled", true);
                                                                            } else {
                                                                                $("#sec-error-div").html("Select Leave Type & Leave For");
                                                                                $("#leave_for").val("");
                                                                            }
                                                                        }
                                                                        //Code & Cond For PL..
                                                                        if (leavetype == "1") {
                                                                            $("#leave_type").attr("disabled", true);
                                                                            $("#sec-error-div").html("");
                                                                            $("#start_date").val("");
                                                                            $("#end_date").val("");
                                                                            $("#count_leave").val("");
                                                                            $("#leave_type").attr("disabled", true);

                                                                            if (leavefor == "2") {
                                                                                $("#halfdaycateg").show();
                                                                            }
                                                                            if (leavefor != "2") {
                                                                                $("#halfdaycateg").hide();
                                                                            }
                                                                            if (leavefor == "3") {
                                                                                $("#sortleavecateg").show();
                                                                            }
                                                                            if (leavefor != "3") {
                                                                                $("#sortleavecateg").hide();
                                                                            }
                                                                        }
                                                                    }


                                                                    //Half Day Pre Lunch Post Lunch Cond..
                                                                    function hdayprelunchpost() {
                                                                        var leavefor = $("#leave_for").val();
                                                                        if (leavefor == "2") {
                                                                            $("#leave_for").attr("disabled", true);
                                                                            $("#sec-error-div").html("");
                                                                        }
                                                                    }


                                                                    //Cond Third..
                                                                    function setcond_startdate() {
                                                                        $("#end_date").val("");
                                                                        $("#count_leave").val("");
                                                                        $("#sec-error-div").html("");

                                                                        var leavetype = $("#leave_type").val();
                                                                        var leavefor = $("#leave_for").val();
                                                                        var start_date = $("#start_date").val();

                                                                        if ((leavetype) && (leavefor) && (start_date)) {
                                                                            $("#leave_type").attr("disabled", true);
                                                                            $("#leave_for").attr("disabled", true);

                                                                            //For Leave PL & RH..
                                                                            if ((leavetype == "2") && (leavefor == "1")) {
                                                                                //Ajax Start..
                                                                                $.ajax({
                                                                                    url: '<?= base_url("ajax_checkrhleave"); ?>',
                                                                                    data: ({rhapplydate: start_date}),
                                                                                    type: 'post',
                                                                                    success: function (data) {
                                                                                        var arr = $.parseJSON(data);
                                                                                        //Selected Date is RH or Not
                                                                                        if (arr.isrh == "0") {
                                                                                            $("#sec-error-div").html("Selected Date is not RH");
                                                                                            $("#start_date").val("");
                                                                                            $("#end_date").val("");
                                                                                            $("#start_date").removeAttr("disabled");
                                                                                            $("#end_date").removeAttr("disabled");
                                                                                        }
                                                                                        if (arr.isrh > 0) {
                                                                                            $("#end_date").val(start_date);
                                                                                            $("#start_date").attr("disabled", true);
                                                                                            $("#end_date").attr("disabled", true);
                                                                                            $("#count_leave").val("1");
                                                                                            //Check RH Leave Applied Limit By Single User..
                                                                                            if (arr.rhleavelimit > 2) {
                                                                                                $("#sec-error-div").html("You has been used RH Leave Limit. You can replan Leave");
                                                                                                $("#leave_type").val("");
                                                                                                $("#leave_for").val("");
                                                                                                $("#start_date").val("");
                                                                                                $("#end_date").val("");
                                                                                                $("#count_leave").val("");
                                                                                                $("#leave_type").removeAttr("disabled");
                                                                                                $("#leave_for").removeAttr("disabled");
                                                                                                $("#start_date").removeAttr("disabled");
                                                                                                $("#end_date").removeAttr("disabled");
                                                                                                $("#count_leave").removeAttr("disabled");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }

                                                                            //if Leave is PL & Half Days..
                                                                            if ((leavetype == "1") && (leavefor == "2")) {
                                                                                $("#end_date").val("");
                                                                                $("#count_leave").val("");
                                                                                $("#end_date").val(start_date);
                                                                                $("#start_date").attr("disabled", true);
                                                                                $("#end_date").attr("disabled", true);
                                                                                $("#count_leave").val("0.5");
                                                                            }
                                                                            //If PL Sort Leave..
                                                                            if ((leavetype == "1") && (leavefor == "3")) {
                                                                                $("#end_date").val("");
                                                                                $("#count_leave").val("");
                                                                                $("#end_date").val(start_date);
                                                                                $("#start_date").attr("disabled", true);
                                                                                $("#end_date").attr("disabled", true);
                                                                                $("#count_leave").val("0.25");
                                                                            }
                                                                        }
                                                                    }


                                                                    //On Submit.. Final Submit..
                                                                    function gotosubmit() {
                                                                        $("#sec-error-div").html("");
                                                                        var leave_type = $("#leave_type").val();
                                                                        var leave_for = $("#leave_for").val();
                                                                        var start_date = $("#start_date").val();
                                                                        var end_date = $("#end_date").val();
                                                                        var reason = $("#reason").val();
                                                                        var count_leave = $("#count_leave").val();
                                                                        var reporting_manager = $("#reporting_manager").val();
                                                                        var half_day_type = $("#half_day_type").val();
                                                                        var sort_leave_exptime = $("#expected_time").val();

                                                                        if ((leave_type) && (leave_for) && (start_date) && (end_date) && (reason) && (count_leave)) {
                                                                            //RH Apply By Ajax..
                                                                            if (leave_type == "2") {
                                                                                $.ajax({
                                                                                    url: '<?= base_url("ajax_savesubmit_leaverequest_rh"); ?>',
                                                                                    data: ({leave_type: leave_type, leave_for: leave_for, start_date: start_date, end_date: end_date, reason: reason, count_leave: count_leave, reporting_manager: reporting_manager}),
                                                                                    type: 'post',
                                                                                    success: function (data) {
                                                                                        var arr = $.parseJSON(data);
                                                                                        if (arr.status > 0) {
                                                                                            window.location = "<?= base_url("myleave"); ?>";
                                                                                        }
                                                                                        if (arr.status == "0") {
                                                                                            $("#sec-error-div").html(arr.error_msg);
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }

                                                                            //PL Apply For Full Day Ajax Section..
                                                                            if ((leave_type == "1") && (leave_for == "1")) {
                                                                                $.ajax({
                                                                                    url: '<?= base_url("ajax_savesubmit_leaverequest_pl"); ?>',
                                                                                    data: ({leave_type: leave_type, leave_for: leave_for, start_date: start_date, end_date: end_date, reason: reason, count_leave: count_leave, reporting_manager: reporting_manager}),
                                                                                    type: 'post',
                                                                                    success: function (data) {
                                                                                        var arr = $.parseJSON(data);
                                                                                        if (arr.status > 0) {
                                                                                            window.location = "<?= base_url("myleave"); ?>";
                                                                                        }
                                                                                        if (arr.status == "0") {
                                                                                            $("#sec-error-div").html(arr.error_msg);

                                                                                            $("#start_date").removeAttr("disabled");
                                                                                            $("#end_date").removeAttr("disabled");
                                                                                            $("#count_leave").removeAttr("disabled");

                                                                                            $("#start_date").val("");
                                                                                            $("#end_date").val("");
                                                                                            $("#count_leave").val("");

                                                                                        }
                                                                                    }
                                                                                });
                                                                            }

                                                                            //PL Apply For Half Day Ajax Section..
                                                                            if ((leave_type == "1") && (leave_for == "2")) {
                                                                                $("#sec-error-div").html("");
                                                                                var half_day_type = $("#half_day_type").val();
                                                                                var sort_leave_exptime = $("#expected_time").val();
                                                                                if ((leave_type) && (leave_for) && (start_date) && (end_date) && (reason) && (count_leave > 0) && (half_day_type)) {
                                                                                    //Ajax..
                                                                                    $.ajax({
                                                                                        url: '<?= base_url("ajax_savesubmit_leaverequest_hd"); ?>',
                                                                                        data: ({leave_type: leave_type, leave_for: leave_for, start_date: start_date, end_date: end_date, reason: reason, count_leave: count_leave, reporting_manager: reporting_manager, half_day_type: half_day_type}),
                                                                                        type: 'post',
                                                                                        success: function (data) {
                                                                                            var arr = $.parseJSON(data);
                                                                                            if (arr.status > 0) {
                                                                                                window.location = "<?= base_url("myleave"); ?>";
                                                                                            }
                                                                                            if (arr.status == "0") {
                                                                                                $("#sec-error-div").html(arr.error_msg);
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    $("#sec-error-div").html("Please Fill all Required fields");
                                                                                }
                                                                            }

                                                                            //Ajax Sort Leave PL 
                                                                            if ((leave_type == "1") && (leave_for == "3")) {
                                                                                $("#sec-error-div").html("");
                                                                                var sort_leave_exptime = $("#expected_time").val();
                                                                                if ((leave_type) && (leave_for) && (start_date) && (end_date) && (reason) && (count_leave) && (sort_leave_exptime)) {
                                                                                    //Ajax..
                                                                                    $.ajax({
                                                                                        url: '<?= base_url("ajax_savesubmit_leaverequest_sortl"); ?>',
                                                                                        data: ({leave_type: leave_type, leave_for: leave_for, start_date: start_date, end_date: end_date, reason: reason, count_leave: count_leave, reporting_manager: reporting_manager, sort_leave_exptime: sort_leave_exptime}),
                                                                                        type: 'post',
                                                                                        success: function (data) {
                                                                                            var arr = $.parseJSON(data);
                                                                                            if (arr.status > 0) {
                                                                                                window.location = "<?= base_url("myleave"); ?>";
                                                                                            }
                                                                                            if (arr.status == "0") {
                                                                                                $("#sec-error-div").html(arr.error_msg);
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    $("#sec-error-div").html("Please Fill all Required fields");
                                                                                }
                                                                            }
                                                                        } else {
                                                                            $("#sec-error-div").html("Please Fill all Required fields");
                                                                        }
                                                                    }


                                                                    //Set Cond End Date..
                                                                    function setcond_enddate() {
                                                                        var leavetype = $("#leave_type").val();
                                                                        var leavefor = $("#leave_for").val();
                                                                        var start_date = $("#start_date").val();
                                                                        var end_date = $("#end_date").val();

                                                                        $("#sec-error-div").html("");
                                                                        $("#count_leave").val("");
                                                                        $("#reason").val("");

                                                                        //End Date For Full Date PL..
                                                                        if ((leavetype == "1") && (leavefor == "1") && (start_date)) {
                                                                            $("#start_date").attr("disabled", true);
                                                                            //Ajax on End Date..
                                                                            $.ajax({
                                                                                url: '<?php echo base_url("ajax_chkdatecomb_pl"); ?>',
                                                                                data: ({pleavetype: leavetype, pleavefor: leavefor, pstart_date: start_date, pend_date: end_date}),
                                                                                type: 'post',
                                                                                success: function (data) {
                                                                                    var arr = $.parseJSON(data);
                                                                                    if (arr.status == "0") {
                                                                                        $("#sec-error-div").html(arr.error_msg);
                                                                                        $("#end_date").val("");
                                                                                    }
                                                                                    if (arr.status == "1") {
                                                                                        $("#sec-error-div").html("");
                                                                                        $("#end_date").attr("disabled", true);
                                                                                        $("#count_leave").val(arr.countday);
                                                                                    }
                                                                                    if (arr.notice == "1") {
                                                                                        $(".js-sweetalert").trigger('click');
                                                                                    }
                                                                                }
                                                                            });
                                                                        }


                                                                        //End Date Cond For PL..
                                                                        if ((leavetype == "1") && (leavefor == "2") && (start_date)) {
                                                                            $("#start_date").attr("disabled", true);
                                                                            //Ajax on End Date..
                                                                            $.ajax({
                                                                                url: '<?php echo base_url("ajax_chkdatecomb_pl"); ?>',
                                                                                data: ({pleavetype: leavetype, pleavefor: leavefor, pstart_date: start_date, pend_date: end_date}),
                                                                                type: 'post',
                                                                                success: function (data) {
                                                                                    var arr = $.parseJSON(data);
                                                                                    if (arr.status == "0") {
                                                                                        $("#sec-error-div").html(arr.error_msg);
                                                                                        $("#end_date").val("");
                                                                                    }
                                                                                    if (arr.status == "1") {
                                                                                        $("#sec-error-div").html("");
                                                                                        $("#end_date").attr("disabled", true);
                                                                                        $("#count_leave").val(arr.countday);
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    //Delete Cancel Leave..
                                                                    function LeaveDeleteAction(leaveid) {
                                                                        if (confirm("Are You sure Cancel/Delete this ? ")) {
                                                                            window.location = "<?= base_url('deleteleavebytid/'); ?>" + leaveid;
                                                                        }
                                                                    }
                                                                    //Set Edit Val..
                                                                    function seteditupdatedata(leaveid) {
                                                                        $.ajax({
                                                                            url: '<?= base_url("ajax_getsingleleaveby_tid"); ?>',
                                                                            data: ({leavetblid: leaveid}),
                                                                            type: 'post',
                                                                            success: function (data) {
                                                                                var arr = $.parseJSON(data);
                                                                                $("#upd_fromdate").val(arr.from_date);
                                                                                $("#upd_todate").val(arr.to_date);
                                                                                $("#upd_countleave").val(arr.appliedleavescount);
                                                                                $("#upd_reason").val(arr.reason);
                                                                                $("#updleave_id").val(leaveid);
                                                                                $("#upd_leavetype").val(arr.leavetype_name);
                                                                                $("#upd_leave_for").val(arr.leaveday);
                                                                            }
                                                                        });
                                                                    }

</script>