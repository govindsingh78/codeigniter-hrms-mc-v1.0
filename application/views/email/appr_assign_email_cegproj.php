<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="date=no" />
        <meta name="format-detection" content="address=no" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="x-apple-disable-message-reformatting" />
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Kreon:400,700|Playfair+Display:400,400i,700,700i|Raleway:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet" />
        <!--<![endif]-->
        <title>CEG Project Appraisal Form Lock </title>

        <style type="text/css" media="screen">
            /* Linked Styles */
            body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none }
            a { color:#000001; text-decoration:none }
            p { padding:0 !important; margin:0 !important } 
            img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
            .mcnPreviewText { display: none !important; }
            .text-footer2 a { color: #ffffff; } 
            /* Mobile styles */
            @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
                .mobile-shell { width: 100% !important; min-width: 100% !important; }
                .m-center { text-align: center !important; }
                .m-left { text-align: left !important; margin-right: auto !important; }
                .center { margin: 0 auto !important; }
                .content2 { padding: 8px 15px 12px !important; }
                .t-left { float: left !important; margin-right: 30px !important; }
                .t-left-2  { float: left !important; }
                .td { width: 100% !important; min-width: 100% !important; }
                .content { padding: 30px 15px !important; }
                .section { padding: 30px 15px 0px !important; }
                .m-br-15 { height: 15px !important; }
                .mpb5 { padding-bottom: 5px !important; }
                .mpb15 { padding-bottom: 15px !important; }
                .mpb20 { padding-bottom: 20px !important; }
                .mpb30 { padding-bottom: 30px !important; }
                .mp30 { padding-bottom: 30px !important; }
                .m-padder { padding: 0px 15px !important; }
                .m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
                .p70 { padding: 30px 0px !important; }
                .pt70 { padding-top: 30px !important; }
                .p0-15 { padding: 0px 15px !important; }
                .p30-15 { padding: 30px 15px !important; }			
                .p30-15-0 { padding: 30px 15px 0px 15px !important; }			
                .p0-15-30 { padding: 0px 15px 30px 15px !important; }
                .text-footer { text-align: center !important; }
                .m-td,
                .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
                .m-block { display: block !important; }
                .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }
                .column,
                .column-dir,
                .column-top,
                .column-empty,
                .column-top-30,
                .column-top-60,
                .column-empty2,
                .column-bottom { float: left !important; width: 100% !important; display: block !important; }
                .column-empty { padding-bottom: 15px !important; }
                .column-empty2 { padding-bottom: 30px !important; }
                .content-spacing { width: 15px !important; }
            }
        </style>
    </head>

    <body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <tr>
                <td align="center" valign="top">
                    <!-- Main -->
                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                        <tr>
                            <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                <!-- Section 1 -->
                                <div mc:repeatable="Select" mc:variant="Section 1">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
                                        <tr>
                                            <img src="http://cegindia.com/assets/images/logo.png" height="100" />
                                        </tr>
                                        <tr>
                                            <td class="p30-15-0" style="padding: 50px 30px 0px;" bgcolor="#ffffff">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="h2-center"style="color:#000000; font-family:'Playfair Display', Times, 'Times New Roman', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">
                                                            <div mc:edit="text_4"> Dear <?php echo @$prefix_name . ". " . @$userfullname; ?>,</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:center; padding-bottom:22px;">
                                                            <p style="text-align: justify;">
                                                                We are pleased to inform that we have designed a comprehensive online Performance Management System (PMS) to assess performance & potential of the employees with respect to position requirements, performance objectives and behaviors in support of the organization's mission and strategic initiatives.
                                                            </p>
                                                            <br>
                                                                <p style="text-align: justify;">
                                                                    All Team Leader/In-charges (IO) at Projects are required to use the Online Performance Appraisal System to complete the Performance Appraisal form in respect to their all eligible team members as per the attached list. <br> The Online Performance Appraisal System can be accessed at <a target="_blank" href="http://hrms.cegindia.com/hrms/"> https://hrms.cegindia.com/hrms </a> and also accessible via mobile phone.
                                                                </p>
                                                                <br></br>
                                                                <p style="text-align: justify;">Target date to close the Performance Appraisal is as follows-</p>
                                                                <br>
                                                                    <p style="text-align: justify;"> Project Team Leader/In-charge (IO) - on or before 2200 hrs of 10 September 2019 (IO - Access will be denied after this date) </p> <br>
                                                                        <p style="text-align: justify;"> Head Coordinator (RO) - on or before 2200 hrs of 13 September 2019 (RO - Access will be denied after this date) </p>
                                                                        <br><br>
                                                                                <p style="text-align: justify;"> <ul> A self explanatory guide </ul> for how to fill in & complete the Online Performance Appraisal System is availabe in HRMS. Should you have any queries, please feel free to contact - </p> <br>
                                                                                    <h4>Mr. Pankaj Sharma - GM (HR) (Extn 721) Mob. 9928070658 <br> Mr. Rahul Saxena (IT) - (Extn 740) Mob. 9887276737 </h4>
                                                                                    <p> Thanks & regards, <b> Team HR </b> .</p>
                                                                                </br>
                                                                                <p> &nbsp; </p>
                                                                                <p> &nbsp; </p>
                                                                                </td>
                                                                                </tr>
                                                                                </table>
                                                                                </td>
                                                                                </tr>
                                                                                </table>
                                                                                </div>

                                                                                </td>
                                                                                </tr>
                                                                                </table>

                                                                                <div class="footer">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td class="content-block powered-by">
                                                                                                Powered by <a target="_blanck" href="http://cegindia.com">Consulting Engineers Group Ltd. </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>

                                                                                </td>
                                                                                </tr>
                                                                                </table>
                                                                                </body>
                                                                                </html>