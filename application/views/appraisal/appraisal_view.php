<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$recappraisalArr = GetAllAppraisalMaster();

$addMoreLimit = 30;
$addMoreLimit = $addMoreLimit - count($projectListArr);
?>

<body class="theme-cyan">
    <div id="pageloader">
        <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
    </div>

    <?php if ($this->session->flashdata('successmsg')) { ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('errormsg')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
        </div>
    <?php } ?>

    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="profile-img-wrap"> 
                                                <?php if (@$userBasicDetailsArr->profileimg) { ?>
                                                    <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/uploads/profile/<?= @$userBasicDetailsArr->profileimg; ?>" alt="user">
                                                <?php } else { ?>
                                                    <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg" alt="user">
                                                <?php } ?> 
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label">Employee Name : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$userBasicDetailsArr->userfullname) ? @$userBasicDetailsArr->prefix_name . " " . @$userBasicDetailsArr->userfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <label class="control-label">Date of Joining : </label>
                                                    <div class="form-group form-focus focused">
                                                        <div class="cal-icon">
                                                            <input class="form-control floating datetimepicker" readonly="" value="<?= (@$userBasicDetailsArr->date_of_joining) ? date("d-m-Y", strtotime($userBasicDetailsArr->date_of_joining)) : ""; ?>" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Department : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$userBasicDetailsArr->department_name) ? @$userBasicDetailsArr->department_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Designation : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$userBasicDetailsArr->position_name) ? @$userBasicDetailsArr->position_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of IO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$userBasicDetailsArr->reporting_manager_name) ? @$userBasicDetailsArr->reporting_manager_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of RO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$userBasicDetailsArr->romngr_userfullname) ? @$userBasicDetailsArr->romngr_prefix_name . " " . @$userBasicDetailsArr->romngr_userfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                        <form method="post" action="<?= base_url('appraisalsave'); ?>" id="myform" >
                                            <div class="col-md-12">
                                                <hr>
                                                <div class="card-box">
                                                    <h5 class="heading-title text-center">Performance and Potential Evaluation</h5>
                                                    <h5 class="card-title text-center">Kindly rate in accordance with the following performance indicators that most closely describes employee performance </h5>
                                                    <h6 class="card-title text-center">(9-10: Excellent | 7-8: Good | 5-6: Average| 3-4:Poor|1-2: Very Poor)</h5><br/><br/>
                                                        <br/>
                                                        <span class="card-title">Performance indicator on 1-10 scale (Rating)</span>
                                                        <br/>
                                                        <table class="table table-bordered" >
                                                            <br/>
                                                            <thead>
                                                                <tr>
                                                                    <th><b>S.No.</b></th>
                                                                    <th><b>Performance indicator </b></th>
                                                                    <th><b>Self Rating (1-10)</b></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (@$recappraisalArr) {
                                                                    foreach (@$recappraisalArr as $kEy => $recRow) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><b><?= $recRow->srno; ?></b></td>
                                                                            <td colspan='2'><b><?= $recRow->performfact_name; ?></b></td>
                                                                        </tr>
                                                                        <?php
                                                                        $laYer2 = GetAllAppraisalMasterLsecond($recRow->fld_id);
                                                                        if ($laYer2) {
                                                                            foreach ($laYer2 as $kEy2 => $recRow2) {
                                                                                ?> 
                                                                                <tr>
                                                                                    <td><?= ($kEy2 + 1); ?></td>
                                                                                    <td><?= $recRow2->performfact_name; ?></td>
                                                                                    <td>
                                                                                        <select required id="performance_rating_self_<?= $recRow2->fld_id; ?>" name="performance_rating_self_<?= $recRow2->fld_id; ?>" class="form-control">
                                                                                            <option value="non"> None of These </option>
                                                                                            <option value="1"> 1 </option>
                                                                                            <option value="2"> 2 </option>
                                                                                            <option value="3"> 3 </option>
                                                                                            <option value="4"> 4 </option>
                                                                                            <option value="5"> 5 </option>
                                                                                            <option value="6"> 6 </option>
                                                                                            <option value="7"> 7 </option>
                                                                                            <option value="8"> 8 </option>
                                                                                            <option value="9"> 9 </option>
                                                                                            <option value="10"> 10 </option>
                                                                                        </select>
                                                                                    </td> 
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <tr>
                                                                            <th colspan='2'><b>Total : <?= $recRow->srno; ?> </b></th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                </div>

                                                <div class="card-box">
                                                    <h5 class="heading-title text-center"> Pen Picture (Limited to 30 only)</h5> <br>
                                                    <div class="card-two">
                                                        <?php
                                                        if ($projectListArr) {
                                                            foreach ($projectListArr as $recD) {
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="control-label email">Project/Special Task Assigned : </label>
                                                                        <div class="form-group form-focus">
                                                                            <select class="form-control" name="project_task_row2[]" id="projsection" onchange="setprojectorother()">
                                                                                <option value="<?= $recD->project_id; ?>"><?= $recD->project_name; ?></option>        
                                                                            </select>  
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label">Activities Performed on Project : </label>
                                                                        <div class="form-group form-focus">
                                                                            <input required type="text" class="form-control floating" name="actvperform_row2[]" id="actvperform_row2" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">IO (Yes / No) : </label>
                                                                        <div class="form-group form-focus"> 
                                                                            <select disabled="disabled" class="form-control floating" name="royesorno_row2[]" id="yesorno">
                                                                                <option value=""> --Select-- </option>
                                                                                <option value="yes"> Yes </option>
                                                                                <option value="no"> No </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label"> RO (Yes / No) : </label>
                                                                        <div class="form-group form-focus">
                                                                            <select disabled="disabled" class="form-control floating" name="ioyesorno_row2[]" id="yesorno">
                                                                                <option value=""> --Select-- </option>
                                                                                <option value="yes"> Yes </option>
                                                                                <option value="no"> No </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="proj_other[]" id="proj_other" value="2">
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <hr size="2" color="blue">
                                                        <div class="row input_fields_wrap">
                                                            <div class="col-md-3">
                                                                <label class="control-label email">Other / Special Task : </label>
                                                                <div class="form-group form-focus">
                                                                    <input type="text" class="form-control" name="project_task_row2[]" id="project_task_row2">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label">Activities Performed : </label>
                                                                <div class="form-group form-focus">
                                                                    <input type="text" class="form-control floating" name="actvperform_row2[]" id="actvperform_row2" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label class="control-label">IO (Yes / No) : </label>
                                                                <div class="form-group form-focus"> 
                                                                    <select disabled="disabled" class="form-control floating" name="royesorno_row2[]" id="yesorno">
                                                                        <option value=""> --Select-- </option>
                                                                        <option value="yes"> Yes </option>
                                                                        <option value="no"> No </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label class="control-label"> RO (Yes / No) : </label>
                                                                <div class="form-group form-focus">
                                                                    <select disabled="disabled" class="form-control floating" name="ioyesorno_row2[]" id="yesorno">
                                                                        <option value=""> --Select-- </option>
                                                                        <option value="yes"> Yes </option>
                                                                        <option value="no"> No </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="add-more">
                                                            <a id="add-two" style="color:white" class="btn btn-primary"><i class="fa fa-plus"></i> Add More</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-box">
                                                    <h5 class="heading-title text-center"> Remarks at the end of the appraisal review meeting of appraisee with IO </h5> <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> A) Existing skills / knowledge not utilized in current job profile : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea class="form-control floating" required="required" name="quest1" id="quest1" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> B) New skills / knowledge acquired in review period : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea class="form-control floating" required="required" name="quest2" id="quest2" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> C) Skills / knowledge that you feel need to be upgraded for better performance : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea class="form-control floating" required="required" name="quest3" id="quest3" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-center m-t-20">
                                                    <input type="hidden" name="actionID" value="<?= @$apprSingleRowDetail->appraisal_cycle_id; ?>" />
                                                    <button class="btn btn-success btn-lg" type="submit"> Save </button>
                                                </div>


                                                <div class="">


                                                </div>



                                                <div class="row-second" style="display:none">
                                                    <div class="col-md-3">
                                                        <div class="form-group form-focus">
                                                            <input type="text" class="form-control" name="project_task_row2[]" id="project_task_row2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group form-focus">
                                                            <input type="text" class="form-control floating" name="actvperform_row2[]" id="actvperform_row2" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group form-focus">
                                                            <select disabled="disabled" class="form-control floating" name="ioyesorno_row2[]" id="yesorno">
                                                                <option value=""> --Select-- </option>
                                                                <option value="yes"> Yes </option>
                                                                <option value="no"> No </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group form-focus">
                                                            <select disabled="disabled" class="form-control floating" name="royesorno_row2[]" id="yesorno">
                                                                <option value=""> --Select-- </option>
                                                                <option value="yes"> Yes </option>
                                                                <option value="no"> No </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="proj_other[]" id="proj_other" value="1">
                                                </div>


                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
                                                                                $(document).ready(function () {
                                                                                    $("#myform").on("submit", function () {
                                                                                        $("#pageloader").fadeIn();
                                                                                    });
                                                                                });

                                                                                $(document).ready(function () {
                                                                                    //card two add limit
                                                                                    var maxGroup = <?= $addMoreLimit; ?>;
                                                                                    $("#add-two").click(function () {
                                                                                        if ($('body').find('.card-two').length < maxGroup) {
                                                                                            var fieldHTML1 = '<br/><div class="card-two"><div class="row">' + $(".row-second").html() + '</div><div class="add-more"><button type="button" id="remove-two" class="btn btn-danger"><i class="fa fa-trash"></i> Remove</button></div></div>';
                                                                                            $('body').find('.card-two:last').after(fieldHTML1);
                                                                                        } else {
                                                                                            alert('Maximum ' + maxGroup + ' Row are allowed.');
                                                                                        }
                                                                                    });
                                                                                    //remove card two
                                                                                    $("body").on("click", "#remove-two", function () {
                                                                                        $(this).parents(".card-two").remove();
                                                                                    });
                                                                                });


        </script>

        <style>
            #pageloader {
                background: rgba( 255, 255, 255, 0.8 );
                display: none;
                height: 100%;
                position: fixed;
                width: 100%;
                z-index: 9999;
            }
            #pageloader img {
                left: 50%;
                margin-left: -32px;
                margin-top: -32px;
                position: absolute;
                top: 50%;
            }
        </style>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>