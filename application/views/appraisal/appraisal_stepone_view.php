<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>My Appraisal</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>            
                    </div>

                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Error ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body">  
                                <h3><a>Dear CEGian,</a></h3>
                                <p> Performance Appraisal is the systematic evaluation of the performance of employees and to understand the abilities of a person for further growth and development.
                                    It is a regular review of an employee's job performance and overall contribution to a company.
                                    A performance appraisal evaluates an employee's skills, achievements, growth and areas of improvement.
                                </p>

                                <p>Performance appraisal has three basic functions : </p> <br>
                                <p> => to provide adequate feedback to each person on his or her performance.</p>
                                <p> => to serve as a basis for modifying or changing behavior toward more effective working habits, and </p>
                                <p> => to provide data to managers with which they may plan future trainings, job assignments, growth and development of each person. </p>
                                <p><b>In view of above, we are initiating our online performance appraisal form with a request for completion according to given timelines : </b></p>
                            </div>

                            <div style="margin: 10px;">
                                <table border='1' align='left' cellpadding='5' cellspacing='5'>
                                    <tr style="color:white; background: #a7a715">
                                        <th>Appraisal Review</th>
                                        <th>Initiation Date </th>
                                        <th>Completion Date</th>
                                        <th>Remarks</th>
                                    </tr>
                                    <tr>
                                        <td> Self </td>
                                        <td rowspan="3"> 27-Feb </td>
                                        <td>04-March</td>
                                        <td rowspan="3"> Access will be denied after this date </td>
                                    </tr>
                                    <tr>
                                        <td> IO </td>
                                        <td> 12-March </td>
                                    </tr>
                                    <tr>
                                        <td> RO </td>
                                        <td> 19-March </td>
                                    </tr>
                                </table> 
                            </div>
                            <div class="card single_post">
                                <div class="body">  
                                    <p> # No 'Self Appraisal' is applicable for Project employees, only IO & RO to fill the 'Appraisal Form'.</p>
                                    <p> Should you have any queries, please feel free to contact - </p>
                                    <p>Mr. Pankaj Sharma HR (Extn. 721)</p>
                                    <p>Mr. Rahul Saxena IT (Extn. 740)</p> 
                                    <p>Thanks & Regards,
                                        (Team HR)
                                </div>
                            </div>


                            <div class="tab-content">
                                <?php // if ($AssignProj_Appraisal == "") { ?>
                                    <div class="body"> 
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No.</th>
                                                        <th>Appr. Cycle</th>
                                                        <th>Last Date</th>
                                                        <th>Status</th>
                                                        <th>Entry / Edit</th>
                                                        <th>Guidelines</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (@$ApprRecListArr) {
                                                        foreach ($ApprRecListArr as $kEy => $appRow) {
                                                            ?>
                                                            <tr style="">
                                                                <td><?= $kEy + 1; ?></td>
                                                                <td><?= ($appRow->cycle_date) ? date("F, Y", strtotime($appRow->cycle_date)) : ""; ?></td>
                                                                <td><?= ($appRow->emp_last_date) ? date("d F, Y", strtotime($appRow->emp_last_date)) : ""; ?></td>
                                                                <td>
                                                                    <?php if ((@$appRow->appr_action_masterID) and ( @$appRow->emp_self_lock != "1")) { ?>
                                                                        <span style="color:green"> Editable </span> &nbsp;
                                                                    <?php } ?>
                                                                    <?php if ((@$appRow->appr_action_masterID) and ( @$appRow->emp_self_lock == "1")) { ?>
                                                                        <span style="color:green"> Locked </span> &nbsp;
                                                                    <?php } ?>
                                                                    <?php if ((@$appRow->appr_action_masterID == "") and ( @$appRow->emp_self_lock == "")) { ?>
                                                                        <span style="color:red"> Pending </span> &nbsp;
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    <?php if (@$appRow->appr_action_masterID == "") { ?>
                                                                        <a href="<?= base_url("appraisal/" . $appRow->id); ?>">
                                                                            <i title="Add" class="fa fa-plus fa-2x"></i></a>
                                                                    <?php } ?>
                                                                    <?php if ((@$appRow->appr_action_masterID) and ( @$appRow->emp_self_lock != "1")) { ?>
                                                                        <a href="<?= base_url("editappraisalbyuser/" . @$appRow->appr_action_masterID); ?>">
                                                                            <i title="Edit"  class="fa fa-edit fa-2x"></i></a>
                                                                    <?php } ?>
                                                                    <?php if (@$appRow->emp_self_lock == "1") { ?>
                                                                        <a href="<?= base_url("view_selfappraisal/" . $appRow->appr_action_masterID); ?>">
                                                                            <span style="color:green"> <i title="Locked" class="fa fa-lock fa-2x"></i> </span>
                                                                        </a>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    <a title="Guideline" target="_blank" href="<?= HOSTNAME . "public/PMS-Guideline.pdf"; ?>">
                                                                        <i class="fa fa-paperclip fa-2x"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td style="color:red" colspan="6"> Record Not Found. </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Sr.No.</th>
                                                        <th>Appr. Cycle</th>
                                                        <th>Last Date</th>
                                                        <th>Status</th>
                                                        <th>Entry / Edit</th>
                                                        <th>Guidelines</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                <?php  // } else { ?>
<!--                                    &nbsp; <a href="<?= base_url("apprai_projsiteofc"); ?>"><button class="btn btn-primary"> Go To List</button></a>-->
                                <?php // } ?>
                            </div>

                            <br>

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>
</div>
</body>


<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>