<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2>
                                <a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth">
                                    <i class="fa fa-arrow-left"></i> 
                                </a> My Appraisal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                <a href="<?= base_url("employee-appraisal"); ?>">
                                    <button style="margin: 0px 5px 0 200px;" class="btn btn-info btn-xs"> << Back </button>
                                </a>
                            </h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                                
                            </ul>
                        </div>            
                    </div>

                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body"> 

                                <div class="tab-content">
                                    <div class="body"> 
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>EmpName</th>
                                                        <th>IO Name</th>
                                                        <th>RO Name</th>
                                                        <th>Appraisal-Cycle/Session</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($allAppraisal): ?>
                                                        <?php foreach ($allAppraisal as $kYk => $rEcR) { ?>
                                                            <tr>
                                                                <td><?= $kYk + 1; ?></td>
                                                                <td><?= $rEcR->userfullname; ?></td>  
                                                                <td><?= $rEcR->reporting_manager_name; ?></td>
                                                                <td><?= $rEcR->iouserfullname; ?></td>
                                                                <td><?= date("F, Y", strtotime($rEcR->cycle_date)); ?></td>
                                                                <td>
                                                                    <?php if ($rEcR->emp_io_lock == "") { ?>
                                                                        <a href="<?= base_url("appraisalviewdetails/" . $rEcR->fld_id); ?>"> Edit </a>
                                                                    <?php } ?>
                                                                    <?php if ($rEcR->emp_io_lock) { ?>
                                                                        <span style="color:green"> <i title="Locked" class="fa fa-lock fa-2x"></i> </span>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } endif; ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>EmpName</th>
                                                        <th>IO Name</th>
                                                        <th>RO Name</th>
                                                        <th>Appraisal-Cycle/Session</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
</body>


<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>