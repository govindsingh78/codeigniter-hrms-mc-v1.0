<?php
$fldID = $singleRowData['singleBaseRec']->fld_id;
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$recappraisalArr = GetAllAppraisalMaster();
?>

<body class="theme-cyan">
    <div id="pageloader">
        <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
    </div>

    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Edit Appraisal Form</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                        <a href="<?= base_url("employee-appraisal"); ?>">
                            <button style="height:37px; margin-top:10px" class="btn btn-primary btn-sm"> << Back </button>
                        </a>
                    </div>
                </div>

                <?php if ($this->session->flashdata('successmsg')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('errormsg')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong> Error ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                    </div>
                <?php } ?>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="profile-img-wrap">
                                                <div class="profile-img-wrap">
                                                    <?php if ($singleRowData['singleBaseRec']->profileimg) { ?>
                                                        <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/uploads/profile/<?= $singleRowData['singleBaseRec']->profileimg; ?>" alt="user">
                                                    <?php } else { ?>
                                                        <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg" alt="user">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label">Employee Name : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->userfullname) ? @$singleRowData['singleBaseRec']->userfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <label class="control-label">Date of Joining : </label>
                                                    <div class="form-group form-focus focused">
                                                        <div class="cal-icon">
                                                            <input class="form-control floating datetimepicker" readonly="" value="<?= (@$singleRowData['singleBaseRec']->date_of_joining) ? date("d-m-Y", strtotime($singleRowData['singleBaseRec']->date_of_joining)) : ""; ?>" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Department : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->department_name) ? @$singleRowData['singleBaseRec']->department_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Designation : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->position_name) ? @$singleRowData['singleBaseRec']->position_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of IO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->rouserfullname) ? @$singleRowData['singleBaseRec']->rouserfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of RO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->iouserfullname) ? @$singleRowData['singleBaseRec']->iouserfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <form method="post" action="<?= base_url('apprsaveupdateas_ro'); ?>" id="myform">
                                            <div class="col-md-12"> 
                                                <hr>
                                                <div class="card-box">
                                                    <h5 class="heading-title text-center">Performance and Potential Evaluation</h5>
                                                    <h5 class="card-title text-center">Kindly rate in accordance with the following performance indicators that most closely describes employee performance </h5>
                                                    <h6 class="card-title text-center">(9-10: Excellent | 7-8: Good | 5-6: Average| 3-4:Poor|1-2: Very Poor)</h6><br/>
                                                    <br/>
                                                    <span class="card-title">Performance indicator on 1-10 scale (Rating)</span>
                                                    <br/>
                                                    <table class="table table-bordered"> <br>
                                                        <thead>
                                                            <tr>
                                                                <th><b>S.No.</b></th>
                                                                <th><b>Performance indicator </b></th>
                                                                <th><b>Self Rating <br> (1-10)</b></th>
                                                                <th><b>Rating by IO <br> (1-10)</b></th>
                                                                <th><b>Remark By IO </b></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if ($recappraisalArr) {
                                                                foreach ($recappraisalArr as $kEy => $recRow) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><b><?= $recRow->srno; ?></b></td>
                                                                        <td colspan='4'><b><?= $recRow->performfact_name; ?></b></td>
                                                                    </tr>
                                                                    <?php
                                                                    $totVal = 0;
                                                                    $totVal2 = 0;
                                                                    $laYer2 = GetAllAppraisalMasterLsecond(@$recRow->fld_id);
                                                                    if ($laYer2) {
                                                                        foreach ($laYer2 as $kEy2 => $recRow2) {
                                                                            @$ROW3e = AppraisalRowSec($fldID, @$recRow2->fld_id);
                                                                            @$totVal += @$ROW3e->perform_fact_rating;
                                                                            @$totVal2 += @$ROW3e->ro_rating;

                                                                            $reqUir = "";
                                                                            if ($ROW3e->ro_rating) {
                                                                                if (($ROW3e->ro_rating > 6) or ( $ROW3e->ro_rating < 3)) {
                                                                                    $reqUir = "Required";
                                                                                } else {
                                                                                    $reqUir = "";
                                                                                }
                                                                            } else {
                                                                                $reqUir = "";
                                                                            }
                                                                            ?>
                                                                            <tr>
                                                                                <td><?= ($kEy2 + 1); ?>
                                                                                    &nbsp;<input type="hidden" <?= (@$ROW3e) ? "checked" : ""; ?> name="chkagree3[]" value="<?= @$ROW3e->fld_id; ?>" />
                                                                                </td>
                                                                                <td width="25%"><?= $recRow2->performfact_name; ?></td>
                                                                                <td width="10%">
                                                                                    <select disabled="disabled" id="performance_rating_self_<?= @$recRow2->fld_id; ?>" name="performance_rating_self_<?= @$recRow2->fld_id; ?>" class="form-control">
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "") ? "selected" : ""; ?> value=""> --Select-- </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "6") ? "selected" : ""; ?> value="6"> 6 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "7") ? "selected" : ""; ?> value="7"> 7 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "8") ? "selected" : ""; ?> value="8"> 8 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "9") ? "selected" : ""; ?> value="9"> 9 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "10") ? "selected" : ""; ?> value="10"> 10 </option>
                                                                                    </select>
                                                                                </td>
                                                                                <td width="10%">
                                                                                    <select onchange="setrequiredcond('<?= @$ROW3e->fld_id; ?>')" id="perf_ratingro_<?= @$ROW3e->fld_id; ?>" name="perf_ratingro_<?= @$ROW3e->fld_id; ?>" class="form-control">
                                                                                        <option <?= (@$ROW3e->ro_rating == "") ? "selected" : ""; ?> value=""> --Select-- </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "6") ? "selected" : ""; ?> value="6"> 6 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "7") ? "selected" : ""; ?> value="7"> 7 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "8") ? "selected" : ""; ?> value="8"> 8 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "9") ? "selected" : ""; ?> value="9"> 9 </option>
                                                                                        <option <?= (@$ROW3e->ro_rating == "10") ? "selected" : ""; ?> value="10"> 10 </option>
                                                                                    </select> 
                                                                                </td>

                                                                                <td width="50%">
                                                                                    <input title="<?= @$ROW3e->ro_remark; ?>" style="color:black" <?= $reqUir; ?> value="<?= @$ROW3e->ro_remark; ?>" placeholder="Remark By IO For :- <?= @$recRow2->performfact_name; ?> ..." type="text" id="perf_remarkbyro_<?= @$ROW3e->fld_id; ?>" name="perf_remarkbyro_<?= @$ROW3e->fld_id; ?>" class="form-control">
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <th><b>Total : <?= $recRow->srno; ?> </b></th>
                                                                        <th></th>
                                                                        <th><?= $totVal; ?></th>
                                                                        <th  colspan='2'><?= $totVal2; ?></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan='3'> -- </th>
                                                                        <th></th>
                                                                        <th></th>
                                                                    </tr>
                                                                    <?php
                                                                    $totVal = 0;
                                                                    $totVal2 = 0;
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div> 



                                                <div class="card-box">
                                                    <h5 class="heading-title text-center">Pen Picture</h5>
                                                    <br/>
                                                    <div class="card-two">
                                                        <?php if ($singleRowData['RecRow2']) { ?>
                                                            <?php
                                                            foreach ($singleRowData['RecRow2'] as $reCr2) {
                                                                if ($reCr2->proj_other == 2) {
                                                                    $projName = get_project_name($reCr2->project_special_taskassign);
                                                                }
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="control-label email">Project / Special Task Assigned : </label>
                                                                        <div class="form-group form-focus">
                                                                            <div class="form-group form-focus">
                                                                                <?php if ($reCr2->proj_other == 1) { ?>
                                                                                    <input type="text" title="<?= $reCr2->project_special_taskassign; ?>" readonly class="form-control floating" value="<?= $reCr2->project_special_taskassign; ?>" name="project_special_taskassign_<?= @$reCr2->fld_id; ?>" />
                                                                                <?php } ?>
                                                                                <?php if ($reCr2->proj_other == 2) { ?>
                                                                                    <span style="color:green"> <?= $projName; ?> </span>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label">Activities Performed : </label>
                                                                        <div class="form-group form-focus">
                                                                            <input type="text" class="form-control floating" readonly title="<?= $reCr2->activ_perform; ?>" value="<?= $reCr2->activ_perform; ?>" name="actvperform_row2" id="actvperform_row2" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label">IO (Yes / No) : </label>
                                                                        <div class="form-group form-focus"> 
                                                                            <select required="required" class="form-control floating" name="royesorno2_<?= @$reCr2->fld_id; ?>" id="royesorno2_<?= @$reCr2->fld_id; ?>">
                                                                                <option <?= ($reCr2->ro_yes_no == "") ? "selected" : ""; ?> value=""> -- Select -- </option>
                                                                                <option <?= ($reCr2->ro_yes_no == "yes") ? "selected" : ""; ?> value="yes"> Yes </option>
                                                                                <option <?= ($reCr2->ro_yes_no == "no") ? "selected" : ""; ?> value="no"> No </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label">RO (Yes / No) : </label>
                                                                        <div class="form-group form-focus">
                                                                            <select disabled="disabled" class="form-control floating" name="ioyesorno_row2" id="yesorno">
                                                                                <option value=""> -- Select -- </option>
                                                                                <option value="yes"> Yes </option>
                                                                                <option value="no"> No </option>
                                                                            </select>
                                                                            &nbsp;<input type="hidden" checked="checked" name="chkagree2[]" value="<?= @$reCr2->fld_id; ?>"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>


                                                <div class="card-box">
                                                    <h5 class="heading-title text-center"> Remarks at the end of the appraisal review meeting of appraisee with IO </h5>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> A) Existing skills / knowledge not utilized in current job profile : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea readonly class="form-control floating" required="required" name="quest1" id="quest1" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_1; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> B) New skills / knowledge acquired in review period : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea readonly class="form-control floating" required="required" name="quest2" id="quest2" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_2; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> C) Skills / knowledge that you feel need to be upgraded for better performance : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea readonly class="form-control floating" required="required" name="quest3" id="quest3" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_3; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-box">
                                                    <h5 class="heading-title text-center"> Appraisal Interview & Discussion </h5><br/>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label email"> Appraisal Interview Conducted on : </label>
                                                            <div class="form-group form-focus">
                                                                <input type="date" required value="<?= ($singleRowData['singleBaseRec']->interviewcond_date_io) ? $singleRowData['singleBaseRec']->interviewcond_date_io : date("Y-m-d"); ?>" name="interviewcond_date_io" id="interviewcond_date_io" class="form-control floating" /><br/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label email"> IO. Name : </label>
                                                            <div class="form-group form-focus">
                                                                <input type="text" readonly value="<?= $singleRowData['singleBaseRec']->rouserfullname; ?>" class="form-control floating" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label email"> RO. Name : </label>
                                                            <div class="form-group form-focus">
                                                                <input type="text" readonly value="<?= $singleRowData['singleBaseRec']->iouserfullname; ?>" class="form-control floating" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="masterfld_id" value="<?= @$fldID; ?>">
                                                </div>

                                                <div class="card-box">
                                                    <h5 class="heading-title text-center"> Final Summary of IO (Appraiser) </h5>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="control-label email"> IO (Reviewer) : </label>
                                                            <div class="form-group form-focus">
                                                                <textarea class="form-control floating" required="required" name="final_ro_reviewer" id="final_ro_reviewer" rows="3"><?= $singleRowData['singleBaseRec']->final_ro_reviewer; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">&nbsp;</div>
                                                    <div class="col-md-4">
                                                        <button class="btn btn-success btn-lg" type="submit"> Save / Update </button>
                                                    </div>
                                                    <div class="col-md-2">&nbsp;</div>
                                                    <div class="col-md-2">&nbsp;</div>

                                                    <?php if (@$singleRowData['singleBaseRec']->final_ro_reviewer) { ?>
                                                        <div class="col-md-2">
                                                            <a href="<?= base_url("appraisal_finalockbyio?lockd=" . base64_encode($fldID)); ?>" class="btn btn-success"> Lock </a>
                                                        </div>
                                                    <?php } ?>

                                                </div>

                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
                                                                                        $(document).ready(function () {
                                                                                            $("#myform,#myform2").on("submit", function () {
                                                                                                $("#pageloader").fadeIn();
                                                                                            });
                                                                                        });
    </script>

    <style>
        #pageloader {
            background: rgba( 255, 255, 255, 0.8 );
            display: none;
            height: 100%;
            position: fixed;
            width: 100%;
            z-index: 9999;
        }
        #pageloader img {
            left: 50%;
            margin-left: -32px;
            margin-top: -32px;
            position: absolute;
            top: 50%;
        }
    </style>


    <?php $this->load->view('admin/includes/footer'); ?>

    <script>
        function delactionrow(delid, masterid) {
            var r = confirm("Are You Sure Delete This ? ");
            if (r == true) {
                window.location = "<?= base_url("deletesinglerow"); ?>/" + delid + "/" + masterid;
            }
        }
    </script>

    <style>
        .modal-content {
            margin-top: 157px;
        }
    </style>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" style="color:red" data-dismiss="modal"> X </button>
                    <h4 class="modal-title">Add new Row (<small>  Performance and Potential Evaluation section </small>)</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url("appr_numrowsincre"); ?>" id="myform2">
                        <div class="form-group" style="width:70%">
                            <label for="email">How Many Rows : </label>
                            <input type="number" min="1" max="<?= $finalRows; ?>" class="form-control" id="numrowss" placeholder="eg:- 1,2,3.." name="numrowss">
                        </div>
                        <input type="hidden" name="user_id" value="<?= $singleRowData['singleBaseRec']->user_id ?>">
                        <input type="hidden" name="masterfld_id" value="<?= @$fldID; ?>">
                        <button type="submit" class="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>