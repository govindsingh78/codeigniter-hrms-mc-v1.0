<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a><?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form id="form-filter"> 
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Business Unit :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="businessunit_name" class="select floating form-control">
															<option value="" selected="selected"> Select Business </option>
															<?php 
															$all_businessunit = get_businessunits();
															if ($all_businessunit):
															foreach ($all_businessunit as $unitrow) {
															?>
															<option value="<?= $unitrow->id; ?>"><?= $unitrow->unitname; ?></option>
															<?php
															}
															endif;
															 ?>
															</select>
                                                        </div>
                                                    </div>
													
													<div class="col-lg-3 col-md-6">
                                                        <b>Select Department :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="department_id" class="select floating form-control">
															<option value="" selected="selected"> Select Department </option>
															<?php
															$all_Activedepartment = get_departments();
															if ($all_Activedepartment):
															foreach ($all_Activedepartment as $deprow) {
															?>
															<option value="<?= $deprow->id; ?>"><?= $deprow->deptname; ?></option>
															<?php
															}
															endif;
															?>
															</select>
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-3 col-md-6 prnm" id="projectnm">
													<div>
                                                        <b>Select Project :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="project_id" class="select floating form-control">
															<option value="" selected="selected"> Select Project </option>
															<?php /*
															$all_Activeprojects = get_project_names();
															if ($all_Activeprojects):
															foreach ($all_Activeprojects as $deprow) {
															?>
															<option value="<?= $deprow->id; ?>"><?= $deprow->project_name; ?></option>
															<?php
															}
															endif;
															*/ ?>
															</select>
															</div>
                                                        </div>
                                                    </div>
													
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
														<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                       <th>Sr. No</th>
                        <th>Employee Name</th>
                        <th class="designation_th">Designation</th>
                        <th>Proj Name</th>
                        <th>DOJ</th>
						<th>Billing Rate aft. esclation</th>
						<th>Original Billing Rate</th>
						<th>Remarks</th>
						<th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sr. No</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>Proj Name</th>
                        <th>DOJ</th>
						<th>Billing Rate aft. esclation</th>
						<th>Original Billing Rate</th>
						<th>Remarks</th>
						<th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	<style>
	#projectnm {
		display:none;
	}
	.table tbody tr td, .table tbody th td {
		word-wrap: break-word;
		white-space: normal !important;
	}
	</style>
	<script>
	$('#businessunit_name').on('change', function(){
   // console.log($('#businessunit_name').val());
   // var selected_buss_unit = $('#businessunit_name').val();
   // alert(abc)
    $('#project_id').html('');
    if($('#businessunit_name').val()==3){ 
		document.getElementById("projectnm").style.display = "block";	
		<?php
		$all_Activeprojects = get_project_names(); ?>
		$('#project_id').append('<option value="" selected="selected"> Select Project </option>');
		<?php if ($all_Activeprojects):
		foreach ($all_Activeprojects as $deprow) {
		?>
		
		$('#project_id').append('<option value="<?= $deprow->id; ?>"><?= $deprow->project_name; ?></option>');
		<?php
		}
		endif;
		?>
        
    }else{
        document.getElementById("projectnm").style.display = "none";	
    }
});
	</script>
<script>
// code by durgesh for update exension number field by ajax 
    function update_appr_acc(emplid)
    {
        var bill_rate = $('#bill_rate_' + emplid).val();
        var orig_bill_rate = $('#orig_bill_rate_' + emplid).val();
        var remark = $('#remark_' + emplid).val();
		// alert(bill_rate+', '+orig_bill_rate+', '+remark)
        $.ajax({
            url: "<?= base_url('Appraisal_Controller/updateextAppacc?empid='); ?>" + emplid + '&billrate=' + bill_rate + '&origbillrate=' + orig_bill_rate+ '&remark=' + remark,
            type: "GET",
            dataType: 'json',
            success: function (response) {
                $('#msg_' + emplid).show();
                $('#msg_' + emplid).html('Updated! Record successfully').fadeOut('20000');
            }
        });
    }
</script>
<script type="text/javascript">
   var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [],
            "ajax": {
                "url": "<?= base_url('ajax_list_appraisal_acc') ?>",
                "type": "POST",
                "data": function (data) {                   
                    data.businessunit_name = $('#businessunit_name').val();
                    data.project_id = $('#project_id').val();
                    data.company_name = $('#company_name').val();
                    data.department_id = $('#department_id').val();
                    data.potion_design_id = $('#potion_design_id').val();
                    data.from_date = $('#from_date').val();
                    data.to_date = $('#to_date').val();
					data.userfullname = $('#userfullname').val();
					data.expr_from = $('#expr-from').val();
					data.expr_to = $('#expr-to').val();

                },
            },
            "dom": 'lBfrtip',
            "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
			columnDefs: [{
				orderable: false,
			  },
			  { 
				targets: 3,
				width: '6px'
			  },
			 ],
            //Set column definition initialisation properties.
            // "columnDefs": [{
					// "width": "5px",
                    // "targets": [0,1,2], //first column / numbering column
                    // "orderable": false, //set not orderable
                // },
            // ],
			// "columns": [
				// { "width": "20%" }],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title">Preview Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		
      </div>
      <div class="modal-body">
       <form method="post" action="<?= base_url('reissue_id_card'); ?>" id="visitingcard_form"> 
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Apply Date</b>
															<input readonly="readonly" name="date_of_initiation" type="text" value="<?php echo date("d-m-Y");?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Name</b>
                                                            <input readonly="readonly" name="userfullname" type="text" value="<?= (@$idcardDetailArr->userfullname) ? $idcardDetailArr->userfullname : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Deparatment</b>
                                                            <input readonly="readonly" type="text" step="any" value="<?= (@$idcardDetailArr->department_name) ? $idcardDetailArr->department_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="mb-3">
                                                            <b>Designation</b>
                                                            <input readonly="readonly" name="position_name" type="text" step="any" value="<?= (@$idcardDetailArr->position_name) ? $idcardDetailArr->position_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Company</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="emailaddress" type="text" step="any" value="<?= (@$idcardDetailArr->company_name) ? $idcardDetailArr->company_name : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
													 <div class="col-lg-4 col-md-6">
                                                        <b>Date of Joining</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="joining_date" type="text" step="any" value="<?= (@$idcardDetailArr->date_of_joining) ? $idcardDetailArr->date_of_joining : ""; ?>" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6">
                                                        <b>Reason for Re-issue</b>
                                                        <div class="input-group mb-3">
                                                            <input readonly="readonly" name="reason_reissue" id="reason_reissue" type="text" step="any" value="" id="" class="form-control">
                                                        </div>
                                                    </div>
													<div class="col-lg-4 col-md-6">
                                                        <div class="mb-4">
                                                            <b></b>
                                                            <input  type="submit" name="submit" class="btn btn-round btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
<script>
$('#modal_click').on('click', function (e) {
	
    var value = $('#select_reason').val();
    $('#reason_reissue').val(value);
});
</script>