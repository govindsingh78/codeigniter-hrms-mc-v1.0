<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>My Appraisal</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>            
                    </div>
                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>



                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body">
                                <div class="tab-content">
                                    <div class="body"> 
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                <thead>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Name</th>
                                                        <th>EmployeeID</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Status</th>
                                                        <th>View</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    if ($RecAsRolist) {
                                                        foreach ($RecAsRolist as $kkEy => $reCd) {
                                                            $apprStatus = LastApprStatusByuserID($reCd->user_id);
                                                            $StAtus = '';
                                                            if (($apprStatus->emp_ro_lock == "1") and ( $apprStatus->emp_io_lock == "1")) {
                                                                $StAtus = "<span style='color:green'>Submitted</span>";
                                                            }
                                                            if (($apprStatus->emp_ro_lock == "") and ( $apprStatus->emp_io_lock == "1")) {
                                                                $StAtus = "<span style='color:red'>Pending</span>";
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?= $kkEy + 1; ?></td>
                                                                <td><?= ($reCd->userfullname) ? $reCd->userfullname : ""; ?></td>
                                                                <td><?= ($reCd->employeeId) ? $reCd->employeeId : ""; ?></td>
                                                                <td><?= ($reCd->emailaddress) ? $reCd->emailaddress : ""; ?></td>
                                                                <td><?= ($reCd->contactnumber) ? $reCd->contactnumber : ""; ?></td>
                                                                <td>
                                                                    <?= $StAtus; ?>
                                                                </td>

                                                                <td>
                                                                    <?php
                                                                     if (($apprStatus->emp_ro_lock == "") and ( $apprStatus->emp_io_lock == "1")) {
                                                                        echo '<a href="' . base_url('appraisalviewasio/' . $reCd->fld_id) . '">View</a>';
                                                                    } else {
                                                                        echo '<span style="color:red"></span>';
                                                                    }
                                                                    ?>
                                                                </td>
        <!--                                                                <td>-</td>-->
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Name</th>
                                                        <th>EmployeeID</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Status</th>
                                                        <th>View</th>
<!--                                                        <th>if(CEGTH)</th>-->
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
</body>


<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>