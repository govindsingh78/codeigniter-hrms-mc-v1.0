<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">

                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>My Appraisal</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>            
                    </div>
                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body">  
                                <div class="tab-content">
                                    <div class="body"> 
                                        
                                        <a href="<?= base_url("apprai_projsiteofc"); ?>">
                                            <button class="btn btn-primary"> Click to View Your Team (CEG Project) </button>
                                        </a>
                                        <p>&nbsp;</p>
                                        
                                        <div class="table-responsive">
                                            <table class="table">
                                                <!--                 Asheesh Static Class    table-bordered table-striped table-hover dataTable js-exportable-->
                                                <thead>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Name</th>
                                                        <th>EmployeeID</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Last Appr. Status</th>
                                                        <th>View</th>  
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    if ($Emplylist) {
                                                        foreach ($Emplylist as $kKey => $reDd) {

                                                            $ApprStatus = LastApprStatusByuserID($reDd->user_id);

                                                            $StAtus = '';
                                                            if (($ApprStatus->emp_io_lock == "") and ( $ApprStatus->emp_self_lock == "1")) {
                                                                $StAtus = "<span style='color:red'>Pending</span>";
                                                            }
                                                            if (($ApprStatus->emp_io_lock == "1") and ( $ApprStatus->emp_self_lock == "1")) {
                                                                $StAtus = "<span style='color:green'>Submitted </span>";
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?= $kKey + 1; ?></td>
                                                                <td><?= ($reDd->userfullname) ? $reDd->userfullname : ""; ?></td>
                                                                <td><?= ($reDd->employeeId) ? $reDd->employeeId : ""; ?></td>
                                                                <td><?= ($reDd->emailaddress) ? $reDd->emailaddress : ""; ?></td>
                                                                <td><?= ($reDd->contactnumber) ? $reDd->contactnumber : ""; ?></td>
                                                                <td><?= $StAtus; ?></td>
                                                                <td>
                                                                    <?php
                                                                    if (($ApprStatus->emp_io_lock == "") and ( $ApprStatus->emp_self_lock == "1")) {
                                                                        echo '<a href="' . base_url('appraisalview/' . $reDd->fld_id) . '">View</a>';
                                                                    } else {
                                                                        echo '<span style="color:red"></span>';
                                                                    }
                                                                    ?>                                                      
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>S.No.</th>
                                                        <th>Name</th>
                                                        <th>EmployeeID</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
<!--                                                        <th>Appraisal Cycle</th>-->
                                                        <th>Last Appr. Status</th>
                                                        <th>View</th>
<!--                                                        <th>if(CEGTH)</th>-->
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
    </div>
</body>

<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>