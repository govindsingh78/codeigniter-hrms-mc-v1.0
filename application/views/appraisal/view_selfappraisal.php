<?php
$fldID = $singleRowData['singleBaseRec']->fld_id;
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$recappraisalArr = GetAllAppraisalMaster();
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>View Appraisal Form Data</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="profile-img-wrap">
                                                <?php if ($singleRowData['singleBaseRec']->profileimg) { ?>
                                                    <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/uploads/profile/<?= $singleRowData['singleBaseRec']->profileimg; ?>" alt="user">
                                                <?php } else { ?>
                                                    <img class="inline-block" height="150" src="http://hrms.cegindia.com/public/media/images/employee-deafult-pic.jpg" alt="user">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="control-label">Employee Name : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->userfullname) ? @$singleRowData['singleBaseRec']->userfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">
                                                    <label class="control-label">Date of Joining : </label>
                                                    <div class="form-group form-focus focused">
                                                        <div class="cal-icon">
                                                            <input class="form-control floating datetimepicker" readonly="" value="<?= (@$singleRowData['singleBaseRec']->date_of_joining) ? date("d-m-Y", strtotime($singleRowData['singleBaseRec']->date_of_joining)) : ""; ?>" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Department : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->department_name) ? @$singleRowData['singleBaseRec']->department_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Designation : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->position_name) ? @$singleRowData['singleBaseRec']->position_name : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of IO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->rouserfullname) ? @$singleRowData['singleBaseRec']->rouserfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label">Name of RO : </label>
                                                    <div class="form-group form-focus focused">
                                                        <input type="text" readonly="" value="<?= (@$singleRowData['singleBaseRec']->iouserfullname) ? @$singleRowData['singleBaseRec']->iouserfullname : ""; ?>" class="form-control floating">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-12"> 
                                            <div class="card-box">
                                                <h5 class="heading-title text-center">Performance and Potential Evaluation</h5>
                                                <h5 class="card-title text-center">Kindly rate in accordance with the following performance indicators that most closely describes employee performance </h5>
                                                <h6 class="card-title text-center">(9-10: Excellent | 7-8: Good | 5-6: Average| 3-4:Poor|1-2: Very Poor)</h5><br/>
                                                    <br/>
                                                    <span class="card-title">Performance indicator on 1-10 scale (Rating)</span>
                                                    <br/>
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th><b>S.No.</b></th>
                                                                <th><b>Performance indicator</b></th>
                                                                <th><b>Self Rating (1-10)</b></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if ($recappraisalArr) {
                                                                foreach ($recappraisalArr as $kEy => $recRow) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><b><?= $recRow->srno; ?></b></td>
                                                                        <td colspan='4'><b><?= $recRow->performfact_name; ?></b></td>
                                                                    </tr>
                                                                    <?php
                                                                    $totVal = 0;
                                                                    $totVal2 = 0;
                                                                    $laYer2 = GetAllAppraisalMasterLsecond(@$recRow->fld_id);
                                                                    if ($laYer2) {
                                                                        foreach ($laYer2 as $kEy2 => $recRow2) {
                                                                            $ROW3e = AppraisalRowSec($fldID, @$recRow2->fld_id);
                                                                            // $totVal += $ROW3e->perform_fact_rating;
                                                                            // $totVal2 += @$ROW3e->ro_rating;
                                                                            ?> 
                                                                            <tr>
                                                                                <td><?= ($kEy2 + 1); ?>
                                                                                    &nbsp;<input type="hidden" <?= (@$ROW3e->perform_fact_rating) ? "checked" : ""; ?> name="chkagree3[]" value="<?= @$ROW3e->fld_id; ?>" />
                                                                                </td>
                                                                                <td><?= $recRow2->performfact_name; ?></td>
                                                                                <td>
                                                                                    <select readonly id="performance_rating_self_<?= @$ROW3e->fld_id; ?>" name="performance_rating_self_<?= @$ROW3e->fld_id; ?>" class="form-control">
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "non") ? "selected" : ""; ?> value=""> None of These </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "1") ? "selected" : ""; ?> value="1"> 1 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "2") ? "selected" : ""; ?> value="2"> 2 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "3") ? "selected" : ""; ?> value="3"> 3 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "4") ? "selected" : ""; ?> value="4"> 4 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "5") ? "selected" : ""; ?> value="5"> 5 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "6") ? "selected" : ""; ?> value="6"> 6 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "7") ? "selected" : ""; ?> value="7"> 7 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "8") ? "selected" : ""; ?> value="8"> 8 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "9") ? "selected" : ""; ?> value="9"> 9 </option>
                                                                                        <option <?= (@$ROW3e->perform_fact_rating == "10") ? "selected" : ""; ?> value="10"> 10 </option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                            </div> 

                                            <div class="card-box">
                                                <h5 class="heading-title text-center">Pen Picture.</h5>
                                                <br/>
                                                <div class="card-two">
                                                    <?php if ($singleRowData['RecRow2']) { ?>
                                                        <?php
                                                        foreach ($singleRowData['RecRow2'] as $reCr2) {
                                                            if ($reCr2->proj_other == 2) {
                                                                $projName = get_project_name($reCr2->project_special_taskassign);
                                                            }
                                                            ?>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label class="control-label email">Project / Special Task Assigned : </label>
                                                                    <div class="form-group form-focus">
                                                                        <?php if ($reCr2->proj_other == 1) { ?>
                                                                            <input type="text" title="<?= $reCr2->project_special_taskassign; ?>" readonly class="form-control floating" value="<?= $reCr2->project_special_taskassign; ?>" name="project_special_taskassign_<?= @$reCr2->fld_id; ?>" />
                                                                        <?php } ?>
                                                                        <?php if ($reCr2->proj_other == 2) { ?>
                                                                            <span style="color:green"> <?= $projName; ?> </span>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label class="control-label">Activities Performed : </label>
                                                                    <div class="form-group form-focus">
                                                                        <input type="text" title="<?= $reCr2->activ_perform; ?>" readonly class="form-control floating"  value="<?= $reCr2->activ_perform; ?>" name="activ_perform_<?= @$reCr2->fld_id; ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>


                                            <div class="card-box">
                                                <h5 class="heading-title text-center"> Remarks at the end of the appraisal review meeting of appraisee with IO </h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="control-label email"> A). &nbsp; Existing skills / knowledge not utilized in current job profile : </label>
                                                        <div class="form-group form-focus">
                                                            <textarea class="form-control floating" readonly name="quest1" id="quest1" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_1; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="control-label email"> B). &nbsp; New skills / knowledge acquired in review period  : </label>
                                                        <div class="form-group form-focus">
                                                            <textarea class="form-control floating" readonly name="quest2" id="quest2" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_2; ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label class="control-label email"> C). &nbsp; Skills / knowledge that you feel need to be upgraded for better performance : </label>
                                                        <div class="form-group form-focus">
                                                            <textarea class="form-control floating" readonly  name="quest3" id="quest3" rows="3"><?= $singleRowData['singleBaseRec']->quest_answ_3; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('admin/includes/footer'); ?>

    <style>
        .modal-content {
            margin-top: 157px;
        }
    </style>
</body>