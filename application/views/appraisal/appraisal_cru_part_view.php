<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a><?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="accordion" id="accordion">
                                    <div>
									<form id="form-filter"> 
                                        <!--<div class="card-header" id="headingOne">-->
                                            <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-6">
                                                        <b>Business Unit :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="businessunit_name" class="select floating form-control">
															<option value="" selected="selected"> Select Business </option>
															<?php 
															$all_businessunit = get_businessunits();
															if ($all_businessunit):
															foreach ($all_businessunit as $unitrow) {
															?>
															<option value="<?= $unitrow->id; ?>"><?= $unitrow->unitname; ?></option>
															<?php
															}
															endif;
															 ?>
															</select>
                                                        </div>
                                                    </div>
													
													<div class="col-lg-3 col-md-6">
                                                        <b>Select Department :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="department_id" class="select floating form-control">
															<option value="" selected="selected"> Select Department </option>
															<?php
															$all_Activedepartment = get_departments();
															if ($all_Activedepartment):
															foreach ($all_Activedepartment as $deprow) {
															?>
															<option value="<?= $deprow->id; ?>"><?= $deprow->deptname; ?></option>
															<?php
															}
															endif;
															?>
															</select>
                                                        </div>
                                                    </div>
													
													<div class="col-lg-3 col-md-6 prnm" id="projectnm">
													<div>
                                                        <b>Select Project :</b>
                                                        <div class="input-group mb-3">
                                                            
                                                           <select id="project_id" class="select floating form-control">
															<option value="" selected="selected"> Select Project </option>
															<?php /*
															$all_Activeprojects = get_project_names();
															if ($all_Activeprojects):
															foreach ($all_Activeprojects as $deprow) {
															?>
															<option value="<?= $deprow->id; ?>"><?= $deprow->project_name; ?></option>
															<?php
															}
															endif;
															*/ ?>
															</select>
															</div>
                                                        </div>
                                                    </div>
													
                                                    
                                                    <div class="col-lg-1 col-md-6">
                                                        <div class="mb-2">
                                                            <b></b>
														<button type="button" id="btn-filter" class="btn btn-success btn-block" style="margin-top:20px;"> Filter </button>
                                                        </div>
                                                    </div>
													<div class="col-lg-1 col-md-6">
                                                        
														<div class="mb-2">
                                                            <b></b>
															<button type="button" id="btn-reset" class="btn btn-primary btn-block" style="margin-top:20px;"> Reset </button>
                                                        </div>
                                                    </div>
													
													
                                                </div>
                                        <!--</div> -->                               
                                        
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                       <th>Sr. No</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>DOJ</th>
                        <th>Project Name</th>
                        <th>Qualification</th>
                        <th>University</th>
                        <th>Passing Year</th>
                        <th>Exp</th>
						<th>Total Marks</th>
						<th>Marks Obtained</th>
						<th>Criticality</th>
						<th>Market Rate</th>
						<th>Remarks</th>
						<th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sr. No</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>DOJ</th>
                        <th>Project Name</th>
                        <th>Qualification</th>
                        <th>University</th>
                        <th>Passing Year</th>
                        <th>Exp</th>
						<th>Total Marks</th>
						<th>Marks Obtained</th>
						<th>Criticality</th>
						<th>Market Rate</th>
						<th>Remarks</th>
						<th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	<style>
	#projectnm {
		display:none;
	}
	.table tbody tr td, .table tbody th td {
		word-wrap: break-word;
		white-space: normal !important;
	}
	</style>
	<script>
	$('#businessunit_name').on('change', function(){
   // console.log($('#businessunit_name').val());
   // var selected_buss_unit = $('#businessunit_name').val();
   // alert(abc)
    $('#project_id').html('');
    if($('#businessunit_name').val()==3){ 
		document.getElementById("projectnm").style.display = "block";	
		<?php
		$all_Activeprojects = get_project_names(); ?>
		$('#project_id').append('<option value="" selected="selected"> Select Project </option>');
		<?php if ($all_Activeprojects):
		foreach ($all_Activeprojects as $deprow) {
		?>
		
		$('#project_id').append('<option value="<?= $deprow->id; ?>"><?= $deprow->project_name; ?></option>');
		<?php
		}
		endif;
		?>
        
    }else{
        document.getElementById("projectnm").style.display = "none";	
    }
});
	</script>
<script>
// code by durgesh for update exension number field by ajax 
    function update_appr_cru(emplid)
    {
        // var total_marks = $('#total_marks_' + emplid);
		// alert(total_marks)
        var total_marks = $('#total_marks_' + emplid).val();
        var marks_obtn = $('#marks_obt' + emplid).val();
        var criticality = $('#criticality' + emplid).val();
        var marks_rate = $('#market_rate' + emplid).val();
        var remarks = $('#remarks_' + emplid).val();
		// alert(total_marks+', '+marks_obtn+', '+criticality+', '+marks_rate+', '+remarks)
        $.ajax({
            url: "<?= base_url('Appraisal_Controller/updateextAppCru?empid='); ?>" + emplid + '&total_marks=' + total_marks + '&marks_obtn=' + marks_obtn+ '&criticality=' + criticality+ '&marks_rate=' + marks_rate+ '&remarks=' + remarks,
            type: "GET",
            dataType: 'json',
            success: function (response) {
                $('#msg_' + emplid).show();
                $('#msg_' + emplid).html('Updated! Record successfully').fadeOut('10000');
            }
        });
    }
</script>
<script type="text/javascript">
   var table;
    $(document).ready(function () {
        table = $('#table').removeAttr('width').DataTable({
            // "scrollY": '500px',
			// "scrollX": true,
			// "scrollCollapse": true,
			"processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [],
            "ajax": {
                "url": "<?= base_url('ajax_list_appraisal_cru') ?>",
                "type": "POST",
                "data": function (data) {                   
                    data.businessunit_name = $('#businessunit_name').val();
                    data.project_id = $('#project_id').val();
                    data.company_name = $('#company_name').val();
                    data.department_id = $('#department_id').val();
                    data.potion_design_id = $('#potion_design_id').val();
                    data.from_date = $('#from_date').val();
                    data.to_date = $('#to_date').val();
					data.userfullname = $('#userfullname').val();
					data.expr_from = $('#expr-from').val();
					data.expr_to = $('#expr-to').val();

                },
            },
            "dom": 'lBfrtip',
            "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
			// "columns": [
				// { "width": "50%" }],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });
</script>
</body>