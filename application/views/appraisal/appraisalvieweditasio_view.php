<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>My Appraisal</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                        <a href="<?= base_url("employee_appraisal_io"); ?>">
                            <button style="height:37px; margin-top:10px" class="btn btn-primary btn-sm"> << Back </button>
                        </a>
                    </div>
                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Danger ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 left-box">
                        <div class="card single_post">
                            <div class="body">  


                                <div class="tab-content">
                                    <div class="body"> 
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>EmpName</th>
                                                        <th>IO Name</th>
                                                        <th> Edit </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($allAppraisal): ?>
                                                        <?php foreach ($allAppraisal as $kYk => $rEcR) { ?>
                                                            <tr>
                                                                <td><?= $kYk + 1; ?></td>
                                                                <td><?= $rEcR->userfullname; ?></td>
                                                                <td><?= $rEcR->reporting_manager_name; ?></td>
                                                                <td>
                                                                    <?php if (($rEcR->emp_io_lock == "1") and ( $rEcR->emp_ro_lock == "") and ( $rEcR->emp_self_lock == "1")) { ?>
                                                                        <a href="<?= base_url("appraisalviewdetailasio/" . $rEcR->fld_id); ?>"> Edit </a>
                                                                    <?php } ?>
                                                                    <?php if ($rEcR->emp_io_lock == "") { ?>
                                                                        <span style="color:red">Pending by IO</span>
                                                                    <?php } ?>
                                                                    <?php if (($rEcR->emp_io_lock == "1") and ( $rEcR->emp_ro_lock == "1") and ( $rEcR->emp_self_lock == "1")) { ?>
                                                                        <span style="color:green"> <i title="Locked" class="fa fa-lock fa-2x"></i> </span>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        <?php } endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
</body>


<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>