<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$TsSingleRecRows = "0";
$TodayDate_Global = date("d-m-Y");
$allProjectArr = get_project_names();
?>

<body class="theme-cyan">
    <div id="pageloader">
        <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
    </div>
    <div id="wrapper">
        <?php $this->load->view('admin/includes/sidebar'); ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($this->session->flashdata('successmsg')) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('errormsg')) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> Error ! </strong> <?= $this->session->flashdata('errormsg'); ?>
                        </div>
                    <?php } ?>
                </div>


                <div class="card">
                    <div class="header">
                        <div class="row">
                            <div class="col-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" autocomplete="off" id="month_change_picker" value="<?= $thisMonthName; ?> <?= $thisYear; ?>" class="date-picker form-control"  >
                                </div> &nbsp;
                            </div>

                            <div class="col-4">
                                <div class="btn-group">
                                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Week <?= $MonthWeek ?> </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?= base_url("timesheet_entry/01/" . $thisMonth . "/" . $thisYear); ?>">Week-1</a>
                                        <a class="dropdown-item" href="<?= base_url("timesheet_entry/02/" . $thisMonth . "/" . $thisYear); ?>">Week-2</a>
                                        <a class="dropdown-item" href="<?= base_url("timesheet_entry/03/" . $thisMonth . "/" . $thisYear); ?>">Week-3</a>
                                        <a class="dropdown-item" href="<?= base_url("timesheet_entry/04/" . $thisMonth . "/" . $thisYear); ?>">Week-4</a>
                                        <a class="dropdown-item" href="<?= base_url("timesheet_entry/05/" . $thisMonth . "/" . $thisYear); ?>">Week-5</a>
                                    </div>
                                </div>
                                <p class="form-text text-muted">
                                    <small>
                                        <?= date("F d, Y", strtotime($weekDateRangeArr['start'])); ?> - <?= date("F d, Y", strtotime($weekDateRangeArr['end'])); ?>
                                    </small>
                                </p>
                            </div>
                            <div class="col-4">
                                <div class="mail-compose m-b-20">
                                    <?php if (@$TsFlag == "") { ?>
                                        <a href="<?= base_url("timesheet_entry?flag=recent"); ?>" class="btn btn-danger btn-block">Recent Timesheet</a>
                                    <?php } ?>
                                    <?php if (@$TsFlag) { ?>
                                        <a href="<?= base_url("timesheet_entry"); ?>" class="btn btn-danger btn-block">All Project Timesheet</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="body table-responsive">
                        <form action="<?= base_url('ts_savesubmit'); ?>" id="frmmDate" method="post">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Project</th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[0])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[0])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[0]))); ?>, <?= date("Y", strtotime($DateRangeArr[0])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_mon" aria-expanded="false" aria-controls="collapse_mon"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_mon">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_mon" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->mon_note) ? $tSNotesArr->mon_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>

                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[1])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[1])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[1]))); ?>, <?= date("Y", strtotime($DateRangeArr[1])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_tue" aria-expanded="false" aria-controls="collapse_tue"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_tue">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_tue" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->tue_note) ? $tSNotesArr->tue_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[2])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[2])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[2]))); ?>, <?= date("Y", strtotime($DateRangeArr[2])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_wed" aria-expanded="false" aria-controls="collapse_wed"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_wed">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_wed" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->wed_note) ? $tSNotesArr->wed_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[3])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[3])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[3]))); ?>, <?= date("Y", strtotime($DateRangeArr[3])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_thu" aria-expanded="false" aria-controls="collapse_thu"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_thu">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_thu" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->thu_note) ? $tSNotesArr->thu_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[4])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[4])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[4]))); ?>, <?= date("Y", strtotime($DateRangeArr[4])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_fri" aria-expanded="false" aria-controls="collapse_fri"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_fri">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_fri" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->fri_note) ? $tSNotesArr->fri_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[5])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[5])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[5]))); ?>, <?= date("Y", strtotime($DateRangeArr[5])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_sat" aria-expanded="false" aria-controls="collapse_sat"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_sat">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_sat" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->sat_note) ? $tSNotesArr->sat_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <?= date("D", strtotime($DateRangeArr[6])); ?><br>
                                            <small>
                                                <?= date("d", strtotime($DateRangeArr[6])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[6]))); ?>, <?= date("Y", strtotime($DateRangeArr[6])); ?>
                                            </small>
                                            <br><a role="button" style="color:white" data-toggle="collapse" href="#collapse_sun" aria-expanded="false" aria-controls="collapse_sun"><i class="icon-bubbles"></i></a>
                                            <div class="collapse m-t-2" id="collapse_sun">
                                                <div class="well">
                                                    <div class="form-group">
                                                        <textarea rows="2" name="dnote_sun" class="form-control no-resize" maxlength="150" placeholder="Day Note (Max 150).. "><?= (@$tSNotesArr->sun_note) ? $tSNotesArr->sun_note : ""; ?></textarea>
                                                    </div> 
                                                </div>
                                            </div>
                                        </th>
                                        <th>Hours</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <?php
                                    if (@$AllProjRecdArr) {
                                        foreach (@$AllProjRecdArr as $kEyy => $rOwS) {
                                            ?>
                                            <tr>
                                                <th colspan="9">
                                                    <ul id="myUL">
                                                        <li><span class="caret"><?= (@$rOwS['project_name']) ? @$rOwS['project_name'] : ""; ?></span>
                                                            <ul class="nested">
                                                                <table class="table">
                                                                    <tr>
                                                                        <th>Task</th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[0])); ?> <?= date("d", strtotime($DateRangeArr[0])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[0]))); ?>, <?= date("Y", strtotime($DateRangeArr[0])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[1])); ?> <?= date("d", strtotime($DateRangeArr[1])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[1]))); ?>, <?= date("Y", strtotime($DateRangeArr[1])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[2])); ?> <?= date("d", strtotime($DateRangeArr[2])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[2]))); ?>, <?= date("Y", strtotime($DateRangeArr[2])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[3])); ?> <?= date("d", strtotime($DateRangeArr[3])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[3]))); ?>, <?= date("Y", strtotime($DateRangeArr[3])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[4])); ?> <?= date("d", strtotime($DateRangeArr[4])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[4]))); ?>, <?= date("Y", strtotime($DateRangeArr[4])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[5])); ?> <?= date("d", strtotime($DateRangeArr[5])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[5]))); ?>, <?= date("Y", strtotime($DateRangeArr[5])); ?> </small></th>
                                                                        <th><small><?= date("D", strtotime($DateRangeArr[6])); ?> <?= date("d", strtotime($DateRangeArr[6])); ?> <?= ShortMonthName(date("m", strtotime($DateRangeArr[6]))); ?>, <?= date("Y", strtotime($DateRangeArr[6])); ?> </small></th>
                                                                        <th><small>Tot</small></th>
                                                                    </tr>
                                                                    <?php
                                                                    if (@$rOwS['taskDetailsArr']) {
                                                                        foreach (@$rOwS['taskDetailsArr'] as $tKEy => $tRows) {
                                                                            $whereArr = ["emp_id" => $this->session->userdata('loginid'), "project_task_id" => $tRows['project_task_id'], "ts_year" => $thisYear, "ts_month" => $thisMonth, "ts_week" => $MonthWeek, "cal_week" => $yearWeek];
                                                                            $TsSingleRecRows = weeklyTsRecByprojTaskID($whereArr);
                                                                            ?>
                                                                            <tr>
                                                                                <td style="color:#000000;"><small><?= (@$tRows['task']) ? @$tRows['task'] : ""; ?></small></td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->mon_duration) ? @$TsSingleRecRows->mon_duration : ""; ?>" name="mondur[]" id="mondur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[0]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[0]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->tue_duration) ? @$TsSingleRecRows->tue_duration : ""; ?>" name="tuedur[]" id="tuedur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[1]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[1]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->wed_duration) ? @$TsSingleRecRows->wed_duration : ""; ?>" name="weddur[]" id="weddur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[2]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[2]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->thu_duration) ? @$TsSingleRecRows->thu_duration : ""; ?>" name="thudur[]" id="thudur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[3]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[3]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->fri_duration) ? @$TsSingleRecRows->fri_duration : ""; ?>" name="fridur[]" id="fridur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[4]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[4]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->sat_duration) ? @$TsSingleRecRows->sat_duration : ""; ?>" name="satdur[]" id="satdur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[5]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[5]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <td>
                                                                                    <small>
                                                                                        <input value="<?= (@$TsSingleRecRows->sun_duration) ? @$TsSingleRecRows->sun_duration : ""; ?>" name="sundur[]" id="sundur_<?= $tRows['project_task_id']; ?>" type="time" class="form-control" min="00:00" max="18:59" <?= (strtotime($DateRangeArr[6]) > strtotime($TodayDate_Global) OR ( strtotime($FirstDate_Month) > strtotime($DateRangeArr[6]))) ? "readonly" : ""; ?> />
                                                                                    </small>
                                                                                </td>
                                                                                <th><?= (@$TsSingleRecRows->week_duration) ? @$TsSingleRecRows->week_duration : "00:00"; ?></th>
                                                                            <input type="hidden" name="taskidarr[]" value="<?= $tRows['project_task_id']; ?>">
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </th>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <th><?= (@$SumTsDurationArr['Mon_DurationSum']) ? @$SumTsDurationArr['Mon_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Tue_DurationSum']) ? @$SumTsDurationArr['Tue_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Wed_DurationSum']) ? @$SumTsDurationArr['Wed_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Thu_DurationSum']) ? @$SumTsDurationArr['Thu_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Fri_DurationSum']) ? @$SumTsDurationArr['Fri_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Sat_DurationSum']) ? @$SumTsDurationArr['Sat_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Sun_DurationSum']) ? @$SumTsDurationArr['Sun_DurationSum'] : ""; ?></th>
                                        <th><?= (@$SumTsDurationArr['Week_DurationSum']) ? @$SumTsDurationArr['Week_DurationSum'] : ""; ?></th>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- Other official Activities-->
                            <div class="row">
                                <div class="col-2">
                                    <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary"> Other Official Activities </button>
                                </div> &nbsp;
                                <div class="col-7">
                                    <textarea placeholder="Weekly Note.." maxlength="200" cols="36" name="weeklynote"><?= (@$tSNotesArr->week_note) ? $tSNotesArr->week_note : ""; ?></textarea>
                                </div> &nbsp;
                                <div class="col-2">
                                    <input type="hidden" name="thismonth" value="<?= $thisMonth; ?>">
                                    <input type="hidden" name="thisYear" value="<?= $thisYear; ?>">
                                    <input type="hidden" name="todayDate" value="<?= $TodayDate_Global; ?>">
                                    <input type="hidden" name="thismonth" value="<?= $thisMonth; ?>">
                                    <input type="hidden" name="MonthWeek" value="<?= $MonthWeek; ?>">
                                    <input type="hidden" name="yearWeek" value="<?= $yearWeek; ?>">
                                    <input type="hidden" name="yearWeek" value="<?= $yearWeek; ?>">
                                    <input type="hidden" name="from_date" value="<?= $weekDateRangeArr['start']; ?>">
                                    <input type="hidden" name="to_date" value="<?= $weekDateRangeArr['end']; ?>">
                                    <input type="submit" name="save" class="btn btn-primary" value="Save"> &nbsp;&nbsp;
                                    <input type="submit" name="submit" class="btn btn-primary" value="Submit & Lock">
                                </div>
                            </div>
                        </form>



                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content" style="width:140%">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Another Project Activities</h4>
                                        <button style="color:red" type="button" class="btn btn-default" data-dismiss="modal">X</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Home-withicon"><i class="fa fa-home"></i> Other Activities</a></li>
                                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-withicon"><i class="fa fa-user"></i> History </a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane show active" id="Home-withicon">
                                                            <div class="body">
                                                                <form action="<?= base_url("save_extra_actvt"); ?>" id="basic-form" method="post">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label class="email"> Project : <span id="reqd"> * </span></label>
                                                                                <select required="required" class="form-control show-tick ms select2" name="ext_project_id" id="ext_project_id" data-placeholder="Select">
                                                                                    <option value=""> -- Select -- </option>
                                                                                    <?php
                                                                                    if ($allProjectArr) {
                                                                                        foreach ($allProjectArr as $RoWs) {
                                                                                            ?>
                                                                                            <option value="<?= $RoWs->id; ?>"><?= $RoWs->project_name; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label class="email"> Date : <span id="reqd"> * </span></label>
                                                                                <input required="required" type="date" class="form-control" name="ext_actdate" id="ext_actdate" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label class="email"> Duration / Time : <span id="reqd"> * </span></label>
                                                                                <input required="required" type="time" min="00:15" max="18:59" class="form-control" name="ext_duration" id="ext_duration" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="form-group">
                                                                                <label class="email"> Work Details : </label>
                                                                                <textarea class="form-control" rows="2"  cols="12" name="ext_workdetails" id="ext_workdetails"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label class="email"> &nbsp; </label> <br>
                                                                                <input type="submit" class="btn btn-primary" name="submit" >
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="Profile-withicon">
                                                            <div class="body table-responsive">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sr.No.</th>
                                                                            <th>Project</th>
                                                                            <th>Date</th>
                                                                            <th>Duration</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        if ($TsAnotherArr) {
                                                                            foreach ($TsAnotherArr as $kEEY => $tsrecD) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td> <?= $kEEY + 1; ?></td>
                                                                                    <td><?= $tsrecD->project_name; ?></td>
                                                                                    <td><?= ($tsrecD->entry_date) ? date("d-m-Y", strtotime($tsrecD->entry_date)) : ""; ?></td>
                                                                                    <td><?= $tsrecD->working_hour; ?></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>Sr.No.</th>
                                                                            <th>Project</th>
                                                                            <th>Date</th>
                                                                            <th>Duration</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script>
                $(document).ready(function () {
                    $("#basic-form,#frmmDate").on("submit", function () {
                        $("#pageloader").fadeIn();
                    });
                });
            </script>
            <style>
                #pageloader{
                    background: rgba( 255, 255, 255, 0.8 );
                    display: none;
                    height: 100%;
                    position: fixed;
                    width: 100%;
                    z-index: 9999;
                }
                #pageloader img{
                    left: 50%;
                    margin-left: -32px;
                    margin-top: -32px;
                    position: absolute;
                    top: 50%;
                }
            </style>

            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
    </div>

</body>


<style>
    ul, #myUL {  list-style-type: none;  }
    #myUL {  margin: 0;  padding: 0; }
    .caret {  cursor: pointer;
              -webkit-user-select: none;
              -moz-user-select: none; /* Firefox 2+ */
              -ms-user-select: none; /* IE 10+ */
              user-select: none; }
    .caret::before {  content: "\25B6"; color: black;  display: inline-block;  margin-right: 6px; }
    .nested { display: none; }
    .active {  display: block;  }
    .ui-datepicker-calendar {
        display: none;
    }
    div#collapse_mon,div#collapse_tue,div#collapse_wed,div#collapse_thu,div#collapse_fri,div#collapse_sat,div#collapse_sun {
        width: 50%;
    }

</style>

<script type="text/javascript">
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
    }
    $(function () {
        $('.date-picker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function (dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                var mydate = new Date(inst.selectedYear, inst.selectedMonth, 1);
                window.location = "<?= base_url("timesheet_entry/01/"); ?>" + (inst.selectedMonth + 1) + "/" + inst.selectedYear;
            }
        });
    });

</script>