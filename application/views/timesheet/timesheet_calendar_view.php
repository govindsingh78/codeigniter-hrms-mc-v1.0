<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>

<body class="theme-cyan">

    <div id="wrapper">
        <?php $this->load->view('admin/includes/sidebar'); ?>
        <div id="main-content">
            <div class="container-fluid">

                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home </li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-12">
                                    <div id='calendar'></div>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Calender Section.. Start -->
            <link href='<?= HOSTNAME ?>calender/packages/core/main.css' rel='stylesheet' /> 
            <script src='<?= HOSTNAME ?>calender/packages/core/main.js'></script>
            <script src='<?= HOSTNAME ?>calender/packages/interaction/main.js'></script>
            <script src='<?= HOSTNAME ?>calender/packages/daygrid/main.js'></script>
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    var calendarEl = document.getElementById('calendar');
                    var calendar = new FullCalendar.Calendar(calendarEl, {
                        plugins: ['interaction', 'dayGrid'],
                        defaultDate: '<?= date("Y-m-d"); ?>',
                        editable: true,
                        eventLimit: true,
                        firstDay: 1,
                        events: [<?= $LeaveAllRecJson . ","; ?> <?= $TourAllRecJson . "," ?> <?= $HoliDaysAllRecJson . "," ?> <?= $HoliDaysAllRecJsonPL . "," ?> <?= $TsMonRecJson . "," ?> <?= $TsTueRecJson . "," ?> <?= $TsThuRecJson . "," ?> <?= $TsWedRecJson . "," ?> <?= $TsFriRecJson . "," ?> <?= $TsSatRecJson . "," ?> <?= $TsSunRecJson . "," ?>]
                    });
                    calendar.render();
                });
            </script>
            <style>
                #calendar {
                    max-width: 77%;
                    margin: 0 auto;
                }
            </style>
            <!-- Calender Section End -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <?php $this->load->view('admin/includes/footer'); ?>
        </div>
    </div>
</body>




