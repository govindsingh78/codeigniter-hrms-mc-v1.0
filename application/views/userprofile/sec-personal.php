

<form action="" method="post" id="personnelForm">
<div class="row clearfix highlight m-1">
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mother's Name : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->mother_nm) ? @$PersonalDetailRecArr->mother_nm : "-"; ?>" name="mother_nm" id="mother_nm"   class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mother's Contact No : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->mothers_contact_no) ? @$PersonalDetailRecArr->mothers_contact_no : "-"; ?>" name="mothers_contact_no" id="mothers_contact_no"   class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Father's Name : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->father_nm) ? @$PersonalDetailRecArr->father_nm : "-"; ?>" name="father_nm" id="father_nm"  class="form-control">
        </div>
    </div>

     <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Father's Contact No : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->fathers_contact_no) ? @$PersonalDetailRecArr->fathers_contact_no : "-"; ?>" name="fathers_contact_no" id="fathers_contact_no"  class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Gender : </label> <br>
            

            <? if($genderidArr){?>
                <select name="genderid" id="genderid" class="form-control">
                    <?php foreach($genderidArr as $key => $val){?>
                    <option value="<?php echo $key; ?>" <?php if($PersonalDetailRecArr->genderid == $key){ echo "selected"; } ?>><?php echo $val; ?></option>
                <?php }?>
                </select>
            <? } ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Marital Status : </label> <br>
            


            <? if($loginMarital){?>
                <select name="maritalstatusname" id="maritalstatusname" class="form-control">
                    <?php foreach($loginMarital as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($PersonalDetailRecArr->maritalstatusid == $val->id){ echo "selected"; } ?>><?php echo $val->maritalstatusname; ?></option>
                <?php }?>
                </select>
            <? } ?>



        </div>
    </div>





<div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Marriage Date (If Married) : </label> <br>
            <input type="date" value="<?= ($PersonalDetailRecArr->marriage_date) ? $PersonalDetailRecArr->marriage_date : ""; ?>" name="marriage_date" id="marriage_date"   class="form-control">
        </div>
    </div>

<div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Spouse Name : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->spouse_name) ? $PersonalDetailRecArr->spouse_name : ""; ?>" name="spouse_name" id="spouse_name"   class="form-control">
        </div>
    </div>

<div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Spouse Occupation : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->spouse_occupation) ? $PersonalDetailRecArr->spouse_occupation : ""; ?>" name="spouse_occupation" id="spouse_occupation"   class="form-control">
        </div>
    </div>

<div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Spouse Contact No. : </label> <br>
            <input type="text" value="<?= ($PersonalDetailRecArr->spouse_contact_no) ? $PersonalDetailRecArr->spouse_contact_no : ""; ?>" name="spouse_contact_no" id="spouse_contact_no"   class="form-control">
        </div>
    </div>




    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Nationality : </label> <br>
            

            <? if($loginNationality){?>
                <select name="nationalitycode" id="nationalitycode" class="form-control">
                    <?php foreach($loginNationality as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($PersonalDetailRecArr->nationalityid == $val->id){ echo "selected"; } ?>><?php echo $val->nationalitycode; ?></option>
                <?php }?>
                </select>
            <? } ?>

           





        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Mother Tongue : </label> <br>
            


             <? if($loginLanguage){?>
                <select name="languagename" id="languagename" class="form-control">
                    <?php foreach($loginLanguage as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($PersonalDetailRecArr->languageid == $val->id){ echo "selected"; } ?>><?php echo $val->languagename; ?></option>
                <?php }?>
                </select>
            <? } ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Date of Birth : </label> <br>
            <input type="date" value="<?= ($PersonalDetailRecArr->dob) ? $PersonalDetailRecArr->dob : ""; ?>" name="dob" id="dob"   class="form-control">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Blood Group : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->bloodgroup) ? @$PersonalDetailRecArr->bloodgroup : "-"; ?>" name="bloodgroup" id="bloodgroup"   class="form-control">
        </div>
    </div>


     <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Height (cm) : </label> <br>
            <input type="number" value="<?= (@$PersonalDetailRecArr->height_in_cm) ? @$PersonalDetailRecArr->height_in_cm : "-"; ?>" name="height_in_cm" id="height_in_cm"   class="form-control">
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Weight : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->weight) ? @$PersonalDetailRecArr->weight : "-"; ?>" name="weight" id="weight"   class="form-control">
        </div>
    </div>

     <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Religion : </label> <br>
            

             
              <select class="form-control dropdown" id="religion" name="religion">
                <option value="" selected="selected" disabled="disabled">-- select one --</option>
                <option value="African Traditional & Diasporic" 
                <?= (@$PersonalDetailRecArr->religion == "African Traditional & Diasporic") ? "selected"  : ""; ?>>African Traditional & Diasporic</option>


                <option value="Agnostic" <?= (@$PersonalDetailRecArr->religion == "Agnostic") ? "selected" : ""; ?> >Agnostic</option>
                <option value="Atheist" <?= (@$PersonalDetailRecArr->religion == "Atheist") ? "selected" : ""; ?>>Atheist</option>
                <option value="Baha'i" <?= (@$PersonalDetailRecArr->religion == "Baha'i") ? "selected" : ""; ?>>Baha'i</option>
                <option value="Buddhism" <?= (@$PersonalDetailRecArr->religion == "Buddhism") ? "selected" : ""; ?>>Buddhism</option>
                <option value="Cao Dai" <?= (@$PersonalDetailRecArr->religion == "Cao Dai") ? "selected" : ""; ?>>Cao Dai</option>
                <option value="Chinese traditional religion" <?= (@$PersonalDetailRecArr->religion == "Chinese traditional religion") ? "selected" : ""; ?>>Chinese traditional religion</option>
                <option value="Christianity" <?= (@$PersonalDetailRecArr->religion == "Christianity") ? "selected" : ""; ?>>Christianity</option>
                <option value="Hinduism" <?= (@$PersonalDetailRecArr->religion == "Hinduism") ? "selected" : ""; ?>>Hinduism</option>
                <option value="Islam" <?= (@$PersonalDetailRecArr->religion == "Islam") ? "selected" : ""; ?>>Islam</option>
                <option value="Jainism" <?= (@$PersonalDetailRecArr->religion == "Jainism") ? "selected" : ""; ?>>Jainism</option>
                <option value="Juche" <?= (@$PersonalDetailRecArr->religion == "Juche") ? "selected" : ""; ?>>Juche</option>
                <option value="Judaism" <?= (@$PersonalDetailRecArr->religion == "Judaism") ? "selected" : ""; ?>>Judaism</option>
                <option value="Neo-Paganism" <?= (@$PersonalDetailRecArr->religion == "Neo-Paganism") ? "selected" : ""; ?>>Neo-Paganism</option>
                <option value="Nonreligious" <?= (@$PersonalDetailRecArr->religion == "Nonreligious") ? "selected" : ""; ?>>Nonreligious</option>
                <option value="Rastafarianism" <?= (@$PersonalDetailRecArr->religion == "Rastafarianism") ? "selected" : ""; ?>>Rastafarianism</option>
                <option value="Secular" <?= (@$PersonalDetailRecArr->religion == "Secular") ? "selected" : ""; ?>>Secular</option>
                <option value="Shinto" <?= (@$PersonalDetailRecArr->religion == "Shinto") ? "selected" : ""; ?>>Shinto</option>
                <option value="Sikhism" <?= (@$PersonalDetailRecArr->religion == "Sikhism") ? "selected" : ""; ?>>Sikhism</option>
                <option value="Spiritism" <?= (@$PersonalDetailRecArr->religion == "Spiritism") ? "selected" : ""; ?>>Spiritism</option>
                <option value="Tenrikyo" <?= (@$PersonalDetailRecArr->religion == "Tenrikyo") ? "selected" : ""; ?>>Tenrikyo</option>
                <option value="Unitarian-Universalism" <?= (@$PersonalDetailRecArr->religion == "Unitarian-Universalism") ? "selected" : ""; ?>>Unitarian-Universalism</option>
                <option value="Zoroastrianism" <?= (@$PersonalDetailRecArr->religion == "Zoroastrianism") ? "selected" : ""; ?>>Zoroastrianism</option>
                <option value="primal-indigenous" <?= (@$PersonalDetailRecArr->religion == "primal-indigenous") ? "selected" : ""; ?>>primal-indigenous</option>
                <option value="Other" <?= (@$PersonalDetailRecArr->religion == "Other") ? "selected" : ""; ?>>Other</option>
              </select>
             
        </div>
    </div>


     <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Passport No. : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->passport_no) ? @$PersonalDetailRecArr->passport_no : "-"; ?>" name="passport_no" id="passport_no"   class="form-control">
        </div>
    </div>

      <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Pan No. : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->pan_no) ? @$PersonalDetailRecArr->pan_no : "-"; ?>" name="pan_no" id="pan_no"   class="form-control">
        </div>
    </div>

 <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Driving License No. : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->driving_license_no) ? @$PersonalDetailRecArr->driving_license_no : "-"; ?>" name="driving_license_no" id="driving_license_no"   class="form-control">
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Ration Card : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->ration_card) ? @$PersonalDetailRecArr->ration_card : "-"; ?>" name="ration_card" id="ration_card"   class="form-control">
        </div>
    </div>
    








    <div class="col-md-3">
        <div class="form-group">   
            <label class="text-muted">Aadhar No. : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->aadhar_no_enrolment) ? @$PersonalDetailRecArr->aadhar_no_enrolment : "-"; ?>" name="aadhar_no_enrolment" id="aadhar_no_enrolment"   class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">   
            <label class="text-muted">SkyPee (Personal) : </label> <br>
            <input type="text" value="<?= (@$PersonalDetailRecArr->skypee_id) ? @$PersonalDetailRecArr->skypee_id : ""; ?>" name="skypee_id" id="skypee_id"   class="form-control">
        </div>
    </div>




    <div class="col-md-12">
        <div class="form-group">   
             <a class="btn btn-info pull-right" href="javascript:void(0)" onclick="updatePersonnelInfo()">Save</a>
        </div>
    </div>
</div>
</form>