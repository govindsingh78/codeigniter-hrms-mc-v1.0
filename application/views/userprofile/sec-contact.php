<div class="row clearfix">
    <div class="col-md-12">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> PERMANENT ADDRESS <a href="javascript:void(0)" class="btn btn-info pull-right" data-toggle="modal" data-target="#editContactAddress"><i class="fa fa-edit"></i> Edit</a></th>
            </tr>
            <tr>
                <th>Street Address : </th>

                

                


                
                <td colspan="3"><?= (@$ContactDetailsRecArr->perm_streetaddress) ? @$ContactDetailsRecArr->perm_streetaddress : ""; ?></td>
            </tr>
            <tr>
                <th>Country : </th>
                <td><?= (@$ContactDetailsRecArr->per_country_name) ? @$ContactDetailsRecArr->per_country_name : ""; ?></td>
                <th>State : </th>
                <td><?= (@$ContactDetailsRecArr->perm_state_name) ? @$ContactDetailsRecArr->perm_state_name : ""; ?></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><?= (@$ContactDetailsRecArr->perm_city_name) ? @$ContactDetailsRecArr->perm_city_name : ""; ?></td>
                <th>Pin Code : </th>
                <td><?= (@$ContactDetailsRecArr->perm_pincode) ? @$ContactDetailsRecArr->perm_pincode : ""; ?></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> CURRENT ADDRESS </th>
            </tr>
            <tr>
                <th>Street Address : </th>
                <td colspan="3"><?= (@$ContactDetailsRecArr->current_streetaddress) ? @$ContactDetailsRecArr->current_streetaddress : ""; ?></td>
            </tr>
            <tr>
                <th>Country : </th>
                <td><?= (@$ContactDetailsRecArr->current_country_name) ? @$ContactDetailsRecArr->current_country_name : ""; ?></td>
                <th>State : </th>
                <td><?= (@$ContactDetailsRecArr->current_state_name) ? @$ContactDetailsRecArr->current_state_name : ""; ?></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><?= (@$ContactDetailsRecArr->current_city_name) ? @$ContactDetailsRecArr->current_city_name : ""; ?></td>
                <th>Pin Code : </th>
                <td><?= (@$ContactDetailsRecArr->current_pincode) ? @$ContactDetailsRecArr->current_pincode : ""; ?></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> EMERGENCY DETAILS </th>
            </tr>
            <tr>
                <th>Name : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_name) ? @$ContactDetailsRecArr->emergency_name : ""; ?></td>
                <th>Relation : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_namerelation) ? @$ContactDetailsRecArr->emergency_namerelation : ""; ?></td>
            </tr>
            <tr>
                <th>Email : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_email) ? @$ContactDetailsRecArr->emergency_email : ""; ?></td>
                <th>Contact No : </th>
                <td><?= (@$ContactDetailsRecArr->emergency_number) ? @$ContactDetailsRecArr->emergency_number : ""; ?></td>
            </tr>
        </tbody>
    </table>

</div></div>






 <div class="modal fade" id="editContactAddress" role="dialog">
    <form id="editContactAddressForm" name="editContactAddressForm" action="">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        
                    <h4 class="modal-title">Manage Contact Address</h4>
                    <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>
                       
                    </div>

                    <div class="modal-body">


                        <div class="row clearfix">
                            <div class="col-md-12">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> PERMANENT ADDRESS  </th>
            </tr>
            <tr>
                <th>Street Address : </th>
                <td colspan="3"><div class="form-group"><input type="text" name="perm_streetaddress" id="perm_streetaddress" value="<?= (@$ContactDetailsRecArr->perm_streetaddress) ? @$ContactDetailsRecArr->perm_streetaddress : ""; ?>" class="form-control"></div></td>
            </tr>

              <!-- $data['loginCountries'] = $this->mastermodel->GetloginCountries();
        $data['loginStates'] = $this->mastermodel->GetloginStates();
        $data['loginCities'] = $this->mastermodel->GetloginCities(); 

$this->db->join('tbl_countries as b', "a.perm_country=b.id", "LEFT");
        $this->db->join('tbl_countries as c', "a.current_country=c.id", "LEFT");
        $this->db->join('tbl_states as d', "a.perm_state=d.id", "LEFT");
        $this->db->join('tbl_states as e', "a.current_state=e.id", "LEFT");
        $this->db->join('tbl_cities as f', "a.perm_city=f.id", "LEFT");
        $this->db->join('tbl_cities as g', "a.current_city=g.id", "LEFT");

    -->

    
  
            
            <tr>
                <th>Country : </th>
                <td><div class="form-group">

                    <? if($loginCountries){?>
                <select name="perm_country_name" id="perm_country_name" class="form-control">
                    <?php foreach($loginCountries as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->perm_country == $val->id){ echo "selected"; } ?>><?php echo $val->country_name; ?></option>
                <?php }?>
                </select>
            <? } ?>



                    </div></td>
                <th>State : </th>
                <td><div class="form-group">

                     <? if($loginStates){?>
                <select name="perm_state_name" id="perm_state_name" class="form-control">
                    <?php foreach($loginStates as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->perm_state == $val->id){ echo "selected"; } ?>><?php echo $val->state_name; ?></option>
                <?php }?>
                </select>
            <? } ?>


                   </div></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><div class="form-group">

                    <? if($loginCities){?>
                <select name="perm_city_name" id="perm_city_name" class="form-control">
                    <?php foreach($loginCities as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->perm_city == $val->id){ echo "selected"; } ?>><?php echo $val->city_name; ?></option>
                <?php }?>
                </select>
            <? } ?>

                    </div></td>
                <th>Pin Code : </th>
                <td><div class="form-group"><input type="text" name="perm_pincode" id="perm_pincode" value="<?= (@$ContactDetailsRecArr->perm_pincode) ? @$ContactDetailsRecArr->perm_pincode : ""; ?>" class="form-control"></div></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> CURRENT ADDRESS </th>
            </tr>
            <tr>
                <th>Street Address : </th>
                <td colspan="3"><div class="form-group"><input type="text" name="current_streetaddress" id="current_streetaddress" value="<?= (@$ContactDetailsRecArr->current_streetaddress) ? @$ContactDetailsRecArr->current_streetaddress : ""; ?>" class="form-control"></div></td>
            </tr>

           


            <tr>
                <th>Country : </th>
                <td><div class="form-group"><? if($loginCountries){?>
                <select name="current_country_name" id="current_country_name" class="form-control">
                    <?php foreach($loginCountries as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->current_country == $val->id){ echo "selected"; } ?>><?php echo $val->country_name; ?></option>
                <?php }?>
                </select>
            <? } ?></div></td>
                <th>State : </th>
                <td><div class="form-group"><? if($loginStates){?>
                <select name="current_state_name" id="current_state_name" class="form-control">
                    <?php foreach($loginStates as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->current_state == $val->id){ echo "selected"; } ?>><?php echo $val->state_name; ?></option>
                <?php }?>
                </select>
            <? } ?></div></td>
            </tr>
            <tr>
                <th>City : </th>
                <td><div class="form-group"><? if($loginCities){?>
                <select name="current_city_name" id="current_city_name" class="form-control">
                    <?php foreach($loginCities as $key => $val){?>
                    <option value="<?php echo $val->id; ?>" <?php if($ContactDetailsRecArr->current_city == $val->id){ echo "selected"; } ?>><?php echo $val->city_name; ?></option>
                <?php }?>
                </select>
            <? } ?></div></td>
                <th>Pin Code : </th>
                <td><div class="form-group"><input type="text" name="current_pincode" id="current_pincode" value="<?= (@$ContactDetailsRecArr->current_pincode) ? @$ContactDetailsRecArr->current_pincode : ""; ?>" class="form-control"></div></td> 
            </tr>
        </tbody>
    </table>

    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> EMERGENCY DETAILS </th>
            </tr>
            <tr>
                <th>Name : </th>
                <td><div class="form-group"><input type="text" name="emergency_name" id="emergency_name" value="<?= (@$ContactDetailsRecArr->emergency_name) ? @$ContactDetailsRecArr->emergency_name : ""; ?>" class="form-control"></div></td>
                <th>Relation : </th>
                <td><div class="form-group"><input type="text" name="emergency_namerelation" id="emergency_namerelation" value="<?= (@$ContactDetailsRecArr->emergency_namerelation) ? @$ContactDetailsRecArr->emergency_namerelation : ""; ?>" class="form-control"></div></td>
            </tr>
            <tr>
                <th>Email : </th>
                <td><div class="form-group"><input type="text" name="emergency_email" id="emergency_email" value="<?= (@$ContactDetailsRecArr->emergency_email) ? @$ContactDetailsRecArr->emergency_email : ""; ?>" class="form-control"></div></td>
                <th>Contact No : </th>
                <td><div class="form-group"><input type="text" name="emergency_number" id="emergency_number" value="<?= (@$ContactDetailsRecArr->emergency_number) ? @$ContactDetailsRecArr->emergency_number : ""; ?>" class="form-control"></div></td>
            </tr>
        </tbody>
    </table>
</div>
</div>

                         
                            <div class="box-content">
                                 
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateContactAddress()">Save</a>



                                </div>
                            </div>
                         
                    </div>
                </div>
            </div>
        </form>
        </div>