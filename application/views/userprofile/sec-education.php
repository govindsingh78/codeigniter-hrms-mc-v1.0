<div class="row clearfix mb-4">
    <div class="col-md-12 table-responsive">

         <a href="javascript:void(0)" class="btn btn-info pull-right mr-6" data-toggle="modal" data-target="#addEducation" style="margin-right: 20px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Education</a>


    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Education Level</th>
                <th>Institution Name</th>
                <th>Course</th>
                <th>Location</th>
                <th>Specialization</th>
                <th>Passing Year</th>
                <th>Marks Obtained</th>
                <th>Percentage</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EduDetailsRecArr) {
                foreach ($EduDetailsRecArr as $kEy => $recD) {
                     if($recD->education_type !== "professional_membership"){
                         $educationTypeLabel = ($recD->education_type) ? ucfirst(str_replace("_", " ", $recD->education_type)) : "N/A";
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->educationlevelcode) ? $recD->educationlevelcode : ""; ?>   <span class='alert alert-warning ml-2'><?= $educationTypeLabel; ?></span> </td>
                        <td><?= ($recD->institution_name) ? $recD->institution_name : ""; ?></td>
                        <td><?= ($recD->course) ? $recD->course : ""; ?></td>
                        <td><?= ($recD->spc_location) ? $recD->spc_location : ""; ?></td>
                        <td><?= ($recD->specialization) ? $recD->specialization : ""; ?></td>
                        <td><?= ($recD->from_date) ? $recD->from_date : ""; ?>-<?= ($recD->to_date) ? $recD->to_date : ""; ?></td>
                        <td><?= ($recD->marks_obtained) ? $recD->marks_obtained : ""; ?> / <?= ($recD->total_marks) ? $recD->total_marks : ""; ?></td>
                        <td><?= ($recD->percentage) ? $recD->percentage : ""; ?></td>
                         <td>

                        <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editEducation<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>


                        




                        <div class="modal fade" id="editEducation<?= $kEy + 1; ?>" role="dialog">
                        <form id="editEducationForm<?= $kEy + 1; ?>" name="editEducationForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Education</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">
                             <div class="eType">
                            
                            <div class="form-group">
                            
                            <select name="education_type" id="education_type_<?= $kEy + 1; ?>" class="form-control" onchange="showHideEducationContent(this.value, <?= $kEy + 1; ?>)">
                            <option value="basic" <?php if($recD->education_type == "basic"){ echo "selected"; } ?>>Basic</option>
                            <option value="professional" <?php if($recD->education_type == "professional"){ echo "selected"; } ?>>Professional</option>
                            <option value="other_certification" <?php if($recD->education_type == "other_certification"){ echo "selected"; } ?>>Certification</option>
                            
                            </select>
                            
                            </div>



                        </div>




                        <div id="showBasic<?= $kEy + 1; ?>" <?php if($recD->education_type !== "professional_membership"){ echo 'style="display: block"'; } else{ echo 'style="display: none"'; } ?>>


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Education Level : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group">
                            <? if($loginEducationLevel){?>
                            <select name="educationlevel" id="educationlevel_<?= $kEy + 1; ?>" class="form-control">
                            <?php foreach($loginEducationLevel as $key => $val){?>
                            <option value="<?php echo $val->id; ?>" <?php if($recD->educationlevel == $val->id){ echo "selected"; } ?>><?php echo $val->educationlevelcode; ?></option>
                            <?php }?>




                            </select>
                            <? } ?>
                            </div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Institution Name : </th>
                        <td><div class="form-group"><input type="text" name="institution_name" id="institution_name_<?= $kEy + 1; ?>" value="<?= ($recD->institution_name) ? $recD->institution_name : ""; ?>" class="form-control"></div></td>
                        </tr>



                        
                         
                       
                        
                        


                        <tr>
                        <th>Course : </th>
                        <td><div class="form-group"><input type="text" name="course" id="course_<?= $kEy + 1; ?>" value="<?= ($recD->course) ? $recD->course : ""; ?>" class="form-control"></div></td>

                        </tr><tr>
                        <th>Location : </th>
                        <td><div class="form-group"><input type="text" name="spc_location" id="spc_location_<?= $kEy + 1; ?>" value="<?= ($recD->spc_location) ? $recD->spc_location : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Specialization : </th>
                        <td><div class="form-group"><input type="text" name="specialization" id="specialization_<?= $kEy + 1; ?>" value="<?= ($recD->specialization) ? $recD->specialization : ""; ?>" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Marks Obtained : </th>
                        <td><div class="form-group"><input type="number" name="marks_obtained" id="marks_obtained_<?= $kEy + 1; ?>" value="<?= ($recD->marks_obtained) ? $recD->marks_obtained : ""; ?>" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>

                         <tr>
                        <th>Total Marks : </th>
                        <td><div class="form-group"><input type="number" name="total_marks" id="total_marks_<?= $kEy + 1; ?>" value="<?= ($recD->total_marks) ? $recD->total_marks : ""; ?>" class="form-control"></div></td>
                        </tr>



                        <tr>
                        <th>Year (From) : </th>
                        <td><div class="form-group"><input type="number" name="from_date" id="from_date_<?= $kEy + 1; ?>" value="<?= ($recD->from_date) ? $recD->from_date : ""; ?>" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>

                        <tr>
                        <th>Year (To) : </th>
                        <td><div class="form-group"><input type="number" name="to_date" id="to_date_<?= $kEy + 1; ?>" value="<?= ($recD->to_date) ? $recD->to_date : ""; ?>" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>
                         <tr>
                        <th>Percentage : </th>
                        <td><div class="form-group"><input type="number" name="percentage" id="percentage_<?= $kEy + 1; ?>" value="<?= ($recD->percentage) ? $recD->percentage : ""; ?>" class="form-control"></div></td>
                        </tr>


                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>


                        

<div class="col-md-12">
                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateEducation('<?= $kEy + 1; ?>')">Save</a>

</div>

                        </div>


                        <div class="box-content">


                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>


                         </td>




                    </tr>
                    <?php
                }
                


                }
            } ?>

        </tbody>
    </table>
</div>
</div>
<div class="row clearfix">
    <div class="col-md-12 table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Membership Type</th>
                <th>Institution Name</th>
                <th>Valid Upto</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EduDetailsRecArr) {
                foreach ($EduDetailsRecArr as $kEy => $recD) {
                    if($recD->education_type == "professional_membership"){
                        $educationTypeLabel = ($recD->education_type) ? ucfirst(str_replace("_", " ", $recD->education_type)) : "N/A";

                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->membership_type) ? $recD->membership_type : ""; ?> <span class='alert alert-warning ml-2'><?= $educationTypeLabel; ?></span></td>
                        <td><?= ($recD->institution_name) ? $recD->institution_name : ""; ?></td>
                         
                        <td><?= ($recD->valid_upto) ? $recD->valid_upto : ""; ?></td>
                         <td>

                        <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editEducation<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>


                        




                        <div class="modal fade" id="editEducation<?= $kEy + 1; ?>" role="dialog">
                        <form id="editEducationForm<?= $kEy + 1; ?>" name="editEducationForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Education</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">
                             <div class="eType">
                            
                            <div class="form-group">
                            
                            <select name="education_type" id="education_type_<?= $kEy + 1; ?>" class="form-control" onchange="showHideEducationContent(this.value, <?= $kEy + 1; ?>)">
                      
                            <option value="professional_membership" <?php if($recD->education_type == "professional_membership"){ echo "selected"; } ?>>Professional Membership</option>
                            </select>
                            
                            </div>



                        </div>




                       
<input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">

                         <div id="showMembership<?= $kEy + 1; ?>" <?php if($recD->education_type == "professional_membership"){ echo 'style="display: block"'; } else{ echo 'style="display: none"'; } ?>>

                        <table class="table table-striped">
                        <tbody>
                        <tr>
                        <th>Membership Type : </th>
                        <td><div class="form-group"><input type="text" name="membership_type" id="membership_type_<?= $kEy + 1; ?>" value="<?= ($recD->membership_type) ? $recD->membership_type : ""; ?>" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Institution Name : </th>
                        <td><div class="form-group"><input type="text" name="membership_institution_name" id="membership_institution_name_<?= $kEy + 1; ?>" value="<?= ($recD->institution_name) ? $recD->institution_name : ""; ?>" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Valid Upto (Year) : </th>
                        <td><div class="form-group"><input type="number" name="valid_upto" id="valid_upto_<?= $kEy + 1; ?>" value="<?= ($recD->valid_upto) ? $recD->valid_upto : ""; ?>" class="form-control"></div></td>
                        </tr>

                        
                        </tbody>

                        </table>


                        </div>

<div class="col-md-12">
                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateEducation('<?= $kEy + 1; ?>')">Save</a>

</div>

                        </div>


                        <div class="box-content">


                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>


                         </td>




                    </tr>
                    <?php
                }
                 

                }
            }  ?>

        </tbody>
    </table>
</div>
</div>




                        <div class="modal fade" id="addEducation" role="dialog">
                        <form id="addEducationForm" name="addEducationForm" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Education</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">

                        <div class="eType">
                            
                            <div class="form-group">
                            
                            <select name="education_type" id="education_type" class="form-control" onchange="showHideEducationContent(this.value, 0)">
                            <option value="basic">Basic</option>
                            <option value="professional">Professional</option>
                            <option value="other_certification">Certification</option>
                            <option value="professional_membership">Professional Membership</option>
                            </select>
                            
                            </div>



                        </div>




                        <div id="showBasic0" style="display: block">

                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Education Level : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group">
                            <? if($loginEducationLevel){?>
                            <select name="educationlevel" id="educationlevel" class="form-control">
                            <?php foreach($loginEducationLevel as $key => $val){?>
                            <option value="<?php echo $val->id; ?>"><?php echo $val->educationlevelcode; ?></option>
                            <?php }?>




                            </select>
                            <? } ?>
                            </div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Institution Name : </th>
                        <td><div class="form-group"><input type="text" name="institution_name" id="institution_name_<?= $kEy + 1; ?>" value="" class="form-control"></div></td>
                        </tr>



                        
                         
                       
                        
                        


                        <tr>
                        <th>Course : </th>
                        <td><div class="form-group"><input type="text" name="course" id="course" value="" class="form-control"></div></td>

                        </tr><tr>
                        <th>Location : </th>
                        <td><div class="form-group"><input type="text" name="spc_location" id="spc_location" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Specialization : </th>
                        <td><div class="form-group"><input type="text" name="specialization" id="specialization" value="" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Marks Obtained : </th>
                        <td><div class="form-group"><input type="number" name="marks_obtained" id="marks_obtained" value="" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>

                         <tr>
                        <th>Total Marks : </th>
                        <td><div class="form-group"><input type="number" name="total_marks" id="total_marks" value="" class="form-control"></div></td>
                        </tr>



                        <tr>
                        <th>Year (From) : </th>
                        <td><div class="form-group"><input type="number" name="from_date" id="from_date" value="" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>

                        <tr>
                        <th>Year (To) : </th>
                        <td><div class="form-group"><input type="number" name="to_date" id="to_date" value="" class="form-control" minlength="4" maxlength="4"></div></td>
                        </tr>

                         <tr>
                        <th>Percentage : </th>
                        <td><div class="form-group"><input type="number" name="percentage" id="percentage" value="" class="form-control"></div></td>
                        </tr>


                        
                        </tbody>
                        </table>
                        </div>


                        <div id="showMembership0" style="display: none">

                        <table class="table table-striped">
                        <tbody>
                        <tr>
                        <th>Membership Type : </th>
                        <td><div class="form-group"><input type="text" name="membership_type" id="membership_type" value="" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Institution Name : </th>
                        <td><div class="form-group"><input type="text" name="membership_institution_name" id="membership_institution_name" value="" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Valid Upto (Year) : </th>
                        <td><div class="form-group"><input type="number" name="valid_upto" id="valid_upto" value="" class="form-control"></div></td>
                        </tr>

                        
                        </tbody>

                        </table>


                        </div>




                    </div>








                        </div>


                        <div class="box-content">
                        <div class="col-md-12">
                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addEducation()">Save</a>

</div>

                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>