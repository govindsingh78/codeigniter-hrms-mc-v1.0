<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$disAbl = 'readonly="readonly"';
$skillArrlv = array("3" => "Medium", "2" => "Expert", "1" => "BASIC");
$genderidArr = array("1" => "Male", "2" => "Female", "3" => "Other");
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content" class="profilepage_1">
            <div class="container-fluid">
                <div class="block-header">

                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>            

                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs">                                
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#official">Official</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#personaldetailsview">Personal</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#contactview">Contact</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#experience">Experience</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#skills">Skills</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#educationdetails">Education</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#familydetails">Family</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#referencesdetails">References</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#documentsdetails">Documents</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#otherdetails">Other</a></li>
                                </ul>
                            </div>
                            <!-- Data -->
                            <div class="tab-content">
                                <!-- Image crop section starts -->

                                <div class="container">
                                <div class="panel panel-default">
                                <div class="panel-body">
                                <div class="row">
                                <div class="col-md-3" style="">
                                <div id="upload-demo-i" style="background:#e1e1e1;width:200px;height:200px;margin-top:30px; margin-left: 10px"><img src="<?php if ($loginUserRecd->profileimg) { echo str_replace("/index.php", "", base_url()).'/'.EMPLPROFILE . $loginUserRecd->profileimg;; } ?>" alt="Profile Pic" style="margin: 25px;
                                height: 150px;"/>  </div>
                                </div>

                                 
                                <div class="col-md-4 text-center" id="upload-demo" style="width:350px; display: none"></div>
                                
                                

                                

                                <div class="col-md-2 text-left" style="padding-top:70px;">
                                 
                                <input type="file" id="upload">
                                <br/>
                                <button class="btn btn-success upload-result mt-2">Upload Image</button>
                                </div>
                               
                                
                                </div>
                                </div>
                                </div>
                                </div>

                                <!-- Image crop section ends -->
                                 

                                <!-- Start Professional -->
                                <div class="tab-pane active" id="official">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-official", compact("ofcRecArr", "disAbl")); ?>
                                    </div>
                                </div>
                                <!-- Close Profile Official -->
                                <!-- Start Personal -->
                                <div class="tab-pane" id="personaldetailsview">
                                    <div class="body">
<!--    $data['loginMarital'] = $this->mastermodel->GetloginMarital();
        $data['loginNationality'] = $this->mastermodel->GetloginNationality();
        $data['loginLanguage'] = $this->mastermodel->GetloginLanguage();
        id, maritalstatusname, nationalitycode, languagename -->
 <?php //print_r($loginLanguage);?>

                                        <?php $this->load->view("userprofile/sec-personal", compact("PersonalDetailRecArr", "disAbl", "genderidArr", "loginMarital", "loginNationality", "loginLanguage")); ?>
                                    </div>
                                </div>
                                <!-- Close Personal -->
                                <!-- Start Professional -->
                                <div class="tab-pane" id="experience">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-experience", compact("EmplExprRecArr", "disAbl")); ?>
                                    </div>
                                </div>
                                <!-- Close Profile Official -->
                                <!-- Start Contact Details -->

                                

                                <div class="tab-pane" id="otherdetails">
                                    <div class="body">


                                       
                                        <?php $this->load->view("userprofile/sec-otherDetails", compact("otherDetailsRecArr", "disAbl", "loginUsersRelatives")); ?>
                                    </div>
                                </div>


                                <div class="tab-pane" id="contactview">
                                    <div class="body">
                                       
                                        <?php $this->load->view("userprofile/sec-contact", compact("ContactDetailsRecArr", "disAbl", "loginCountries", "loginStates", "loginCities")); ?>
                                    </div>
                                </div>
                                <!-- Close Contact Details -->
                                <!-- Start Skills -->
                                <div class="tab-pane" id="skills">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-skill", compact("EmplSkillRecArr", "disAbl", "skillArrlv")); ?>
                                    </div>
                                </div>
                                <!-- Close Skills -->
                                <!-- Start Educational -->
                                <div class="tab-pane" id="educationdetails">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-education", compact("EduDetailsRecArr", "disAbl", "loginEducationLevel")); ?>

                                        
                                    </div>
                                </div>
                                <!-- Close Educational -->
                                <!-- Start Family Details -->
                                <div class="tab-pane" id="familydetails">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-family", compact("FamilyDetailsRecArr", "disAbl")); ?> 
                                    </div>
                                </div>


                                <div class="tab-pane" id="referencesdetails">
                                    <div class="body">
                                        <?php $this->load->view("userprofile/sec-references", compact("ReferencesDetailsRecArr", "disAbl")); ?> 
                                    </div>
                                </div>
                                <!-- Close Family Details -->
                                <!-- Start Documents Details Section-->
                                <div class="tab-pane" id="documentsdetails">
                                    <div class="body">
                                        <div class="row clearfix">

                                            <div class="col-md-12 ">
                                                <a href="javascript:void(0)" class="btn btn-info pull-right" data-toggle="modal" data-target="#addDocument" style="margin-right: 20px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Document</a>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No.</th>

                                                        <th>Doc Name</th>
                                                        <th>Doc Detail</th>
                                                        <th>Document</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>







                                                    <?php
                                                    // echo "<pre>";
                                                    //  print_r($EmployeeDocsDetailRec); die;
                                                    if ($EmployeeDocsDetailRec) {
                                                        foreach ($EmployeeDocsDetailRec as $kEy => $recD) {
                                                            if ($recD->attachments) {
                                                                $docsArray = json_decode($recD->attachments);
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?= $kEy + 1; ?></td>


                             

                                                                <td> 
                                                                <?php 
                                                                if($recD->doc_type == 0){

                                                                    if($loginEducationLevel){ 

                                                                    foreach($loginEducationLevel as $key => $val){ 
                                                                    if($recD->doc_type_id == $val->id){  
                                                                    echo $val->educationlevelcode; 
                                                                    }
                                                                    }
                                                                }
                                                                    echo "<span class='alert alert-warning ml-2'>Educational Category</span>";


                                                                }else{

                                                                if($getOtherDocType){ 

                                                                foreach($getOtherDocType as $key => $val){ 
                                                                if($recD->doc_type_id == $val->id){  
                                                                echo $val->doc_name; 
                                                                }
                                                                }
                                                            }
                                                                    echo "<span class='alert alert-warning ml-2'>Other Category</span>";
                                                                }
                                                                ?> 


                                                               
                                                                </td>
                                                                <td><?= (@$recD->name) ? @$recD->name : ""; ?></td>
                                                                <td>
                                                                    <?php
                                                                    echo (@$docsArray[0]->new_name) ? "<a target='_blank' href='" . str_replace("/index.php", "/", base_url(EMPLOYEEDOCS)) . $docsArray[0]->new_name . "'> View / Download </a>" : "";
                                                                    ?>
                                                                </td>



                                                                <td>

                        <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editDocument<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>


                        




                        <div class="modal fade" id="editDocument<?= $kEy + 1; ?>" role="dialog">
                        <form id="editDocumentForm<?= $kEy + 1; ?>" name="editDocumentForm<?= $kEy + 1; ?>" action="" enctype="multipart/form-data" method="POST">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Document</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                       

                        <div class="col-md-4 m-2 p-2 "><input type="radio" name="doc_type" id="doc_type_education" value="0" <?= (@$recD->doc_type == 0) ? "checked" : ""; ?> onclick="showHideDocSelect(0, <?= $kEy + 1; ?>)"> &nbsp; Education Doc </div>


                        <div class="col-md-4 m-2 p-2"><input type="radio" name="doc_type" id="doc_type_other" value="1" <?= (@$recD->doc_type == 1) ? "checked" : ""; ?> onclick="showHideDocSelect(1, <?= $kEy + 1; ?>)"> &nbsp; Other Doc </div>
 <div class="col-md-12">
                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Doc Type : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->




                            <div id="showEducationSelect<?= $kEy + 1; ?>" class="highlight" <?= (@$recD->doc_type == 0) ? 'style="display: block"' : 'style="display: none"'; ?>>       
                            <div class="form-group">
                            <? if($loginEducationLevel){?>
                            <select name="educationlevel" id="educationlevel_<?= $kEy + 1; ?>" class="form-control">
                            <?php foreach($loginEducationLevel as $key => $val){?>
                            <option value="<?php echo $val->id; ?>" <?php if($recD->doc_type_id == $val->id){ echo "selected"; } ?>><?php echo $val->educationlevelcode; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>

                            </div>
                            </div>

                            <div id="showOtherSelect<?= $kEy + 1; ?>"  class="highlight" <?= (@$recD->doc_type == 1) ? 'style="display: block"' : 'style="display: none"'; ?>>
                             <div class="form-group">
                            <? if($getOtherDocType){?>
                            <select name="other_doc" id="other_doc_<?= $kEy + 1; ?>" class="form-control">
                            <?php foreach($getOtherDocType as $key => $val){?>
                            <option value="<?php echo $val->id; ?>" <?php if($recD->doc_type_id == $val->id){ echo "selected"; } ?>><?php echo $val->doc_name; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>
                            </div>

                            </div>
                            

                                


                        </td>

                        </tr>


                       
                        <tr>
                        <th>Doc Detail : </th>
                        <td>
                           
                            <div class="form-group">
                                <input type="text" name="doc_name" id="doc_name_<?= $kEy + 1; ?>" value="<?= (@$recD->name) ? @$recD->name : ""; ?>" class="form-control">
                            
                            </div>


                        </td>

                        </tr>


                        <tr>
                     

                        <th>Upload Doc : </th>
                        <td><div class="form-group"><input type="file" name="doc_file" id="doc_file_<?= $kEy + 1; ?>"></div></td>
                        </tr>



                        
                         
                       
                        
                         


                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateDocument('<?= $kEy + 1; ?>')">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>


                         </td>







                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td style="color:red" colspan="3"> Record Not Found. </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>

                                             </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Close Documents Details Section-->








                        <div class="modal fade" id="addDocument" role="dialog">
                        <form id="addDocumentForm" name="addDocumentForm" action="" enctype="multipart/form-data" method="POST">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Document</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        

                        <div class="col-md-4 m-2 p-2"><input type="radio" name="doc_type" id="doc_type_education" value="0" <?= (@$recD->doc_type == 0) ? "checked" : ""; ?> onclick="showHideDocSelect(0, 0)"> &nbsp; Education Doc </div>


                        <div class="col-md-4 m-2 p-2"><input type="radio" name="doc_type" id="doc_type_other" value="1" <?= (@$recD->doc_type == 1) ? "checked" : ""; ?> onclick="showHideDocSelect(1, 0)"> &nbsp; Other Doc </div>


                        <div class="col-md-12">

                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Doc Type : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                             
                            <div id="showEducationSelect0" class="highlight" style="display: block;">       
                            <div class="form-group">
                            <? if($loginEducationLevel){?>
                            <select name="educationlevel" id="educationlevel" class="form-control">
                            <?php foreach($loginEducationLevel as $key => $val){?>
                            <option value="<?php echo $val->id; ?>"><?php echo $val->educationlevelcode; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>
                            </div>
                            </div>

                            <div id="showOtherSelect0"  class="highlight"  style="display: none;">
                             <div class="form-group">
                            <? if($getOtherDocType){?>
                            <select name="other_doc" id="other_doc" class="form-control">
                            <?php foreach($getOtherDocType as $key => $val){?>
                            <option value="<?php echo $val->id; ?>"><?php echo $val->doc_name; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>
                            </div>

                            </div>
                            

                                


                        </td>

                        </tr>


                        
                        <tr>
                        <th>Doc Detail : </th>
                        <td>
                           
                            <div class="form-group">
                                <input type="text" name="doc_name" id="doc_name" value="" class="form-control">
                            
                            </div>


                        </td>

                        </tr>
                        
                        <tr>
                     

                        <th>Upload Doc : </th>
                        <td><div class="form-group"><input type="file" name="doc_file" id="doc_file"></div></td>
                        </tr>



                        
                         
                       
                        
                         


                        
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addDocument()">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

<!-- <script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').removeAttr('src');
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        $("#uploaderror2").hide();
        $("#uploaderror3").hide();

        $('INPUT[type="file"]').change(function () {
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'JPG':
                case 'jpeg':
                case 'png':
                case 'PNG':
                    $('#uploadButton').attr('disabled', false);
                    $("#uploaderror2").hide();
                    break;
                default:
                    $("#uploaderror2").show();
                    $("#uploaderror2").html('This is not an allowed file type.');
                    this.value = '';
            }
        });
        $('INPUT[type="file"]').bind('change', function () {
            var imgfilesize = (this.files[0].size / 1020);
            if (imgfilesize > 577) {
                $("#uploaderror2").show();
                $("#uploaderror2").html('Reducing File Size Max Uploads (500 KB). ');
                this.value = '';
            } else {
                $("#uploaderror2").hide();
                readURL(this);
            }
        });
    });

    $(function () {
        // photo upload
        $('#btn-upload-photo').on('click', function () {
            $(this).siblings('#filePhoto').trigger('click');
        });
        // plans
        $('.btn-choose-plan').on('click', function () {
            $('.plan').removeClass('selected-plan');
            $('.plan-title span').find('i').remove();
            $(this).parent().addClass('selected-plan');
            $(this).parent().find('.plan-title').append('<span><i class="fa fa-check-circle"></i></span>');
        });
    });

</script> -->

<link rel="stylesheet" href="<?= FRONTASSETS; ?>jquery-ui.css">
<script src="<?= FRONTASSETS; ?>jquery-ui.js"></script>
<script src='<?= FRONTASSETS; ?>select2/dist/js/select2.min.js' type='text/javascript'></script>
<link href='<?= FRONTASSETS; ?>select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>



  
  <script src="<?= FRONTASSETS; ?>js/croppie.js"></script>
  
  <link rel="stylesheet" href="<?= FRONTASSETS; ?>css/croppie.css">



<script type="text/javascript">

$uploadCrop = $('#upload-demo').croppie({

    enableExif: true,

    viewport: {

        width: 200,

        height: 200,

        type: 'circle'

    },

    boundary: {

        width: 300,

        height: 300

    }

});

     

$('#upload').on('change', function () { 

    var reader = new FileReader();

    $("#upload-demo").show();

    reader.onload = function (e) {

        $uploadCrop.croppie('bind', {

            url: e.target.result

        }).then(function(){

            console.log('jQuery bind complete');

        });

            

    }

    reader.readAsDataURL(this.files[0]);

});

     

$('.upload-result').on('click', function (ev) {

    $uploadCrop.croppie('result', {

        type: 'canvas',

        size: 'viewport'

    }).then(function (resp) {

     

        $.ajax({

            url: "<?= base_url('userProfile_control/post'); ?>",

            type: "POST",

            data: {"image":resp},
            dataType: "json",

            success: function (data) {

                html = '<img src="' + resp + '" style="margin: 25px; height: 150px;"/>';

                $("#upload-demo-i").html(html);

                console.log(data.msg);

                toastr.success(data.msg , 'Message', {timeOut: 5000});

                $("#upload-demo").hide();

            }

        });

    });

});

   


    function updatePersonnelInfo() {

    var data = $("#personnelForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updatePersonnelData'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    },

    });
    }

    function updateContactAddress() {

    var data = $("#editContactAddressForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateContactAddress'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editContactAddress').modal('toggle');
    },

    });
    }


    function updateOtherDetails() {

    var data = $("#editOtherDetailsForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateOtherDetails'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
   
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
     
    },

    });
    }



       
 function addExperience() {

    var data = $("#addExperienceForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addExperience'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addExperience').modal('toggle');
    },

    });
    }

function updateExperience(key_id) {

    var data = $("#editExperienceForm"+key_id).serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateExperience'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editExperience'+key_id).modal('toggle');
    },

    });
    }

function updateSkills(key_id) {

    var data = $("#editSkillsForm"+key_id).serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateSkills'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editSkills'+key_id).modal('toggle');
    },

    });
    }




    function updateEducation(key_id) {

    var data = $("#editEducationForm"+key_id).serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateEducation'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editEducation'+key_id).modal('toggle');
    },

    });
    }

    function addEducation() {

    var data = $("#addEducationForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addEducation'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addEducation').modal('toggle');
    },

    });
    }


    function addSkills() {

    var data = $("#addSkillsForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addSkills'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addSkills').modal('toggle');
    },

    });
    }





function updateFamily(key_id) {

    var data = $("#editFamilyForm"+key_id).serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateFamily'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editFamily'+key_id).modal('toggle');
    },

    });
    }

    function addFamily() {

    var data = $("#addFamilyForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addFamily'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addFamily').modal('toggle');
    },

    });
    }




function updateReferences(key_id) {

    var data = $("#editReferencesForm"+key_id).serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateReferences'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
     
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editReferences'+key_id).modal('toggle');
    },

    });
    }

    function addReferences() {

    var data = $("#addReferencesForm").serialize();

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addReferences'); ?>",
    data: data,
    dataType: "json",
    success: function(responData) {
     
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addReferences').modal('toggle');
    },

    });
    }



function updateDocument(key_id) {

    var data =  new FormData($("#editDocumentForm"+key_id)[0]);

    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/updateDocument'); ?>",
    data: data,
    processData: false,
    contentType: false,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#editDocument'+key_id).modal('toggle');
    },

    });
    }

    function addDocument() {

    
    var data = new FormData($('#addDocumentForm')[0]);
 
    $.ajax({
    type: 'POST',
    url: "<?php echo base_url('userProfile_control/addDocument'); ?>",
    data: data,
    processData: false,
    contentType: false,
    dataType: "json",
    success: function(responData) {
    //toastr.info(responData.msg , 'Message', {timeOut: 5000});
    console.log(responData.msg);
    toastr.success(responData.msg , 'Message', {timeOut: 5000});
    $('#addDocument').modal('toggle');
    },

    });
    }

function showHideDocSelect(doc_type, key_id){

    if(doc_type === 0)
    {

        $("#showEducationSelect"+key_id).show();
        $("#showOtherSelect"+key_id).hide();

    }else{
        $("#showEducationSelect"+key_id).hide();
        $("#showOtherSelect"+key_id).show();
    }

}


 


function showHideEducationContent(doc_type, key_id){

    if(doc_type == "professional_membership")
    {

        $("#showMembership"+key_id).show();
        $("#showBasic"+key_id).hide();

    }else{
        $("#showMembership"+key_id).hide();
        $("#showBasic"+key_id).show();
    }

}










function showHide(doc_val, key_id){

    if(doc_val === "yes" || doc_val === "reference")
    {

        $("#showHide_"+key_id).show();
         

    }else{
        $("#showHide_"+key_id).hide();
    }

}

</script>