<div class="row clearfix">
    <div class="col-md-12">

         <a href="javascript:void(0)" class="btn btn-info pull-right mr-6" data-toggle="modal" data-target="#addReferences" style="margin-right: 20px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add References</a>


    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Reference Name</th>
                <th>Organisation</th>
                <th>Designation</th>
                <th>Contact Number</th>
                <th>Reference Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($ReferencesDetailsRecArr) {
                foreach ($ReferencesDetailsRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->reference_name) ? $recD->reference_name : ""; ?></td>
                        <td><?= ($recD->organisation) ? $recD->organisation : ""; ?></td>
                        <td><?= ($recD->designation) ? $recD->designation : ""; ?></td>
                        <td><?= ($recD->contact_no) ? $recD->contact_no : ""; ?></td>
                        <td><?= ($recD->reference_type == "personnel") ? "Personnel" : "Professional"; ?></td>
                       <td>

                        <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editReferences<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>


                        




                        <div class="modal fade" id="editReferences<?= $kEy + 1; ?>" role="dialog">
                        <form id="editReferencesForm<?= $kEy + 1; ?>" name="editReferencesForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage References</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Reference Name : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group"><input type="text" name="reference_name" id="reference_name_<?= $kEy + 1; ?>" value="<?= ($recD->reference_name) ? $recD->reference_name : ""; ?>" class="form-control"></div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Organisation : </th>
                        <td><div class="form-group"><input type="text" name="organisation" id="organisation_<?= $kEy + 1; ?>" value="<?= ($recD->organisation) ? $recD->organisation : ""; ?>" class="form-control"></div></td>
                        </tr>



                        
                         
                        
                        
                        


                        <tr>
                        <th>Designation : </th>
                        <td><div class="form-group"><input type="text" name="designation" id="designation_<?= $kEy + 1; ?>" value="<?= ($recD->designation) ? $recD->designation : ""; ?>" class="form-control"></div></td>

                        </tr><tr>
                        <th>Contact Number : </th>
                        <td><div class="form-group"><input type="text" name="contact_no" id="contact_no_<?= $kEy + 1; ?>" value="<?= ($recD->contact_no) ? $recD->contact_no : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Reference Type : </th>
                        <td><div class="form-group"> 



                        <select name="reference_type" id="reference_type_<?= $kEy + 1; ?>" class="form-control">
                        
                        <option value="personnel" <?php if($recD->reference_type == "personnel"){ echo "selected"; } ?>>Personnel</option>
                        <option value="professional" <?php if($recD->reference_type == "professional"){ echo "selected"; } ?>>Professional</option>
                        
                        </select>
                         


                        </div></td>
                        </tr>

                         
                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateReferences('<?= $kEy + 1; ?>')">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>


                         </td>





                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red" colspan="7"> Record Not Found. </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>
</div>



                        <div class="modal fade" id="addReferences" role="dialog">
                        <form id="addReferencesForm" name="addReferencesForm" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Add References</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Reference Name : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group"><input type="text" name="reference_name" id="reference_name" value="" class="form-control"></div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Organisation : </th>
                        <td><div class="form-group"><input type="text" name="organisation" id="organisation" value="" class="form-control"></div></td>
                        </tr>



                        
                         
                        
                        
                        


                        <tr>
                        <th>Designation : </th>
                        <td><div class="form-group"><input type="text" name="designation" id="designation" value="" class="form-control"></div></td>

                        </tr><tr>
                        <th>Contact Number : </th>
                        <td><div class="form-group"><input type="text" name="contact_no" id="contact_no" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Reference Type : </th>
                        <td><div class="form-group"> 



                        <select name="reference_type" id="reference_type" class="form-control">
                        
                        <option value="personnel">Personnel</option>
                        <option value="professional">Professional</option>
                        
                        </select>
                         


                        </div></td>
                        </tr>

                         
                         
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addReferences()">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>