<div class="row clearfix">
    <div class="col-md-12">
        <form id="editOtherDetailsForm" name="editOtherDetailsForm" action="">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th style="color:#49c5b6" colspan="4"> Other Details </th>
            </tr>
            <tr>
                <th>Candidate Source : </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="candidate_source" id="candidate_source" class="form-control" onchange="showHide(this.value , 7)">
                    <option value="">Please Select</option>
                    <option value="naukri" <?= (@$otherDetailsRecArr->candidate_source == "naukri") ? "selected" : ""; ?>>Naukri.com</option>
                    <option value="reference" <?= (@$otherDetailsRecArr->candidate_source == "reference") ? "selected" : ""; ?>>Reference</option>
                    <option value="other" <?= (@$otherDetailsRecArr->candidate_source == "other") ? "selected" : ""; ?>>Other</option>
                    </select>
                    </div>
                </td>
            </tr>

      
 
 

            <tr id="showHide_7" <?= (@$otherDetailsRecArr->candidate_source == "reference") ? 'style=""' : 'style="display: none"'; ?>>
                <th>Candidate Reference
 : </th>

  
                <td colspan="3"> 
                    <div class="form-group">
                        <input type="text" name="candidate_reference" id="candidate_reference" value="<?= (@$otherDetailsRecArr->candidate_reference) ? @$otherDetailsRecArr->candidate_reference : ""; ?>" class="form-control">
                    </div>
               
                </td>
            </tr>


            

            

             <tr>
                <th>Have you ever been convicted of a criminal offence                                                          
                : </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="criminal_offense" id="criminal_offense" class="form-control" onchange="showHide(this.value , 6)">
                    <option value="yes" <?= (@$otherDetailsRecArr->criminal_offense == "yes") ? "selected" : ""; ?> >Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->criminal_offense == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
             <tr id="showHide_6" <?= (@$otherDetailsRecArr->criminal_offense == "yes") ? 'style=""' : 'style="display: none"'; ?>>
                <th>If yes please give details              
                
 : </th>
                <td colspan="3"> 
                    <div class="form-group">
                        


                         <textarea name="criminal_offense_detail" id="criminal_offense_detail"   class="form-control"><?= (@$otherDetailsRecArr->criminal_offense_detail) ? @$otherDetailsRecArr->criminal_offense_detail : ""; ?></textarea>
                    </div>
               
                </td>
            </tr>


             <tr>
                <th>Have you ever been under medical treatment / counselling for drug or alcohol abuse                                                                      
 </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="drug_alcohal_abuse" id="drug_alcohal_abuse" class="form-control" onchange="showHide(this.value , 5)">
                    <option value="yes" <?= (@$otherDetailsRecArr->drug_alcohal_abuse == "yes") ? "selected" : ""; ?>>Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->drug_alcohal_abuse == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
             <tr id="showHide_5" <?= (@$otherDetailsRecArr->drug_alcohal_abuse == "yes") ? 'style=""' : 'style="display: none"'; ?>>
                <th>If yes please give details              
                
 : </th>
                <td colspan="3"> 
                    <div class="form-group">



                         <textarea name="drug_abuse_detail" id="drug_abuse_detail"   class="form-control"><?= (@$otherDetailsRecArr->drug_abuse_detail) ? @$otherDetailsRecArr->drug_abuse_detail : ""; ?></textarea>
                    </div>
               
                </td>
            </tr>
             



             <tr>
                <th>Have you any pre existing medical condition / illness                                                                       
                                                                     
 </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="pre_existing_illness" id="pre_existing_illness" class="form-control" onchange="showHide(this.value , 4)">
                    <option value="yes" <?= (@$otherDetailsRecArr->pre_existing_illness == "yes") ? "selected" : ""; ?>>Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->pre_existing_illness == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
             <tr id="showHide_4" <?= (@$otherDetailsRecArr->pre_existing_illness == "yes") ? 'style=""' : 'style="display: none"'; ?>>
                <th>If yes please give details              
                
 : </th>
                <td colspan="3"> 
                    <div class="form-group">
                        


                         <textarea name="pre_existing_illness_detail" id="pre_existing_illness_detail"   class="form-control"><?= (@$otherDetailsRecArr->pre_existing_illness_detail) ? @$otherDetailsRecArr->pre_existing_illness_detail : ""; ?></textarea>
                    </div>
               
                </td>
            </tr>



             <tr>
                <th>Do you suffer from any partial defect or partial disability                                                                     
                                                                       
                                                                     
 </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="partial_disabilities" id="partial_disabilities" class="form-control" onchange="showHide(this.value , 3)">
                    <option value="yes" <?= (@$otherDetailsRecArr->partial_disabilities == "yes") ? "selected" : ""; ?>>Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->partial_disabilities == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
             <tr id="showHide_3" <?= (@$otherDetailsRecArr->partial_disabilities == "yes") ? 'style=""' : 'style="display: none"'; ?>>
                <th>If yes please give details              
                
 : </th>
                <td colspan="3"> 
                    <div class="form-group">

                        <textarea name="partial_disabilities_detail" id="partial_disabilities_detail"   class="form-control"><?= (@$otherDetailsRecArr->partial_disabilities_detail) ? @$otherDetailsRecArr->partial_disabilities_detail : ""; ?></textarea>


                        
                    </div>
               
                </td>
            </tr>






             <tr>
                <th>Do you have any relative employed in CEG or any subsidiary of CEG                                                                       
                                                                   
                                                                       
                                                                     
 </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="relative_employed_ceg" id="relative_employed_ceg" class="form-control" onchange="showHide(this.value , 2)">
                    <option value="yes" <?= (@$otherDetailsRecArr->relative_employed_ceg == "yes") ? "selected" : ""; ?>>Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->relative_employed_ceg == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
            

<tr id="showHide_2" <?= (@$otherDetailsRecArr->relative_employed_ceg == "yes") ? 'style=""' : 'style="display: none"'; ?>>
    <th>Relation   
                                      
            
                
 : </th>
                <td > 
                    <div class="form-group">
                        <input type="text" name="employed_ceg_relation" id="employed_ceg_relation" value="<?= (@$otherDetailsRecArr->employed_ceg_relation) ? @$otherDetailsRecArr->employed_ceg_relation : ""; ?>" class="form-control">
                    </div>
               
                </td>
                <th> Emp Code   
               
                
 : </th>
                <td > 
                    <div class="form-group">


                         <? if($loginUsersRelatives){?>
                <select name="employed_ceg_user_id" id="employed_ceg_user_id" class="form-control">
                    <?php foreach($loginUsersRelatives as $key => $val){?>
                    <option value="<?php echo $val->employeeId; ?>" <?php if($otherDetailsRecArr->employed_ceg_user_id == $val->employeeId){ echo "selected"; } ?>><?php echo $val->userfullname; ?></option>
                <?php }?>
                </select>
            <? } ?>
                         
                    </div>
               
                </td>
            </tr>







             <tr>
                <th>Have you been previously employed with CEG                                                                      
                                                                       
                                                                   
                                                                       
                                                                     
 </th>
                <td colspan="3"> 
                    <div class="form-group"> 
                    <select name="previous_employed_with_ceg" id="previous_employed_with_ceg" class="form-control" onchange="showHide(this.value , 1)">
                    <option value="yes" <?= (@$otherDetailsRecArr->previous_employed_with_ceg == "yes") ? "selected" : ""; ?>>Yes</option>
                    <option value="no" <?= (@$otherDetailsRecArr->previous_employed_with_ceg == "no") ? "selected" : ""; ?>>No</option>
                    
                    </select>
                    </div>
                </td>
            </tr>
             <tr id="showHide_1" <?= (@$otherDetailsRecArr->previous_employed_with_ceg == "yes") ? 'style=""' : 'style="display: none"'; ?>>
                <th>Employee Code                                                                       
  
                                      
            
                
 : </th>
                <td> 
                    <div class="form-group">
                      


                         <? if($loginUsersRelatives){?>
                <select name="previous_employee_code" id="previous_employee_code" class="form-control">
                    <?php foreach($loginUsersRelatives as $key => $val){?>
                    <option value="<?php echo $val->employeeId; ?>" <?php if($otherDetailsRecArr->previous_employee_code == $val->employeeId){ echo "selected"; } ?>><?php echo $val->userfullname; ?></option>
                <?php }?>
                </select>
            <? } ?>
                         
                    </div>
               
                </td>


                 <th>Reason for leaving 
   
               
                
 : </th>
                <td > 
                    <div class="form-group">
                        <textarea name="previous_reason_for_leaving_ceg" id="previous_reason_for_leaving_ceg"   class="form-control"><?= (@$otherDetailsRecArr->previous_reason_for_leaving_ceg) ? @$otherDetailsRecArr->previous_reason_for_leaving_ceg : ""; ?></textarea>
                    </div>
               
                </td>


            </tr>

 


<tr>
                
                <td colspan="4"> 
                    <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateOtherDetails()">Save</a>
                    </div>
               
                </td>
            </tr>





        </tbody>
    </table>
 </form>
</div></div>






