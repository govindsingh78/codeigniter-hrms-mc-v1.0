<div class="row clearfix">
    <div class="col-md-12">

         <a href="javascript:void(0)" class="btn btn-info pull-right mr-6" data-toggle="modal" data-target="#addFamily" style="margin-right: 20px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Family</a>


    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Family Name</th>
                <th>Family Relation</th>
                <th>Family DOB</th>
                <th>Aadhar Number</th>
                <th>Dependent or Not</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($FamilyDetailsRecArr) {
                foreach ($FamilyDetailsRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->dependent_name) ? $recD->dependent_name : ""; ?></td>
                        <td><?= ($recD->dependent_relation) ? $recD->dependent_relation : ""; ?></td>
                        <td><?= ($recD->dependent_dob) ? date("d-m-Y", strtotime($recD->dependent_dob)) : ""; ?></td>
                        <td><?= ($recD->aadhar_no) ? $recD->aadhar_no : ""; ?></td>
                        <td><?= ($recD->dependent_select) ? $recD->dependent_select : ""; ?></td>
                       <td>

                        <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editFamily<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>


                        




                        <div class="modal fade" id="editFamily<?= $kEy + 1; ?>" role="dialog">
                        <form id="editFamilyForm<?= $kEy + 1; ?>" name="editFamilyForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Family</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Family Name : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group"><input type="text" name="dependent_name" id="dependent_name_<?= $kEy + 1; ?>" value="<?= ($recD->dependent_name) ? $recD->dependent_name : ""; ?>" class="form-control"></div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Family Relation : </th>
                        <td><div class="form-group"><input type="text" name="dependent_relation" id="dependent_relation_<?= $kEy + 1; ?>" value="<?= ($recD->dependent_relation) ? $recD->dependent_relation : ""; ?>" class="form-control"></div></td>
                        </tr>



                        
                         
                        
                        
                        


                        <tr>
                        <th>Family DOB : </th>
                        <td><div class="form-group"><input type="date" name="dependent_dob" id="dependent_dob_<?= $kEy + 1; ?>" value="<?= ($recD->dependent_dob) ? $recD->dependent_dob : ""; ?>" class="form-control"></div></td>

                        </tr><tr>
                        <th>Aadhar Number : </th>
                        <td><div class="form-group"><input type="text" name="aadhar_no" id="aadhar_no_<?= $kEy + 1; ?>" value="<?= ($recD->aadhar_no) ? $recD->aadhar_no : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Dependent or Not : </th>
                        <td><div class="form-group"><input type="text" name="dependent_select" id="dependent_select_<?= $kEy + 1; ?>" value="<?= ($recD->dependent_select) ? $recD->dependent_select : ""; ?>" class="form-control"></div></td>
                        </tr>

                         
                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateFamily('<?= $kEy + 1; ?>')">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>


                         </td>





                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red" colspan="7"> Record Not Found. </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>
</div>


 <div class="modal fade" id="addFamily" role="dialog">
                        <form id="addFamilyForm" name="addFamilyForm" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Family</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Family Name : </th>
                        <td>



                            <!-- loginEducationLevel >> id, educationlevelcode  -->
                            <div class="form-group"><input type="text" name="dependent_name" id="dependent_name" value="" class="form-control"></div>



                        </td>

                        </tr>
                        <tr>
                     

                        <th>Family Relation : </th>
                        <td><div class="form-group"><input type="text" name="dependent_relation" id="dependent_relation" value="" class="form-control"></div></td>
                        </tr>



                        
                         
                        
                        
                        


                        <tr>
                        <th>Family DOB : </th>
                        <td><div class="form-group"><input type="date" name="dependent_dob" id="dependent_dob" value="" class="form-control"></div></td>

                        </tr><tr>
                        <th>Aadhar Number : </th>
                        <td><div class="form-group"><input type="text" name="aadhar_no" id="aadhar_no" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Dependent or Not : </th>
                        <td><div class="form-group"><input type="text" name="dependent_select" id="dependent_select" value="" class="form-control"></div></td>
                        </tr>

                        
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addFamily()">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>