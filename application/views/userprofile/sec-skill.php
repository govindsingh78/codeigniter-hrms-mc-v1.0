<div class="row clearfix">
    <div class="col-md-12">
        <a href="javascript:void(0)" class="btn btn-info pull-right mr-6" data-toggle="modal" data-target="#addSkills" style="margin-right: 60px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Skill</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Skill Name</th>
                <th>Years Experience</th>
                <th>Competency Level</th>
                <th>Last Use</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EmplSkillRecArr) {
                foreach ($EmplSkillRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->skillname) ? $recD->skillname : ""; ?></td>
                        <td><?= ($recD->yearsofexp) ? $recD->yearsofexp : ""; ?></td>
                        <td><?= ($recD->competencylevelid) ? $skillArrlv[$recD->competencylevelid] : ""; ?></td>
                        <td><?= ($recD->year_skill_last_used) ? date("d-m-Y", strtotime($recD->year_skill_last_used)) : ""; ?></td>
                        <td><a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editSkills<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>




                        <div class="modal fade" id="editSkills<?= $kEy + 1; ?>" role="dialog">
                        <form id="editSkillsForm<?= $kEy + 1; ?>" name="editSkillsForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Skills</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Skill Name : </th>
                        <td><div class="form-group"><input type="text" name="skillname" id="skillname_<?= $kEy + 1; ?>" value="<?= ($recD->skillname) ? $recD->skillname : ""; ?>" class="form-control"></div></td>

                        </tr>
                        <tr>

                        <th>Years Experience : </th>
                        <td><div class="form-group"><input type="number" name="yearsofexp" id="yearsofexp_<?= $kEy + 1; ?>" value="<?= ($recD->yearsofexp) ? $recD->yearsofexp : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Competency Level : </th>
                        <td><div class="form-group">


                            



                            <? if($skillArrlv){?>
                            <select name="competencylevelid" id="competencylevelid_<?= $kEy + 1; ?>" class="form-control">
                            <?php foreach($skillArrlv as $key => $val){?>
                            <option value="<?php echo $key; ?>" <?php if($recD->competencylevelid == $key){ echo "selected"; } ?>><?php echo $val; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>


                        </div></td>

                        </tr><tr>
                        <th>Last Use : </th>
                        <td><div class="form-group"><input type="date" name="year_skill_last_used" id="year_skill_last_used_<?= $kEy + 1; ?>" value="<?= ($recD->year_skill_last_used) ? $recD->year_skill_last_used : ""; ?>" class="form-control"></div></td>
                        </tr>
                         


                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateSkills('<?= $kEy + 1; ?>')">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>






                        </td>

                    </tr>
                    <?php
                }
            } else { ?>
                <tr>
                    <td style="color:red" colspan="5"> Record Not Found. </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>


<div class="modal fade" id="addSkills" role="dialog">
                        <form id="addSkillsForm" name="addSkillsForm" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Add Skills</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Skill Name : </th>
                        <td><div class="form-group"><input type="text" name="skillname" id="skillname" value="" class="form-control"></div></td>

                        </tr>
                        <tr>

                        <th>Years Experience : </th>
                        <td><div class="form-group"><input type="number" name="yearsofexp" id="yearsofexp" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Competency Level : </th>
                        <td><div class="form-group">


                            



                            <? if($skillArrlv){?>
                            <select name="competencylevelid" id="competencylevelid" class="form-control">
                            <?php foreach($skillArrlv as $key => $val){?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                            <?php }?>
                            </select>
                            <? } ?>


                        </div></td>

                        </tr><tr>
                        <th>Last Use : </th>
                        <td><div class="form-group"><input type="date" name="year_skill_last_used" id="year_skill_last_used" value="" class="form-control"></div></td>
                        </tr>
                         


                         
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addSkills()">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>