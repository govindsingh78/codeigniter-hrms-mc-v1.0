<div class="row clearfix">
    <div class="col-md-12">
        <a href="javascript:void(0)" class="btn btn-info pull-right" data-toggle="modal" data-target="#addExperience" style="margin-right: 20px;
    margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Experience</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sr.No.</th>
                <th>Company Name</th>
                <th>Company Website</th>
                <th>Company Location</th>
                <th>Designation</th>
                <th>From</th>
                <th>To</th>
                <th>Last Salary (Withdrawn)</th>
                <th>Reason for Leaving</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($EmplExprRecArr) {
                foreach ($EmplExprRecArr as $kEy => $recD) {
                    ?>
                    <tr>
                        <td><?= $kEy + 1; ?></td>
                        <td><?= ($recD->comp_name) ? $recD->comp_name : ""; ?></td>
                        <td><?= ($recD->comp_website) ? $recD->comp_website : ""; ?></td>
                        <td><?= ($recD->comp_location) ? $recD->comp_location : ""; ?></td>
                        <td><?= ($recD->designation) ? $recD->designation : ""; ?></td>
                        <td><?= ($recD->from_date) ? date("d-m-Y", strtotime($recD->from_date)) : ""; ?></td>
                        <td><?= ($recD->to_date) ? date("d-m-Y", strtotime($recD->to_date)) : ""; ?></td>
                        <td><?= ($recD->last_salary_drawn) ? $recD->last_salary_drawn : ""; ?></td>
                        <td><?= ($recD->reason_for_leaving) ? $recD->reason_for_leaving : ""; ?></td>
                        <td><a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#editExperience<?= $kEy + 1; ?>"><i class="fa fa-edit"></i> Edit</a>




                        <div class="modal fade" id="editExperience<?= $kEy + 1; ?>" role="dialog">
                        <form id="editExperienceForm<?= $kEy + 1; ?>" name="editExperienceForm<?= $kEy + 1; ?>" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Manage Experience</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Company Name : </th>
                        <td><div class="form-group"><input type="text" name="comp_name" id="comp_name_<?= $kEy + 1; ?>" value="<?= ($recD->comp_name) ? $recD->comp_name : ""; ?>" class="form-control"></div></td>

                        </tr>
                        <tr>

                        <th>Website : </th>
                        <td><div class="form-group"><input type="text" name="comp_website" id="comp_website_<?= $kEy + 1; ?>" value="<?= ($recD->comp_website) ? $recD->comp_website : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Designation : </th>
                        <td><div class="form-group"><input type="text" name="designation" id="designation_<?= $kEy + 1; ?>" value="<?= ($recD->designation) ? $recD->designation : ""; ?>" class="form-control"></div></td>

                        </tr><tr>
                        <th>From (Working) : </th>
                        <td><div class="form-group"><input type="date" name="from_date" id="from_date_<?= $kEy + 1; ?>" value="<?= ($recD->from_date) ? $recD->from_date : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>To (Working) : </th>
                        <td><div class="form-group"><input type="date" name="to_date" id="to_date_<?= $kEy + 1; ?>" value="<?= ($recD->to_date) ? $recD->to_date : ""; ?>" class="form-control"></div></td>
                        </tr>

                        <!-- last_salary_drawn
                        comp_location
                        reason_for_leaving -->
                        <tr>
                        <th>Company Location : </th>
                        <td><div class="form-group"><input type="text" name="comp_location" id="comp_location_<?= $kEy + 1; ?>" value="<?= ($recD->comp_location) ? $recD->comp_location : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Last Salary (Withdrawn) : </th>
                        <td><div class="form-group"><input type="text" name="last_salary_drawn" id="last_salary_drawn_<?= $kEy + 1; ?>" value="<?= ($recD->last_salary_drawn) ? $recD->last_salary_drawn : ""; ?>" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Reason for Leaving : </th>
                        <td><div class="form-group"><textarea name="reason_for_leaving" id="reason_for_leaving_<?= $kEy + 1; ?>"   class="form-control">
                            <?= ($recD->reason_for_leaving) ? $recD->reason_for_leaving : ""; ?>

                        </textarea></div></td>
                        </tr>

                        <input type="hidden" name="table_id" name="table_id" value="<?= $recD->id; ?>">
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="updateExperience('<?= $kEy + 1; ?>')">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>






                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td style="color:red" colspan="6"> Record Not Found. </td>
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div></div>




                        <div class="modal fade" id="addExperience" role="dialog">
                        <form id="addExperienceForm" name="addExperienceForm" action="">
                        <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-header">

                        <h4 class="modal-title">Add Experience</h4>
                        <button type="button" class="close" style="color:red" data-dismiss="modal">&times;</button>

                        </div>

                        <div class="modal-body">


                        <div class="row clearfix">
                        <div class="col-md-12">


                        <table class="table table-striped">
                        <tbody>

                        <tr>
                        <th>Company Name : </th>
                        <td><div class="form-group"><input type="text" name="comp_name" id="comp_name" value="" class="form-control"></div></td>

                        </tr>
                        <tr>

                        <th>Website : </th>
                        <td><div class="form-group"><input type="text" name="comp_website" id="comp_website" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Designation : </th>
                        <td><div class="form-group"><input type="text" name="designation" id="designation" value="" class="form-control"></div></td>

                        </tr><tr>
                        <th>From (Working) : </th>
                        <td><div class="form-group"><input type="date" name="from_date" id="from_date" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>To (Working) : </th>
                        <td><div class="form-group"><input type="date" name="to_date" id="to_date" value="" class="form-control"></div></td>
                        </tr>

                        <tr>
                        <th>Company Location : </th>
                        <td><div class="form-group"><input type="text" name="comp_location" id="comp_location" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Last Salary (Withdrawn) : </th>
                        <td><div class="form-group"><input type="text" name="last_salary_drawn" id="last_salary_drawn" value="" class="form-control"></div></td>
                        </tr>
                        <tr>
                        <th>Reason for Leaving : </th>
                        <td><div class="form-group"><textarea name="reason_for_leaving" id="reason_for_leaving"   class="form-control">
                            

                        </textarea></div></td>
                        </tr>


                         
                        </tbody>
                        </table>
                        </div>
                        </div>


                        <div class="box-content">

                        <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm pull-right" onclick="addExperience()">Save</a>



                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        </form>
                        </div>

