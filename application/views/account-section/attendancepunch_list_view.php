<?php
error_reporting(0);
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$BgcolorCodeArr = array("Weekly-Off" => "#e27f03", "Weekly-Off" => "#e27f03", "holidays" => "#202823", "leaves" => "#00a1de", "tours" => "#52C68C");
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> User Profile</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12">

                        <div class="card">
                            <div class="body">
                                <form method="post" action="<?= base_url("myattendance"); ?>">
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="mb-3">
                                                        <b> Month : </b>
                                                        <select class="form-control" name="attendance_month" data-placeholder="Select">
                                                            <option <?= (@$atten_month == "") ? "Selected" : ""; ?> value=""> -- All -- </option>
                                                            <option <?= (@$atten_month == "01") ? "Selected" : ""; ?> value="01"> January </option>
                                                            <option <?= (@$atten_month == "02") ? "Selected" : ""; ?> value="02"> February </option>
                                                            <option <?= (@$atten_month == "03") ? "Selected" : ""; ?> value="03"> March </option>
                                                            <option <?= (@$atten_month == "04") ? "Selected" : ""; ?> value="04"> April </option>
                                                            <option <?= (@$atten_month == "05") ? "Selected" : ""; ?> value="05"> May </option>
                                                            <option <?= (@$atten_month == "06") ? "Selected" : ""; ?> value="06"> June </option>
                                                            <option <?= (@$atten_month == "07") ? "Selected" : ""; ?> value="07"> July </option>
                                                            <option <?= (@$atten_month == "08") ? "Selected" : ""; ?> value="08"> August </option>
                                                            <option <?= (@$atten_month == "09") ? "Selected" : ""; ?> value="09"> September </option>
                                                            <option <?= (@$atten_month == "10") ? "Selected" : ""; ?> value="10"> October </option>
                                                            <option <?= (@$atten_month == "11") ? "Selected" : ""; ?> value="11"> November </option>
                                                            <option <?= (@$atten_month == "12") ? "Selected" : ""; ?> value="12"> December </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="mb-3">
                                                        <b> Year : </b>
                                                        <select class="form-control" name="attendance_year" data-placeholder="Select">  
                                                            <option <?= (@$atten_year == "2020") ? "Selected" : ""; ?> value="2020"> 2020 </option>
                                                            <option <?= (@$atten_year == "2021") ? "Selected" : ""; ?> value="2021"> 2021 </option>
                                                            <option <?= (@$atten_year == "2022") ? "Selected" : ""; ?> value="2022"> 2022 </option>
                                                            <option <?= (@$atten_year == "2023") ? "Selected" : ""; ?> value="2023"> 2023 </option>
                                                            <option <?= (@$atten_year == "2024") ? "Selected" : ""; ?> value="2024"> 2024 </option>
                                                            <option <?= (@$atten_year == "2025") ? "Selected" : ""; ?> value="2025"> 2025 </option>
                                                            <option <?= (@$atten_year == "2026") ? "Selected" : ""; ?> value="2026"> 2025 </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="mb-2">
                                                        <br>
                                                        <input type="submit" value="Filter Record" name="filter" class="btn btn-round btn-primary">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="card">
                            <?php
                            if ($BgcolorCodeArr) {
                                foreach ($BgcolorCodeArr as $keeyy => $ColoROw) {
                                    ?>
                                    <span class="btn btn-default" style="color:white;background:<?= $ColoROw; ?>"><?= ucfirst($keeyy); ?></span>
                                    <?php
                                }
                            }
                            ?>

                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Date</th>
                                                <th>In Time</th>
                                                <th>Out Time</th>
                                                <th>Late By</th>
                                                <th>Working Hours</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if ($dateRangeArr) {
                                                foreach ($dateRangeArr as $kEy => $dataRow) {
                                                    $colorCode = null;
                                                    $Dispdate = date("d-m-Y", strtotime($dataRow));
                                                    $punchRec = getSingleDateAttByUserID($Dispdate, $machine_userid);
                                                    $AllPunchDetail = GetAllPunchDetail($Dispdate, $machine_userid);
                                                    $punchWorkingHour = GetTotalWorkingHourWithPunch(date("d-m-Y", strtotime($dataRow)), $machine_userid);

                                                    $firstReturn = date("d-m-Y", strtotime("first saturday of " . date('M', strtotime($Dispdate)) . " " . date('Y', strtotime($Dispdate)) . ""));
                                                    $thirdReturn = date("d-m-Y", strtotime("third saturday of " . date('M', strtotime($Dispdate)) . " " . date('Y', strtotime($Dispdate)) . ""));

                                                    if (date('D', strtotime($Dispdate)) == 'Sun') {
                                                        $colorCode = "Weekly-Off";
                                                    }
                                                    if (strtotime($Dispdate) == strtotime($firstReturn)) {
                                                        $colorCode = "Weekly-Off";
                                                    }
                                                    if (strtotime($Dispdate) == strtotime($thirdReturn)) {
                                                        $colorCode = "Weekly-Off";
                                                    }
                                                    if (in_array($Dispdate, $holidaysDateArr)) {
                                                        $colorCode = "holidays";
                                                    }
                                                    if (in_array($Dispdate, $leaveDateArr)) {
                                                        $colorCode = "leaves";
                                                    }
                                                    if (in_array($Dispdate, $tourDateArr)) {
                                                        $colorCode = "tours";
                                                    }

                                                    if (strtotime($dataRow) <= strtotime(date("d-m-Y"))) {
                                                        ?>
                                                        <tr style="background:<?= ($colorCode) ? $BgcolorCodeArr[$colorCode] . ";color:white;" : "" ?>" >
                                                            <td><?= $kEy + 1; ?></td>
                                                            <td><?= ($dataRow) ? $Dispdate : ""; ?></td>
                                                            <td><?= (@$punchRec['intime']) ? @$punchRec['intime'] : ""; ?></td>
                                                            <td><?= (@$punchRec['outtime']) ? @$punchRec['outtime'] : ""; ?></td>
                                                            <td><?= (@$punchRec['lateby']) ? round(@$punchRec['lateby']) : "0"; ?> Minutes</td>
                                                            <td><?= ($punchWorkingHour) ? date('H:i', mktime(0, $punchWorkingHour)) : ""; ?></td>
                                                            <td>
                                                                <?php if ($AllPunchDetail['intime']): ?>
                                                                    <ul id="myUL">
                                                                        <li><span class="caret">Details</span>
                                                                            <ul class="nested">
                                                                                <table class="table" style="color:black">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Punch-In</th>
                                                                                            <th>Punch-Out</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if ($AllPunchDetail['intime']):
                                                                                            foreach ($AllPunchDetail['intime'] as $keyAtt => $recAtt) { ?>
                                                                                                <tr>
                                                                                                    <td><?= ($recAtt) ? $recAtt : ""; ?></td>
                                                                                                    <td><?= (@$AllPunchDetail['outtime'][$keyAtt]) ? @$AllPunchDetail['outtime'][$keyAtt] : "<span style='color:red'>MP</span>"; ?></td>
                                                                                                </tr>
                                                                                            <?php } endif;
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="7"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Date</th>
                                                <th>Intime</th>
                                                <th>Out Time</th>
                                                <th>Late By</th>
                                                <th>Working hour</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

<style>
    ul, #myUL {
        list-style-type: none;
    }
    #myUL {
        margin: 0;
        padding: 0;
    }
    .caret {
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;
    }
    .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
    }
    .nested {
        display: none;
    }
    .active {
        display: block;
    }
</style>

<script>
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
    }
</script>
