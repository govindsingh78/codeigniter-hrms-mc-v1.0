<?php
$HrPermUserIdsArr = GetUserIDHRPermission();
$loginid = $this->session->userdata('loginid');
?>
<style>
    @media only screen and (max-width: 1024px) {
        .user-account li.dropdown.selected_header 
        {
            display: none;
        }
    }
</style>
<?php
// $id = $this->session->userdata('loginid');
// $chkIoRo = check_self_IO_RO($id);
// echo "<pre>";print_r($chkIoRo); die; 
?>
<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>
        <div class="navbar-brand">
            <a href="<?= base_url(""); ?>">
                <img src="http://www.cegindia.com/images/logo.png" style="width:200px!important;" alt="CEG" class="img-responsive logo">
            </a>                
        </div>
        <div class="navbar-right">
            <form id="navbar-search" class="navbar-form search-form" style="margin: 15px 0 0 0;">
                <input value="" class="form-control" placeholder="Search here" type="text" id="search">
                <button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
            </form>                
            <div id="navbar-menu" style="height: 65px; margin-top: -17px;">
                <ul class="nav navbar-nav"> 
                    <div class="user-account" style="margin: 0px !important;">
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Organisation</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Organisation Chart</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-equalizer"></i> <span>Charter of Duties</span></a></li>
                                <!--<li class="menu-heading">BILLING</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>JD</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>KRA</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">About Us</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Vision</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-equalizer"></i> <span>Mission</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-lock"></i> <span>Values</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-bell"></i> <span>Quality Policy</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-bell"></i> <span>From MD's Desk</span></a></li>
                                <!--<li class="menu-heading">BILLING</li>-->

                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">WhatsUp</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">NEWS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>News</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Events</span></a></li>
                                <!--<li class="divider"></li>-->
                                <!--<li class="menu-heading" style="color:#fff">EVENTS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>Sports</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Cultural</span></a></li> 
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Vacancies</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Internal</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>External</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Future</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Policies</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">NEWS</li>-->
                                <li><a href="../public/uploads/policy/Performance_Management_Policy.pdf" target="_blank"><i class="icon-note"></i> <span>PMS</span></a></li>
                                <li><a href="../public/uploads/policy/Service-Rules.pdf" target="_blank"><i class="icon-note"></i> <span>Service Rules</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Policies</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Employee Manual</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Circulars</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Office Orders</span></a></li>
                                <!--<li class="divider"></li>-->
                                <!--<li class="menu-heading">Policies</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>ESIC</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-credit-card"></i> <span>EPF</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Medicalim</span></a></li> 
                                <li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Group Insurance</span></a></li> 
                                <li><a href="javascript:void(0);"><i class="icon-printer"></i> <span>Benevolent Fund</span></a></li> 
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Training</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Training Calender</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Knowledge Bank</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Calender</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Calender</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Planner</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> Directory</span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown selected_header">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">Suggesions</a>
                            <ul class="dropdown-menu user-menu account">
                                <!--<li class="menu-heading">ACCOUNT SETTINGS</li>-->
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>Feedback</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span>General</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> Housekeeping Complaints</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> IT Complaints</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> HR Helpdesk</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> Contact Details</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> Write to us</span></a></li>
                                <li><a href="javascript:void(0);"><i class="icon-note"></i> <span> Confidential Message</span></a></li>
                            </ul>
                        </li>


                        <ul class="nav navbar-nav">
                            <div class="user-account">
                                <li>
                                    <div class="dropdown icon text-info">
                                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-logout"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right account" >
                                            <?php if (in_array($loginid, $HrPermUserIdsArr)) { ?>
                                                <li><a href="../../myhrms_hr"><i class="icon-user"></i>HR Functions</a></li>
                                                <li class="divider"></li>
                                            <?php } ?>

                                            <?php
                                            $identTeam = $this->session->userdata('team_identifier');
                                            $identSelf = $this->session->userdata('self_identifier');
                                            if ($identTeam == '1' || $identSelf == '0'):
                                                ?>
                                                <li><a href="<?= base_url("userdashboard") ?>"><i class="icon-user"></i>Employee</a></li>
                                                <li class="divider"></li>
                                                <?php
                                                $identTeam = $this->session->userdata('team_identifier');
                                                $identSelf = $this->session->userdata('self_identifier');
                                            elseif ($identTeam == '0' && $identSelf == '1'):

                                                $id = $this->session->userdata('loginid');
                                                $chkIoRo = check_self_IO_RO($id);
                                                // print_r($chkIoRo); die;
                                                $teamchk = array();
                                                foreach ($chkIoRo as $val):
                                                    $teamchk[] = $val->reporting_manager;
                                                endforeach;
                                                if (in_array($id, $teamchk)):
                                                    ?>
                                                    <li class="team-io-ro"><a href="<?= base_url('team_under_officers'); ?>"><i class="icon-user"></i>Team</a></li>
                                                    <li class="divider"></li>
                                                <?php else: ?>

                                                <?php
                                                endif;
                                            endif;
                                            ?>
                                            <li class="divider"></li>
                                            <li><a href="<?= base_url('logout'); ?>"><i class="icon-power"></i>Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </div>


                            <?php /* ?><li class="dropdown">
                              <div class="dropdown icon text-info">
                              <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-logout"></i></a>
                              <ul class="dropdown-menu dropdown-menu-right account" >
                              <?php if (in_array($loginid, $HrPermUserIdsArr)) { ?>
                              <li><a href="../../myhrms_hr"><i class="icon-user"></i>HR Functions</a></li>
                              <?php } ?>

                              <?php
                              $id = $this->session->userdata('loginid');
                              $chkIoRo = check_self_IO_RO($id);
                              // print_r($chkIoRo); die;
                              $teamchk = array();
                              foreach($chkIoRo as $val):
                              $teamchk[] = $val->reporting_manager;
                              endforeach;
                              if(in_array($id,$teamchk)): ?>
                              <!-- -->
                              <li class="divider"></li>
                              <li class="team-io-ro"><a href="<?= base_url('team_under_officers'); ?>"><i class="icon-user"></i>Team</a></li>
                              <li class="divider"></li>
                              <?php else: ?>

                              <?php
                              endif; ?>
                              <li><a href="<?= base_url('logout'); ?>"><i class="icon-power"></i>Logout</a></li>
                              </ul>
                              </div>
                              </li><?php */ ?>
                        </ul>
                        <!--<ul class="nav navbar-nav">
                            <div class="user-account">
                                <li>
                                    <div class="dropdown icon text-info">
                                        <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><i class="icon-logout fa-lg"></i></strong></a>
                                        <ul class="dropdown-menu dropdown-menu-right account">
                        <?php if (in_array($loginid, $HrPermUserIdsArr)) { ?>
                                                                <li><a href="../../myhrms_hr"><i class="icon-user"></i>HR Functions</a></li>
                        <?php } ?>
                                            <li class="divider"></li>
                                            <li><a href="<?= base_url('logout'); ?>"><i class="icon-power"></i>Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </div>
                        </ul>-->
                    </div>
            </div>
        </div>
</nav>