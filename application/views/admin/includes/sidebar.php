<?php 
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = array_filter(explode('/', $uri_path));
$menuType = 2;
$loginUserRec = GetBasicRecLoginUser();
?>
<?php if ((in_array("add_project", $uri_segments)) or ( in_array("editproject", $uri_segments))) { ?>
    <style>
        li#mymenu_2{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<?php if ((in_array("package_view", $uri_segments)) or ( in_array("managework_in_package", $uri_segments))) { ?>
    <style>
        li#mymenu_9{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<?php if (in_array("clearance_work", $uri_segments)) { ?>
    <style>
        li#mymenu_11{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<?php if (in_array("phys_progress_details", $uri_segments)) { ?>
    <style>
        li#mymenu_13{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<?php if ((in_array("habitation_on_workid", $uri_segments)) or ( in_array("entryinhabitation", $uri_segments))) { ?>
    <style>
        li#mymenu_12{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<?php if ((in_array("milestone_fin_progr", $uri_segments)) or ( in_array("entryinmilestone", $uri_segments))) { ?>
    <style>
        li#mymenu_14{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 1;
}
?>
<!-- Menu 2 Lvl !-->
<?php if ((in_array("district_management", $uri_segments)) or ( in_array("editdistrict", $uri_segments))) { ?>
    <style>
        li#mymenu_3{background:#c8e9d7;}
    </style>
    <?php
    $menuType = 2;
}
?>
<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <?php if ($loginUserRec->profileimg) { ?>
                <img src="<?= EMPLPROFILE . $loginUserRec->profileimg; ?>" class="rounded-circle user-photo" alt="">
            <?php } ?>  
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?= $this->session->userdata('username'); ?></strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    <li><a href="<?= base_url('myprofile'); ?>"><i class="icon-user"></i>My Profile</a></li>
                    <li><a href="#"><i class="icon-envelope-open"></i>Messages</a></li>
                    <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="<?= base_url('logout'); ?>"><i class="icon-power"></i>Logout</a></li>
                </ul>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Employee</a></li>
        </ul>
        <div class="tab-content p-l-0 p-r-0">   
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">
                        <?php
                        $MasterMenuArr = getAllMasterMenu();
                        if ($MasterMenuArr) {
                            foreach ($MasterMenuArr as $rcdRows) { ?>
                                <li class="">
                                    <a style="cursor: pointer" class="<?= ($rcdRows->is_in_childmenu == "1") ? "has-arrow" : ""; ?>"><i class="icon-home"></i><span><?= $rcdRows->menu_name; ?></span></a>
                                    <?php
                                    $childMenuArr = getChildMenuByParentID($rcdRows->fld_id);
                                    if ($childMenuArr) {
                                        foreach ($childMenuArr as $childmenuRows) { ?>
                                            <ul>
                                                <li id="mymenu_<?= $childmenuRows->fld_id; ?>" class="">
                                                    <a href="<?= base_url($childmenuRows->action_url); ?>"><?= $childmenuRows->menu_name; ?></a>
                                                </li>
                                            </ul>
                                            <?php
                                         }
                                      }
                                    ?>
                                </li>
                            <?php }
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>          
    </div>
</div>