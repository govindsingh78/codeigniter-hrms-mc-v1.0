<!--Start Section Form All Element Included  --> 
<script src="<?= FRONTASSETS; ?>bundles/libscripts.bundle.js"></script> 
<script src="<?= FRONTASSETS; ?>bundles/vendorscripts.bundle.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery.maskedinput/jquery.maskedinput.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>multi-select/js/jquery.multi-select.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?= ASSETSVENDOR; ?>nouislider/nouislider.js"></script>
<script src="<?= ASSETSVENDOR; ?>select2/select2.min.js"></script>
<script src="<?= FRONTASSETS; ?>bundles/mainscripts.bundle.js"></script>
<script src="<?= FRONTASSETS; ?>js/pages/forms/advanced-form-elements.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Close Section For Data Table  -->

<!-- Start Javascript -->
<script src="<?= FRONTASSETS; ?>bundles/datatablescripts.bundle.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="<?= ASSETSVENDOR; ?>sweetalert/sweetalert.min.js"></script>
<script src="<?= FRONTASSETS; ?>js/pages/tables/jquery-datatable.js"></script>
 
<script src="<?= ASSETSVENDOR; ?>toastr/toastr.js"></script>
<!-- Close Javascript -->

<style>
    #reqd{color:red}
    ul#ui-id-1 {width:100%!important;}
</style>

<script>
//For search the user record coded by durgesh(16-01-2020)  

    $(function () {
        var availableTags = [
<?php
$user_report = getuserrecord();
foreach ($user_report as $rrow) {
    ?>
                {img: "<?php if (!empty($rrow->profileimg)) { ?><img class='img-circle' src='http://hrms.cegindia.com/public/uploads/profile/<?php echo $rrow->profileimg; ?>' width='40'/><?php } else { ?><img class='img-circle' src='http://hrms.cegindia.com/hrms/assets/img/user.jpg' width='40'/><?php } ?>", id: "<?php echo $rrow->businessunit_name; ?>", value: "<a href=<?= base_url('myprofile') ?>><?php echo ucwords(strtolower($rrow->userfullname)); ?></a>", cp: "<?php echo $rrow->extension_number; ?>", em: "<?php echo $rrow->emailaddress; ?>", ph: "<?php echo $rrow->contactnumber; ?>"},

<?php } ?>
                    ];
                    $("#search").autocomplete({
                        source: availableTags,
                    });
                    $.ui.autocomplete.prototype._renderMenu = function (ul, items) {
                        var self = this;
                        //table definitions
                        ul.append("<table class='table table-bordered' style='width:100%'><thead><tr><th><b>Profile</b></th><th><b>Business unit</b></th><th><b>Emp Name</b></th><th><b>Extension No.<b></th><th><b>Email<b></th><th><b>Contact No.<b></th></tr></thead><tbody></tbody></table>");
                        $.each(items, function (index, item) {
                            self._renderItemData(ul, ul.find("table tbody"), item);
                        });
                    };
                    $.ui.autocomplete.prototype._renderItemData = function (ul, table, item) {
                        return this._renderItem(table, item).data("ui-autocomplete-item", item);
                    };
                    $.ui.autocomplete.prototype._renderItem = function (table, item) {
                        return $("<tr class='ui-menu-item' role='presentation'></tr>")
                                .data("item.autocomplete", item)
                                .append("<td >" + item.img + "</td>" + "<td >" + item.id + "</td>" + "<td>" + item.value + "</td>" + "<td>" + item.cp + "</td>" + "<td>" + item.em + "</td>" + "<td>" + item.ph + "</td>")
                                .appendTo(table);
                    };
                });

</script> 

</body>
</html>