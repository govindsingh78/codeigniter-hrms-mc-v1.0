<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a><?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								<li><a href="<?= base_url("team_under_officers"); ?>">
                                    <button style="margin: 0px 0px 0 20px;" class="btn btn-info btn-xs"> << Back </button>
                                </a></li>
							</ul>
							
                        </div>
						
                    </div>
                </div>
				
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">

                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Employee Name </th>
                                                <th>EMPID</th>
                                                <th>Email</th>
                                                <th>Contact</th>
                                                <th>Department</th>
                                                <th>Designation</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if ($myTeamDetailsArr) {
                                                foreach ($myTeamDetailsArr as $kEy => $dataRow) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->userfullname) ? $dataRow->prefix_name . ". " . $dataRow->userfullname : ""; ?></td>
                                                        <td><?= ($dataRow->employeeId) ? $dataRow->employeeId : ""; ?></td>
                                                        <td><?= ($dataRow->emailaddress) ? $dataRow->emailaddress : ""; ?></td>
                                                        <td><?= ($dataRow->contactnumber) ? $dataRow->contactnumber : ""; ?></td>
                                                        <td><?= ($dataRow->department_name) ? $dataRow->department_name : ""; ?></td>
                                                        <td><?= ($dataRow->position_name) ? $dataRow->position_name : ""; ?></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="8"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Employee Name </th>
                                                <th>EMPID</th>
                                                <th>Email</th>
                                                <th>Contact</th>
                                                <th>Department</th>
                                                <th>Designation</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>