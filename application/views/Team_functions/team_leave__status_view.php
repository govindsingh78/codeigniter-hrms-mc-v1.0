<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>Employee Leave Management</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div> 
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12">
                       
						<div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Sr. No</th>
						<th>Emp Name</th>
						<th>Leave From</th>
						<th>Leave To</th>
						<th>No of Day</th>
						<th>L.Type</th>
						<th>Status</th>
						<th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Sr. No</th>
						<th>Emp Name</th>
						<th>Leave From</th>
						<th>Leave To</th>
						<th>No of Day</th>
						<th>L.Type</th>
						<th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
       
    </div>
	
<script type="text/javascript">
    var table;

    $(document).ready(function () {

        //datatables
        var compid = $('#companynames').val();
        // alert('etre'+compid);

        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('Team_fun_Controller/ajax_list_leave_team') ?>",
                "type": "POST",
                "data": function (data) {
                    data.start_dates = $('#start_dates').val();
                    data.end_dates = $('#end_dates').val();
                },
            },
            "dom": 'lBfrtip',
            "buttons": [
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

        });

        // var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        // $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });

    });



</script>
 <?php $this->load->view('admin/includes/footer'); ?>
</body>