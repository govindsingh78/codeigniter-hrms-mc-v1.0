<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a><?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<div class="row clearfix">
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card text-center bg-info">
							<div class="body"><a href="<?= base_url('team_under_officers_list'); ?>" class="pull-right"><i class="icon-plus" style="color:#fff"></i></a>
								<div class="p-15 text-light">
									<h3><?= $teamArr;?></h3>
									<span>Team</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card text-center bg-secondary">
							<div class="body"><a href="<?= base_url('tour_under_officers_list'); ?>" class="pull-right"><i class="icon-plus" style="color:#fff"></i></a>
								<div class="p-15 text-light">
									<h3><?= $myTeamTourDetailsRec;?></h3>
									<span>On Tour</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card text-center bg-warning">
							<div class="body"><a href="<?= base_url('leave_under_officers_list'); ?>" class="pull-right"><i class="icon-plus" style="color:#000"></i></a>
								<div class="p-15 text-light">
									<h3><?= $myTeamLeaveDetailsRecArr1;?></h3>
									<span>On Leave</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="card text-center bg-dark">
							<div class="body"><a href="<?= base_url('absent_under_officers_list'); ?>" class="pull-right"><i class="icon-plus" style="color:#fff"></i></a>
								<div class="p-15 text-light">
									<h3><?= $totalAbsent;?></h3>
									<span>Absent</span>
								</div>
							</div>
						</div>
					</div>                
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
							<h4>Team Present (<?= count($myTeamPresentDetailsArr); ?>)</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                        <thead>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Employee Name </th>
                                                <th>EMPID</th>
                                                <th>Contact</th>
                                                <th>Designation</th>
                                                <th>In-Time</th>
                                                <th>Out-Time</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
											// echo "<pre>"; print_r($myTeamPresentDetailsArr);
                                            if ($myTeamPresentDetailsArr) {
                                                foreach ($myTeamPresentDetailsArr as $kEy => $dataRow) {
													$Dispdate = date("d-m-Y");
													// echo $Dispdate; die;
													$machine_userid = $this->session->userdata('loginid');
													$punchRec = getSingleDateAttByUserID($Dispdate, $dataRow->machine_id);
                                                    $AllPunchDetail = GetAllPunchDetail($Dispdate, $dataRow->machine_id);
                                                    ?>
                                                    <tr>
                                                        <td><?= $kEy + 1; ?></td>
                                                        <td><?= ($dataRow->userfullname) ? $dataRow->prefix_name . ". " . $dataRow->userfullname : ""; ?></td>
                                                        <td><?= ($dataRow->employeeId) ? $dataRow->employeeId : ""; ?></td>
                                                        <td><?= ($dataRow->contactnumber) ? $dataRow->contactnumber : ""; ?></td>
                                                        <td><?= ($dataRow->position_name) ? $dataRow->position_name : ""; ?></td>
                                                        <td><?= (@$punchRec['intime']) ? @$punchRec['intime'] : ""; ?></td>
                                                        <td><?= (@$punchRec['outtime']) ? @$punchRec['outtime'] : ""; ?></td>
														<td>
                                                                <?php if ($AllPunchDetail['intime']): ?>
                                                                    <ul id="myUL">
                                                                        <li><span class="caret">Details</span>
                                                                            <ul class="nested">
                                                                                <table class="table" style="color:black">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Punch-In</th>
                                                                                            <th>Punch-Out</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if ($AllPunchDetail['intime']):
                                                                                            foreach ($AllPunchDetail['intime'] as $keyAtt => $recAtt) { ?>
                                                                                                <tr>
                                                                                                    <td><?= ($recAtt) ? $recAtt : ""; ?></td>
                                                                                                    <td><?= (@$AllPunchDetail['outtime'][$keyAtt]) ? @$AllPunchDetail['outtime'][$keyAtt] : "<span style='color:red'>In Office</span>"; ?></td>
                                                                                                </tr>
                                                                                            <?php } endif;
                                                                                        ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td style="color:red" colspan="8"> Record Not Found. </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>Sr.No.</th>
                                                <th>Employee Name </th>
                                                <th>EMPID</th>
                                                <th>Contact</th>
                                                <th>Designation</th>
                                                <th>In-Time</th>
                                                <th>Out-Time</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
	<style>
    ul, #myUL {
        list-style-type: none;
    }
    #myUL {
        margin: 0;
        padding: 0;
    }
    .caret {
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;
    }
    .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
    }
    .nested {
        display: none;
    }
    .active {
        display: block;
    }
</style>

<script>
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
    }
</script>
</body>