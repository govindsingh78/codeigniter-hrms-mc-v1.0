<?php
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
$allProjArr = get_all_active_project();
$GetAllEmplArr = get_all_Activeempls();
$GetAllEmplArr1 = get_all_Activeempls_a();
$isorNotArr = array('1' => 'Yes', '0' => 'No');
$LoginbunitId = $this->tank_auth->get_user_bunit_by_id($this->tank_auth->get_user_id());
?>

<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/jquery.dataTables.min.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/dataTables.colVis.css' ?>" rel="stylesheet">
<link href="<?= HOSTNAME . 'assets/back_end/datatables/css/buttons.dataTables.min.css' ?>" rel="stylesheet">
<link rel="stylesheet" href="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/css/jquery-ui.css">
<link rel="stylesheet" href="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/css/bootstrap-select.css">
<script src="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/js/jquery.min.js"></script>
<script src='<?= HOSTNAME; ?>src/jquery-customselect.js'></script>
<link href='<?= HOSTNAME; ?>src/jquery-customselect.css' rel='stylesheet' />

<div class="page-wrapper">
<div class="content container-fluid">
<?php if ($this->session->flashdata('successmsg')): ?>
<div class="alert alert-info alert-dismissible fade in">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<strong> Success ! </strong> <?php echo $this->session->flashdata('successmsg'); ?>
</div>
<?php endif; ?>
    
<div class="row">
<div class="col-sm-6">
<h4 style="color:#3c8dbc" class="page-title"> Ticket List</h4>
</div>
<div class="col-sm-6 text-right m-b-30">
<a href="<?= base_url('travelticket'); ?>">
 <button type="button" class="btn btn-primary rounded"><i class="fa fa-angle-double-left"></i> Back To Ticket</button>
</a>
</div>
</div>
    

    
<form id="form-filter" class="">
<div class="row filter-row">
<div class="col-md-4 col-sm-12 col-xs-12">
<div class="form-group form-focus select-focus">
<label class="control-label"> Bussiness Unit : </label>
<select required name="bunit" id="bunit"  class="select floating">
<option value="">-- Select B Unit --</option>
<option value="1" > CEG </option>
<option value="2" > CEGTH </option>
<option value="3" > CEG Project</option>
</select>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
<div class="form-group form-focus select-focus">
<label class="control-label"> Employee : </label>
<select name="traveluserid" id="traveluserid" class="select floating">
<option value="" selected="selected">--select--</option>
<?php
if ($GetAllEmplArr) {
foreach ($GetAllEmplArr as $Emprowrec) {
?>
<option value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
<?php
}
}
?>
</select>
</div>
</div>
    
<div class="col-md-4 col-sm-12 col-xs-12">
 <div class="form-group form-focus select-focus">
<label class="control-label"> Project : </label>
<select name="travelproject" id="travelproject" class="select floating">
<option value="" selected="selected">-- Select Project --</option>
<?php
if ($allProjArr) {
foreach ($allProjArr as $rowrec) {
?>
<option value="<?= $rowrec->id; ?>"><?= $rowrec->project_name; ?></option>
<?php
}
}
?>
</select>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
 <div class="form-group form-focus">
<label class="control-label"> Travel Date From : </label>
<div class="cal-icon"><input type="text" name="travdatefrom" id="travdatefrom" class="form-control datetimepicker" /></div>      
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
<div class="form-group form-focus">
<label class="control-label"> Travel Date To : </label>
<div class="cal-icon"><input type="text" name="travdateto" id="travdateto" class="form-control datetimepicker" /></div>   
</div>
</div>
<div class="col-md-2 col-sm-12 col-xs-12">
<label class="control-label"> &nbsp; </label>
<button type="button" id="btn-filter" class="btn btn-success btn-block"> Filter </button> &nbsp;
</div>
<div class="col-md-2 col-sm-12 col-xs-12">
<label class="control-label"> &nbsp; </label>
<button type="button" id="btn-reset" class="btn btn-primary btn-block">Reset</button>
</div>
    
</div>
</form>      



            <div class="panel panel-default" style="overflow-x: scroll;"> 
                <div class="row">
                    <div class="col-md-12">
                        <div id="colvis"></div>
                    </div>
                </div>
                <table id="table" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Travelers Name</th>
                            <th>Department</th>
                            <th>Journey From</th>
                            <th>Journey To</th>
                            <th>Travel Date</th>
                            <th>Booking Date</th>
                            <th>Project</th>
                            <th>Cost</th>
<!--                            <th>Invoice</th>-->
                            <th>Trip Type</th>
                             <th>Airlines</th>
                            <th>Approved By</th>
                            <th>Entry Date</th>
                            <th>Entry By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Sr. No</th>
                            <th>Travelers Name</th>
                            <th>Department</th>
                            <th>Journey From</th>
                            <th>Journey To</th>
                            <th>Travel Date</th>
                            <th>Booking Date</th>
                            <th>Project</th>
                            <th>Cost</th>
<!--                            <th>Invoice</th>-->
                            <th>Trip Type</th>
                            <th>Airlines</th>
                            <th>Approved By</th>
                            <th>Entry Date</th>
                            <th>Entry By</th>
                             <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
    </div>
</div>


<?PHP $this->load->view("back_end/includes/footer"); ?>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jquery.dataTables.min.js'; ?>"></script>
<!--<script src="<?= HOSTNAME . 'assets/back_end/bootstrap/js/bootstrap.min.js'; ?>"></script>-->
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.colVis.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/dataTables.buttons.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.flash.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/pdfmake.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/jszip.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/vfs_fonts.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.html5.min.js'; ?>"></script>
<script src="<?= HOSTNAME . 'assets/back_end/datatables/js/buttons.print.min.js'; ?>"></script>
<script src="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/js/bootstrap-select.js"></script>
<script src="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/js/jquery-ui.js"></script>

<style>
    #table_length{margin-left:20px;}
    #table_filter{margin-right:2%;}
    .selectpicker  {background-color: #ffff;}
</style>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= site_url('ajax_list_tickets'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.bunit = $('#bunit').val();
                    data.traveluserid = $('#traveluserid').val();
                    data.travelproject = $('#travelproject').val();
                    data.travdatefrom = $('#travdatefrom').val();
                    data.travdateto = $('#travdateto').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            "dom": 'lBfrtip',
            "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
            "columnDefs": [{"targets": [0], "orderable": false,
                }, ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

        });
        var colvis = new $.fn.dataTable.ColVis(table);
        $('#colvis').html(colvis.button());
        $('#btn-filter').click(function () {
            table.ajax.reload();
        });
        $('#btn-reset').click(function () {
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });
    });
</script>
</body>
</html>