<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$allProjArr = get_all_active_projectnew();
$allProjcegthArr = get_all_active_projectnewcegth();
$GetAllEmplArr = get_all_Activeempls();
$GetAllEmplArr1 = get_all_Activeempls_a();

$UserempID = $this->tank_auth->get_user_id();

$tripTypeArr = array('1' => 'One-Way', '2' => 'Round-Way');
$airlinesListArr = array('Air_India' => 'Air India',
    'Air_India_Express' => 'Air India Express',
    'AirAsia_India' => 'AirAsia India',
    'Alliance_Air' => 'Alliance Air',
    'IndiGo' => 'IndiGo',
    'Jet_Airways' => 'Jet Airways',
    'SpiceJet' => 'SpiceJet',
    'TruJet' => 'TruJet',
    'GoAir' => 'GoAir',
    'Vistara' => 'Vistara'
);
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<?php if ($this->session->flashdata('successmsg')): ?>
					<div class="alert alert-info alert-dismissible ">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
					</div>
				<?php endif; ?>
					
				<?php if ($this->session->flashdata('errormsg')): ?>
					<div class="alert alert-danger alert-dismissible ">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success ! </strong> <?= $this->session->flashdata('errormsg'); ?>
					</div>
				<?php endif; ?>
				
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
							<div class="row">
							<div class="col-xs-10">
							<h4 class="page-title" >Add Travel Ticket Summary:</h4>
							</div>
							</div>
							<?php form_open('travelticket_save', array('class' => '', 'id' => 'travelticket_saveinvtform')); ?>
								<div class="row">
								
									
								<div class="col-lg-2 col-md-6">
									<b>Employee :</b>
									<div class="input-group mb-3">
										<img src="<?= HOSTNAME . "assets/avatar.jpg"; ?>" id="userimg" class="img-circle" alt="Cinque Terre" width="100" height="105"> 
									</div>
								</div>
								
								<div class="col-lg-4 col-md-6">
									<b>Details :</b>
									<div class="input-group mb-3">
										<table class="table" style="border:green 1px solid;">
										<thead>
											<tr>
												<th width="24%">Department</th>
												<th width="24%">B-Unit </th>
												<th width="24%">Email</th>
												<th width="24%">Contact</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td id="dvdepartment_name"></td>
												<td id="dvbunit"></td>
												<td id="dvemail"></td>
												<td id="dvcontact"></td>
											</tr>
										</tbody>
									</table>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
									<b>Bussiness Unit For Project :</b>
									<div class="input-group mb-3">
										<select name="otheremplbunit" id="otheremplbunit" onchange="setprojbunitwise()" class="form-control" >
											<option value=""> -- Select -- </option>
											<option value="1"> CEG </option>
											<option value="2"> CEGTH </option>
											<option value="3"> CEG-Project </option>
									</select>
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Employee Name :</b>
									<div class="input-group mb-3">
										<select required name="empid" id="empid" onchange="getEmpDetails()"  onchange="getEmpDetailsother()" class="form-control">
									<option value="" selected="selected">-- Select Employee --</option>
									<?php
									if ($GetAllEmplArr) {
										foreach ($GetAllEmplArr as $Emprowrec) {
											?>
											<option value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
											<?php
										}
									}
									?>
									<option value="other"> Other </option>
									</select>
									</div>
								</div>	

									

									</div>
									
									
									<div class="row filter-row" id="empothersection" style="border:1px solid green; padding:15px; margin:5px; width: 67%; display: none;"> 

									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label" style="color:green"> Other Employee Name : </label>
										<input type="text" name="otherempl" id="otherempl" class="form-control floating">
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label" style="color:green"> Details About Other Emp : </label>
										<input type="text" name="otherempldetails" id="otherempldetails" class="form-control floating">
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus select-focus">
										<label class="control-label" style="color:green"> Details About Other Emp : </label>
										<select name="otherempbunit" id="otherempbunit" class="form-control">
											<option value=""> -- Select B-Unit- </option>
											<option value="1"> CEG </option>
											<option value="2"> CEGTH </option>
										</select>
									</div>
									</div>
										
									</div>
									
									<div class="row filter-row">
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Journey From : <span id="lvlspan">*</span> </label>
										<input type="text" required name="jfrom" placeholder="Eg:- Jaipur,Delhi.." id="jfrom" class="form-control floating" />   
									</div>
									</div>
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Journey To : <span id="lvlspan">*</span> </label>
										<input type="text" required name="jto" id="jto" placeholder="Eg:- Pune,Udaipur.." class="form-control floating" />   
									</div>
									</div>
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Date of Travel : <span id="lvlspan">*</span> </label>
										<input type="date" required name="dateoftravel" id="dateoftravel" class="form-control floating" />
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Booking Date : <span id="lvlspan">*</span> </label>
										<input type="date" required name="dateofbooking" id="dateofbooking" class="form-control floating" />
									</div>
									</div>
										
									<!-- B unit CEG -->
									<div class="col-md-3 col-xs-12" id="projceg" style="display:none" >
									<div class="form-group form-focus select-focus">
										<label class="control-label"> Project CEG : <span id="lvlspan">*</span> </label>
										<div class="side-by-side clearfix">
											<select required data-placeholder="Choose Project" id="cegprojdropdown" style="width:80%" name="tickproject[]" class="form-control chosen-select" multiple tabindex="4">
												<option value=""></option>
												<?php
												if ($allProjArr) {
													foreach ($allProjArr as $rowrec) {
														?>
														<option value="<?= $rowrec->id; ?>"><?= $rowrec->project_name; ?></option>
														<?php
													}
												}
												?>
											</select>
										</div>
									</div>
									</div>

									<!-- B unit CEG TH Start -->
									<div class="col-md-3 col-sm-12 col-xs-12" id="projcegth" style="display:none" >
									<div class="form-group form-focus select-focus">
										<label class="control-label"> Project CEGTH : <span id="lvlspan">*</span> </label>
										<div class="side-by-side clearfix">
											<select required data-placeholder="Choose Project" id="cegthprojdropdown" name="tickproject[]" style="width:80%" class="form-control chosen-select" multiple tabindex="4">
												<option value=""></option>
												<?php
												if ($allProjcegthArr) {
													foreach ($allProjcegthArr as $cegthrowrec) {
														?>
														<option value="<?= @$cegthrowrec->id; ?>"><?= @$cegthrowrec->project_name; ?> ( <?= @$cegthrowrec->projectcode; ?> ) </option>
														<?php
													}
												}
												?>
											</select>
										</div>
									</div>
									</div>
									<!-- B unit CEG TH Start -->

									</div>
									
									<div class="row filter-row">
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Cost : <span id="lvlspan">*</span> </label>
										<input type="number" min="1" required name="tickcost"  id="tickcost" class="form-control floating" />   
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus select-focus">
										<label class="control-label"> Approved By : <span id="lvlspan">*</span> </label>
										<select required name="approvedby" id="approvedby" class="form-control">
											<option value="" selected="selected">-- Select Approved By --</option>
											<?php
											if ($GetAllEmplArr1) {
												foreach ($GetAllEmplArr1 as $Emprowrec1) {
													?>
													<option value="<?= $Emprowrec1->user_id; ?>"><?= $Emprowrec1->userfullname . " ( " . $Emprowrec1->employeeId . " ) "; ?></option>
													<?php
												}
											}
											?>
										</select>  
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus select-focus">
										<label class="control-label"> Trip Type : <span id="lvlspan">*</span> </label>
										<select required name="triptype" onchange="setroundtrip()" id="triptype" class="form-control">
											<option value="1" > One Way </option>
											<option value="2" >Round Trip </option>
										</select>
									</div>
									</div>    
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus select-focus">
										<label class="control-label"> Select Airlines : <span id="lvlspan">*</span> </label>
										<select name="airlines" required id="airlines" class="form-control">
											<option value="" > -- Select Airlines -- </option>
											<?php
											if ($airlinesListArr) {
												foreach ($airlinesListArr as $kkey => $rowr) {
													?>
													<option value="<?= $kkey; ?>" > <?= $rowr; ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
									</div>
										
									<div class="col-md-3 col-xs-12">
									<div class="form-group form-focus">
										<label class="control-label"> Comment :  </label>
										<input type="text" name="tickcomment"  id="tickcomment" class="form-control floating" />   
									</div>
									</div>  
									<div class="col-lg-2 col-md-6">
									<img  src="<?= HOSTNAME; ?>assets/f1.gif" height="70">
								</div>	
									<div class="col-md-2 col-sm-12 col-xs-12">
										<div class="form-group">
											<div class="col-sm-8"> <br>
												<button type="Submit" id="btn-filter" class="btn btn-primary"> Save / Submit </button> 
											</div>
										</div>
									</div>	

									<div class="col-sm-2 col-xs-2"> 
									<b style="color:#fff">.</b>
									<div class="input-group mb-3">
											<a href="<?= base_url('travelticket_list'); ?>">
											<button type="button" class="btn btn-primary"> View All ..</button>
											</a>
										</div>
									
									</div>
									</div>
									
									<div class="row filter-row">
									<div id="divreturn" style="display:none">
									<div class="col-md-2 col-sm-12 col-xs-12">
										<label for="email"> Return Journey From : <span id="lvlspan">*</span> </label>
										<input type="text" name="returnjfrom" value="" id="returnjfrom" class="form-control" />   
									</div>
									<div class="col-md-2 col-sm-12 col-xs-12">
										<label for="email"> Return Journey To : <span id="lvlspan">*</span> </label>
										<input type="text" name="returnjto" id="returnjto" value="" class="form-control" />   
									</div>
									<div class="col-md-2 col-sm-12 col-xs-12">
										<label for="email"> Return Booking Date : <span id="lvlspan">*</span> </label>
										<input type="text" name="returndateofbooking" readonly id="returndateofbooking" class="form-control" />
									</div>
									<div class="col-md-2 col-sm-12 col-xs-12">
										<label for="email"> Return Travel Date : <span id="lvlspan">*</span> </label>
										<input type="date" name="returndateoftravel" id="returndateoftravel" class="form-control" />

									</div>
									<div class="col-md-2 col-sm-12 col-xs-12">
										<label for="email"> Return Airlines : <span id="lvlspan">*</span> </label>
										<select name="returnairlines" id="returnairlines" class="form-control">
											<option value="" > -- Select Airlines -- </option>
											<?php
											if ($airlinesListArr) {
												foreach ($airlinesListArr as $kkey => $rowr) {
													?>
													<option value="<?= $kkey; ?>" > <?= $rowr; ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
									
									
									</div>

									
											   
									</div>
									
									
									
									<?= form_close(); ?> 
									
								</div>
							</div>
						</div>
					</div>          
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="tabledata" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
											<th>Sr.no</th>
											<th>Emp Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Trip-Type</th>
											<th>Airlines</th>
											<th>Cost</th>
											<th>Entry Date</th>
											<th>Trip</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                           <?php
												$srn = 0;
												if ($latestrec) {
													foreach ($latestrec as $recrw) {
														?>
														<tr>
															<td><?= $srn += 1; ?></td>
															<td><?php
																if ($recrw->hrms_or_otherempl == '1') {
																	echo $recrw->userfullname;
																} else {
																	$decoderV = json_decode($recrw->otherempl_details, true);
																	print @$decoderV['empname'] . " (<span style='color:red'> Other </span>)";
																}
																?></td>
															<td><?= $recrw->department_name; ?></td>
															<td><?= $recrw->trav_from; ?></td>
															<td><?= $recrw->trav_to; ?></td>
															<td><?= date("d-m-Y", strtotime($recrw->dateoftravel)); ?></td>
															<td><?= date("d-m-Y", strtotime($recrw->dateofbooking)); ?></td>
								<!--                                    <td><?php // $recrw->trip_invoiceno;                                           ?></td>-->
															<td><?= $tripTypeArr[$recrw->trip_type]; ?></td>
															<td><?= $recrw->airlines; ?></td>
															<td><?php
																if ($recrw->other < 2) {
																	echo number_format($recrw->tickcost, 2);
																} else {
																	echo '';
																}
																?>
															</td>
															<td><?= date("d-m-Y H:i:s A", strtotime($recrw->entry_date)); ?></td>
															<td>
																<?php if (($recrw->other == '1') and ( $recrw->hrms_or_otherempl == '1')) { ?> 
																	<?php if ($UserempID != '287') { ?>
																		<a href="<?= base_url('editticket/'.$recrw->fld_id); ?>">Edit</a>
																		<?php
																	} else {
																		echo 'Single-Trip';
																	}
																	?>  
																<?php } else { ?>
																	<span style="color:green;">Round-Trip</span>
																<?php } ?>
															</td>
														</tr>
													<?php } ?>
												<?php } ?>
                                        </tbody>

                                        <tfoot>
                                            <tr>
											<th>Sr.no</th>
											<th>Emp Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Trip-Type</th>
											<th>Airlines</th>
											<th>Cost</th>
											<th>Entry Date</th>
											<th>Trip</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
</body>
<?php $this->load->view('admin/includes/footer'); ?>