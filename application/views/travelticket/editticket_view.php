<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$allProjArr = get_all_active_projectnew();
$allProjcegthArr = get_all_active_projectnewcegth();
$GetAllEmplArr = get_all_Activeempls();
$GetAllEmplArr1 = get_all_Activeempls_a();
$tripTypeArr = array('1' => 'One-Way', '2' => 'Round-Way');
$airlinesListArr = array('Air_India' => 'Air India',
    'Air_India_Express' => 'Air India Express',
    'AirAsia_India' => 'AirAsia India',
    'Alliance_Air' => 'Alliance Air',
    'IndiGo' => 'IndiGo',
    'Jet_Airways' => 'Jet Airways',
    'SpiceJet' => 'SpiceJet',
    'TruJet' => 'TruJet',
    'GoAir' => 'GoAir',
    'Vistara' => 'Vistara'
);
$projArrec = '';
if ($tickdetailRow->tickproject) {
    $projArrec = explode(",", $tickdetailRow->tickproject);
}
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<?php if ($this->session->flashdata('successmsg')): ?>
					<div class="alert alert-info alert-dismissible ">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
					</div>
				<?php endif; ?>
				<?php if ($this->session->flashdata('errormsg')): ?>
					<div class="alert alert-danger alert-dismissible ">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success ! </strong> <?= $this->session->flashdata('errormsg'); ?>
					</div>
				<?php endif; ?>
				
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
							<div class="row">
							<div class="col-lg-9">
							<h4 class="page-title" >Edit Travel Ticket Summary:</h4>
							</div>
							<div class="col-lg-3">
							<a href="<?= base_url('travelticket'); ?>">
											<button type="button" id="btn-filter" class="btn btn-primary"> Back </button>
										</a>
							</div>
							</div>
							<?= form_open(base_url('travelticket_update'), array('class' => '', 'id' => 'travelticket_updateinvtform')); ?>
								<div class="row">
								
									
								<div class="col-lg-3 col-md-6">
									<b>Employee Name :</b>
									<div class="input-group mb-3">
										<select disabled="disabled" class="form-control">
										<?php
										if ($GetAllEmplArr) {
										foreach ($GetAllEmplArr as $Emprowrec) {
										?>
										<option <?= ($tickdetailRow->travel_empid == $Emprowrec->user_id) ? 'Selected' : ''; ?> value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
										<?php
										}
										}
										?>
										</select>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
									<b>Trip Type :</b>
									<div class="input-group mb-3">
										<select disabled="disabled" class="form-control">
										<option <?= ($tickdetailRow->trip_type == '1') ? 'Selected' : ''; ?> value="1" > One Way </option>
										<option <?= ($tickdetailRow->trip_type == '2') ? 'Selected' : ''; ?> value="2" >Round Trip </option>
										</select>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
									<b>Trip Invoice No :</b>
									<div class="input-group mb-3">
										<input type="text" disabled="disabled" value="<?= @$tickdetailRow->trip_invoiceno; ?>" class="form-control floating" /> 
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Journey From :</b>
									<div class="input-group mb-3">
										<input type="text" required name="jfrom" placeholder="Eg:- Jaipur,Delhi.." id="jfrom" value="<?= @$tickdetailRow->trav_from; ?>" class="form-control floating" />   
									</div>
								</div>	

									<div class="col-lg-3 col-md-6">
									<b>Journey To :</b>
									<div class="input-group mb-3">
										<input type="text" required name="jto" id="jto" placeholder="Eg:- Pune,Udaipur.." value="<?= @$tickdetailRow->trav_to; ?>" class="form-control floating" />   
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Travel Date To :</b>
									<div class="input-group mb-3">
										<input type="date" name="travdateto" id="travdateto" class="form-control datetimepicker" />
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Date of Travel :</b>
									<div class="input-group mb-3">
										<input type="date" required name="dateoftravel" id="dateoftravel" value="<?= @$tickdetailRow->dateoftravel; ?>" class="form-control floating" />
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
								<b>Booking Date</b>
									<div class="input-group mb-3">
										<input type="date" required name="dateofbooking" id="dateofbooking" value="<?= @$tickdetailRow->dateoftravel; ?>" class="form-control floating" />
									</div>
								</div>	
								
								<?php if (($tickdetailRow->businessunit_id == '1') OR ( $tickdetailRow->businessunit_id == '3')) { ?>
								<div class="col-lg-3 col-md-6">
								<b>Project CEG</b>
									<div class="input-group mb-3">
										<select required data-placeholder="Choose Project" name="tickproject[]" class="form-control">
											<?php
											if ($allProjArr) {
												foreach ($allProjArr as $rowrec) {
													?>
													<option <?php
													if (in_array($rowrec->id, $projArrec)) {
														echo "selected";
													}
													?> value="<?= $rowrec->id; ?>"> <?= $rowrec->project_name; ?>
													</option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>	
								<?php } ?>

								<?php if ($tickdetailRow->businessunit_id == '2') { ?>
								<div class="col-lg-3 col-md-6">
								<b>Project CEGTH</b>
									<div class="input-group mb-3">
										<select required data-placeholder="Choose Project" name="tickproject[]" class="form-control">
											<?php
											if ($allProjcegthArr) {
												foreach ($allProjcegthArr as $rowcegth) {
													?>
													<option <?php
													if (in_array($rowcegth->id, $projArrec)) {
														echo "selected";
													}
													?> value="<?= $rowcegth->id; ?>"> <?= $rowcegth->project_name; ?> ( <?= @$rowcegth->projectcode; ?> ) </option>
														<?php
													}
												}
												?>
										</select>
									</div>
								</div>	
								<?php } ?>
								
								<div class="col-lg-3 col-md-6">
								<b>Cost</b>
									<div class="input-group mb-3">
										<input type="number" min="1" required name="tickcost" value="<?= (@$tickdetailRow->tickcost > 0) ? @$tickdetailRow->tickcost : @$tickdetailRow->returntickcost; ?>" id="tickcost" class="form-control floating" />   
									</div>
								</div>
								
								<!--<div class="col-lg-3 col-md-6">
								<b>Approved By :</b>
									<div class="input-group mb-3">
										<select required name="approvedby" id="approvedby" class="form-control">
											<option value="" selected="selected">-- Select Approved By --</option>
											<?php
											if ($GetAllEmplArr1) {
												foreach ($GetAllEmplArr1 as $Emprowrec1) {
													?>
													<option <?= (@$tickdetailRow->approvedby == $Emprowrec1->user_id) ? 'Selected' : ''; ?> value="<?= $Emprowrec1->user_id; ?>"><?= $Emprowrec1->userfullname . " ( " . $Emprowrec1->employeeId . " ) "; ?></option>
													<?php
												}
											}
											?>
										</select> 
									</div>
								</div>-->
								
								<div class="col-lg-3 col-md-6">
								<b>Select Airlines :</b>
									<div class="input-group mb-3">
										<select name="airlines" id="airlines" class="form-control">
											<option <?= ($tickdetailRow->airlines == '') ? 'Selected' : ''; ?> value="" > -- Select Airlines -- </option>
											<?php
											if ($airlinesListArr) {
												foreach ($airlinesListArr as $kkey => $rowr) {
													?>
													<option <?= ($tickdetailRow->airlines == $kkey) ? 'Selected' : ''; ?> value="<?= $kkey; ?>" > <?= $rowr; ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Comment :</b>
									<div class="input-group mb-3">
										<input type="text" name="tickcomment" value="<?= @$tickdetailRow->tickcomment; ?>" id="tickcomment" class="form-control floating" />   
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Return Journey From :</b>
									<div class="input-group mb-3">
										<input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returntrav_from; ?>" name="returnjfrom" id="returnjfrom" class="form-control floating" />   
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Return Journey To :</b>
									<div class="input-group mb-3">
										<input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returntrav_to; ?>" name="returnjto" id="returnjto" class="form-control floating" />   
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Return Booking Date :</b>
									<div class="input-group mb-3">
										<input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returndateofbooking; ?>" name="returndateofbooking" id="returndateofbooking" class="form-control floating" />
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Return Travel Date :</b>
									<div class="input-group mb-3">
										<input type="date" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returndateoftravel; ?>" name="returndateoftravel" id="returndateoftravel" class="form-control floating" />
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
								<b>Return Airlines :</b>
									<div class="input-group mb-3">
										<select name="returnairlines" id="returnairlines" class="form-control">
											<option value="" > -- Select Airlines -- </option>
											<?php
											if ($airlinesListArr) {
												foreach ($airlinesListArr as $kkey => $rowr) {
													?>
													<option <?= ($tickdetailRow->returnairlines == $kkey) ? 'Selected' : ''; ?>  <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= $kkey; ?>" > <?= $rowr; ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								
								<div class="col-lg-2 col-md-6">
								<b>&nbsp;</b>
									<div class="input-group mb-3">
										<input type="hidden" name="fld_id_one" value="<?= @$tickdetailRow->fld_id; ?>">
										<input type="hidden" name="fld_id_two" value="<?= @$tickdetailRow->returnfld_id; ?>">
										<input type="hidden" name="fld_triptype" value="<?= @$tickdetailRow->trip_type; ?>">
										<button type="submit" id="btn-filter" class="btn btn-primary">Save/Update</button> 
									</div>
								</div>
									</div>
									
									</form>   
									
								</div>
							</div>
						</div>
					</div>          
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
											<th>Sr. No</th>
											<th>Travelers Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Project</th>
											<th>Cost</th>
				<!--                            <th>Invoice</th>-->
											<th>Trip Type</th>
											 <th>Airlines</th>
											<th>Approved By</th>
											<th>Entry Date</th>
											<th>Entry By</th>
											<th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                           
                                        </tbody>

                                        <tfoot>
                                            <tr>
											<th>Sr. No</th>
											<th>Travelers Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Project</th>
											<th>Cost</th>
				<!--                            <th>Invoice</th>-->
											<th>Trip Type</th>
											 <th>Airlines</th>
											<th>Approved By</th>
											<th>Entry Date</th>
											<th>Entry By</th>
											<th>Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
</body>
<style>
    #table_length{margin-left:20px;}
    #table_filter{margin-right:2%;}
    .selectpicker  {background-color: #ffff;}
</style>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('ajax_list_tickets'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.bunit = $('#bunit').val();
                    data.traveluserid = $('#traveluserid').val();
                    data.travelproject = $('#travelproject').val();
                    data.travdatefrom = $('#travdatefrom').val();
                    data.travdateto = $('#travdateto').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            "dom": 'lBfrtip',
            "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
            "columnDefs": [{"targets": [0], "orderable": false,
                }, ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

        });
        // var colvis = new $.fn.dataTable.ColVis(table);
        // $('#colvis').html(colvis.button());
        $('#btn-filter').click(function () {
            table.ajax.reload();
        });
        $('#btn-reset').click(function () {
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>