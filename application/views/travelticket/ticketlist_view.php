<?php
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
$allProjArr = get_all_active_project();
$GetAllEmplArr = get_all_Activeempls();
$GetAllEmplArr1 = get_all_Activeempls_a();
$isorNotArr = array('1' => 'Yes', '0' => 'No');
$LoginbunitId = $this->tank_auth->get_user_bunit_by_id($this->tank_auth->get_user_id());
?>
<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>

        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?></h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active"><?= ($title) ? $title : ""; ?></li>
								
							</ul>
							
                        </div>
						
                    </div>
                </div>
				<?php if ($this->session->flashdata('successmsg')): ?>
				<div class="alert alert-info alert-dismissible ">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong> Success ! </strong> <?php echo $this->session->flashdata('successmsg'); ?>
				</div>
				<?php endif; ?>
					
				<?php if ($this->session->flashdata('errormsg')): ?>
					<div class="alert alert-danger alert-dismissible ">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success ! </strong> <?= $this->session->flashdata('errormsg'); ?>
					</div>
				<?php endif; ?>
				
				<div class="row clearfix">
					<div class="col-lg-12">
                        <div class="card">
							<div class="body">
							<div class="row">
							<div class="col-xs-10">
							<h4 class="page-title" >Add Travel Ticket Summary:</h4>
							</div>
							</div>
							<form id="form-filter" class="">
								<div class="row">
								
									
								<div class="col-lg-3 col-md-6">
									<b>Business Unit :</b>
									<div class="input-group mb-3">
										<select required name="bunit" id="bunit"  class="form-control">
											<option value="">-- Select B Unit --</option>
											<option value="1" > CEG </option>
											<option value="2" > CEGTH </option>
											<option value="3" > CEG Project</option>
										</select>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
									<b>Employee :</b>
									<div class="input-group mb-3">
										<select name="traveluserid" id="traveluserid" class="form-control">
										<option value="" selected="selected">--select--</option>
										<?php
										if ($GetAllEmplArr) {
										foreach ($GetAllEmplArr as $Emprowrec) {
										?>
										<option value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
										<?php
										}
										}
										?>
										</select>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-6">
									<b>Bussiness Unit For Project :</b>
									<div class="input-group mb-3">
										<select name="otheremplbunit" id="otheremplbunit" onchange="setprojbunitwise()" class="form-control" >
											<option value=""> -- Select -- </option>
											<option value="1"> CEG </option>
											<option value="2"> CEGTH </option>
											<option value="3"> CEG-Project </option>
									</select>
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Project :</b>
									<div class="input-group mb-3">
										<select name="travelproject" id="travelproject" class="form-control">
										<option value="" selected="selected">-- Select Project --</option>
										<?php
										if ($allProjArr) {
										foreach ($allProjArr as $rowrec) {
										?>
										<option value="<?= $rowrec->id; ?>"><?= $rowrec->project_name; ?></option>
										<?php
										}
										}
										?>
										</select>
									</div>
								</div>	

									<div class="col-lg-3 col-md-6">
									<b>Travel Date From :</b>
									<div class="input-group mb-3">
										<input type="date" name="travdatefrom" id="travdatefrom" class="form-control datetimepicker" />
									</div>
								</div>	

								<div class="col-lg-3 col-md-6">
									<b>Travel Date To :</b>
									<div class="input-group mb-3">
										<input type="date" name="travdateto" id="travdateto" class="form-control datetimepicker" />
									</div>
								</div>	

								<div class="col-lg-2 col-md-6">
									<b>&nbsp;</b>
									<div class="input-group mb-3">
										<button type="button" id="btn-filter" class="btn btn-success btn-block"> Filter </button>
									</div>
								</div>	

								<div class="col-lg-2 col-md-6">
								<b>&nbsp;</b>
									<div class="input-group mb-3">
										<button type="button" id="btn-reset" class="btn btn-primary btn-block">Reset</button>
									</div>
								</div>	
								<div class="col-lg-2 col-md-6">
								<b>&nbsp;</b>
									<div class="input-group mb-3">
										<a href="<?= base_url('travelticket'); ?>">
										 <button type="button" class="btn btn-primary rounded"><i class="fa fa-angle-double-left"></i> Back To Ticket</button>
										</a>
									</div>
								</div>	
									</div>
									
									</form>   
									
								</div>
							</div>
						</div>
					</div>          
				</div>
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card">
							
                            <div class="body">
                                <div class="table-responsive">
                                    <table id="table" class="table table-striped display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
											<th>Sr. No</th>
											<th>Travelers Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Project</th>
											<th>Cost</th>
				<!--                            <th>Invoice</th>-->
											<th>Trip Type</th>
											 <th>Airlines</th>
											<th>Approved By</th>
											<th>Entry Date</th>
											<th>Entry By</th>
											<th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                           
                                        </tbody>

                                        <tfoot>
                                            <tr>
											<th>Sr. No</th>
											<th>Travelers Name</th>
											<th>Department</th>
											<th>Journey From</th>
											<th>Journey To</th>
											<th>Travel Date</th>
											<th>Booking Date</th>
											<th>Project</th>
											<th>Cost</th>
				<!--                            <th>Invoice</th>-->
											<th>Trip Type</th>
											 <th>Airlines</th>
											<th>Approved By</th>
											<th>Entry Date</th>
											<th>Entry By</th>
											<th>Action</th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
<script src="<?= FRONTASSETS; ?>jquery.min.js"></script>
</body>
<style>
    #table_length{margin-left:20px;}
    #table_filter{margin-right:2%;}
    .selectpicker  {background-color: #ffff;}
</style>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('ajax_list_tickets'); ?>",
                "type": "POST",
                "data": function (data) {
                    data.bunit = $('#bunit').val();
                    data.traveluserid = $('#traveluserid').val();
                    data.travelproject = $('#travelproject').val();
                    data.travdatefrom = $('#travdatefrom').val();
                    data.travdateto = $('#travdateto').val();
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                },
            },
            "dom": 'lBfrtip',
            "buttons": [{extend: 'collection', text: 'Export', buttons: ['copy', 'excel', 'csv', 'pdf', 'print']}],
            "columnDefs": [{"targets": [0], "orderable": false,
                }, ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

        });
        // var colvis = new $.fn.dataTable.ColVis(table);
        // $('#colvis').html(colvis.button());
        $('#btn-filter').click(function () {
            table.ajax.reload();
        });
        $('#btn-reset').click(function () {
            $('#form-filter')[0].reset();
            table.ajax.reload(); //just reload table
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>