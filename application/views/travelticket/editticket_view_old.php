<?PHP
$this->load->view('back_end/includes/head');
$this->load->view('back_end/includes/header');
$this->load->view('back_end/includes/sidebar');
$allProjArr = get_all_active_projectnew();
$allProjcegthArr = get_all_active_projectnewcegth();
$GetAllEmplArr = get_all_Activeempls();
$GetAllEmplArr1 = get_all_Activeempls_a();
$tripTypeArr = array('1' => 'One-Way', '2' => 'Round-Way');
$airlinesListArr = array('Air_India' => 'Air India',
    'Air_India_Express' => 'Air India Express',
    'AirAsia_India' => 'AirAsia India',
    'Alliance_Air' => 'Alliance Air',
    'IndiGo' => 'IndiGo',
    'Jet_Airways' => 'Jet Airways',
    'SpiceJet' => 'SpiceJet',
    'TruJet' => 'TruJet',
    'GoAir' => 'GoAir',
    'Vistara' => 'Vistara'
);
$projArrec = '';
if ($tickdetailRow->tickproject) {
    $projArrec = explode(",", $tickdetailRow->tickproject);
}
?>

<link rel="stylesheet" href="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/css/jquery-ui.css">
<link rel="stylesheet" href="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/css/bootstrap-select.css">
<script src="<?PHP echo HOSTNAME ?>assets/jscss_online_to_local/js/jquery.min.js"></script>

<div class="page-wrapper">
<div class="content container-fluid">
    
<?php if ($this->session->flashdata('successmsg')): ?>
    <div class="alert alert-info alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success ! </strong> <?= $this->session->flashdata('successmsg'); ?>
    </div>
<?php endif; ?>
<?php if ($this->session->flashdata('errormsg')): ?>
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success ! </strong> <?= $this->session->flashdata('errormsg'); ?>
    </div>
<?php endif; ?>
	
	
<div class="row">
<div class="col-xs-8">
    <h4 class="page-title"> View & Edit Travel Ticket Detsils </h4>
</div>
<div class="col-xs-4 text-right m-b-20">
<a href="<?= base_url('travelticket'); ?>">
    <button type="button" id="btn-filter" class="btn btn-primary"> Back </button>
</a>
</div>	
</div>		

<?= form_open(base_url('travelticket_update'), array('class' => '', 'id' => 'travelticket_updateinvtform')); ?>
<div class="row filter-row">
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
<label class="control-label"> Employee Name : <span id="lvlspan">*</span> </label>
<select disabled="disabled" class="select floating">
<?php
if ($GetAllEmplArr) {
foreach ($GetAllEmplArr as $Emprowrec) {
?>
<option <?= ($tickdetailRow->travel_empid == $Emprowrec->user_id) ? 'Selected' : ''; ?> value="<?= $Emprowrec->user_id; ?>"><?= $Emprowrec->userfullname . " ( " . $Emprowrec->employeeId . " ) "; ?></option>
<?php
}
}
?>
</select>
</div>
</div>

<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
<label class="control-label"> Trip Type : <span id="lvlspan">*</span> </label>
<select disabled="disabled" class="select floating">
<option <?= ($tickdetailRow->trip_type == '1') ? 'Selected' : ''; ?> value="1" > One Way </option>
<option <?= ($tickdetailRow->trip_type == '2') ? 'Selected' : ''; ?> value="2" >Round Trip </option>
</select>
</div>
</div>

<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
<label class="control-label"> Trip Invoice No : <span id="lvlspan">*</span> </label>
<input type="text" disabled="disabled" value="<?= @$tickdetailRow->trip_invoiceno; ?>" class="form-control floating" />   
</div>
</div>


<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
<label class="control-label"> Journey From : <span id="lvlspan">*</span> </label>
<input type="text" required name="jfrom" placeholder="Eg:- Jaipur,Delhi.." id="jfrom" value="<?= @$tickdetailRow->trav_from; ?>" class="form-control floating" />   
</div>
</div>

<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
<label class="control-label"> Journey To : <span id="lvlspan">*</span> </label>
<input type="text" required name="jto" id="jto" placeholder="Eg:- Pune,Udaipur.." value="<?= @$tickdetailRow->trav_to; ?>" class="form-control floating" />   
</div>
</div>    
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
<label class="control-label"> Date of Travel : <span id="lvlspan">*</span> </label>
<input type="date" required name="dateoftravel" id="dateoftravel" value="<?= @$tickdetailRow->dateoftravel; ?>" class="form-control floating" />
</div>
 </div>   
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Booking Date : <span id="lvlspan">*</span> </label>
    <input type="date" required name="dateofbooking" id="dateofbooking" value="<?= @$tickdetailRow->dateoftravel; ?>" class="form-control floating" />
</div>
</div>
    
<?php if (($tickdetailRow->businessunit_id == '1') OR ( $tickdetailRow->businessunit_id == '3')) { ?>
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
        <label class="control-label"> Project CEG : <span id="lvlspan">*</span> </label>

            <select required data-placeholder="Choose Project" name="tickproject[]" class="select floating">
                <?php
                if ($allProjArr) {
                    foreach ($allProjArr as $rowrec) {
                        ?>
                        <option <?php
                        if (in_array($rowrec->id, $projArrec)) {
                            echo "selected";
                        }
                        ?> value="<?= $rowrec->id; ?>"> <?= $rowrec->project_name; ?>
                        </option>
                        <?php
                    }
                }
                ?>
            </select>

    </div>
    </div>
<?php } ?>

<?php if ($tickdetailRow->businessunit_id == '2') { ?>
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
        <label class="control-label"> Project CEGTH : <span id="lvlspan">*</span> </label>

            <select required data-placeholder="Choose Project" name="tickproject[]" class="select floating">
                <?php
                if ($allProjcegthArr) {
                    foreach ($allProjcegthArr as $rowcegth) {
                        ?>
                        <option <?php
                        if (in_array($rowcegth->id, $projArrec)) {
                            echo "selected";
                        }
                        ?> value="<?= $rowcegth->id; ?>"> <?= $rowcegth->project_name; ?> ( <?= @$rowcegth->projectcode; ?> ) </option>
                            <?php
                        }
                    }
                    ?>
            </select>

    </div>
   </div>
<?php } ?>




<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Cost : <span id="lvlspan">*</span> </label>
    <input type="number" min="1" required name="tickcost" value="<?= (@$tickdetailRow->tickcost > 0) ? @$tickdetailRow->tickcost : @$tickdetailRow->returntickcost; ?>" id="tickcost" class="form-control floating" />   
</div>
</div>
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
    <label class="control-label"> Approved By : <span id="lvlspan">*</span> </label>
    <select required name="approvedby" id="approvedby" class="select floating">
        <option value="" selected="selected">-- Select Approved By --</option>
        <?php
        if ($GetAllEmplArr1) {
            foreach ($GetAllEmplArr1 as $Emprowrec1) {
                ?>
                <option <?= (@$tickdetailRow->approvedby == $Emprowrec1->user_id) ? 'Selected' : ''; ?> value="<?= $Emprowrec1->user_id; ?>"><?= $Emprowrec1->userfullname . " ( " . $Emprowrec1->employeeId . " ) "; ?></option>
                <?php
            }
        }
        ?>
    </select>  
</div>
</div>

<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
    <label class="control-label"> Select Airlines : <span id="lvlspan">*</span> </label>
    <select name="airlines" id="airlines" class="select floating">
        <option <?= ($tickdetailRow->airlines == '') ? 'Selected' : ''; ?> value="" > -- Select Airlines -- </option>
        <?php
        if ($airlinesListArr) {
            foreach ($airlinesListArr as $kkey => $rowr) {
                ?>
                <option <?= ($tickdetailRow->airlines == $kkey) ? 'Selected' : ''; ?> value="<?= $kkey; ?>" > <?= $rowr; ?></option>
                <?php
            }
        }
        ?>
    </select>
</div>
</div>
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Comment :  </label>
    <input type="text" name="tickcomment" value="<?= @$tickdetailRow->tickcomment; ?>" id="tickcomment" class="form-control floating" />   
</div>
</div>
    
<div id="divreturn" style="<?= ($tickdetailRow->trip_type == '1') ? 'display:none' : ''; ?>">
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Return Journey From : <span id="lvlspan">*</span> </label>
    <input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returntrav_from; ?>" name="returnjfrom" id="returnjfrom" class="form-control floating" />   
</div>
</div>    
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Return Journey To : <span id="lvlspan">*</span> </label>
    <input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returntrav_to; ?>" name="returnjto" id="returnjto" class="form-control floating" />   
</div>
</div>
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Return Booking Date : <span id="lvlspan">*</span> </label>
    <input type="text" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returndateofbooking; ?>" name="returndateofbooking" id="returndateofbooking" class="form-control floating" />
</div>
</div>
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus">
    <label class="control-label"> Return Travel Date : <span id="lvlspan">*</span> </label>
    <input type="date" <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= @$tickdetailRow->returndateoftravel; ?>" name="returndateoftravel" id="returndateoftravel" class="form-control floating" />
</div>
</div>
    
<div class="col-sm-4 col-xs-12">
<div class="form-group form-focus select-focus">
    <label class="control-label"> Return Airlines : <span id="lvlspan">*</span> </label>
    <select name="returnairlines" id="returnairlines" class="select floating">
        <option value="" > -- Select Airlines -- </option>
        <?php
        if ($airlinesListArr) {
            foreach ($airlinesListArr as $kkey => $rowr) {
                ?>
                <option <?= ($tickdetailRow->returnairlines == $kkey) ? 'Selected' : ''; ?>  <?= ($tickdetailRow->trip_type == '2') ? 'required' : ''; ?> value="<?= $kkey; ?>" > <?= $rowr; ?></option>
                <?php
            }
        }
        ?>
    </select>
</div>
</div>
</div>
<div class="col-sm-2 col-xs-12">
<input type="hidden" name="fld_id_one" value="<?= @$tickdetailRow->fld_id; ?>">
<input type="hidden" name="fld_id_two" value="<?= @$tickdetailRow->returnfld_id; ?>">
<input type="hidden" name="fld_triptype" value="<?= @$tickdetailRow->trip_type; ?>">
<button type="submit" id="btn-filter" class="btn btn-primary">Save/Update</button> 
</div>
      
</div>    
<?= form_close(); ?>

       
</div>
</div>   
    
<?PHP $this->load->view("back_end/includes/footer"); ?>
<script src="<?= HOSTNAME ?>assets/jscss_online_to_local/js/bootstrap-select.js"></script>
<script src="<?= HOSTNAME ?>assets/jscss_online_to_local/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<?= HOSTNAME ?>src/chosen.css">
<script src="<?= HOSTNAME ?>src/chosen.jquery.js" type="text/javascript"></script>
<script src="<?= HOSTNAME ?>src/init.js" type="text/javascript" charset="utf-8"></script>

<style>
    #table_length{margin-left:20px;}
    #table_filter{margin-right:2%;}
    .selectpicker  {background-color: #ffff;}
    #lvlspan{color:red}
</style>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= site_url('ajax_list_assetslist') ?>",
                "type": "POST",
                "data": function (data) {
                    data.<?= $this->security->get_csrf_token_name(); ?> = "<?= $this->security->get_csrf_hash(); ?>";
                },

            },
            "dom": 'lBfrtip',
            "buttons": [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ]
                }
            ],
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
        $('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });
    });

    $(function () {
        $("#empid,#project,#approvedby").customselect();
    });

    function setroundtrip() {
        var triptype = $("#triptype").val();
        if (triptype > 1) {
            var bookingdate = $("#dateofbooking").val();
            var journeyfrom = $("#jfrom").val();
            var journeyto = $("#jto").val();
            if (bookingdate == '') {
                alert('Please Enter Date of Booking Details.');
                $("#triptype").val('1');
                $("#divreturn").hide();
                $("#returndateoftravel").removeAttr("required");
                $("#returnjfrom").removeAttr("required");
                $("#returnjto").removeAttr("required");
                return false;
            }
            if (journeyfrom == '') {
                alert('Please Enter Travel Location From.');
                $("#triptype").val('1');
                $("#divreturn").hide();
                $("#returndateoftravel").removeAttr("required");
                $("#returnjfrom").removeAttr("required");
                $("#returnjto").removeAttr("required");
                return false;
            }
            if (journeyto == '') {
                alert('Please Enter Travel Location To.');
                $("#triptype").val('1');
                $("#divreturn").hide();
                $("#returndateoftravel").removeAttr("required");
                $("#returnjfrom").removeAttr("required");
                $("#returnjto").removeAttr("required");
                return false;
            }
            if (bookingdate && journeyfrom && journeyto) {
                $("#divreturn").show();
                $("#returndateoftravel").attr("required", "required");
                $("#returndateofbooking").val(bookingdate);
                $("#returnjfrom").val(journeyto);
                $("#returnjto").val(journeyfrom);
                $("#returnjfrom").attr("required", "required");
                $("#returnjto").attr("required", "required");
            }
        } else {
            $("#divreturn").hide();
            $("#returndateoftravel").removeAttr("required");
            $("#returnjfrom").removeAttr("required");
            $("#returnjto").removeAttr("required");
        }
    }
</script>