<?php
error_reporting(0);
$CountShortLeave = CountShortLeave_Current_Month_LoginUser();
$this->load->view('admin/includes/head');
$this->load->view('admin/includes/header');
//##################### Monthly Delay Minute Count in Month ######################

$tOtLateMinute = 0;
$totMinuteShouldBe = 0;
if (@$allPunchDataArr) {
    foreach ($allPunchDataArr as $kEy => $reCw) {
        $FunllInTim = date("H:i:s A", strtotime($reCw->firstintime));
        if ((strtotime($FunllInTim) > strtotime("09:30:59 AM")) AND ( strtotime($FunllInTim) < strtotime("10:01:00 AM"))) {
            $tOtLateMinute += round(abs(strtotime($FunllInTim) - strtotime("09:30:00 AM")) / 60, 2);
        }
        $totMinuteShouldBe += 510;
    }
    $totMinuteShouldBe = ($totMinuteShouldBe - 510);
}

$crossno10_00 = 0;
if (@$allPunchDataArr) {
    foreach ($allPunchDataArr as $kEy => $reCw) {
        $FunllInTim = date("H:i:s A", strtotime($reCw->firstintime));
        if ((strtotime($FunllInTim) > strtotime("10:00:59 AM")) AND ( strtotime($FunllInTim) < strtotime("01:01:00 PM"))) {
            $crossno10_00 += 1;
        }
    }
}
$totPerCtg = ($totPunchMinute / $totMinuteShouldBe) * 100;
?>

<body class="theme-cyan">
    <div id="wrapper">
        <?php
        $this->load->view('admin/includes/sidebar');
        ?>
        <div id="main-content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-5 col-md-8 col-sm-12">                        
                            <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?= ($title) ? $title : ""; ?> </h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="icon-home"></i></a></li>                            
                                <li class="breadcrumb-item active">Home / <?= ($title) ? $title : ""; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body"> 
                                <div class="icon text-info"><img src="<?= HOSTNAME; ?>assets/icon/out-Time.png" > </div>
                                <div class="content">
                                    <div class="text">(Out Time) <?= ($YesterdayOutTimeDetails) ? date("d M", strtotime($YesterdayOutTimeDetails)) : "Error"; ?><sup></sup></div>
                                    <h5 class="number"><?= ($YesterdayOutTimeDetails) ? date("h:i A", strtotime($YesterdayOutTimeDetails)) : "Error"; ?></h5>
                                </div>
                                <hr>
                                <div class="icon text-warning"><img src="<?= HOSTNAME; ?>assets/icon/In-Time.png" > </div>
                                <div class="content">
                                    <div class="text">(In Time) <?= date("d M"); ?><sup></sup> </div>
                                    <h5 class="number"><?= ($TodayInTimeDetails) ? date("h:i A", strtotime($TodayInTimeDetails)) : "Error"; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon text-warning"><img src="<?= HOSTNAME; ?>assets/icon/2.png" > </div>
                                <div class="content">
                                    <div class="text">Late Minutes(<?= date("M"); ?>)
                                        <?php if ((($tOtLateMinute) > TATALDELAY) or ( $crossno10_00 > 0)) { ?>
                                            <a href="<?= base_url("myshort_leave_list?filtermonth=" . date('m') . "&filteryear=" . date('Y') . "&submit=1"); ?>">
                                                <small class="pull-right btn btn-primary btn-xs" style="font-size:10px !important"> Apply Leave </small>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <h5 class="number"><?= round($tOtLateMinute); ?> min</h5>
                                </div>
                                <hr>
                                <div class="icon"><img src="<?= HOSTNAME; ?>assets/icon/3.png" > </div>
                                <div class="content">
                                    <div class="text">Late Minutes Left</div>
                                    <h5 class="number">
                                        <?php
                                        $ResultTot = (TATALDELAY - round($tOtLateMinute));
                                        echo ($ResultTot > 0) ? round($ResultTot) : "0";
                                        ?> min
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon text-danger"><img src="<?= HOSTNAME; ?>assets/icon/4.png" > </div>
                                <div class="content">
                                    <div class="text">Qtr Day Leave (Availed)</div>
                                    <h5 class="number"><?= ($CountShortLeave < 9) ? $CountShortLeave : "8"; ?></h5>
                                </div>
                                <hr>
                                <div class="icon text-success"><img src="<?= HOSTNAME; ?>assets/icon/5.png" > </div>
                                <div class="content">
                                    <div class="text">Qtr Day Leave (Balance)</div>
                                    <h5 class="number">
                                        <?php
                                        $avlbShortLeave = (QTRDAYLEAVE - $CountShortLeave);
                                        echo ($avlbShortLeave > 0) ? $avlbShortLeave : "0";
                                        ?>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div style="margin: 0px 0px 0 75px;" class="icon"><img src="<?= HOSTNAME; ?>assets/icon/6.png" > </div>
                                <div class="content" style="height:72px;text-align: center;">
                                    <div class="text" style="margin: 60px 0px 0px 0px;">Availability for work during the month</div>
                                    <h5 class="number"><?= round($totPerCtg); ?>%</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <p style="margin:0 0 0 0;" >Performance Appraisal for <span style="color:green;">2019&nbsp;-&nbsp;2020</span> is initiated. Last date of submission for <b>Self</b> <span style="color:red;">4&nbsp;-&nbsp;Mar</span>, <b>IO</b> <span style="color:red;">12&nbsp;-&nbsp;Mar</span> & <b>RO</b> <span style="color:red;">19&nbsp;-&nbsp;Mar</span>.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/includes/footer'); ?>
    </div>
</body>

