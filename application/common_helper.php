<?PHP

//current_url
function thisurl() {
    $actual_link = (isset($_SERVER['HTTP']) ? "http" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $actual_link;
}

//Get Education By User ID...Though Durgesh
function GetEducationByUserid($userid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("main_empeducationdetails");
    $ci->db->where(array('isactive' => '1', 'user_id' => $userid));
    $ci->db->order_by("id", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}

//Get Experience By User ID...Though Durgesh
function GetExperienceByUserid($userid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("main_empexperiancedetails");
    $ci->db->where(array('isactive' => '1', 'user_id' => $userid));
    $ci->db->order_by("id", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}

//code by durgesh for package work details
function getpkgworkdetail($pkg_sum_id, $work_id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['pkg_work_id' => $work_id, 'pkg_summary_id' => $pkg_sum_id]);
    $datapkgworkdetail = $ci->db->get('package_work_detail')->row();
    return ($datapkgworkdetail) ? $datapkgworkdetail : null;
}

//code by durgesh for city name
function getprojectname($projid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $projid, 'status' => "1"]);
    $dataproject = $ci->db->get('project_master')->row();
    return ($dataproject) ? $dataproject : null;
}

//code by durgesh for contractor name
function getcontractorname($contractorid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $contractorid, 'status' => "1"]);
    $dataContractor = $ci->db->get('contractor_master')->row();
    return ($dataContractor) ? $dataContractor : null;
}

//code by durgesh for city name
function getcityname($cityid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['fld_id' => $cityid, 'status' => "1"]);
    $dataCity = $ci->db->get('tbl_cities')->row();
    return ($dataCity) ? $dataCity : null;
}

//code by durgesh for package data
function get_package_data($proj_summr_id, $pkg_id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['proj_summ_id' => $proj_summr_id, "pkg_number" => $pkg_id]);
    $datapackage = $ci->db->get('package_summary')->result();
    return ($datapackage) ? $datapackage : null;
}

function getAllMasterMenu() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => "0", 'status' => "1", 'menu_category' => "1"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

function getSecondAllMasterMenu() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => "0", 'status' => "1"]);
    $ci->db->where(['fld_id' => "15"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

function getChildMenuByParentID($parentID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => $parentID, 'status' => "1"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

function checkRolePermission($accessurl) {
    $ci = & get_instance();
    $LoginUserRole = $ci->session->userdata('assign_role');
    if ($LoginUserRole == "1") {
        $accesArr = array("add_project", "project_data_ajax", "editproject", "district_management", "district_data_ajax", "editdistrict", "component_management", "component_data_ajax", "edit_component");
    }
    if (in_array($accessurl, $accesArr)) {
        return true;
    } else {
        redirect(base_url("access_denied"));
    }
}

// Get Single Rec Details By Clearnce ID..
function GetSingleRec_Clearance($tblWorkID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['work_id' => $tblWorkID, 'status' => "1"]);
    $dataMenu = $ci->db->get('clearance')->row();
    return ($dataMenu) ? $dataMenu : null;
}

// Get Single Rec Details By Physical Progress ID..
function GetSingleRec_PhysicalProgress($tblWorkID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['work_id' => $tblWorkID, 'status' => "1"]);
    $dataMenu = $ci->db->get('physical_progress_details')->row();
    return ($dataMenu) ? $dataMenu : null;
}

//Get Profile Details..
function GetBasicRecLoginUser() {
    $ci = & get_instance();
    $ci->load->database();
    $dataRec = $ci->mastermodel->GetBasicRecLoginUser();
    return ($dataRec) ? $dataRec : null;
}

//Week no of Month ..
function weekOfMonth($date) {
    $ci = & get_instance();
    list($y, $m, $d) = explode('-', date('Y-m-d', strtotime($date)));
    $w = 1;
    for ($i = 1; $i <= $d; ++$i) {
        if ($i > 1 && date('w', strtotime("$y-$m-$i")) == 0) {
            ++$w;
        }
    }
    return $w;
}

//Tour Status...
function TourStatusRec() {
    $ci = & get_instance();
    $respon = ["0" => "<span style='color:red'>New Request</span>",
        "1" => "<span style='color:green'>Approved by IO/TL</span>",
        "2" => "<span style='color:#667eea'>Rejected by IO/TL</span>",
        "3" => "<span style='color:#764ba2'>Save by PC</span>",
        "4" => "<span style='color:#337ab7'>Lock by PC</span>",
        "5" => "<span style='color:#2196f3'>Save by HR</span>",
        "6" => "<span style='color:#009688'>Lock by HR</span>",
        "7" => "<span style='color:#9c27b0'>Save by Committee</span>",
        "8" => "<span style='color:#2196f3'>Lock by Committee</span>",
        "9" => "<span style='color:#f44336'>Save by RO</span>",
        "10" => "<span style='color:#6b1ef3'>Lock by RO</span>"];
    return $respon;
}

/* * ************* For search the user record coded by durgesh(16-01-2020)************ */

function getuserrecord() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select('b.*');
    $ci->db->from('main_users as a');
    $ci->db->join('main_employees_summary as b', 'b.user_id=a.id', 'left');
    $ci->db->where('a.isactive', '1');
    $ci->db->order_by('a.id');
    $ci->db->group_by('a.id');
    $result = $ci->db->get()->result();
    return ($result) ? $result : nul;
}

//Get YesterDay Out time By User Machine Id..
function GetYesterdayOutTime($userID) {
    $ci = & get_instance();
    $ci->load->model('mastermodel');
    $recDetails = $ci->mastermodel->GetYesterdayOutTime($userID);
    return ($recDetails) ? $recDetails : null;
}

function getSingleDateAttByUserID($punchDate, $userID) {
    $ci = & get_instance();
    $punchRec1 = $ci->mastermodel->getSingleDateAttByUserID($punchDate, $userID);
    return ($punchRec1) ? $punchRec1 : null;
}

function GetTotalWorkingHourWithPunch($punchDate, $userID) {
    $ci = & get_instance();
    $punchRec2 = $ci->mastermodel->GetTotalWorkingHourWithPunch($punchDate, $userID);
    return ($punchRec2) ? $punchRec2 : null;
}

function GetAllPunchDetail($punchDate, $userID) {
    $ci = & get_instance();
    $punchRec2 = $ci->mastermodel->GetAllPunchDetail($punchDate, $userID);
    return ($punchRec2) ? $punchRec2 : null;
}

//Get All Appraisal Master.. Code By Asheesh
function GetAllAppraisalMaster() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("appr_performancefactor_master");
    $ci->db->where(array('status' => '1', 'parent_id' => '0', 'master_group' => '1'));
    $ci->db->order_by("fld_id", "ASC");
    $dataArr = $ci->db->get()->result();
    return ($dataArr) ? $dataArr : null;
}

function GetAllAppraisalMasterLsecond($parentId) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("appr_performancefactor_master");
    $ci->db->where(array('status' => '1', 'parent_id' => $parentId));
    $ci->db->order_by("fld_id", "ASC");
    $dataArr = $ci->db->get()->result();
    return ($dataArr) ? $dataArr : null;
}

//Get All Sub Categ..
function AllAppraisalMasterSubcat() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("appr_performancefactor_master");
    $ci->db->where(array('status' => '1', 'parent_id !=' => '0'));
    $ci->db->order_by("fld_id", "ASC");
    $dataArr = $ci->db->get()->result();
    return ($dataArr) ? $dataArr : null;
}

//Get Thumb Code...
function thumbCodeById($loginID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("thumbcode");
    $ci->db->from("emp_otherofficial_data");
    $ci->db->where(array('status' => '1', 'user_id' => $loginID));
    $dataArr = $ci->db->get()->row();
    return ($dataArr) ? $dataArr->thumbcode : null;
}

//Get All Appr..
function AppraisalRowSec($fldid, $masterID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("*");
    $ci->db->from("appr_performfactor");
    $ci->db->where(array('status' => '1', 'appr_parent_id' => $fldid, 'perform_fact_masterid' => $masterID));
    $ci->db->order_by("fld_id", "ASC");
    $dataArr = $ci->db->get()->row();
    return ($dataArr) ? $dataArr : null;
}

//Get Project Name By Id..
function get_project_name1($id) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where('id', $id);
    $data = $ci->db->get('cegth_project')->row_array();
    return $data['project_name'];
}

//Project Name By Id..
function get_project_name($prjid) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("id,project_name");
    $ci->db->from("tm_projects");
    $ci->db->where(array('is_active' => '1', 'id' => $prjid));
    $data = $ci->db->get()->row_array();
    return ($data) ? $data['project_name'] : 'null';
}

//Password Show..
function GetPassByPayRollIDwithName($PayRollIDwithName) {
    $ci = & get_instance();
    $ci->db->select("password");
    $ci->db->from("payslip_pass");
    $ci->db->where(array('payroll_with_name' => $PayRollIDwithName));
    $ci->db->order_by("id", "DESC");
    $data = $ci->db->get()->row();
    return ($data) ? $data->password : null;
}

//Month Arr..
function MonthNameArr($kEEy) {
    $ci = & get_instance();
    $YearArr = array("01" => 'january', "02" => 'february', "03" => 'march', "04" => 'april', "05" => 'may', "06" => 'june', "07" => 'july', "08" => 'august', "09" => 'september', "10" => 'october', "11" => 'november', "12" => 'december');
    return ($YearArr) ? $YearArr[$kEEy] : null;
}

//Tour Status...
function ShortLeaveStatusRec() {
    $ci = & get_instance();
    $respon = ["1" => "<span style='color:red'>Applied Leave & Pending for approval </span>",
        "2" => "<span style='color:green'>Applied & Approved</span>",
        "3" => "<span style='color:#667eea'>Rejected by IO/TL</span>",
        "4" => "<span style='color:#764ba2'>Deleted</span>"];
    return $respon;
}

function CountShortLeave_Current_Month_LoginUser() {
    $ci = & get_instance();
    $loginUserID = $ci->session->userdata('loginid');
    $cMonth = date("m");
    $cYear = date("Y");
    $ci->db->select("a.id");
    $ci->db->from("short_leave as a");
    $ci->db->where(array('a.user_id' => $loginUserID, 'a.month' => $cMonth, 'a.year' => $cYear));
    $CountShortLeave = $ci->db->get()->num_rows();
    return ($CountShortLeave) ? $CountShortLeave : "0";
}

//(HR Functions Use) Employee Ids for HR Permissions..
function GetUserIDHRPermission() {
    $ci = & get_instance();
    $ci->db->select("a.user_id");
    $ci->db->from("main_employees_summary as a");
    $ci->db->where(array('a.isactive' => '1', "a.department_id" => "11"));
    $empHridArr = $ci->db->get()->result();
    $recArr = array("297", "296");
    if ($empHridArr) {
        foreach ($empHridArr as $roWs) {
            $recArr[] = $roWs->user_id;
        }
    }
    return ($recArr) ? $recArr : null;
}

function chkApprProjofcStatus($user_id) {
    //$user_id = '1002';
    $MasterStatusArr = array("1" => "<span style='color:green'> Filled by IO </span>", "2" => "<span style='color:green'> Locked by IO </span>", "3" => "<span style='color:green'> Filled by RO </span>", "4" => "<span style='color:blue'> Locked by RO </span>", "0" => "<span style='color:red'> Pending </span>");
    $jobGroupID = '';
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("a.jobtitle_id");
    $ci->db->from("main_employees_summary as a");
    $ci->db->where(array('a.isactive' => '1', 'a.user_id' => $user_id));
    $SngldataArr = $ci->db->get()->row();
    if ($SngldataArr) {
        $jobGroupID = $SngldataArr->jobtitle_id;
    }

    //Layer 2 ..
    if ($jobGroupID == '8' or $jobGroupID == '9' or $jobGroupID == '10') {
        $ci->db->select("bc.current_status");
        $ci->db->from("appr_proj_formdata_bc as bc");
        $ci->db->where(array('bc.status' => '1', 'bc.user_id' => $user_id, 'bc.session_appr_year' => '2020'));
        $SngldataArr = $ci->db->get()->row();
    }
    //Layer 3 ..
    if ($jobGroupID == '11' or $jobGroupID == '12') {
        $ci->db->select("de.current_status");
        $ci->db->from("appr_proj_formdata_de as de");
        $ci->db->where(array('de.status' => '1', 'de.user_id' => $user_id, 'de.session_appr_year' => '2020'));
        $SngldataArr = $ci->db->get()->row();
    }
    return ($SngldataArr) ? $MasterStatusArr[$SngldataArr->current_status] : null;
}

//Return Month
function getDateDiffMonth($from_ymd, $to_ymd) {
    $datetime1 = new DateTime($from_ymd);
    $datetime2 = new DateTime($to_ymd);
    $interval = $datetime2->diff($datetime1);
    return (($interval->format('%y') * 12) + $interval->format('%m'));
}

//Get Current week of Month..
function CurrentweekOfMonth() {
    $ci = & get_instance();
    $firstOfMonth = strtotime(date("Y-m-d"));
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}

function ShortMonthName($m) {
    $ci = & get_instance();
    $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
    return $months[(int) $m];
}

//Full Name Got ..
function FullMonthName($m) {
    $ci = & get_instance();
    $months = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July ',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December');
    return $months[(int) $m];
}

//Project Task..
function TaskProjIDByprojTaskId($projTaskId) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("a.project_id,a.task_id");
    $ci->db->from(" tm_project_tasks as a");
    $ci->db->where(array('a.is_active' => '1', 'a.id' => $projTaskId));
    $SngldataArr = $ci->db->get()->row();
    return ($SngldataArr) ? $SngldataArr : null;
}

//Last Status of Appraisal By UserId..
function LastApprStatusByuserID($userID) {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select("a.emp_io_lock,a.emp_self_lock,a.fld_id,a.emp_ro_lock");
    $ci->db->from("appr_action_master as a");
    $ci->db->where(array('a.status' => '1', 'a.appraisal_cycle_id' => '3', 'a.user_id' => $userID));
    $ci->db->order_by("a.fld_id", "DESC");
    $SngldataArr = $ci->db->get()->row();
    return ($SngldataArr) ? $SngldataArr : null;
}

//Weekly Ts Recd..
function weeklyTsRecByprojTaskID($WhereArr) {
    $ci = & get_instance();
    $ci->load->model('Timesheet/Timesheet_Model', 'timesheetmodel');
    $recD_Arr = $ci->timesheetmodel->weeklyTsRecByprojTaskID($WhereArr);
    return ($recD_Arr) ? $recD_Arr : null;
}

//=============coded by gaurav==============//
function get_businessunits() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,unitname FROM `main_businessunits` WHERE isactive='1' ");
    return $query->result();
}

function get_companyname() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT * FROM `tbl_companyname` WHERE is_active='1' ORDER BY company_name");
    return $query->result();
}

function get_departments() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,deptname FROM `main_departments` WHERE isactive='1' ");
    return $query->result();
}

function get_designation() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,positionname FROM `main_positions` WHERE isactive='1' ");
    return $query->result();
}

function get_project_names() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,project_name FROM `tm_projects` WHERE is_active='1' ORDER BY project_name ASC ");
    return $query->result();
}

//=========(Gaurav) StartData 29-02-2020============//
//Get My Team Details..
function check_self_IO_RO($userID) {
    $ci = & get_instance();
    $ci->db->select('a.user_id,a.reporting_manager');
    $ci->db->from('main_employees_summary as a');
    // $ci->db->join("emp_otherofficial_data as b","b.user_id=a.user_id","left");
    $ci->db->where(array('a.reporting_manager' => $userID, 'a.isactive' => '1'));
    // $ci->db->where(array('a.reporting_manager ', $userID));
    // $ci->db->where(array('a.isactive' => '1'));
    $RecRows = $ci->db->get()->result();
    // $RecRows = $ci->db->get()->num_rows();
    return ($RecRows) ? $RecRows : '0';
    // return $RecRows;
}

//=========(Gaurav) Ebd Data 05-03-2020============//
function getAllMasterMenu_for_team() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => "0", 'status' => "1", 'menu_category' => "3"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

function get_ro_details_by_Id($userID) {
    $ci = & get_instance();
    $ci->db->select('a.userfullname');
    $ci->db->from('main_employees_summary as a');
    $ci->db->where(array('a.user_id' => $userID, 'a.isactive' => '1'));
    $RecRows = $ci->db->get()->row();
    return ($RecRows) ? $RecRows->userfullname : null;
}

function getAllMasterMenu_for_team_as_ro() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => "0", 'status' => "1", 'menu_category' => "4"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

//================Code End here (Guarav) =============//
//=========(Gaurav) Ebd Data 11-03-2020============//
function get_edulevel_name($eduId) {
    $ci = & get_instance();
    $ci->db->select('a.educationlevelcode');
    $ci->db->from('main_educationlevelcode as a');
    $ci->db->where(array('a.id' => $eduId, 'a.isactive' => '1'));
    $RecRows = $ci->db->get()->row();
    return ($RecRows) ? $RecRows->educationlevelcode : null;
}

//================Code End here (Guarav) =============//
//============= Coded by gaurav on 13-03-2020 =================//
function getAllMasterMenu_for_courier() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->where(['parent' => "0", 'status' => "1", 'menu_category' => "5"]);
    $dataMenu = $ci->db->get('menu_master')->result();
    return ($dataMenu) ? $dataMenu : null;
}

//Get All Courier Categ...
function list_cour_categ_type() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select('a.*');
    $ci->db->from('cour_categ_type as a');
    $ci->db->where(array('a.status' => '1'));
    $queryRecArr = $ci->db->get()->result();
    return ($queryRecArr) ? $queryRecArr : false;
}

//Get All Active Employee Details...
function get_all_Activeempls() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select(array('user_id', 'userfullname', 'employeeId'));
    $ci->db->from("main_employees_summary");
    $ci->db->where(array('isactive' => '1'));
    $ci->db->order_by("userfullname", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}

function get_all_active_projectnew() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,project_name FROM `tm_projects` WHERE (is_active='1' OR is_active='3') AND project_type='billable' ORDER BY `project_name` ASC ");
    return $query->result();
}

//Get All Project Of CEGTH....
function get_all_active_projectnewcegth() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,project_name,projectcode FROM `cegth_project` WHERE status='1' ORDER BY `project_name` ASC ");
    return $query->result();
}

function get_all_Activeempls_a() {
    $ci = & get_instance();
    $ci->load->database();
    $ci->db->select(array('user_id', 'userfullname', 'employeeId'));
    $ci->db->from("main_employees_summary");
    $ci->db->where(array('isactive' => '1', 'jobtitle_name' => 'A-CEG'));
    $ci->db->order_by("userfullname", "ASC");
    $data = $ci->db->get()->result();
    return ($data) ? $data : null;
}

//Get All Active Project Code By Gaurav
function get_all_active_project() {
    $ci = & get_instance();
    $ci->load->database();
    $query = $ci->db->query("SELECT id,project_name FROM `tm_projects` WHERE is_active='1' AND (`id`!=86 and `id`!=41 and `id`!=119 and `id`!=93 and `id`!=132 and `id`!=94 and `id`!=92 and `id`!=16 and `id`!=116 and `id`!=115 and `id`!=15 and `id`!=114 and `id`!=131 and `id`!=130 and `id`!=113 and `id`!=121 and `id`!=117) ORDER BY `project_name` ASC ");
    return $query->result();
}

//=========(Gaurav) Ebd Data 29-02-2020============//
//================Code End here (Guarav) =============//
?>